const { verifyToken } = require('../helpers/jsonwebtoken');

module.exports = {

	isUser(req, res, next) {
		verifyToken(req.headers.token, function(err, decoded) {
			if (err) {
				res.status(500).json('Token anda tidak sah');
			} else {
				req.user = decoded;
				next();
			}
		})
	}

}
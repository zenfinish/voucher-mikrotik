const mysql = require('../helpers/mysql.js');

class Limitation {

	static create(data) {
		return new Promise((resolve, reject) => {
			mysql.query(`
				INSERT INTO limitation(name, download, upload, transfer, masa_aktif)
				VALUES('${data.name}', '${data.download}', '${data.upload}', '${data.transfer}', '${data.masa_aktif}')
			`, function(error, result) {
				if (error) {
					reject(error);
				} else {
					resolve(result);
				}
			});
		});
	}

	static findByIdAndDelete(limitation_id) {
		return new Promise((resolve, reject) => {
			mysql.query(`
				DELETE FROM limitation WHERE id = '${limitation_id}'
			`, function(error, result) {
				if (error) {
					reject(error);
				} else {
					resolve(result);
				}
			});
		});
	}

	static find() {
		return new Promise((resolve, reject) => {
			mysql.query(`
				SELECT * FROM limitation ORDER BY tgl DESC
			`, function(error, result) {
				if (error) {
					reject(error);
				} else {
					resolve(result);
				}
			});
		});
	}
	
}

module.exports = Limitation;

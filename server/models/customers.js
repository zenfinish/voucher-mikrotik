const mysql = require('../helpers/mysql');

class Customers {

	static findByHp(hp) {
		return new Promise((resolve, reject) => {
			const query = `
				SELECT * FROM customers WHERE hp = '${hp}'
			`;
			mysql.query(query, function(error, result) {
				if (error) {
					reject(error.sqlMessage);
				} else {
					if (result.length === 0) {
                        reject("Data Customer Tidak Ditemukan.");
                    } else {
                        resolve(result[0]);
                    };
				};
			});
		});
	}

    static insert(data) {
		return new Promise((resolve, reject) => {
			mysql.query(`
				INSERT INTO customers (hp, password)
                VALUES ('${data.hp}', '${data.password}')
			`, function(error, result) {
				if (error) {
					// console.log("keluar=======", error)
					reject(error.sqlMessage);
				} else {
					resolve("Ok.");
				};
			});
		});
	}

	static updatePassword(data) {
		return new Promise((resolve, reject) => {
			mysql.query(`
				UPDATE customers SET
					password = '${data.password}'
				WHERE hp = '${data.hp}'
			`, function(error, result) {
				if (error) {
					// console.log("keluar=======", error)
					reject(error.sqlMessage);
				} else {
					resolve("Ok.");
				};
			});
		});
	}
	
}

module.exports = Customers;

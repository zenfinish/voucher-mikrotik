class MemberTransactions {

	static findByBulanAndTahun(connection, data) {
		return new Promise((resolve, reject) => {
			connection.query(`
				SELECT
					a.*,
					b.name
				FROM member_transactions a
				LEFT JOIN members b ON a.member_id = b.id
				WHERE
					(a.bulan_bayar BETWEEN '${data.tahun}-${data.dari_bulan}-15' AND '${data.tahun}-${data.sampai_bulan}-15')
			`, function(error, result) {
				if (error) {
					reject(error.sqlMessage);
				} else {
					resolve(result);
				}
			});
		});
	}

	static create(connection, data) {
		return new Promise((resolve, reject) => {
			connection.query(`
				INSERT INTO member_transactions (member_id, bulan_bayar, jumlah, img)
				VALUES ('${data.member_id}', '${data.tahun}-${data.bulan}-15', '${data.jumlah}', '${data.img}')
			`, function(error, result) {
				if (error) {
					reject(error.sqlMessage);
				} else {
					resolve(result);
				}
			});
		});
	}

	static findByMemberId(connection, data) {
		return new Promise((resolve, reject) => {
			const query = `
				SELECT
					DATE_FORMAT(a.bulan_bayar, '%d-%M-%Y') AS bulan_bayar,
					DATE_FORMAT(a.tgl, '%Y-%M-%d %H:%i:%s') AS tgl,
					a.img,
					a.jumlah
				FROM member_transactions a
				WHERE
					a.member_id = '${data.member_id}'
				ORDER BY a.bulan_bayar DESC
			`;
			connection.query(query, function(error, result) {
				if (error) {
					reject(error.sqlMessage);
				} else {
					resolve(result);
				}
			});
		});
	}

	static findTotal(connection) {
		return new Promise((resolve, reject) => {
			const query = `
				SELECT SUM(jumlah) AS total	FROM member_transactions
			`;
			connection.query(query, function(error, result) {
				if (error) {
					reject(error.sqlMessage);
				} else {
					resolve(result[0]);
				}
			});
		});
	}

	static removeByMemberTransactionId(connection, data) {
		return new Promise((resolve, reject) => {
			const query = `
				DELETE FROM member_transactions WHERE id = '${data.member_transaction_id}'
			`;
			connection.query(query, function(error, result) {
				if (error) {
					reject(error.sqlMessage);
				} else {
					resolve(result);
				}
			});
		});
	}
	
}

module.exports = MemberTransactions;

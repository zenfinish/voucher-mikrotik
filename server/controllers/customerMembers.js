const mysql = require('../helpers/mysql');

class CustomerMembersController {

	static get(req, res) {
		const query = `
			SELECT
				b.*
			FROM customer_members a
			JOIN customers b ON a.customer_id = b.id
		`;
		// console.log(query)
		mysql.query(query, function(error, result) {
			if (error) return res.status(500).json(error.sqlMessage);
			
			res.status(200).json(result);
		});
	}

};

module.exports = CustomerMembersController;

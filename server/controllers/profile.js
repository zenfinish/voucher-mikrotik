const Profile = require('../models/profile.js');
const api = require('../helpers/connect-mikrotik.js');
const mysql = require('../helpers/mysql.js');

class ProfileController {

	static post(req, res) {
		const { name, price, kode, jenis } = req.body;

		mysql.query(`
			INSERT INTO profile (name, price, kode, jenis)
			VALUES ('${name}', '${price}', '${kode}', '${jenis}')
		`, function(error, result) {
			if (error) {
				res.status(500).json(error.sqlMessage);
			} else {
				res.status(201).json(result);
			};
		});
	}

	static get(req, res) {
		const whereData = [];

		whereData.push(`a.hapus IS NULL`);
		whereData.push(`a.jenis = 1`);
		whereData.push(`a.midtrans = "1"`);
		
		mysql.query(`
			SELECT
				a.*,
				a.id AS profile_id
			FROM profile a
			${
				whereData.length === 0 ? '' :
				`WHERE ${whereData.join(' && ')}`
			}
			ORDER BY a.tgl DESC
		`, function(error, result) {
			if (error) {
				res.status(500).json(error.sqlMessage);
			} else {
				res.status(200).json(result);
			};
		});
	}

	static get2(req, res) {
		const { hapus, limit, jenis } = req.query;

		const whereData = [];
		if (hapus) {
			if (hapus === "true") {
				whereData.push(`a.hapus IS NOT NULL`);
			} else if (hapus === "false") {
				whereData.push(`a.hapus IS NULL`);
			};
		};
		if (jenis) jenis === "1" ? whereData.push(`a.jenis = 1`) : jenis === "2" ? whereData.push(`a.jenis = 2`) : null;
		
		mysql.query(`
			SELECT
				a.*,
				a.id AS profile_id
			FROM profile a
			${
				whereData.length === 0 ? '' :
				`WHERE ${whereData.join(' && ')}`
			}
			ORDER BY a.tgl DESC
			${
				limit && limit === 'all' ? '' :
				limit && limit !== 'all' ? `LIMIT ${limit}` :
				`LIMIT 100`
			}
		`, function(error, result) {
			if (error) {
				res.status(500).json(error.sqlMessage);
			} else {
				res.status(200).json(result);
			};
		});
	}

	static getReguler(req, res) {
		Profile.getReguler()
		.then(function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error.message);
		});
	}

	static getMember(req, res) {
		Profile.getMember()
		.then(function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error.message);
		});
	}

	static deleteProfileId(req, res) {
		const { profile_id } = req.params;

		mysql.query(`
			DELETE FROM profile WHERE id = '${profile_id}'
		`, function(error, result) {
			if (error) {
				res.status(500).json(error.sqlMessage);
			} else {
				res.status(201).json(result);
			};
		});
	}

};

module.exports = ProfileController;
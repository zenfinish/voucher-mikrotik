const Role = require('../models/role.js');

class RoleController {

	static get(req, res) {
		Role.find()
		.then(function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error.message);
		});
	}

};

module.exports = RoleController;
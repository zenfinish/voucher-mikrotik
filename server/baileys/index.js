const { rmSync, readdir } = require('fs');
const { join } = require('path');
const pino = require("pino");
const {
    DisconnectReason,
    useMultiFileAuthState,
    makeInMemoryStore,
    Browsers,
    delay,
} = require('@adiwajshing/baileys');
const makeWASocket = require('@adiwajshing/baileys').default;
const { toDataURL } = require('qrcode');
const { sendSocket } = require("../socketio");
// const randomstring = require("randomstring");
// const mysql = require('../helpers/mysql');

const sessions = new Map();
const retries = new Map();

const getSessions = () => {
    const data = [...sessions.entries()];
    return data;
};

const getSession = () => {
    return sessions.get(process.env.SESSION_ID_BAILEYS) ?? null
};

const sessionsDir = (sessionId = '') => {
    return join('./baileys', 'sessions', sessionId ? sessionId : '')
};

const shouldReconnect = (sessionId) => {
    let maxRetries = parseInt(process.env.MAX_RETRIES ?? 0)
    let attempts = retries.get(sessionId) ?? 0

    maxRetries = maxRetries < 1 ? 1 : maxRetries

    if (attempts < maxRetries) {
        ++attempts

        console.log('Reconnecting...', { attempts, sessionId })
        retries.set(sessionId, attempts)

        return true
    }

    return false
};

const createSession = async (sessionId = process.env.SESSION_ID_BAILEYS, res, req) => {
    const sessionFile = 'md_' + sessionId;
    const logger = pino({ level: 'warn' });
    const store = makeInMemoryStore({ logger });

    let state, saveState;

    ;({ state, saveCreds: saveState } = await useMultiFileAuthState(sessionsDir(sessionFile)));

    const waConfig = {
        auth: state,
        // printQRInTerminal: true,
        logger,
        browser: Browsers.ubuntu('Chrome'),
    };

    const wa = makeWASocket(waConfig);

    store.readFromFile(sessionsDir(`${sessionId}_store.json`));
    store.bind(wa.ev);

    sessions.set(sessionId, { ...wa, store });
    
    // wa.ev.on('messages.upsert', async chat => {
    //     if (chat.messages) {
    //         const m = chat.messages[0];
    //         const fromMe = m.key.fromMe;
            
    //         if (!fromMe) {
    //             const text = m.message.conversation ? m.message.conversation : m.message.extendedTextMessage ? m.message.extendedTextMessage.text : null;
    //             const sender = m.key.remoteJid;
    //             const body = (text).split(" ");
    //             const sandi = body[0];
    
    //             if (sandi.toUpperCase() === "V") {
    //                 voucher(conn, body, sender);
    //             } else if (sandi.toUpperCase() === "REPORT") {
    //                 report(conn, body, sender);
    //             } else if (sandi.toUpperCase() === "KUOTA") {
    //                 kuota(conn, body, sender);
    //             } else if (sandi.toUpperCase() === "BROADCAST") {
    //                 broadCast(conn);
    //             } else {
    //                 await conn.sendMessage(sender, { text: '👋 Bila ada kendala hubungi nomor. 085384980303 atau 08117230699!' });
    //             };
    //         };

    //     };
    // });

    
    wa.ev.on('creds.update', (data) => {
        if (sessions.size > 1) {
            return;
        };
        // const numberCred = data.me.id.split(":")[0];
        // if (numberCred === "6282183784080") {
        //     req.app.get('io').emit('ScanBarcode', {
        //         ket: `Masuk Registrasi Atas Nam`,
        //     });
        //     return;
        // };
        saveState(data);
    });

    wa.ev.on('connection.update', async (update) => {
        const { connection, lastDisconnect } = update;
        const statusCode = lastDisconnect?.error?.output?.statusCode;

        if (connection === 'open') {
            retries.delete(sessionId);
        };

        if (connection === 'close') {
            if (statusCode === DisconnectReason.loggedOut || !shouldReconnect(sessionId)) {
                if (res && !res.headersSent) {
                    res.status(500).json('Unable to create session.');
                };
                sendSocket("SessionDeleted", { sessionId });
                return deleteSession(sessionId);
            }

            setTimeout(
                () => { createSession(sessionId, res) },
                statusCode === DisconnectReason.restartRequired ? 0 : parseInt(process.env.RECONNECT_INTERVAL ?? 0)
            );
        };

        if (update.qr) {
            if (res && !res.headersSent) {
                try {
                    const qr = await toDataURL(update.qr);
                    res.status(200).json({
                        text: 'QR code received, please scan the QR code.',
                        qr,
                    });
                    return;
                } catch {
                    res.status(500).json('Unable to create QR code.');
                };
            };

            try {
                await wa.logout();
            } catch {

            } finally {
                deleteSession(sessionId);
            };
        };
    });
};

const deleteSession = (sessionId) => {
    const sessionFile = 'md_' + sessionId;
    const storeFile = `${sessionId}_store.json`;
    const rmOptions = { force: true, recursive: true };

    rmSync(sessionsDir(sessionFile), rmOptions)
    rmSync(sessionsDir(storeFile), rmOptions)

    sessions.delete(sessionId)
    retries.delete(sessionId)
};

const init = () => {
    readdir('./baileys/sessions', (err, files) => {
        if (err) {
            throw err
        };

        for (const file of files) {
            if (!file.startsWith('md_') || file.endsWith('_store')) {
                continue;
            };
            const filename = file.replace('.json', '');
            const isLegacy = filename.split('_', 1)[0] !== 'md';
            const sessionId = filename.substring(isLegacy ? 7 : 3);

            createSession(sessionId);
        };
    });
};

const sendMessage = async (session, receiver, message, delayMs = 1000) => {
    try {
        await delay(parseInt(delayMs));
        return session.sendMessage(receiver, message);
    } catch {
        return Promise.reject(null); // eslint-disable-line prefer-promise-reject-errors
    };
};

// const textSent = (data) => {
//     return `${data.voucher} Anda Adalah:
// User. ${data.username}
// Password. ${data.password}
// Bila ada kendala hubungi nomor. 085384980303
// Voucer ini hanya bisa dipakai untuk 1 perangkat
// Cara Cek Kuota Ketik: kuota(spasi)username`;
// };

// const cariProfil = (kode) => {
//     return new Promise((resolve, reject) => {
//         mysql.query(`
//             SELECT * FROM profile WHERE kode = '${kode.toUpperCase()}' && hapus IS NULL
//         `, function(error, result) {
//             if (error) {
//                 reject(error);
//             } else {
//                 if (result.length > 0) {
//                     resolve(result[0]);
//                 } else {
//                     reject("Kode Tidak Ditemukan");
//                 };
//             };
//         });
//     });
// };

// const report = (conn, body, sender) => {
//     const tglDari = body[1];
//     const tglSampai = body[2];
//     let query;
//     if (tglDari.toUpperCase() === 'DAY') {
//         query = `
//             SELECT SUM(a.price) AS total
//             FROM transaction a
//             WHERE a.salah IS NULL && a.tgl >= NOW() - INTERVAL 1 DAY
//         `;
//     } else if (tglDari.toUpperCase() === 'MONTH') {
//         query = `
//             SELECT SUM(a.price) AS total
//             FROM transaction a
//             WHERE a.salah IS NULL && a.tgl >= NOW() - INTERVAL 1 MONTH
//         `;
//     } else if (tglDari.toUpperCase() === 'WEEK') {
//         query = `
//             SELECT SUM(a.price) AS total
//             FROM transaction a
//             WHERE a.salah IS NULL && a.tgl >= NOW() - INTERVAL 1 WEEK
//         `;
//     } else {
//         query = `
//             SELECT SUM(price) AS total
//             FROM transaction a
//             WHERE a.salah IS NULL && (a.tgl BETWEEN '${tglDari}' AND '${tglSampai} 23:59:59')
//         `;
//     };
    
//     mysql.query(query, (error, result) => {
//         if (error) {
//             conn.sendMessage(sender, { text: `👋 ${error}` });
//         } else {
//             conn.sendMessage(sender, { text: `Jumlah: Rp. ${Number(result[0].total).toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}` });
//         };
//     });
// };

// const voucher = (conn, body, sender) => {
//     const kode = body[1];
//     const hp = body[2];
//     const nomor = '0' + sender.replace("@s.whatsapp.net", "").substring(2);

//     // await conn.isOnWhatsApp ('62223612')
//     // in legacy
//     // await sock.onWhatsApp('62223612')
//     // in MD

//     conn.onWhatsApp('62' + hp.substring(1))
//     .then((result) => {
//         if (!result) return conn.sendMessage(sender, { text: `👋 GAGAL Kirim Ke: ${hp}` });

//         cekPengirim(nomor)
//         .then((result) => {
//             const user_id = result.id;
//             const propinsi_id = result.propinsi_id;
        
//             cariProfil(kode)
//             .then((result) => {
//                 const profil = result.name;
//                 const price = result.price;
//                 const sql_date = result.sql_date;
//                 const max_data = result.max_data;

//                 mysql.beginTransaction((error) => {
//                     if (error) return conn.sendMessage(sender, { text: '👋 GAGAL!' });
//                     const groupname = result.groupname;
        
//                     let username1 = randomstring.generate({
//                         length: 2,
//                         charset: 'alphabetic',
//                         capitalization: 'uppercase'
//                     });
//                     let username2 = randomstring.generate({
//                         length: 2,
//                         charset: 'numeric',
//                         capitalization: 'uppercase'
//                     });
//                     let username = `${username1}${username2}`;
//                     const password = randomstring.generate({
//                         length: 5,
//                         charset: 'numeric',
//                         capitalization: 'uppercase'
//                     });
//                     mysql.query(`
//                         INSERT INTO transaction(username, password, voucher, price, propinsi_id, user_id, hp)
//                         VALUES('${username}', '${password}', '${profil}', '${price}', '${propinsi_id}', '${user_id}', '${hp}')
//                     `, (error, result) => {
//                         if (error) {
//                             mysql.rollback(() => {
//                                 conn.sendMessage(sender, { text: '👋 GAGAL!' });
//                             });
//                         } else {
//                             const transaction_id = result.insertId;
        
//                             mysql.query(`
//                                 SELECT
//                                     DATE_FORMAT(transaction.tgl + ${sql_date}, '%d %b %Y %T') AS tgl,
//                                     transaction.username,
//                                     transaction.password,
//                                     transaction.voucher,
//                                     user.name AS user_name
//                                 FROM transaction
//                                 LEFT JOIN user ON transaction.user_id = user.id
//                                 WHERE transaction.id = '${transaction_id}'
//                             `, (error, result) => {
//                                 if (error) {
//                                     mysql.rollback(() => {
//                                         conn.sendMessage(sender, { text: '👋 GAGAL!' });
//                                     });
//                                 } else {
//                                     const voucher = result[0].voucher;
//                                     const tgl = result[0].tgl;
                                    
//                                     let sql = "INSERT INTO radcheck(username, attribute, op, value, transaction_id) VALUES ?";
//                                     let values = [
//                                         [username, 'Cleartext-Password', ':=', password, transaction_id],
//                                         [username, 'Expiration', ':=', tgl, transaction_id],
//                                         [username, 'Simultaneous-Use', ':=', '1', transaction_id],
//                                     ];
//                                     if (max_data) {
//                                         values.push([username, 'Max-Data', ':=', max_data, transaction_id]);
//                                     };
//                                     mysql.query(sql, [values], (error, result) => {
//                                         if (error) {
//                                             mysql.rollback(() => {
//                                                 conn.sendMessage(sender, { text: '👋 GAGAL!' });
//                                             });
//                                         } else {
        
//                                             let sql = "INSERT INTO radusergroup(username, groupname, transaction_id) VALUES ?";
//                                             let values = [
//                                                 [username, groupname, transaction_id],
//                                             ];;
//                                             mysql.query(sql, [values], (error, result) => {
//                                                 if (error) {
//                                                     mysql.rollback(() => {
//                                                         conn.sendMessage(sender, { text: '👋 GAGAL!' });
//                                                     });
//                                                 } else {
                                                    
//                                                     conn.sendMessage(`62${hp.substring(1)}@s.whatsapp.net`, {
//                                                         text: textSent({ username, password, voucher })
//                                                     })
//                                                     .then((result) => {
//                                                         mysql.commit(() => {
//                                                             conn.sendMessage(sender, { text: 'BERHASIL...' });
//                                                         });
//                                                     })
//                                                     .catch((error) => {
//                                                         // console.log("keluar====", error)
//                                                         mysql.rollback(() => {
//                                                             conn.sendMessage(sender, { text: `👋 ${error}` });
//                                                         });
//                                                     });
//                                                 };
//                                             });
//                                         };
//                                     });
//                                 };
//                             });
//                         };
//                     });
//                 });
//             })
//             .catch((error) => {
//                 conn.sendMessage(sender, { text: `👋 ${error}` });
//             });
//         })
//         .catch((error) => {
//             conn.sendMessage(sender, { text: `👋 ${error}` });
//         });
//     })
//     .catch((error) => {
//         conn.sendMessage(sender, { text: `👋 GAGAL Kirim Ke: ${hp}` });
//     });
// };

// const kuota = (conn, body, sender) => {
//     const username = body[1];
//     const query = `
//         SELECT
//             a.username,
//             (SUM(a.acctinputoctets) / 1048576) AS download,
//             (SUM(a.acctoutputoctets) / 1048576) AS upload,

//             b.value AS expiration
//         FROM radacct a
//         JOIN (
//             SELECT
//                 a.username,
//                 a.value
//             FROM radcheck a
//             WHERE a.attribute = 'Expiration' && a.username = '${username}'
//         ) b ON a.username = b.username
//         WHERE a.username = '${username}'
//         GROUP BY a.username
//     `;
//     mysql.query(query, (error, result) => {
//         if (error) {
//             conn.sendMessage(sender, { text: `👋 Error Di Server` });
//         } else {
//             if (result.length > 0) {
//                 conn.sendMessage(sender, { text: `Username: ${result[0].username}
// Download: ${Math.round(Number(result[0].download))}
// Upload: ${Math.round(Number(result[0].upload))}
// Total: ${Number((result[0].download + result[0].upload) / 1000).toFixed(2)} GB
// Expiration: ${result[0].expiration}` });
//             } else {
//                 conn.sendMessage(sender, { text: `👋 Data Tidak Ditemukan.` });
//             };
//         };
//     });
// };

// const broadCast = (conn) => {
//     mysql.query(`
//         SELECT
//             a.hp
//         FROM vouchers a
//         WHERE a.broadcast IS NULL
//         ORDER BY a.tgl DESC
//     `, (error, result) => {
//         if (error) {
//             console.log(error)
//         } else {
//             let counter = 0;
//             const eksekusi = setInterval(() => {
//                 // console.log("masuk=======", result[counter])

//                 mysql.beginTransaction((error) => {
//                     if (error) return console.log("keluar===", error.sqlMessage);

//                     mysql.query(`
//                         UPDATE vouchers SET broadcast = 1 WHERE hp = '${result[counter].hp}'
//                     `, (error, result2) => {
//                         if (error) return console.log("keluar2===", error.sqlMessage);

//                         conn.sendMessage(`62${result[counter].hp.substring(1)}@s.whatsapp.net`, {
//                             text: `Halo pelanggan setia verdhotspot...
// Oktober ceria promo ceria !
// Promo diperpanjang hingga 31 Oktober 2022
// Dapatkan voucher unlimited hanya 50 ribu !!!
// Untuk info lebih lanjut 08117230699`
//                         })
//                         .then((result3) => {
//                             mysql.commit();
//                         })
//                         .catch((error) => {
//                             mysql.rollback();
//                         });
//                     });
//                 });

//                 counter++;
//                 if (counter === result.length) clearInterval(eksekusi);
//             }, 20000); // 20 Detik
//         };
//     });
// };

// const cekPengirim = (nomor) => {
//     return new Promise((resolve, reject) => {
//         mysql.query(`
//             SELECT * FROM user WHERE whatsapp = '${nomor}'
//         `, function(error, result) {
//             if (error) {
//                 reject(error);
//             } else {
//                 if (result.length > 0) {
//                     resolve(result[0]);
//                 } else {
//                     reject("Nomor Tidak Berlaku");
//                 };
//             };
//         });
//     });
// };

module.exports = {
    createSession,
    init,
    getSessions,
    getSession,
    sendMessage,
};

const Module = require("../models/module");

const getModule = (user_id) => {
  return new Promise((resolve, reject) => {
    Module.findByUserId(user_id)
    .then(function(result) {
      resolve(result);
    })
    .catch(function(error) {
      reject(error);
    });
  });
};

module.exports = getModule;


const express = require('express');
const axios = require('axios');
const { isUser } = require('../middlewares/is-user.js');
const mysql = require('../helpers/mysql');
const { createVoucher } = require("../helpers");
const { sendSocket } = require("../socketio");

const router = express.Router();

const sendWA = (receiver, text) => {
    return new Promise((resolve, reject) => {
        axios.post(`${process.env.WHATSAPP_URL}/chats/send/`, {
            receiver,
            message: {
                text,
            },
            token: process.env.WHATSAPP_TOKEN,
        })
        .then(result => {
            resolve(result);
        })
        .catch(error => {
            // console.log("sendMessage=========", error.response.data)
            reject(error);
        });
    });
};

const textSent = (data) => {
    return `${data.voucher} Anda Adalah:
User. ${data.username}
Password. ${data.password}
Bila ada kendala hubungi nomor. 085384980303
Voucer ini hanya bisa dipakai untuk 1 perangkat
Cara Cek Kuota Ketik: kuota(spasi)username`;
};

router.post('/cek', isUser, (req, res) => {
    const { mt_order_id } = req.body;
});

router.post('/', isUser, (req, res) => {
    
});

router.get('/', isUser, (req, res) => {
    const { mt_order_id } = req.query;

    const whereData = [];
    if (mt_order_id) whereData.push(`a.mt_order_id = '${mt_order_id}'`);

    const query = `
        SELECT
            a.*,
            DATE_FORMAT(a.tgl, '%d %M %Y, %k:%i:%s') AS tgl_indo,
            b.name AS profile_name,
            b.kode
        FROM transactions a
        LEFT JOIN profile b ON a.profile_id = b.id
        WHERE
        ${
            whereData.length === 0 ? '' :
            whereData.join(' && ')
        }
    `;
    // console.log(query)
    mysql.query(query, function(error, result) {
        if (error) return res.status(500).json(error.sqlMessage);

        if (result.length > 0) {
            res.status(200).json(result[0]);
        } else {
            res.status(500).json(`Data Order ID: ${mt_order_id} Tidak Ditemukan`);
        };
    });
});

router.patch('/', isUser, async (req, res) => {
    const { mt_order_id, mt_transaction_id, mt_transaction_status, mt_json, kode, profile_name, hp } = req.body;

    let username;
    let password;
    let endtime;
    let errorCreateVoucher = false;

    const setData = [];
    if (mt_transaction_id) setData.push(`mt_transaction_id = '${mt_transaction_id}'`);
    if (mt_transaction_status) {
        setData.push(`mt_transaction_status = '${mt_transaction_status}'`);
        if (mt_transaction_status === "settlement") {

            await createVoucher(kode)
            .then(result => {
                username = result.username;
                password = result.password;
                endtime = result.endtime;
                // console.log("guugk=======", result)
            })
            .catch(error => {
                // console.log("Error Create Voucher =====", error)
                errorCreateVoucher = true;
            });
        };
    };
    if (mt_json) setData.push(`mt_json = '${mt_json}'`);
    if (username) setData.push(`username = '${username}'`);
    if (password) setData.push(`password = '${password}'`);
    if (endtime) setData.push(`endtime = '${endtime}'`);

    if (errorCreateVoucher) {
        return res.status(500).json("Error Pada Saat Create Voucher");
    };

    mysql.beginTransaction(error => {
        if (error) return res.status(500).json(error.sqlMessage);

        const query = `
            UPDATE transactions SET
                ${setData.join(', ')}
            WHERE mt_order_id = '${mt_order_id}'
        `;
        // console.log(query)
        mysql.query(query, function(error, result) {
            if (error) {
                // console.log("updateTransaction=======", error)
                return res.status(500).json(error.sqlMessage);
            };
    
            const query = `
                SELECT
                    a.*,
                    DATE_FORMAT(a.tgl, '%d %M %Y, %k:%i:%s') AS tgl_indo,
                    b.name AS profile_name,
                    b.kode
                FROM transactions a
                LEFT JOIN profile b ON a.profile_id = b.id
                WHERE a.mt_order_id = '${mt_order_id}'
            `;
            // console.log(query)
            mysql.query(query, function(error, result) {
                if (error) {
                    // console.log("masuk=======", error)
                    mysql.rollback(function(error) {
                        if (error) {
                            return mysql.rollback(function() {
                                throw error;
                            });
                        };
                        return res.status(500).json(error.sqlMessage);
                    });
                };
    
                mysql.commit(function(error) {
                    if (error) {
                        return mysql.rollback(function() {
                            throw err;
                        });
                    };

                    sendSocket(mt_order_id, result[0]);
                    if (mt_transaction_status === "settlement") {
                        sendWA(`62${hp.substring(1)}@s.whatsapp.net`, textSent({ username, password, voucher: profile_name }))
                        .then((result) => {
                            return;
                        })
                        .catch((error) => {
                            return;
                        });
                    };
                    return res.status(201).json(result[0]);
                });
            });
        });
    });
});

module.exports = router;
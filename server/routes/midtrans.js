const express = require('express');
const axios = require("axios");

const router = express.Router();

let getCurrentTimestamp = () => {
	return "" + Math.round(new Date().getTime() / 1000);
};

router.get('/snap/token', (req, res) => {
    axios({
        url: "https://app.sandbox.midtrans.com/snap/v1/transactions",
        method: "post",
        headers: {
            "Content-Type": "application/json",
            Accept: "application/json",
            Authorization:
                "Basic " +
                Buffer.from("SB-Mid-server-54ZxYnGHGimQO9HmJ4TTgXnw").toString("base64")
                // Above is API server key for the Midtrans account, encoded to base64
        },
        data: {
            transaction_details: {
                order_id: "order-csb-" + getCurrentTimestamp(),
                gross_amount: 10000
            },
            credit_card: {
                secure: true
            },
            customer_details: {
                first_name: "Johny",
                last_name: "Kane",
                email: "testmidtrans@mailnesia.com",
                phone: "08111222333"
            }
        }
    }).then((snapResponse) => {
        let snapToken = snapResponse.data.token;
        console.log("Retrieved snap token:", snapToken);
        res.status(200).json(snapToken)
        // Pass the Snap Token to frontend, render the HTML page
        // res.send(getMainHtmlPage(snapToken, handleMainRequest));
    }, (error) => {
        // res.send(`Fail to call API w/ error ${error}`);
        console.log("keluar======", error);
    });
});

module.exports = router;

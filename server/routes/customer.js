const express = require('express');

const CustomerController = require('../controllers/customer');

const router = express.Router();

router.get('/gantipassword/cektoken/:token', CustomerController.getGantipasswordCektokenToken);

router.post('/register', CustomerController.postRegister);
router.post('/login', CustomerController.postLogin);
router.post('/cektoken', CustomerController.postCektoken);
router.post('/resetpassword', CustomerController.postResetpassword);
router.post('/gantipassword', CustomerController.postGantipassword);

module.exports = router;

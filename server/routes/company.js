const express = require('express');

const CompanyController = require('../controllers/company');

const router = express.Router();

router.patch('/', CompanyController.patch);

module.exports = router;
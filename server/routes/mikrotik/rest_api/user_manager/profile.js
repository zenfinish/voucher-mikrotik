const express = require('express');
const router = express.Router();
const {
    get: getProfile,
    patch: patchProfile,
} = require('../../../../mikrotik/rest_api/user_manager/profile');

router.get('/', (req, res) => {
    getProfile()
    .then(result => {
        res.status(200).json(result.data);
    })
    .catch(error => {
        res.status(500).json(error)
    });
});

router.patch('/:id', (req, res) => {
    patchProfile(req.params.id, req.body)
    .then(result => {
        res.status(201).json(result.data);
    })
    .catch(error => {
        res.status(500).json(error)
    });
});

module.exports = router;

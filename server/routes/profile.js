const express = require('express');

const ProfileController = require('../controllers/profile.js');
const { isUser } = require('../middlewares/is-user.js');

const router = express.Router();

router.get('/reguler', isUser, ProfileController.getReguler);

router.get('/member', isUser, ProfileController.getMember);

router.delete('/:profile_id', isUser, ProfileController.deleteProfileId);

router.get('/2', isUser, ProfileController.get2);

router.post('/', isUser, ProfileController.post);
router.get('/', isUser, ProfileController.get);

module.exports = router;

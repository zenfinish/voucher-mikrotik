const express = require('express');
const MembersController = require('../../controllers/members');
const router = express.Router();

const multer = require('multer');
const storage = multer.memoryStorage();
const upload = multer({ storage });

router.get('/dokumen/modem/:id_membertransaksi', MembersController.getTransaksiDokumenModemIdMembertransaksi);
router.get('/dokumen/rumah/:id_membertransaksi', MembersController.getTransaksiDokumenRumahIdMembertransaksi);
router.get('/dokumen/redaman/:id_membertransaksi', MembersController.getTransaksiDokumenRedamanIdMembertransaksi);
router.get('/dokumen/ktp/:id_membertransaksi', MembersController.getTransaksiDokumenKtpIdMembertransaksi);

router.post('/detail/bayar', MembersController.postDetailBayar);
router.post('/detail', MembersController.postDetail);
router.get('/detail', MembersController.getTransaksiDetail);

router.get('/:id_membertransaksi', MembersController.getTransaksiIdMembertransaksi);

router.get('/', MembersController.getTransaksi);
router.post('/',
    upload.fields([
        { name: 'modem' },
        { name: 'rumah' },
        { name: 'redaman' },
        { name: 'ktp' }
    ]),
    MembersController.postTransaksi);
  
module.exports = router;

const express = require('express');

const CustomerMembersController = require('../controllers/customerMembers');

const router = express.Router();

router.get('/', CustomerMembersController.get);

module.exports = router;

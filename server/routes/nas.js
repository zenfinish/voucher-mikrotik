const express = require('express');
const mysql = require('../helpers/mysql.js');

const router = express.Router();

router.get('/', (req, res) => {
    mysql.query(`
        SELECT
            a.*,
            a.id AS nas_id
        FROM nas a
    `, function(error, result) {
        if (error) {
            res.status(500).json(error.sqlMessage);
        } else {
            res.status(200).json(result);
        };
    });
});

router.get('/:nas_id', (req, res) => {
    const { nas_id } = req.params;

    mysql.query(`
        SELECT
            a.*,
            a.id AS nas_id
        FROM nas a
        WHERE a.id = ${nas_id}
    `, function(error, result) {
        if (error) {
            res.status(500).json(error.sqlMessage);
        } else {
            if (result.length === 0) {
                res.status(500).json("Data Tidak Ditemukan");
            } else {
                res.status(200).json(result[0]);
            };
        };
    });
});

router.post('/', (req, res) => {
    const { url, username, password } = req.body;

    mysql.query(`
        INSERT INTO nas (url, username, password)
        VALUES ('${url}', '${username}', '${password}')
    `, function(error, result) {
        if (error) {
            res.status(500).json(error.sqlMessage);
        } else {
            res.status(201).json('Ok');
        };
    });
});

router.put('/:nas_id', (req, res) => {
    const { nas_id } = req.params;
    const { url, username, password } = req.body;

    mysql.query(`
        UPDATE nas SET
            url = "${url}",
            username = "${username}",
            password = "${password}"
        WHERE id = ${nas_id}
    `, function(error, result) {
        if (error) {
            res.status(500).json(error.sqlMessage);
        } else {
            res.status(201).json('Ok');
        };
    });
});

router.delete('/:nas_id', (req, res) => {
    mysql.query(`
        DELETE FROM nas WHERE id = ${req.params.nas_id}
    `, function(error, result) {
        if (error) {
            res.status(500).json(error.sqlMessage);
        } else {
            res.status(200).json(result);
        };
    });
});

module.exports = router;

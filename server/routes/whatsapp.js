const express = require('express');

const WhatsappController = require('../controllers/whatsapp');

const router = express.Router();

router.get('/sessions', WhatsappController.getSessions);
router.post('/sessions/add', WhatsappController.postSessionsAdd);
router.post('/login', WhatsappController.postLogin);

module.exports = router;

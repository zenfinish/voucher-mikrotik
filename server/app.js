require('dotenv').config();

const express = require('express');
const cors = require('cors');
const http = require('http');
const https = require('https');
const fs = require('fs');
// const cron = require('node-cron');

const { init: initBaileys } = require("./baileys");
const { init: initSocket } = require("./socketio");
const { isVersion } = require('./middlewares/isVersion');
const { isUser } = require('./middlewares/is-user');
const mysql = require('./helpers/mysql');

const app = express();
const httpServer = http.createServer(app);
const httpsServer = https.createServer({
    key: fs.readFileSync(process.env.PRIVKEY),
    cert: fs.readFileSync(process.env.CERT),
}, app);

const port = process.env.PORT || 3002;

app.use(cors());
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

app.use('/public', express.static('public'));

app.use('/company', isVersion, require('./routes/company'));
app.use('/users', isVersion, require('./routes/user.js'));
app.use('/mikrotik', isVersion, isUser, require('./routes/mikrotik'));
app.use('/transactions', isVersion, require('./routes/transactions'));
app.use('/transaction', isVersion, require('./routes/transaction'));
app.use('/profile', isVersion, require('./routes/profile.js'));
app.use('/pemberitahuan', isVersion, require('./routes/pemberitahuan.js'));
app.use('/limitation', isVersion, require('./routes/limitation.js'));
app.use('/role', isVersion, require('./routes/role.js'));
app.use('/member', isVersion, require('./routes/members'));
app.use('/customers', isVersion, require('./routes/customers.js'));
app.use('/customer', isVersion, require('./routes/customer'));
app.use('/customer-members', isVersion, require('./routes/customerMembers'));
app.use('/whatsapp', isVersion, require('./routes/whatsapp'));
app.use('/midtrans', isVersion, require('./routes/midtrans'));
app.use('/nas', isVersion, require('./routes/nas'));

app.get('/paket', isUser, (req, res) => {
    mysql.query(`
        SELECT
            a.*
        FROM paket a
    `, function(error, result) {
        if (error) {
            res.status(500).json(error.sqlMessage);
        } else {
            res.status(200).json(result);
        };
    });
});

app.use('*', function(req, res) {
    res.status(404).json(`Link Tidak Ditemukan`);
});

const updateGagal = (data, socket) => {
    return new Promise((resolve, reject) => {
        mysql.query(`
            UPDATE transactions SET mt_transaction_status = 'gagal' WHERE id = ${data.id}
        `, (error, result) => {
            if (error) {
                reject(error);
            } else {
                const dataSend = { ...data, mt_transaction_status: "gagal", };
                socket.emit(data.mt_order_id, dataSend);
                resolve(result);
            };
        });
    });
};

const eksekusi = (socket) => setInterval(() => {
    mysql.query(`
        SELECT
            a.*,
            DATE_FORMAT(a.tgl, '%d %M %Y, %k:%i:%s') AS tgl_indo,
            TIMESTAMPDIFF(SECOND, a.tgl, NOW()) AS jarak,

            b.name AS profile_name,
            b.kode
        FROM transactions a
        LEFT JOIN profile b ON a.profile_id = b.id
        WHERE a.mt_transaction_status IS NULL
    `, async (error, result) => {
        if (!error) {
            for (let i = 0; i < result.length; i++) {
                if (result[i].jarak > 360) {
                    await updateGagal(result[i], socket);
                };
            };
        };
    });
}, 5000);

// 0: detik ke-0 setiap menit
// 12: pukul 12 siang (jam ke-12 pada format 24 jam)
// 1: tanggal 1
// *: setiap bulan
// *: setiap hari dalam seminggu
// *: setiap tahun

// cron.schedule('0 12 1 * * *', () => {
//     console.log('Tugas dijalankan pada tanggal 1 jam 12 siang.');
// });

httpServer.listen(port, function() {
    process.env.MODE === "DEVELOPMENT" ? console.log(`HTTP: Running on port ${port}`) : null;
    initSocket(httpServer, (socket) => {
        process.env.MODE === "DEVELOPMENT" ? eksekusi(socket) : null;
    });
    
    // const { createVoucher } = require("./helpers");
    // mysql.query(`
    //     SELECT
    //         a.*
    //     FROM transaction a
    //     WHERE
    //         a.guguk IS NULL &&
    //         a.tgl >= NOW() - INTERVAL 32 DAY &&
    //         a.price = 25000
    //     ORDER BY id DESC
    // `, async (error, result) => {
        
    //     for (let i = 0; i < result.length; i++) {
    //         await createVoucher("25", result[i].username, result[i].password)
    //         .then(result => {
    //             console.log("masuk=======", result)
    //         })
    //         .catch(error => {
    //             console.log("keluar=======", error)
    //         })
    //     }
    // });

});

httpsServer.listen(process.env.PORT_HTTPS, function() {
    initBaileys();
    initSocket(httpsServer, (socket) => {
        process.env.MODE === "DEVELOPMENT" ? eksekusi(socket) : null;
    });
    process.env.MODE === "DEVELOPMENT" ? console.log(`HTTPS: Running on port ${process.env.PORT_HTTPS}`) : null;
});

// const csv = require('csv-parser');
// const results = [];

// const insertMember = data => {
//     return new Promise((resolve, reject) => {
//         const query = `
//             INSERT INTO member (nama, hp, alamat)
//             VALUES ('${data.member}', ${data.no_wa === "" ? `NULL` : `0${data.no_wa}`}, '-')
//         `;
//         mysql.query(query, function(error, result) {
//             if (error) {
//                 reject(error.sqlMessage);
//             } else {
//                 resolve(result.insertId);
//             };
//         });
//     });
// };
// const insertMemberTransaksi = (data, id_member) => {
//     return new Promise((resolve, reject) => {
//         const feb = data.tgl_aktivasi.split('/');
//         const tgl_aktivasi = `20${feb[2]}-${feb[1]}-${feb[0]}`;
//         const query = `
//             INSERT INTO member_transaksi (id_member, id_paket, nama_paket, tgl_langganan, kode, koordinat)
//             VALUES (?, ?, ?, ?, ?, ?)
//         `;
//         mysql.query(query, [
//             id_member,
//             data.id_paket,
//             data.nama_paket,
//             tgl_aktivasi,
//             data.username === "" ? null : data.username,
//             data.koordinat === "" ? null : data.koordinat,
//         ], function(error, result) {
//             if (error) {
//                 reject(error.sqlMessage);
//             } else {
//                 resolve(result.insertId);
//             };
//         });
//     });
// };
// const insertMemberTransaksiDetail = (data, id_membertransaksi) => {
//     return new Promise((resolve, reject) => {
//         const query = `
//             INSERT INTO member_transaksi_detail (
//                 id_membertransaksi,
//                 id_paket,
//                 nama_paket,
//                 tgl_aktivasi,
//                 tgl_selesai,
//                 bulan,
//                 tahun,
//                 tarif_awal,
//                 tarif,
//                 tgl_bayar
//             ) VALUES (
//                 '${id_membertransaksi}',
//                 '${data.id_paket}',
//                 '${data.nama_paket}',
//                 '${data.tgl_aktivasi}',
//                 '${data.tgl_selesai}',
//                 '${data.bulan}',
//                 '${data.tahun}',
//                 '${data.tarif_awal}',
//                 '${data.tarif}',
//                 '${data.tgl_bayar}'
//             )
//         `;
//         mysql.query(query, function(error, result) {
//             if (error) {
//                 reject(error.sqlMessage);
//             } else {
//                 resolve(result.insertId);
//             };
//         });
//     });
// };

// fs.createReadStream('file.csv')
// .pipe(csv())
// .on('data', (row) => {
//     results.push(row);
// })
// .on('end', async () => {
//     for (let i = 0; i < results.length; i++) {
//         await insertMember(results[i])
//         .then(result => {

//             insertMemberTransaksi(results[i], result)
//             .then(async result => {

//                 // if (results[i].feb !== '') {
//                 //     const feb = results[i].feb.split('/');
//                 //     const tgl_bayar = `20${feb[2]}-${feb[1]}-${feb[0]}`;
//                 //     await insertMemberTransaksiDetail({
//                 //         ...results[i],
//                 //         tgl_aktivasi: '2024-02-05',
//                 //         tgl_selesai: '2024-03-04',
//                 //         bulan: '2',
//                 //         tahun: '2024',
//                 //         tarif_awal: results[i].nom_feb,
//                 //         tarif: results[i].nom_feb,
//                 //         tgl_bayar,
//                 //     }, result)
//                 //     .then(async result => {
        
//                 //     })
//                 //     .catch(error => {
//                 //         console.log("Error insertMemberTransaksiDetail 2 = ", error)
//                 //     });
//                 // };

//                 // if (results[i].mar !== '') {
//                 //     const mar = results[i].mar.split('/');
//                 //     const tgl_bayar = `20${mar[2]}-${mar[1]}-${mar[0]}`;
//                 //     await insertMemberTransaksiDetail({
//                 //         ...results[i],
//                 //         tgl_aktivasi: '2024-03-05',
//                 //         tgl_selesai: '2024-04-04',
//                 //         bulan: '3',
//                 //         tahun: '2024',
//                 //         tarif_awal: results[i].nom_mar,
//                 //         tarif: results[i].nom_mar,
//                 //         tgl_bayar,
//                 //     }, result)
//                 //     .then(async result => {
        
//                 //     })
//                 //     .catch(error => {
//                 //         console.log("Error insertMemberTransaksiDetail 3 = ", error)
//                 //     });
//                 // };

//                 // if (results[i].apr !== '') {
//                 //     const apr = results[i].apr.split('/');
//                 //     const tgl_bayar = `20${apr[2]}-${apr[1]}-${apr[0]}`;
//                 //     await insertMemberTransaksiDetail({
//                 //         ...results[i],
//                 //         tgl_aktivasi: '2024-04-05',
//                 //         tgl_selesai: '2024-05-04',
//                 //         bulan: '4',
//                 //         tahun: '2024',
//                 //         tarif_awal: results[i].nom_apr,
//                 //         tarif: results[i].nom_apr,
//                 //         tgl_bayar,
//                 //     }, result)
//                 //     .then(async result => {
        
//                 //     })
//                 //     .catch(error => {
//                 //         console.log("Error insertMemberTransaksiDetail 4 = ", error)
//                 //     });
//                 // };

//                 // if (results[i].mei !== '') {
//                 //     const mei = results[i].mei.split('/');
//                 //     const tgl_bayar = `20${mei[2]}-${mei[1]}-${mei[0]}`;
//                 //     await insertMemberTransaksiDetail({
//                 //         ...results[i],
//                 //         tgl_aktivasi: '2024-05-05',
//                 //         tgl_selesai: '2024-06-04',
//                 //         bulan: '5',
//                 //         tahun: '2024',
//                 //         tarif_awal: results[i].nom_mei,
//                 //         tarif: results[i].nom_mei,
//                 //         tgl_bayar,
//                 //     }, result)
//                 //     .then(async result => {
        
//                 //     })
//                 //     .catch(error => {
//                 //         console.log("Error insertMemberTransaksiDetail 5 = ", error)
//                 //     });
//                 // };

//                 // if (results[i].jun !== '') {
//                 //     const jun = results[i].jun.split('/');
//                 //     const tgl_bayar = `20${jun[2]}-${jun[1]}-${jun[0]}`;
//                 //     await insertMemberTransaksiDetail({
//                 //         ...results[i],
//                 //         tgl_aktivasi: '2024-06-05',
//                 //         tgl_selesai: '2024-07-04',
//                 //         bulan: '6',
//                 //         tahun: '2024',
//                 //         tarif_awal: results[i].nom_jun,
//                 //         tarif: results[i].nom_jun,
//                 //         tgl_bayar,
//                 //     }, result)
//                 //     .then(async result => {
        
//                 //     })
//                 //     .catch(error => {
//                 //         console.log("Error insertMemberTransaksiDetail 6 = ", error)
//                 //     });
//                 // };

//                 if (results[i].jul !== '') {
//                     const jul = results[i].jul.split('/');
//                     const tgl_bayar = `20${jul[2]}-${jul[1]}-${jul[0]}`;
//                     await insertMemberTransaksiDetail({
//                         ...results[i],
//                         tgl_aktivasi: '2024-07-05',
//                         tgl_selesai: '2024-08-04',
//                         bulan: '7',
//                         tahun: '2024',
//                         tarif_awal: results[i].nom_jul,
//                         tarif: results[i].nom_jul,
//                         tgl_bayar,
//                     }, result)
//                     .then(async result => {
        
//                     })
//                     .catch(error => {
//                         console.log("Error insertMemberTransaksiDetail 7 = ", error)
//                     });
//                 };

//                 if (results[i].agu !== '') {
//                     const agu = results[i].agu.split('/');
//                     const tgl_bayar = `20${agu[2]}-${agu[1]}-${agu[0]}`;
//                     await insertMemberTransaksiDetail({
//                         ...results[i],
//                         tgl_aktivasi: '2024-08-05',
//                         tgl_selesai: '2024-09-04',
//                         bulan: '8',
//                         tahun: '2024',
//                         tarif_awal: results[i].nom_agu,
//                         tarif: results[i].nom_agu,
//                         tgl_bayar,
//                     }, result)
//                     .then(async result => {
        
//                     })
//                     .catch(error => {
//                         console.log("Error insertMemberTransaksiDetail 8 = ", error)
//                     });
//                 };

//                 if (results[i].sep !== '') {
//                     const sep = results[i].sep.split('/');
//                     const tgl_bayar = `20${sep[2]}-${sep[1]}-${sep[0]}`;
//                     await insertMemberTransaksiDetail({
//                         ...results[i],
//                         tgl_aktivasi: '2024-09-05',
//                         tgl_selesai: '2024-10-04',
//                         bulan: '9',
//                         tahun: '2024',
//                         tarif_awal: results[i].nom_sep,
//                         tarif: results[i].nom_sep,
//                         tgl_bayar,
//                     }, result)
//                     .then(async result => {
        
//                     })
//                     .catch(error => {
//                         console.log("Error insertMemberTransaksiDetail 9 = ", error)
//                     });
//                 };

//                 if (results[i].okt !== '') {
//                     const okt = results[i].okt.split('/');
//                     const tgl_bayar = `20${okt[2]}-${okt[1]}-${okt[0]}`;
//                     await insertMemberTransaksiDetail({
//                         ...results[i],
//                         tgl_aktivasi: '2024-10-05',
//                         tgl_selesai: '2024-11-04',
//                         bulan: '10',
//                         tahun: '2024',
//                         tarif_awal: results[i].nom_okt,
//                         tarif: results[i].nom_okt,
//                         tgl_bayar,
//                     }, result)
//                     .then(async result => {
        
//                     })
//                     .catch(error => {
//                         console.log("Error insertMemberTransaksiDetail 10 = ", error)
//                     });
//                 };

//                 if (results[i].nov !== '') {
//                     const nov = results[i].nov.split('/');
//                     const tgl_bayar = `20${nov[2]}-${nov[1]}-${nov[0]}`;
//                     await insertMemberTransaksiDetail({
//                         ...results[i],
//                         tgl_aktivasi: '2024-11-05',
//                         tgl_selesai: '2024-12-04',
//                         bulan: '11',
//                         tahun: '2024',
//                         tarif_awal: results[i].nom_nov,
//                         tarif: results[i].nom_nov,
//                         tgl_bayar,
//                     }, result)
//                     .then(async result => {
        
//                     })
//                     .catch(error => {
//                         console.log("Error insertMemberTransaksiDetail 11 = ", error)
//                     });
//                 };
//             })
//             .catch(error => {
//                 console.log("Error insertMemberTransaksi = ", error)
//             });
//         })
//         .catch(error => {
//             console.log("Error insertMember = ", error)
//         });
//     };

//     console.log("berhasil=====================================")
// });

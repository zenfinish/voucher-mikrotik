const Connection = require("../../Connection");

const get = (data) => {
    const dq = [];
    if (data) {
        const { group, name, disabled } = data;
        if (group) dq.push(`group=${group}`);
        if (name) dq.push(`name=${name}`);
        if (disabled) dq.push(`disabled=${disabled}`); // yes or no
        if (data['.id']) dq.push(`.id=${data['.id']}`);
    };
    return Connection.get(`user-manager/user?${dq.join('&')}`);
};

const getSession = (data) => {
    const dq = [];
    if (data) {
        const { user } = data;
        if (user) dq.push(`user=${user}`);
    };
    return Connection.get(`user-manager/session?${dq.join('&')}`);
};

const getGroup = () => {
    return Connection.get(`user-manager/user/group`);
};

const save = (data) => {
    return Connection.put(`user-manager/user`, data);
};

const putUserManagerUserProfile = (data) => {
    return Connection.put(`user-manager/user-profile`, data);
};

const patchIdMikrotik = (id_mikrotik, data) => {
    return Connection.patch(`user-manager/user/${id_mikrotik}`, data);
};

module.exports = {
    get,
    getGroup,
    getSession,
    save,
    putUserManagerUserProfile,
    patchIdMikrotik,
};

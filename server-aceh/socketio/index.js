const { Server } = require("socket.io");

let ioServerG;

const init = (server, callback) => {
    const ioServer = new Server(server, {
        cors: {
            origin: process.env.WEB_CUSTOMER_DASHBOARD_URL,
        },
    });
    ioServerG = ioServer;
    ioServer.on('connection', function(socket) {
        callback(socket)
    });
};

const sendSocket = (text, value) => {
    ioServerG.emit(text, value);
};

module.exports = {
    init,
    sendSocket,
};

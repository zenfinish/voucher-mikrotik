const Connection = require("../../Connection");

const get = () => {
    return Connection.get(`user-manager/limitation`);
};

// const patch = (id, { price }) => {
//     const data = {};
//     if (price) data['price'] = price;
//     return Connection.patch(`user-manager/limitation/${id}`, data);
// };

const remove = (id) => {
    return Connection.delete(`user-manager/limitation/${id}`);
};

const save = (data) => {
    return Connection.post(`user-manager/limitation`, data);
};

module.exports = {
    get,
    // patch,
    remove,
    save,
};

const limitation = require("./limitation");
const profile = require("./profile");
const profileLimitation = require("./profile_limitation");
const user = require("./user");

module.exports = {
	limitation,
	profile,
	profileLimitation,
	user,
};
const Connection = require("../../Connection");

const get = () => {
    return Connection.get(`user-manager/profile`);
};

const patch = (id, { price }) => {
    const data = {};
    if (price) data['price'] = price;
    return Connection.patch(`user-manager/profile/${id}`, data);
};

const remove = (id) => {
    return Connection.delete(`user-manager/profile/${id}`);
};

const save = (data) => {
    return Connection.post(`user-manager/profile`, data);
};

module.exports = {
    get, patch, remove, save,
};

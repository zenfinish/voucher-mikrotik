const mysql = require('../helpers/mysql.js');

class CompanyController {

	static patch(req, res) {
		const { tgl_jt, tgl_invoice } = req.body;

		const setData = [];
		if (tgl_jt) setData.push(`tgl_jt = '${tgl_jt}'`);
		if (tgl_invoice) setData.push(`tgl_invoice = '${tgl_invoice}'`);

		const query = `
			UPDATE company SET
				${setData.join(', ')}
		`;
		// console.log(query)
		mysql.query(query, function(error, result) {
			if (error) return res.status(500).json(error.sqlMessage);

			res.status(201).json(result);
		});
	}

};

module.exports = CompanyController;

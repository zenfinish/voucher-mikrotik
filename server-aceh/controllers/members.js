const randomstring = require("randomstring");
const api = require('../helpers/connect-mikrotik.js');
const Member = require('../models/member.js');
const MemberCalon = require('../models/member_calon.js');
const MemberTransactions = require('../models/member_transactions.js');
const mysql = require('../helpers/mysql.js');
const fs = require("fs");
const {
    save: createUserMikrotik,
	putUserManagerUserProfile,
	patchIdMikrotik,
	// get: getUserManager,
} = require('../mikrotik/rest_api/user_manager/user');
// const {
// 	get: getUserManager,
// } = require('../mikrotik/rest_api/user_manager/profile');

// getUserManager()
// .then(result => {
// 	result.data.forEach(item => {
// 		console.log(item);
// 	});
// })

const updateDisabled = (data) => {
	return new Promise((resolve, reject) => {
		mysql.query(`
			UPDATE members SET
				disabled = ${data.disabled === 'true' ? '1' : `NULL`}
			WHERE filter_id = '${data['.id']}';
		`, function(error, result) {
			if (error) {
				reject(error.sqlMessage);
			} else {
				resolve('berhasil=====')
			}
		});
	});
}

const getFilterId = (data) => {
	return new Promise((resolve, reject) => {
		api.connect()
		.then(async function(conn) {
			const chan = conn.openChannel();
			await chan.write('/ip/firewall/filter/get', {
				'.id': `${data}`
			})
			.then(function(result) {
				const data = result.data[0];
				const datas = data.split(';');
				let datass = {};
				for (let i = 0; i < datas.length; i++) {
					let guguk = datas[i].split('=');
					datass[guguk[0]] = guguk[1];
				}
				resolve(datass);
			})
			.catch(function(error) {
				reject(error);
			});
		})
		.catch(function(error) {
			reject(error);
		});
	});
}

class MembersController {

	static getCalon(req, res) {
		MemberCalon.find(mysql)
		.then(function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error.message);
		})
	}

	static get(req, res) {
		Member.find(mysql)
		.then(function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error.message);
		})
	}

	static getSearch(req, res) {
		const { limit } = req.query;
		const { search } = req.headers;
		
		mysql.query(`
			SELECT
				a.*
			FROM member a
			WHERE a.nama LIKE "%${search}%" || a.hp LIKE "%${search}%"
			LIMIT ${limit}
		`, function(error, result) {
			if (error) {
				res.status(500).json(error.sqlMessage);
			} else {
				res.status(200).json(result);
			};
		});
	}

	static getProfiles(req, res) {
		mysql.query(`
			SELECT
				a.id AS member_profile_id,
				a.name AS member_profile_name,
				a.rate_limit,
				a.price
			FROM member_profiles a
		`, function(error, result) {
			if (error) {
				res.status(500).json(error.sqlMessage);
			} else {
				res.status(200).json(result);
			}
		})
	}

	static getFilterFilterId(req, res) {
		api.connect()
		.then(function(conn) {
			const chan = conn.openChannel();
			
			chan.write('/ip/firewall/filter/get', {
				'.id': `${req.params.filter_id}`
			})
			.then(function(result) {
				const data = result.data[0];
				const datas = data.split(';');
				let datass = {};
				for (let i = 0; i < datas.length; i++) {
					let guguk = datas[i].split('=');
					datass[guguk[0]] = guguk[1];
				}
				res.status(200).json(datass);
			})
			.catch(function(error) {
				res.status(500).json(error);
			});
		})
		.catch(function(error) {
			res.status(500).json(error);
		});
	}

	static getTransactionsTotal(req, res) {
		MemberTransactions.findTotal(mysql)
		.then(function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error);
		});
	}

	static getTransactionsMemberId(req, res) {
		MemberTransactions.findByMemberId(mysql, req.params)
		.then(function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error);
		});
	}

	static deleteTransactionsMemberTransactionId(req, res) {
		mysql.beginTransaction(function(error) {
			if (error) res.status(500).json(error);

			MemberTransactions.removeByMemberTransactionId(mysql, req.params)
			.then(function(result) {

				fs.unlink(`../public/${req.headers.img}`, (error) => {
					if (error) {
						mysql.rollback();
						res.status(500).json(error);
					} else {
						mysql.commit(function(error) {
							if (error) {
								mysql.rollback();
								res.status(500).json(error);
							} else {
								res.status(200).json(result);
							}
						});
					}
				});
				
			})
			.catch(function(error) {
				mysql.rollback();
				res.status(500).json(error);
			});

		});
	}

	static putFilterEnableFilterId(req, res) {
		api.connect()
		.then(function(conn) {
			const chan = conn.openChannel();
			
			chan.write('/ip/firewall/filter/enable', {
				'.id': `${req.params.filter_id}`
			})
			.then(function(result) {
				updateDisabled({ disabled: null, ['.id']: req.params.filter_id })
				.then(function(result) {
					res.status(201).json(result);
				})
				.catch(function(error) {
					res.status(500).json(error);
				});
			})
			.catch(function(error) {
				res.status(500).json(error);
			});
		})
		.catch(function(error) {
			res.status(500).json(error);
		});
	}

	static putFilterDisableFilterId(req, res) {
		api.connect()
		.then(function(conn) {
			const chan = conn.openChannel();
			
			chan.write('/ip/firewall/filter/disable', {
				'.id': `${req.params.filter_id}`
			})
			.then(function(result) {
				
				updateDisabled({ disabled: 'true', ['.id']: req.params.filter_id })
				.then(function(result) {
					res.status(201).json(result);
				})
				.catch(function(error) {
					res.status(500).json(error);
				});
			})
			.catch(function(error) {
				res.status(500).json(error);
			});
		})
		.catch(function(error) {
			res.status(500).json(error);
		});
	}

	static patchStatusFilter(req, res) {
		mysql.query(`
			SELECT * FROM members
		`, function(error, result) {
			if (error) {
				res.status(500).json(error.sqlMessage);
			} else {
				for (let i = 0; i < result.length; i++) {
					getFilterId(result[i].filter_id)
					.then(async function(result2) {
						
						await updateDisabled(result2)
						.catch(function(error) {
							return res.status(500).json(error);
						});
					})
					.catch(function(error) {
						return res.status(500).json(error);
					});
				}
				mysql.query(`
					SELECT * FROM members
				`, function(error, result) {
					if (error) {
						res.status(500).json(error.sqlMessage);
					} else {
						res.status(200).json(result);
					}
				});
			}
		});
	}

	static post(req, res) {
		// createUserMikrotik({
		// 	name: "serre",
		// 	password: "dggfd",
		// })
		// .then(result => {
		// 	console.log(result.data)
		// })
		// .catch(error => {
		// 	console.log(error.response.data)
		// })
		
		const { dataPribadi, dataLayanan } = req.body;
		const modem = req.files.modem ? req.files.modem[0].buffer : null;
		const rumah = req.files.rumah ? req.files.rumah[0].buffer : null;
		const redaman = req.files.redaman ? req.files.redaman[0].buffer : null;
		const ktp = req.files.ktp ? req.files.ktp[0].buffer : null;

		const dataPribadi2 = JSON.parse(dataPribadi);
		const dataLayanan2 = JSON.parse(dataLayanan);

		if (!dataPribadi2) return res.status(500).json("Data pribadi tidak boleh kosong");
		if (!dataPribadi2.nama) return res.status(500).json("Nama tidak boleh kosong");
		if (!dataPribadi2.hp) return res.status(500).json("Hp tidak boleh kosong");
		if (!dataPribadi2.alamat) return res.status(500).json("Alamat tidak boleh kosong");

		if (!dataLayanan2) return res.status(500).json("Data layanan tidak boleh kosong");
		if (!dataLayanan2.tgl_langganan) return res.status(500).json("Tanggal aktivasi tidak boleh kosong");
		if (!dataLayanan2.id_paket) return res.status(500).json("Paket tidak boleh kosong");
		if (!dataLayanan2.odp) return res.status(500).json("No ODP tidak boleh kosong");
		if (!dataLayanan2.koordinat) return res.status(500).json("Koordinat tidak boleh kosong");
		if (!dataLayanan2.tipe_modem) return res.status(500).json("Tipe modem tidak boleh kosong");
		if (!dataLayanan2.sn_modem) return res.status(500).json("S/N modem tidak boleh kosong");
		if (!dataLayanan2.tarif) return res.status(500).json("Tarif tidak boleh kosong");

		if (!modem) return res.status(500).json("Foto modem tidak boleh kosong");
		if (!rumah) return res.status(500).json("Foto rumah tidak boleh kosong");
		if (!redaman) return res.status(500).json("Foto redaman tidak boleh kosong");
		if (!ktp) return res.status(500).json("Foto ktp tidak boleh kosong");

		if (req.files.modem[0].size > 500000) return res.status(500).json("Foto modem tidak boleh lebih dari 500KB");
		if (req.files.rumah[0].size > 500000) return res.status(500).json("Foto rumah tidak boleh lebih dari 500KB");
		if (req.files.redaman[0].size > 500000) return res.status(500).json("Foto redaman tidak boleh lebih dari 500KB");
		if (req.files.ktp[0].size > 500000) return res.status(500).json("Foto ktp tidak boleh lebih dari 500KB");

		const modem_type = req.files.modem[0].mimetype;
		const rumah_type = req.files.rumah[0].mimetype;
		const redaman_type = req.files.redaman[0].mimetype;
		const ktp_type = req.files.ktp[0].mimetype;

		mysql.beginTransaction(error => {
			if (error) return res.status(500).json(error.sqlMessage);
	
			const query = `
				INSERT INTO member (nama, hp, alamat)
				VALUES ('${dataPribadi2.nama}', '${dataPribadi2.hp}', '${dataPribadi2.alamat}')
			`;
			mysql.query(query, function(error, result) {
				if (error) {
					res.status(500).json(error.sqlMessage);
				} else {
					const id_member = result.insertId;

					const query = `
						INSERT INTO member_transaksi (
							id_member,
							id_paket,
							nama_paket,
							tgl_langganan,
							odp,
							koordinat,
							tipe_modem,
							sn_modem,
							tarif
						) VALUES (
							'${id_member}',
							'${dataLayanan2.id_paket}',
							'${dataLayanan2.nama_paket}',
							'${dataLayanan2.tgl_langganan}',
							'${dataLayanan2.odp}',
							'${dataLayanan2.koordinat}',
							'${dataLayanan2.tipe_modem}',
							'${dataLayanan2.sn_modem}',
							'${dataLayanan2.tarif}'
						)
					`;
					mysql.query(query, function(error, result) {
						if (error) {
							mysql.rollback(errorRollback => {
								if (errorRollback) {
									throw errorRollback;
								} else {
									res.status(500).json(error.sqlMessage);
								};
							});
						} else {
							const id_membertransaksi = result.insertId;

							const query = `
								INSERT INTO member_transaksi_dokumen (
									id_membertransaksi,
									modem,
									modem_type,
									rumah,
									rumah_type,
									redaman,
									redaman_type,
									ktp,
									ktp_type
								)
								VALUES (?,?,?,?,?,?,?,?,?)
							`;
							mysql.query(query, [
								id_membertransaksi,
								modem,
								modem_type,
								rumah,
								rumah_type,
								redaman,
								redaman_type,
								ktp,
								ktp_type,
							], function(error, result) {
								if (error) {
									mysql.rollback(errorRollback => {
										if (errorRollback) {
											throw errorRollback;
										} else {
											res.status(500).json(error.sqlMessage);
										};
									});
								} else {
									
									const query = `
										SELECT
											nomor,
											DATE_FORMAT(tgl_langganan, '%d%m%Y') AS tgl_langganan2
										FROM member_transaksi WHERE id = ${id_membertransaksi}
									`;
									mysql.query(query, function(error, result) {
										if (error) {
											mysql.rollback(errorRollback => {
												if (errorRollback) {
													throw errorRollback;
												} else {
													res.status(500).json(error.sqlMessage);
												};
											});
										} else {
											const { nomor, tgl_langganan2 } = result[0];

											// mysql.commit(errorCommit => {
											// 	if (errorCommit) {
											// 		throw errorCommit;
											// 	} else {
											// 		res.status(201).json("Ok");
											// 	};
											// });

											const name = `${dataPribadi2.nama.toLowerCase().replace(/ /g, '.')}.${`${nomor}${tgl_langganan2}`}`;
											const password = randomstring.generate({
												length: 6,
												charset: 'alphabetic',
												capitalization: 'uppercase'
											});
											createUserMikrotik({
												name,
												password,
												// disabled: "yes",
											})
											.then(result => {
												const id_mikrotik = result.data['.id'];

												putUserManagerUserProfile({
													"profile": dataLayanan2.id_paket,
													"user": name
												})
												.then(result => {

													mysql.commit(errorCommit => {
														if (errorCommit) {
															throw errorCommit;
														} else {
															
															const query = `
																UPDATE member_transaksi SET
																	id_mikrotik = "${id_mikrotik}",
																	kode = "${name}",
																	password = "${password}"
																WHERE id = ${id_membertransaksi}
															`;
															mysql.query(query);
															res.status(201).json("Ok");
														};
													});
												})
												.catch(error => {
													mysql.rollback(errorRollback => {
														if (errorRollback) {
															throw errorRollback;
														} else {
															res.status(500).json(error.response.data.message);
														};
													});
												});
											})
											.catch(error => {
												mysql.rollback(errorRollback => {
													if (errorRollback) {
														throw errorRollback;
													} else {
														res.status(500).json(error.response.data.message);
													};
												});
											});
										};
									});
								};
							});
						};
					});
				};
			});
		});
	}

	static postTransaksi(req, res) {
		const { dataLayanan } = req.body;
		const modem = req.files.modem ? req.files.modem[0].buffer : null;
		const rumah = req.files.rumah ? req.files.rumah[0].buffer : null;
		const redaman = req.files.redaman ? req.files.redaman[0].buffer : null;
		const ktp = req.files.ktp ? req.files.ktp[0].buffer : null;

		const dataLayanan2 = JSON.parse(dataLayanan);
		const id_member = dataLayanan2.id_member;

		if (!dataLayanan2) return res.status(500).json("Data layanan tidak boleh kosong");
		if (!dataLayanan2.tgl_langganan) return res.status(500).json("Tanggal aktivasi tidak boleh kosong");
		if (!dataLayanan2.id_paket) return res.status(500).json("Paket tidak boleh kosong");
		if (!dataLayanan2.odp) return res.status(500).json("No ODP tidak boleh kosong");
		if (!dataLayanan2.koordinat) return res.status(500).json("Koordinat tidak boleh kosong");
		if (!dataLayanan2.tipe_modem) return res.status(500).json("Tipe modem tidak boleh kosong");
		if (!dataLayanan2.sn_modem) return res.status(500).json("S/N modem tidak boleh kosong");
		if (!dataLayanan2.tarif) return res.status(500).json("Tarif tidak boleh kosong");

		if (!modem) return res.status(500).json("Foto modem tidak boleh kosong");
		if (!rumah) return res.status(500).json("Foto rumah tidak boleh kosong");
		if (!redaman) return res.status(500).json("Foto redaman tidak boleh kosong");
		if (!ktp) return res.status(500).json("Foto ktp tidak boleh kosong");

		if (req.files.modem[0].size > 500000) return res.status(500).json("Foto modem tidak boleh lebih dari 500KB");
		if (req.files.rumah[0].size > 500000) return res.status(500).json("Foto rumah tidak boleh lebih dari 500KB");
		if (req.files.redaman[0].size > 500000) return res.status(500).json("Foto redaman tidak boleh lebih dari 500KB");
		if (req.files.ktp[0].size > 500000) return res.status(500).json("Foto ktp tidak boleh lebih dari 500KB");

		const modem_type = req.files.modem[0].mimetype;
		const rumah_type = req.files.rumah[0].mimetype;
		const redaman_type = req.files.redaman[0].mimetype;
		const ktp_type = req.files.ktp[0].mimetype;

		mysql.beginTransaction(error => {
			if (error) return res.status(500).json(error.sqlMessage);
	
			const query = `
				INSERT INTO member_transaksi (
					id_member,
					id_paket,
					nama_paket,
					tgl_langganan,
					odp,
					koordinat,
					tipe_modem,
					sn_modem,
					tarif,
					nomor
				) VALUES (
					'${id_member}',
					'${dataLayanan2.id_paket}',
					'${dataLayanan2.nama_paket}',
					'${dataLayanan2.tgl_langganan}',
					'${dataLayanan2.odp}',
					'${dataLayanan2.koordinat}',
					'${dataLayanan2.tipe_modem}',
					'${dataLayanan2.sn_modem}',
					'${dataLayanan2.tarif}',
					'${dataLayanan2.nomor}'
				)
			`;
			mysql.query(query, function(error, result) {
				if (error) {
					res.status(500).json(error.sqlMessage);
				} else {
					const id_membertransaksi = result.insertId;

					const query = `
						INSERT INTO member_transaksi_dokumen (
							id_membertransaksi,
							modem,
							modem_type,
							rumah,
							rumah_type,
							redaman,
							redaman_type,
							ktp,
							ktp_type
						)
						VALUES (?,?,?,?,?,?,?,?,?)
					`;
					mysql.query(query, [
						id_membertransaksi,
						modem,
						modem_type,
						rumah,
						rumah_type,
						redaman,
						redaman_type,
						ktp,
						ktp_type,
					], function(error, result) {
						if (error) {
							mysql.rollback(errorRollback => {
								if (errorRollback) {
									throw errorRollback;
								} else {
									res.status(500).json(error.sqlMessage);
								};
							});
						} else {
							
							const query = `
								SELECT
									nomor,
									DATE_FORMAT(tgl_langganan, '%d%m%Y') AS tgl_langganan2
								FROM member_transaksi WHERE id = ${id_membertransaksi}
							`;
							mysql.query(query, function(error, result) {
								if (error) {
									mysql.rollback(errorRollback => {
										if (errorRollback) {
											throw errorRollback;
										} else {
											res.status(500).json(error.sqlMessage);
										};
									});
								} else {
									const { nomor, tgl_langganan2 } = result[0];

									// mysql.commit(errorCommit => {
									// 	if (errorCommit) {
									// 		throw errorCommit;
									// 	} else {
											
									// 		res.status(201).json("Ok");
									// 	};
									// });

									const name = `${dataPribadi2.nama.toLowerCase().replace(/ /g, '.')}.${`${nomor}${tgl_langganan2}`}`;
									const password = randomstring.generate({
										length: 6,
										charset: 'alphabetic',
										capitalization: 'uppercase'
									});
									createUserMikrotik({
										name,
										password,
										disabled: "yes",
									})
									.then(result => {
										const id_mikrotik = result.data['.id'];

										putUserManagerUserProfile({
											"profile": dataLayanan2.id_paket,
											"user": name
										})
										.then(result => {

											mysql.commit(errorCommit => {
												if (errorCommit) {
													throw errorCommit;
												} else {
													
													const query = `
														UPDATE member_transaksi SET
															id_mikrotik = "${id_mikrotik}"
														WHERE id = ${id_membertransaksi}
													`;
													mysql.query(query);
													res.status(201).json("Ok");
												};
											});
										})
										.catch(error => {
											mysql.rollback(errorRollback => {
												if (errorRollback) {
													throw errorRollback;
												} else {
													res.status(500).json(error.response.data.message);
												};
											});
										});
									})
									.catch(error => {
										mysql.rollback(errorRollback => {
											if (errorRollback) {
												throw errorRollback;
											} else {
												res.status(500).json(error.response.data.message);
											};
										});
									});
								};
							});
						};
					});
				};
			});
		});
	}

	static postDaftar(req, res) {
		const { dataPribadi } = req.body;
		
		if (dataPribadi && dataPribadi.nomor === "") return res.status(500).json("No. Pelanggan Tidak Boleh Kosong");
		if (dataPribadi && dataPribadi.nama === "") return res.status(500).json("Nama Tidak Boleh Kosong");
		if (dataPribadi && dataPribadi.hp === "") return res.status(500).json("No. Handphone Tidak Boleh Kosong");
		if (dataPribadi && !dataPribadi.tgl_lahir) return res.status(500).json("Tgl. Lahir Tidak Boleh Kosong");
		
		// const getCurrentTimestamp = () => {
		// 	return "" + Math.round(new Date().getTime() / 1000);
		// };
		// const mt_order_id = "M-" + getCurrentTimestamp();

		mysql.beginTransaction(error => {
			if (error) return res.status(500).json(error.sqlMessage);
	
			const query = `
				INSERT INTO customers (hp, email, nomor, nik, nama, jkel, alamat, kabupaten, kecamatan)
				VALUES ('${dataPribadi.hp}', '${dataPribadi.email}', '${dataPribadi.nomor}', '${dataPribadi.nik}', '${dataPribadi.nama}', '${dataPribadi.jkel}', '${dataPribadi.alamat}', '${dataPribadi.kabupaten}', '${dataPribadi.kecamatan}')
			`;
			mysql.query(query, function(error, result) {
				if (error) return res.status(500).json(error.sqlMessage);
		
				const customer_id = result.insertId;
				query = `
					INSERT INTO customer_members (customer_id)
					VALUES ('${customer_id}')
				`;
				mysql.query(query, function(error, result) {
					if (error) {
						mysql.rollback(function(error) {
							if (error) {
								return mysql.rollback(function() {
									throw error;
								});
							};
							return res.status(500).json(error.sqlMessage);
						});
					};
		
					query = `
						INSERT INTO transactions (price, role_id, hp)
						VALUES ('${price}', 2, '${dataPribadi.hp}')
					`;
					mysql.query(query, function(error, result) {
						if (error) {
							mysql.rollback(function(error) {
								if (error) {
									return mysql.rollback(function() {
										throw error;
									});
								};
								return res.status(500).json(error.sqlMessage);
							});
						};
			
						createUserMikrotik({
							name: dataPribadi.nomor,
							password: null,
							group: null,
						})
						.then(result => {

							mysql.commit(function(error) {
								if (error) {
									return mysql.rollback(function() {
										throw err;
									});
								};
			
								return res.status(201).json(result[0]);
							});
						})
						.catch(error => {
							mysql.rollback(function(error) {
								if (error) {
									return mysql.rollback(function() {
										throw error;
									});
								};
								return res.status(500).json(error.sqlMessage);
							});
						});
					});
				});
			});
		});
	}

	static postBayar(req, res) {
		MemberTransactions.create(mysql, req.body)
		.then(function(result) {
			res.status(201).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error);
		});
	}

	static postProfile(req, res) {
		const { member_profile_name, rate_limit, price } = req.body;
		
		api.connect()
		.then(function(conn) {
			
			const chan = conn.openChannel();
			chan.write('/ppp/profile/add', {
				'local-address': `${process.env.LOCAL_ADDRESS}`,
				'only-one': 'no',
				'remote-address': 'pool-pppoe',
				'use-encryption': 'yes',

				'name':`${member_profile_name}`,
				'rate-limit':`${rate_limit}`,
			})
			.then(function(result3) {
				const member_profile_id = result3.data[0];

				mysql.query(`
					INSERT INTO member_profiles (id, name, rate_limit, price)
					VALUES ('${member_profile_id}', '${member_profile_name}', '${rate_limit}', '${price}')
				`, function(error, result) {
					if (error) {
						chan.write('/ppp/profile/remove', {
							'.id': `${member_profile_id}`
						})
						.then(function(result) {
							res.status(500).json(error);
						})
						// .catch(function(error) {
						// 	console.log('error==========', error);
						// });
					} else {
						res.status(201).json({ ...req.body, member_profile_id: member_profile_id });
					}
				});
			})
			.catch(function(error) {
				res.status(500).json(error);
			});
		});
	}

	static getTransaksiDokumenModemIdMembertransaksi(req, res) {
		const { id_membertransaksi } = req.params ;
		
		const query = `
			SELECT
				a.id_membertransaksi,
				a.modem,
				a.modem_type
			FROM member_transaksi_dokumen 	a
			WHERE a.id_membertransaksi = ${id_membertransaksi}
		`;
		mysql.query(query, function(error, result) {
			if (error) {
				res.status(500).json(error.sqlMessage);
			} else {
				if (result.length === 1) {
					const image = result[0];

					res.writeHead(200, {
						'Content-Type': image.modem_type,
						'Content-Disposition': `inline; filename="${image.id_membertransaksi}"`,
					});
					res.end(image.modem);
				} else {
					res.status(500).json("Data tidak ditemukan");
				};
			};
		});
	}

	static getTransaksiDokumenRumahIdMembertransaksi(req, res) {
		const { id_membertransaksi } = req.params ;
		
		const query = `
			SELECT
				a.id_membertransaksi,
				a.rumah,
				a.rumah_type
			FROM member_transaksi_dokumen 	a
			WHERE a.id_membertransaksi = ${id_membertransaksi}
		`;
		mysql.query(query, function(error, result) {
			if (error) {
				res.status(500).json(error.sqlMessage);
			} else {
				if (result.length === 1) {
					const image = result[0];

					res.writeHead(200, {
						'Content-Type': image.rumah_type,
						'Content-Disposition': `inline; filename="${image.id_membertransaksi}"`,
					});
					res.end(image.rumah);
				} else {
					res.status(500).json("Data tidak ditemukan");
				};
			};
		});
	}

	static getTransaksiDokumenRedamanIdMembertransaksi(req, res) {
		const { id_membertransaksi } = req.params ;
		
		const query = `
			SELECT
				a.id_membertransaksi,
				a.redaman,
				a.redaman_type
			FROM member_transaksi_dokumen 	a
			WHERE a.id_membertransaksi = ${id_membertransaksi}
		`;
		mysql.query(query, function(error, result) {
			if (error) {
				res.status(500).json(error.sqlMessage);
			} else {
				if (result.length === 1) {
					const image = result[0];

					res.writeHead(200, {
						'Content-Type': image.redaman_type,
						'Content-Disposition': `inline; filename="${image.id_membertransaksi}"`,
					});
					res.end(image.redaman);
				} else {
					res.status(500).json("Data tidak ditemukan");
				};
			};
		});
	}

	static getTransaksiDokumenKtpIdMembertransaksi(req, res) {
		const { id_membertransaksi } = req.params ;
		
		const query = `
			SELECT
				a.id_membertransaksi,
				a.ktp,
				a.ktp_type
			FROM member_transaksi_dokumen 	a
			WHERE a.id_membertransaksi = ${id_membertransaksi}
		`;
		mysql.query(query, function(error, result) {
			if (error) {
				res.status(500).json(error.sqlMessage);
			} else {
				if (result.length === 1) {
					const image = result[0];

					res.writeHead(200, {
						'Content-Type': image.ktp_type,
						'Content-Disposition': `inline; filename="${image.id_membertransaksi}"`,
					});
					res.end(image.ktp);
				} else {
					res.status(500).json("Data tidak ditemukan");
				};
			};
		});
	}

	static getTransaksiIdMembertransaksi(req, res) {
		const { id_membertransaksi } = req.params ;
		
		const query = `
			SELECT
				a.id AS id_membertransaksi,
				a.nomor,
				DATE_FORMAT(a.tgl_langganan, '%Y-%m-%d') AS tgl_langganan,
				DATE_FORMAT(a.tgl_langganan, '%d%m%Y') AS tgl_langganan2,
				a.odp,
				a.koordinat,
				a.tipe_modem,
				a.sn_modem,
				a.tarif,
				a.id_mikrotik,
				a.kode,
				a.id_paket,
				a.alamat AS alamat_modem,
				a.status,

				b.nama AS nama_member,
				b.alamat AS alamat_member,
				b.hp,

				c.nama AS nama_paket
			FROM member_transaksi 	a
			LEFT JOIN member		b ON a.id_member = b.id
			LEFT JOIN paket			c ON a.id_paket = c.id
			WHERE a.id = ${id_membertransaksi}
		`;
		mysql.query(query, function(error, result) {
			if (error) {
				res.status(500).json(error.sqlMessage);
			} else {
				if (result.length === 1) {
					res.status(200).json(result[0]);
				} else {
					res.status(500).json("Data tidak ditemukan");
				};
			};
		});
	}

	static patchIdMembertransaksi(req, res) {
		const { id_membertransaksi } = req.params ;
		const {
			id_paket,
			nama_paket,
			tarif,
			koordinat,
			alamat_modem,
			odp,
			tipe_modem,
			sn_modem,

			id_mikrotik,
			kode,
		} = req.body;
		
		const setData = [];

		if (id_paket) setData.push(`id_paket = "${id_paket}"`);
		if (nama_paket) setData.push(`nama_paket = "${nama_paket}"`);
		if (tarif) setData.push(`tarif = "${tarif}"`);
		if (koordinat) setData.push(`koordinat = ?`);
		if (alamat_modem) setData.push(`alamat = "${alamat_modem}"`);
		if (odp) setData.push(`odp = "${odp}"`);
		if (tipe_modem) setData.push(`tipe_modem = "${tipe_modem}"`);
		if (sn_modem) setData.push(`sn_modem = "${sn_modem}"`);

		mysql.beginTransaction(error => {
			if (error) return res.status(500).json(error.sqlMessage);

			const query = `
				UPDATE member_transaksi SET
					${setData.join(', ')}
				WHERE id = '${id_membertransaksi}'
			`;
			mysql.query(query, [koordinat], function(error, result) {
				if (error) {
					res.status(500).json(error.sqlMessage);
				} else {
					
					putUserManagerUserProfile({
						"profile": id_paket,
						"user": kode,
					})
					.then(result => {
	
						mysql.commit(errorCommit => {
							if (errorCommit) {
								throw errorCommit;
							} else {
								
								res.status(201).json("Updated");
							};
						});
					})
					.catch(error => {
						mysql.rollback(errorRollback => {
							if (errorRollback) {
								throw errorRollback;
							} else {
								res.status(500).json(error.response.data.message);
							};
						});
					});
				};
			});
		});
	}

	static postDetailBayar(req, res) {
		const { id_mikrotik, metode, id_membertransaksidetail } = req.body;
		const { user_id } = req.user;

		mysql.beginTransaction(error => {
			if (error) return res.status(500).json(error.sqlMessage);

			const query = `
				UPDATE member_transaksi_detail SET
					id_users = ${user_id},
					metode = "${metode}",
					tgl_bayar = NOW()
				WHERE id = ${id_membertransaksidetail}
			`;
			mysql.query(query, function(error, result) {
				if (error) {
					res.status(500).json(error.sqlMessage);
				} else {
					
					patchIdMikrotik(id_mikrotik, {
						disabled: "no",
					})
					.then(result => {
	
						mysql.commit(errorCommit => {
							if (errorCommit) {
								throw errorCommit;
							} else {
								
								res.status(201).json("Ok");
							};
						});
					})
					.catch(error => {
						mysql.rollback(errorRollback => {
							if (errorRollback) {
								throw errorRollback;
							} else {
								res.status(500).json(error.response.data.message);
							};
						});
					});
				};
			});
		});
	}

	static postDetail(req, res) {
		const {
			id_membertransaksi,
			id_paket,
			nama_paket,
			tgl_aktivasi,
			tgl_selesai,
			bulan,
			tahun,
			tarif_awal,
			tarif,
			tgl_bayar,
		} = req.body;
		const { user_id } = req.user;

		const query = `
			INSERT INTO member_transaksi_detail (
				id_membertransaksi,
				id_paket,
				nama_paket,
				tgl_aktivasi,
				tgl_selesai,
				bulan,
				tahun,
				tarif_awal,
				tarif,
				tgl_bayar,
				id_users
			) VALUES (
				"${id_membertransaksi}",
				"${id_paket}",
				"${nama_paket}",
				"${tgl_aktivasi}",
				"${tgl_selesai}",
				"${bulan}",
				"${tahun}",
				"${tarif_awal}",
				"${tarif}",
				"${tgl_bayar}",
				"${user_id}"
			)
		`;
		mysql.query(query, function(error, result) {
			if (error) {
				res.status(500).json(error.sqlMessage);
			} else {
				res.status(201).json("Ok");
			};
		});
	}

	static getTransaksiDetail(req, res) {
		const { page, limit, bulan, tahun, id_membertransaksi } = req.query;
		const offset = (parseInt(page) - 1) * limit;
		const where = [];

		if (bulan) where.push(`a.bulan = ${bulan}`); 
		if (tahun) where.push(`a.tahun = ${tahun}`); 
		if (id_membertransaksi) where.push(`a.id_membertransaksi = ${id_membertransaksi}`); 
		
		const query = `
			SELECT
				a.id AS id_membertransaksidetail,
				a.tarif_awal,
				a.tarif,
				DATE_FORMAT(a.tgl_aktivasi, '%Y-%m-%d') AS tgl_aktivasi,
				DATE_FORMAT(a.tgl_selesai, '%Y-%m-%d') AS tgl_selesai,
				DATE_FORMAT(a.tgl_bayar, '%Y-%m-%d %T') AS tgl_bayar,
				a.id_users,
				CAST(a.bulan AS INT) AS bulan,
				a.tahun,
				a.is_baru,
				a.id_paket,
				
				b.nomor,
				DATE_FORMAT(b.tgl_langganan, '%Y-%m-%d') AS tgl_langganan,
				DATE_FORMAT(b.tgl_langganan, '%d%m%Y') AS tgl_langganan2,
				b.odp,
				b.id_mikrotik,
				b.kode,

				c.nama AS nama_member,

				d.nama AS nama_paket
			FROM member_transaksi_detail 	a
			LEFT JOIN member_transaksi		b ON a.id_membertransaksi = b.id
			LEFT JOIN member				c ON b.id_member = c.id
			LEFT JOIN paket					d ON a.id_paket = d.id
			${
				where.length > 0 ? `WHERE ${where.join(' && ')}` : ''
			}
			ORDER BY a.id DESC
			LIMIT ${limit} OFFSET ${offset}
		`;
		mysql.query(query, function(error, result) {
			if (error) {
				res.status(500).json(error.sqlMessage);
			} else {
				res.status(200).json({
					page,
					limit,
					datas: result,
				});
			};
		});
	}

	static postNonaktif(req, res) {
		const { id_mikrotik, id_membertransaksi } = req.body;

		mysql.beginTransaction(error => {
			if (error) return res.status(500).json(error.sqlMessage);

			const query = `
				UPDATE member_transaksi SET
					status = "2"
				WHERE id = ${id_membertransaksi}
			`;
			mysql.query(query, function(error, result) {
				if (error) {
					res.status(500).json(error.sqlMessage);
				} else {
					
					patchIdMikrotik(id_mikrotik, {
						disabled: "yes",
					})
					.then(result => {
	
						mysql.commit(errorCommit => {
							if (errorCommit) {
								throw errorCommit;
							} else {
								
								res.status(201).json("Ok");
							};
						});
					})
					.catch(error => {
						mysql.rollback(errorRollback => {
							if (errorRollback) {
								throw errorRollback;
							} else {
								res.status(500).json(error.response.data.message);
							};
						});
					});
				};
			});
		});
	}

	static postAktif(req, res) {
		const { id_mikrotik, id_membertransaksi } = req.body;

		mysql.beginTransaction(error => {
			if (error) return res.status(500).json(error.sqlMessage);

			const query = `
				UPDATE member_transaksi SET
					status = NULL
				WHERE id = ${id_membertransaksi}
			`;
			mysql.query(query, function(error, result) {
				if (error) {
					res.status(500).json(error.sqlMessage);
				} else {
					
					patchIdMikrotik(id_mikrotik, {
						disabled: "no",
					})
					.then(result => {
	
						mysql.commit(errorCommit => {
							if (errorCommit) {
								throw errorCommit;
							} else {
								
								res.status(201).json("Ok");
							};
						});
					})
					.catch(error => {
						mysql.rollback(errorRollback => {
							if (errorRollback) {
								throw errorRollback;
							} else {
								res.status(500).json(error.response.data.message);
							};
						});
					});
				};
			});
		});
	}

	static getTransaksi(req, res) {
		const { page, limit, id_member } = req.query;
		const offset = (parseInt(page) - 1) * limit;
		const where = [];

		if (id_member) where.push(`a.id_member = ${id_member}`); 
		
		const query = `
			SELECT
				a.id AS id_membertransaksi,
				a.nomor,
				DATE_FORMAT(a.tgl_langganan, '%Y-%m-%d') AS tgl_langganan,
				DATE_FORMAT(a.tgl_langganan, '%d%m%Y') AS tgl_langganan2,
				a.odp,
				a.tarif,
				a.id_mikrotik,
				a.kode,
				a.status,
				a.password,

				b.nama AS nama_member,

				c.nama AS nama_paket
			FROM member_transaksi 	a
			LEFT JOIN member		b ON a.id_member = b.id
			LEFT JOIN paket			c ON a.id_paket = c.id
			${
				where.length > 0 ? `WHERE ${where.join(' && ')}` : ''
			}
			ORDER BY a.id DESC
			LIMIT ${limit} OFFSET ${offset}
		`;
		mysql.query(query, function(error, result) {
			if (error) {
				res.status(500).json(error.sqlMessage);
			} else {
				res.status(200).json({
					page,
					limit,
					datas: result,
				});
			};
		});
	}

	static deleteMemberId(req, res) {
		const { member_id } = req.params;
		
		api.connect()
		.then(function(conn) {
			
			const chan = conn.openChannel();
			// const password = randomstring.generate({
			// 	length: 6,
			// 	charset: 'numeric',
			// 	capitalization: 'uppercase'
			// });
			// chan.write('/ip/firewall/filter/getall')
			chan.write('/ppp/secret/remove', {
				numbers: `${member_id}`
			})
			.then(function(result3) {
				// console.log('masuk======', JSON.stringify(result3))
				Member.remove(mysql, { member_id: member_id })
				.then(function(result) {
					res.status(201).json(result);
				})
				.catch(function(error) {
					res.status(500).json(error);
				});
			})
			.catch(function(error) {
				res.status(500).json(error);
			});
		});
	}

	static patchMemberId(req, res) {
		const { member_id } = req.params;
		const { hp, alamat } = req.body;
		
		const query = `
			UPDATE member SET
				hp = '${hp}',
				alamat = "${alamat}"
			WHERE id = ${member_id}
		`;
		mysql.query(query, function(error, result) {
			if (error) {
				res.status(500).json(error.sqlMessage);
			} else {
				res.status(201).json("Updated");
			};
		});
	}

};

module.exports = MembersController;
const axios = require('axios');
const Customer = require("../models/customers");
const mysql = require('../helpers/mysql');
const { hashingPassword, comparePassword } = require('../helpers/bcrypt');
const token = require('../helpers/jsonwebtoken');

const sendWA = (receiver, text) => {
    return new Promise((resolve, reject) => {
        axios.post(`${process.env.WHATSAPP_URL}/chats/send/`, {
            receiver,
            message: {
                text,
            },
            token: process.env.WHATSAPP_TOKEN,
        })
        .then(result => {
            resolve(result);
        })
        .catch(error => {
            // console.log("sendMessage=========", error.response.data)
            reject(error);
        });
    });
};

class CustomerController {

	static getGantipasswordCektokenToken(req, res) {
		const { token: token2 } = req.params;
		token.verifyToken(token2, function(error, decoded) {
			if (error) {
				res.status(500).json("Token Tidak Sesuai");
			} else {
				res.status(201).json(decoded);
			};
		});
	}

	static postRegister(req, res) {
		Customer.insert({
			...req.body,
			password: hashingPassword(req.body.password),
		})
		.then(result => {
			res.status(200).json(result);
		})
		.catch(error => {
			const errorKu = error.substring(error.length - 11);
			if (errorKu === "'hp_UNIQUE'") {
				res.status(500).json(`Nomor HP ${req.body.hp} Sudah Pernah Terdaftar, Silahkan Reset Password Jika Lupa Password.`);
			} else {
				res.status(500).json(error);
			}
		});
	}

	static postLogin(req, res) {
		const { hp, password } = req.body;

		if (!hp) return res.status(500).json("No. Handphone Tidak Boleh Kosong.");
		
		Customer.findByHp(hp)
		.then(result => {
			const customer = result;

			if (customer.password) {
				const compare = comparePassword(password, customer.password);
		
				if (compare) {
					delete customer.password;
					token.createToken({ ...customer }, function(error, token) {
						if (error) {
							res.status(500).json(error);
						} else {
							res.status(201).json(token);
						};
					});
				} else {
					res.status(500).json("Password Salah.");
				};
			} else {
				res.status(500).json("Password Masih Kosong, Silahkan Update Password Dengan Mengklik Lupa Password");
			};
		})
		.catch(error => {
			return res.status(500).json(error);
		});
	}

	static postCektoken(req, res) {
		token.verifyToken(req.headers.token, function(error, decoded) {
			if (error) {
				res.status(500).json(error);
			} else {
				res.status(200).json(decoded);
			};
		});
	}

	static async postResetpassword(req, res) {
		const { hp } = req.body;

		let customer;
		if (!hp) return res.status(500).json("No. Handphone Tidak Boleh Kosong.");
		await Customer.findByHp(hp)
		.then(result => {
			customer = result;
		})
		.catch(error => {
			return res.status(500).json(error);
		});

		const key_time = Date.now();
		mysql.beginTransaction(error => {
			if (error) return res.status(500).json(error.sqlMessage);
	
			const query = `
				INSERT INTO customers_log_password (customer_id, key_time)
                VALUES ('${customer.id}', '${key_time}')
			`;
			// console.log(query)
			mysql.query(query, function(error, result) {
				if (error) {
					// console.log("updateTransaction=======", error)
					return res.status(500).json(error.sqlMessage);
				};

				token.createToken({ hp: customer.hp, key_time }, function(error, token2) {
					if (error) {
						mysql.rollback(function(error) {
							if (error) {
								return mysql.rollback(function() {
									throw error;
								});
							};
							return res.status(500).json(error);
						});
					} else {
						
						sendWA(`62${hp.substring(1)}@s.whatsapp.net`, `Silahkan Ganti Password Anda Dengan Mengklik Tautan Di Bawah Ini:
${process.env.WEB_CUSTOMER_DASHBOARD_URL + '/#/gantipassword/' + token2}`,)
						.then((result) => {
							
							mysql.commit(function(error) {
								if (error) {
									return mysql.rollback(function() {
										throw err;
									});
								} else {
									return res.status(201).json("Berhasil.");
								};
							});
						})
						.catch((error) => {
							console.log("guguk======", error)
							mysql.rollback(function(error2) {
								if (error2) {
									return mysql.rollback(function() {
										throw error2;
									});
								} else {
									return res.status(500).json(error);
								};
							});
						});
					};
				});
				
			});
		});
	}

	static async postGantipassword(req, res) {
		const { hp, password_baru } = req.body;

		Customer.updatePassword({
			hp,
			password: hashingPassword(password_baru),
		})
		.then(result => {
			res.status(201).json("Ok");
		})
		.catch(error => {
			return res.status(500).json(error);
		});
	}

};

module.exports = CustomerController;

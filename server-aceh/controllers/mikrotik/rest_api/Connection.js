const axios = require('axios');

const Connection = axios.create({
	baseURL: process.env.MIKROTIK_URL,
    auth: {
        username: process.env.MIKROTIK_USERNAME,
        password: process.env.MIKROTIK_PASSWORD
    },
});

// api.interceptors.request.use(
//    (config) => {
//       const token = localStorage.getItem('token');
//       if (token) {
//          config.headers['token'] = `${token}`;
//       };
//       return config;
//    },
// );

module.exports = Connection;

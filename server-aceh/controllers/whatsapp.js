const { getSessions, createSession } = require('../baileys/index');

class WhatsappController {

	static getSessions(req, res) {
		res.status(200).json(getSessions());
	}

	static postSessionsAdd(req, res) {
		createSession(req.body.sessionId, res, req);
	}

	static postLogin(req, res) {
		// Pemberitahuan.find()
		// .then(function(result) {
		// 	res.status(200).json(result);
		// })
		// .catch(function(error) {
		// 	res.status(500).json(error.message);
		// });
	}

};

module.exports = WhatsappController;

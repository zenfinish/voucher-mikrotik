const Transaction = require('../models/transaction.js');
const MemberTransactions = require('../models/member_transactions');
const mysql = require('../helpers/mysql.js');

class TransactionController {

	static get(req, res) {
		const { hp, limit } = req.query;

		const whereData = [];
		if (hp) whereData.push(`a.hp = '${hp}'`);

		const query = `
			SELECT
				a.*,
				DATE_FORMAT(a.tgl, '%d %M %Y, %k:%i:%s') AS tgl_indo,
				b.name AS profile_name,
				b.kode
			FROM transactions a
			LEFT JOIN profile b ON a.profile_id = b.id
			${
				whereData.length === 0 ? '' :
				`WHERE ${whereData.join(' && ')}`
			}
			${
				limit && limit === 'all' ? '' :
				limit && limit !== 'all' ? `LIMIT ${limit}` :
				`LIMIT 100`
			}
			ORDER BY a.tgl DESC
		`;
		// console.log(query)
		mysql.query(query, function(error, result) {
			if (error) return res.status(500).json(error.sqlMessage);

			res.status(200).json(result);
		});
	}

	static getTransaction(req, res) {
		Transaction.find(req.body)
		.then(function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error.message);
		})
	}

	static getPerUser(req, res) {
		Transaction.getPerUser(req.user.id)
		.then(function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error.message);
		});
	}

	static getPerId(req, res) {
		Transaction.findById(req.params.transaction_id)
		.then(function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error.message);
		});
	}

	static getTotal(req, res) {
		Transaction.getTotal()
		.then(function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error.message);
		})
	}

	static getHarian(req, res) {
		Transaction.getHarian()
		.then(function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error.message);
		})
	}

	static getBulanan(req, res) {
		Transaction.getBulanan()
		.then(function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error.message);
		})
	}

	static getTahunan(req, res) {
		Transaction.getTahunan()
		.then(function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error.message);
		})
	}

	static getMember(req, res) {
		MemberTransactions.findByBulanAndTahun(mysql, req.query)
		.then(function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error.message);
		})
	}

	static getVoucherAll(req, res) {
		Transaction.getAll(mysql, req.query)
		.then(function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error.message);
		})
	}

};

module.exports = TransactionController;

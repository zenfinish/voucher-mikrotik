const mysql = require('../helpers/mysql.js');

class TransactionMembersController {

	static get(req, res) {
		const { hp, limit } = req.query;

		const whereData = [];
		if (hp) whereData.push(`a.hp = '${hp}'`);

		const query = `
			SELECT
				a.*,
				DATE_FORMAT(a.tgl, '%d %M %Y, %k:%i:%s') AS tgl_indo,
				b.name AS profile_name,
				b.kode
			FROM transaction_members a
			LEFT JOIN profile b ON a.profile_id = b.id
			${
				whereData.length === 0 ? '' :
				`WHERE ${whereData.join(' && ')}`
			}
			${
				limit && limit === 'all' ? '' :
				limit && limit !== 'all' ? `LIMIT ${limit}` :
				`LIMIT 100`
			}
			ORDER BY a.tgl DESC
		`;
		// console.log(query)
		mysql.query(query, function(error, result) {
			if (error) return res.status(500).json(error.sqlMessage);

			res.status(200).json(result);
		});
	}

};

module.exports = TransactionMembersController;

const randomstring = require("randomstring");
const ApiMikrotik = require("./ApiMikrotik");

const createVoucher = (kode) => {
    return new Promise((resolve, reject) => {
        const username1 = randomstring.generate({
            length: 3,
            charset: 'alphabetic',
            capitalization: 'uppercase'
        });
        const username2 = randomstring.generate({
            length: 3,
            charset: 'numeric',
            capitalization: 'uppercase'
        });
        const username = `${username1}${username2}`;
        const password = 'verd';
    
        ApiMikrotik.put(`user-manager/user`, {
            "disabled": "false",
            "group": "default",
            "name": username,
            "otp-secret": "",
            "password": password,
            "shared-users": "1"
        })
        .then(result => {
            
            ApiMikrotik.put(`user-manager/user-profile`, {
                "profile": kode,
                "user": username
            })
            .then(result => {
                // console.log("wkwkwk====", result)
                // {
                //     ".id": "*3",
                //     "end-time": "dec/28/2022 17:34:20",
                //     "profile": "5",
                //     "state": "running-active",
                //     "user": "test"
                // }
                resolve({ username, password, endtime: result.data['end-time'], })
            })
            .catch(error => {
                reject(error.response.data)
            });
        })
        .catch(error => {
            // console.log("error mikrotik======", error)
            reject(error.response.data)
        });
    });
};

module.exports = createVoucher;

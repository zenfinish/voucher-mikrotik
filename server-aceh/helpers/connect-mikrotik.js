// https://www.npmjs.com/package/mikronode
// https://stackoverflow.com/questions/51727130/mikronode-with-express-js-typeerror-sentence-get-do-is-not-a-function
const mikronode = require('mikronode');

require('dotenv').config();

module.exports = {
    
    connect() {
		const device = new mikronode(`${process.env.IP_MIKROTIK}`);
		// device.setDebug(mikronode.DEBUG);
        return device.connect()
            .then(([login]) => login(`${process.env.USER_MIKROTIK}`, `${process.env.PASS_MIKROTIK}`))
            .catch(function(error) {
                console.log('masuk=======', error)
            });
    }

}
const mysql = require('../helpers/mysql.js');

class User {

	static findOne(username) {
		return new Promise((resolve, reject) => {
			const query = `
				SELECT
					*, id AS user_id
				FROM users
				WHERE username = '${username}'
			`;
			mysql.query(query, function(error, result) {
				if (error) {
					reject(error);
				} else {
					resolve(result[0]);
				};
			});
		});
	}

	static findById(id) {
		return new Promise((resolve, reject) => {
			mysql.query(`
				SELECT
					*
				FROM users
				WHERE id = '${id}'
			`, function(error, result) {
				if (error) {
					reject(error);
				} else {
					resolve(result[0]);
				};
			});
		});
	}

	static findCompany() {
		return new Promise((resolve, reject) => {
			mysql.query(`
				SELECT
					*
				FROM company
			`, function(error, result) {
				if (error) {
					reject(error);
				} else {
					resolve(result[0]);
				};
			});
		});
	}

	static findMdlSub(id_mdl) {
		return new Promise((resolve, reject) => {
			mysql.query(`
				SELECT
					*
				FROM mdl_sub
				WHERE id_mdl = '${id_mdl}'
			`, function(error, result) {
				if (error) {
					reject(error);
				} else {
					resolve(result);
				};
			});
		});
	}

	static find() {
		return new Promise((resolve, reject) => {
			mysql.query(`
				SELECT
					a.id,
					a.username,
					a.name
				FROM users a
			`, function(error, result) {
				if (error) {
					reject(error);
				} else {
					resolve(result);
				};
			});
		});
	}

	static create(data) {
		return new Promise((resolve, reject) => {
			mysql.query(`
				INSERT INTO user(username, password, name, role_id, hapus)
				VALUES('${data.username}', '${data.password}', '${data.name}', '${data.role_id}', '0')
			`, function(error, result) {
				if (error) {
					reject(error);
				} else {
					resolve(result);
				}
			});
		});
	}

	static findByIdAndUpdate(data) {
		return new Promise((resolve, reject) => {
			mysql.query(`
				UPDATE user SET
					name = '${data.name}',
					password = '${data.password}'
				WHERE username = '${data.username}'
			`, function(error, result) {
				if (error) {
					reject(error);
				} else {
					resolve(result);
				}
			});
		});
	}

	static updatePassword(data) {
		return new Promise((resolve, reject) => {
			mysql.query(`
				UPDATE user SET
					password = '${data.password}'
				WHERE username = '${data.username}'
			`, function(error, result) {
				if (error) {
					reject(error);
				} else {
					resolve(result);
				}
			});
		});
	}
	
}

module.exports = User;

const mysql = require('../helpers/mysql.js');

class Role {

	static find() {
		return new Promise((resolve, reject) => {
			mysql.query(`
				SELECT
					*
				FROM role
			`, function(error, result) {
				if (error) {
					reject(error);
				} else {
					resolve(result);
				}
			});
		});
	}
	
}

module.exports = Role;

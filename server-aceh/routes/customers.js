const express = require('express');

const CustomersController = require('../controllers/customers');

const router = express.Router();

router.post('/register', CustomersController.postRegister);
router.post('/login', CustomersController.postLogin);

module.exports = router;

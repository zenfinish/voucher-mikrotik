const express = require('express');

const router = express.Router();

router.use('/user-manager', require('./user_manager'));

module.exports = router;

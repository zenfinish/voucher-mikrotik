const express = require('express');
const router = express.Router();
const {
    get: getLimitation,
} = require('../../../../mikrotik/rest_api/user_manager/limitation');

router.get('/', (req, res) => {
    getLimitation()
    .then(result => {
        res.status(200).json(result.data);
    })
    .catch(error => {
        res.status(500).json(error)
    });
});

module.exports = router;

const express = require('express');
const router = express.Router();
const {
    get: getUser,
    getGroup,
} = require('../../../../mikrotik/rest_api/user_manager/user');

router.get('/group', (req, res) => {
    getGroup()
    .then(result => {
        res.status(200).json(result.data);
    })
    .catch(error => {
        res.status(500).json(error)
    });
});

router.get('/', (req, res) => {
    getUser(req.query)
    .then(result => {
        res.status(200).json(result.data);
    })
    .catch(error => {
        res.status(500).json(error)
    });
});

module.exports = router;

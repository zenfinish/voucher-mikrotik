const express = require('express');

const router = express.Router();

router.use('/limitation', require('./limitation'));
router.use('/profile', require('./profile'));
router.use('/profile-limitation', require('./profileLimitation'));
router.use('/user', require('./user'));

module.exports = router;

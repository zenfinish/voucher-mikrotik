const express = require('express');
const router = express.Router();
const {
    get: getProfileLimitation,
} = require('../../../../mikrotik/rest_api/user_manager/profile_limitation');

router.get('/', (req, res) => {
    getProfileLimitation()
    .then(result => {
        res.status(200).json(result.data);
    })
    .catch(error => {
        res.status(500).json(error)
    });
});

module.exports = router;

const express = require('express');
const {
    getSession,
    get: getUserManagerUser,
} = require('../../mikrotik/rest_api/user_manager/user');
const router = express.Router();

// getUserManagerUser({
//     // ['.id']: req.query['.id'],
// })
// .then((result) => {
//     console.table(result.data)
// })
// .catch(error => {
//     console.log("guguk====", error.response.data)
// })

router.use('/rest-api', require('./rest_api'));

router.get('/user-manager/session', (req, res) => {
    const { user } = req.query;

    getSession({
        user,
    })
    .then((result) => {
        res.status(200).json(result.data);
    })
    .catch(error => {
        res.status(500).json(error.response.data)
    })
});

router.get('/user-manager/user', (req, res) => {
    getUserManagerUser({
        ['.id']: req.query['.id'],
    })
    .then((result) => {
        res.status(200).json(result.data);
    })
    .catch(error => {
        res.status(500).json(error.response.data)
    })
});

module.exports = router;
const express = require('express');

const LimitationController = require('../controllers/limitation.js');
const { isUser } = require('../middlewares/is-user.js');

const router = express.Router();

router.post('/', isUser, LimitationController.post);
router.get('/', isUser, LimitationController.get);

module.exports = router;
module.exports = {

	isVersion(req, res, next) {
        if (req.headers.version !== process.env.WEB_CUSTOMER_DASHBOARD_VERSION) {
            res.status(500).json('Versi App Terupdate, Silahkan Refresh Browser Anda.');
        } else {
            next();
        };
	},

}
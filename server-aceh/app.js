require('dotenv').config();

const express = require('express');
const cors = require('cors');
const http = require('http');
// const https = require('https');
// const fs = require('fs');
// const cron = require('node-cron');

// const { init: initBaileys } = require("./baileys");
const { init: initSocket } = require("./socketio");
const { isVersion } = require('./middlewares/isVersion');
const { isUser } = require('./middlewares/is-user');
const mysql = require('./helpers/mysql');

const app = express();
const httpServer = http.createServer(app);
// const httpsServer = https.createServer({
//     key: fs.readFileSync(process.env.PRIVKEY),
//     cert: fs.readFileSync(process.env.CERT),
// }, app);

const port = process.env.PORT || 3003;

app.use(cors());
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

app.use('/public', express.static('public'));

app.use('/company', isVersion, require('./routes/company'));
app.use('/users', isVersion, require('./routes/user.js'));
app.use('/mikrotik', isVersion, isUser, require('./routes/mikrotik'));
app.use('/transactions', isVersion, require('./routes/transactions'));
app.use('/transaction', isVersion, require('./routes/transaction'));
app.use('/profile', isVersion, require('./routes/profile.js'));
app.use('/pemberitahuan', isVersion, require('./routes/pemberitahuan.js'));
app.use('/limitation', isVersion, require('./routes/limitation.js'));
app.use('/role', isVersion, require('./routes/role.js'));
app.use('/member', isVersion, require('./routes/members'));
app.use('/customers', isVersion, require('./routes/customers.js'));
app.use('/customer', isVersion, require('./routes/customer'));
app.use('/customer-members', isVersion, require('./routes/customerMembers'));
app.use('/whatsapp', isVersion, require('./routes/whatsapp'));
app.use('/midtrans', isVersion, require('./routes/midtrans'));
app.use('/nas', isVersion, require('./routes/nas'));

app.get('/paket', isUser, (req, res) => {
    mysql.query(`
        SELECT
            a.*
        FROM paket a
    `, function(error, result) {
        if (error) {
            res.status(500).json(error.sqlMessage);
        } else {
            res.status(200).json(result);
        };
    });
});

app.use('*', function(req, res) {
    res.status(404).json(`Link Tidak Ditemukan`);
});

const updateGagal = (data, socket) => {
    return new Promise((resolve, reject) => {
        mysql.query(`
            UPDATE transactions SET mt_transaction_status = 'gagal' WHERE id = ${data.id}
        `, (error, result) => {
            if (error) {
                reject(error);
            } else {
                const dataSend = { ...data, mt_transaction_status: "gagal", };
                socket.emit(data.mt_order_id, dataSend);
                resolve(result);
            };
        });
    });
};

const eksekusi = (socket) => setInterval(() => {
    mysql.query(`
        SELECT
            a.*,
            DATE_FORMAT(a.tgl, '%d %M %Y, %k:%i:%s') AS tgl_indo,
            TIMESTAMPDIFF(SECOND, a.tgl, NOW()) AS jarak,

            b.name AS profile_name,
            b.kode
        FROM transactions a
        LEFT JOIN profile b ON a.profile_id = b.id
        WHERE a.mt_transaction_status IS NULL
    `, async (error, result) => {
        if (!error) {
            for (let i = 0; i < result.length; i++) {
                if (result[i].jarak > 360) {
                    await updateGagal(result[i], socket);
                };
            };
        };
    });
}, 5000);

// 0: detik ke-0 setiap menit
// 12: pukul 12 siang (jam ke-12 pada format 24 jam)
// 1: tanggal 1
// *: setiap bulan
// *: setiap hari dalam seminggu
// *: setiap tahun

// cron.schedule('0 12 1 * * *', () => {
//     console.log('Tugas dijalankan pada tanggal 1 jam 12 siang.');
// });

httpServer.listen(port, function() {
    process.env.MODE === "DEVELOPMENT" ? console.log(`HTTP: Running on port ${port}`) : null;
    initSocket(httpServer, (socket) => {
        process.env.MODE === "DEVELOPMENT" ? eksekusi(socket) : null;
    });
    
    // const { createVoucher } = require("./helpers");
    // mysql.query(`
    //     SELECT
    //         a.*
    //     FROM transaction a
    //     WHERE
    //         a.guguk IS NULL &&
    //         a.tgl >= NOW() - INTERVAL 32 DAY &&
    //         a.price = 25000
    //     ORDER BY id DESC
    // `, async (error, result) => {
        
    //     for (let i = 0; i < result.length; i++) {
    //         await createVoucher("25", result[i].username, result[i].password)
    //         .then(result => {
    //             console.log("masuk=======", result)
    //         })
    //         .catch(error => {
    //             console.log("keluar=======", error)
    //         })
    //     }
    // });

});

// httpsServer.listen(process.env.PORT_HTTPS, function() {
//     initBaileys();
//     initSocket(httpsServer, (socket) => {
//         process.env.MODE === "DEVELOPMENT" ? eksekusi(socket) : null;
//     });
//     process.env.MODE === "DEVELOPMENT" ? console.log(`HTTPS: Running on port ${process.env.PORT_HTTPS}`) : null;
// });

import React from "react";
import { HashRouter, Routes, Route } from "react-router-dom";
import Login from "./views/Login";
import Register from "./views/Register";
import LupaPassword from "./views/LupaPassword";
import GantiPassword from "./views/GantiPassword";
import Dashboard from "./views/Dashboard";
import { RecoilRoot } from 'recoil';

export default function App() {

	return (
		<RecoilRoot>
			<HashRouter
				forceRefresh={true}
			>
				<Routes>
					<Route path="/" element={<Login />} />
					<Route path="/register" element={<Register />} />
					<Route path="/lupapassword" element={<LupaPassword />} />
					<Route path="/gantipassword/:token" element={<GantiPassword />} />
					<Route path="/dashboard/*" element={<Dashboard />} />
				</Routes>
			</HashRouter>
		</RecoilRoot>
	);

};
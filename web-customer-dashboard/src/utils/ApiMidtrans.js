import axios from 'axios';
import packageJson from '../../package.json';

const ApiMidtrans = axios.create({
	baseURL: process.env.REACT_APP_SERVER_MIDTRANS_URL,
});

ApiMidtrans.interceptors.request.use(
    config => {
        const token = localStorage.getItem('token');
        const version = packageJson.version;
        
        config.headers['token'] = `${token}`;
        config.headers['version'] = `${version}`;

        return config;
    },
);

export default ApiMidtrans;

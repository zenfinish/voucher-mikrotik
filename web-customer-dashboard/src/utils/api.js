import packageJson from '../../package.json';
import axios from 'axios';

const api = axios.create({
	baseURL: process.env.REACT_APP_SERVER_URL,
});

api.interceptors.request.use(
    config => {
        const version = packageJson.version;
        const token = localStorage.getItem('token');

        config.headers['token'] = `${token}`;
        config.headers['version'] = `${version}`;

        return config;
    },
);

export default api;

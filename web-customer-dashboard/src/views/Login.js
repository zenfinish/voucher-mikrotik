import React, { useState, useEffect } from "react";
import { FiRefreshCcw } from "react-icons/fi";
import { Link } from "react-router-dom";
import { createCaptcha } from "../helpers";
import { api } from "../utils";
import { Spin, notification } from 'antd';
import { useNavigate } from 'react-router-dom';

export default function Login() {
	const [captcha, setCaptcha] = useState(null);
	const [loading, setLoading] = useState(false);
	const [dataSend, setDataSend] = useState({
		hp: "",
		password: "",
		valueCaptcha: "",
	});

	const classInput = "border border-black w-full p-2 rounded";
	const navigate = useNavigate();
	
	useEffect(() => {
		setCaptcha(createCaptcha());
	}, []);

	const postLogin = (e) => {
		e.preventDefault();
		const { valueCaptcha } = dataSend;
		if (captcha !== valueCaptcha) {
			notification.error({
				message: 'Captcha Tidak Sesuai, Silahkan Ulang Lagi.',
			});
			setCaptcha(createCaptcha());
		} else {
			setLoading(true);
			api.post(`customer/login`, dataSend)
			.then(result => {
				setLoading(false);
				notification.success({
					message: 'Login Berhasil.',
				});
				localStorage.setItem("token", result.data);
				navigate("/dashboard");
			})
			.catch(error => {
				setLoading(false);
				notification.error({
					message: `Gagal. ${error.response.data}`,
				});
			});
		};
	};

	return (
		<>
			<div className="grid h-screen place-items-center">
				<div className="flex flex-col justify-center items-center gap-2">
					<img
						src={require('../assets/imgs/verd.png')}
						alt=""
						className="w-64 lg:w-96 h-auto"
					/>
					<form
						className="grid grid-cols-1 gap-1 w-64 lg:w-96 "
						onSubmit={postLogin}
						autoComplete="off"
					>
						<div>
							<label>No. Whatsapp</label>
							<input
								type="text"
								className={classInput}
								value={dataSend.hp}
								onChange={(e) => setDataSend({ ...dataSend, hp: e.target.value })}
								disabled={loading}
							/>
						</div>
						<div>
							<label>Password</label>
							<input
								type="password"
								className={classInput}
								value={dataSend.password}
								onChange={(e) => setDataSend({ ...dataSend, password: e.target.value })}
								disabled={loading}
							/>
						</div>
						<div>
							<div className="p-1 line-through text-2xl flex justify-between">
								<span>{captcha}</span>
								<FiRefreshCcw onClick={() => setCaptcha(createCaptcha())} />
							</div>
							<input
								type="text"
								className={classInput}
								value={dataSend.valueCaptcha}
								onChange={(e) => setDataSend({ ...dataSend, valueCaptcha: e.target.value })}
								disabled={loading}
								placeholder="Ketik Captcha Diatas"
							/>
						</div>
						<button className={classInput + " bg-blue-200 font-bold mb-2"} disabled={loading}>LOGIN</button>
						{
							loading ?
								<div className="text-center">
									<Spin size="large" />
								</div>
							:
								<div className="grid grid-cols-2 gap-1">
									<Link to="/lupapassword" className="underline font-bold">
										LUPA PASSWORD
									</Link>
									<Link to="/register" className="underline text-right font-bold">
										REGISTER
									</Link>
								</div>
						}
					</form>
					<div className="w-64 lg:w-96 text-center">
						<p>Silahkan Login Untuk Melakukan Pembelian Voucher / Register Jika Belum Mempunyai Akun.</p>
						<p>Gunakan Nomor Handphone Yang Terdaftar Di Whatsapp.</p>	
					</div>
				</div>
			</div>

		</>
	);
};

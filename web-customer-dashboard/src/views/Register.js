import React, { useState, useEffect } from "react";
import { FiRefreshCcw } from "react-icons/fi";
import { createCaptcha } from "../helpers";
import { Modal, Spin, notification } from 'antd';
import { api } from "../utils";
import { useNavigate } from 'react-router-dom';

const Register = () => {
	const [captcha, setCaptcha] = useState(null);
	const [loading, setLoading] = useState(false);
	const [dataSend, setDataSend] = useState({
		hp: "",
		password: "",
		ulangiPassword: "",
		valueCaptcha: "",
	});

	const classInput = "border border-black w-full p-2 rounded";
	const navigate = useNavigate();
	
	useEffect(() => {
		setCaptcha(createCaptcha());
	}, []);

	const postRegister = (e) => {
		e.preventDefault();
		const { password, ulangiPassword, valueCaptcha } = dataSend;
		if (password !== ulangiPassword) {
			Modal.error({
				title: 'Password & Ulangi Password Tidak Sama',
				centered: true,
			});
		} else if (captcha !== valueCaptcha) {
			Modal.error({
				title: 'Captcha Tidak Sesuai, Silahkan Ulang Lagi',
				centered: true,
			});
			setCaptcha(createCaptcha());
		} else {
			setLoading(true);
			api.post(`customer/register`, dataSend)
			.then(result => {
				setLoading(false);
				notification.success({
					message: 'Register Berhasil, Silahkan Login',
				});
				navigate("/");
			})
			.catch(error => {
				setLoading(false);
				Modal.error({
					title: `Gagal. ${error.response.data.substring(error.response.data.length - 9) === 'hp_UNIQUE' ? 'Nomor HP Yang Dimasukkan Sudah Pernah Terdaftar, Silahkan Reset Password Jika Lupa Password.' : error.response.data}`,
					centered: true,
				});
			});
		};
	};

	return (
		<>
			<div className="grid h-screen place-items-center">
				<form className="grid grid-cols-1 gap-1 w-64" onSubmit={postRegister} autoComplete="off">
					<div>
						<label>No. Handphone</label>
						<input
							type="text"
							className={classInput}
							value={dataSend.hp}
							onChange={(e) => setDataSend({ ...dataSend, hp: e.target.value })}
						/>
					</div>
					{/* <div>
						<label>Email</label>
						<input
							type="text"
							className={classInput}
							value={dataSend.hp}
							onChange={(e) => setDataSend({ ...dataSend, hp: e.target.value })}
						/>
					</div> */}
					<div>
						<label>Masukkan Password</label>
						<input
							type="password"
							className={classInput}
							value={dataSend.password}
							onChange={(e) => setDataSend({ ...dataSend, password: e.target.value })}
						/>
					</div>
					<div>
						<label>Ulangi Password</label>
						<input
							type="password"
							className={classInput}
							value={dataSend.ulangiPassword}
							onChange={(e) => setDataSend({ ...dataSend, ulangiPassword: e.target.value })}
						/>
					</div>
					<div>
						<div className="p-1 line-through text-2xl flex justify-between">
							<span>{captcha}</span>
							<FiRefreshCcw onClick={() => setCaptcha(createCaptcha())} />
						</div>
						<input
							type="text"
							className={classInput}
							value={dataSend.valueCaptcha}
							onChange={(e) => setDataSend({ ...dataSend, valueCaptcha: e.target.value })}
							placeholder="Ketik Captcha Diatas"
						/>
					</div>
					<div>
						<button className={classInput}>REGISTER</button>
					</div>
				</form>
			</div>
			<Modal open={loading} centered footer={null} closable={false}>
				<Spin size='large'>
				</Spin>
			</Modal>
		</>
	);
};

export default Register;

import React from 'react';

class Sk extends React.Component {

	render() {
		return (
			<>
				<p>1.	Masa berlaku voucher adalah 3 x 24 jam sejak tanggal voucher diterbitkan</p>
				<p>2.	Belum menggunakan lebih dari 100 MB kuota</p>
				<p>3.	Pengajuan refund dapat menghubungi 0721 8050085 / 0853 8498 0303 WA</p>
				<p>4.	Pengajuan refund juga dapat dilakukan langsung ke kantor PT Lintas Persada Optika</p>
				<p>5.	Ketentuan 1 dan 2 Tidak berlaku bila mana layanan kami mati lebih dari 24 jam</p>
			</>
		);
	}

}

export default (Sk);
import React from 'react';
import { useRecoilValue } from "recoil";
import { notification, Skeleton, Modal } from 'antd';
import { api } from "../../../utils";
import { customerState } from "../../../recoil/customer";
import socketIOClient from 'socket.io-client';
import Card from "./Card";

const Index = () => {
	const customer = useRecoilValue(customerState);

	return <VoucherSaya customer={customer} />;
};

class VoucherSaya extends React.Component {

	state = {
		socket: socketIOClient(process.env.REACT_APP_SERVER_URL, {
			transports: ['websocket']
		}),
		
		loading: false,

		data: [],
	}
	
	componentDidMount() {
		const { hp } = this.props.customer;

		if (hp) {
			this.fetchData();
		};
	}

	componentDidUpdate(prevProps) {
		if (this.props.customer !== prevProps.customer) {
			this.fetchData();
		};
	}

	componentWillUnmount() {
        this.state.socket.disconnect();
    }

	fetchData = () => {
		const { customer } = this.props;
		this.setState({ loading: true }, () => {
			api.get(`/transactions?hp=${customer.hp}&limit=all`)
			.then(result => {
				const data = result.data;
				this.setState({ data, loading: false });
			})
			.catch(error => {
				if(error.toJSON().message === 'Network Error'){
					Modal.error({
						title: "No internet connection"
					});
				} else {
					this.setState({ loading: false });
					notification.error({
						message: error.response.data,
					});
				};
			});
		});
	}
	
	render() {
		const { loading, data, socket } = this.state;
		
		return (
			<>
				<div className="p-1">
					<div className="font-semibold p-2">Voucher Saya</div>
					{
						loading ?
						<Skeleton />
						:
						<div className="grid grid-cols-1 gap-3 md:grid-cols-2 lg:grid-cols-4">
							{
								data.map((row, i) => (
									<Card
										key={i}
										data={row}
										klikDetail={() => this.setState({ aktif: row, openDetail: true })}
										socket={socket}
									/>
								))
							}

						</div>
					}
				</div>
			</>
		);
	}

}

export default (Index);
import React from 'react';
import { Tag, Spin, Divider } from 'antd';
// import { api } from "../../../utils";

class Card extends React.Component {

	state = {
		data: this.props.data,
		
		loading: true,
	}

	componentDidMount() {
		const { socket } = this.props;
		const { data } = this.state;

		socket.on(data.mt_order_id, (data) => {
			this.setState({ data }, () => {
				if (
					data.mt_transaction_status === "capture" ||
					data.mt_transaction_status === "settlement" ||
					data.mt_transaction_status === "deny" ||
					data.mt_transaction_status === "cancel" ||
					data.mt_transaction_status === "expire" ||
					data.mt_transaction_status === "gagal"
				) {
					this.setState({ loading: false });
				};
			});
		});

        if (
			data.mt_transaction_status !== "capture" &&
			data.mt_transaction_status !== "settlement" &&
			data.mt_transaction_status !== "deny" &&
			data.mt_transaction_status !== "cancel" &&
			data.mt_transaction_status !== "expire" &&
			data.mt_transaction_status !== "gagal"
		) {

		} else {
			this.setState({ loading: false });
		};
	}

	render() {
		const { data, loading } = this.state;
		const mt_json = JSON.parse(data.mt_json);
		let classColor = "rgb(254 242 242)";
			if (data.mt_transaction_status === "pending") classColor = "rgb(254 252 232)";
			if (data.mt_transaction_status === "settlement") classColor = "rgb(240 253 244)";
		
		return (
			<div
				className={`rounded border border-black col-span-1 w-full p-2`}
				style={{
					backgroundColor: classColor,
				}}
			>
				{
					loading ?
						<>
							<div className="flex justify-between">
								<Spin />
								<span className="text-xs">Proses Pengecekan...</span>
							</div>
							<Divider />
						</>
					: null
				}
				<div className={`flex flex-col justify-between ${loading ? '' : 'h-full'}`}>
					<div>
						<div className="flex justify-between">
							<span>Tanggal:</span>
							<span>{data.tgl_indo}</span>
						</div>
						<div className="flex justify-between">
							<span>ID Order:</span>
							<span>{data.mt_order_id}</span>
						</div>
						<div className="flex justify-between">
							<span>Nama:</span>
							<span><b>{data.profile_name}</b></span>
						</div>
						{
							mt_json ?
							<>
								<div className="flex justify-between">
									<span>Payment Type:</span>
									<span>{mt_json.payment_type}</span>
								</div>
								{
									mt_json.payment_type === "cstore" ?
									<>
										<div className="flex justify-between">
											<span>Nama Store:</span>
											<span>{mt_json.store}</span>
										</div>
										<div className="flex justify-between">
											<span>Kode Pembayaran:</span>
											<span>{mt_json.payment_code}</span>
										</div>
									</> :
									mt_json.payment_type === "bank_transfer" ?
									<>
										<div className="flex justify-between">
											<span>Nama Bank:</span>
											<span>{mt_json.permata_va_number ? "permata" : mt_json.va_numbers[0].bank}</span>
										</div>
										<div className="flex justify-between">
											<span>No.VA:</span>
											<span>{mt_json.permata_va_number ? mt_json.permata_va_number : mt_json.va_numbers[0].va_number}</span>
										</div>
									</>	: null
								}
							</>
							: null
						}
					</div>
					<div>
						<Divider />
						<div>
							<Tag
								className="w-full text-center"
								color={
									data.mt_transaction_status === "pending" ? '#FF8C00' :
									data.mt_transaction_status === "settlement" ? '#87d068' :
									'#f50'
								}
							>
								{
									!data.mt_transaction_status ? 'Belum Melakukan Pembayaran' :
									data.mt_transaction_status === "settlement" ? 'Pembayaran Berhasil' :
									data.mt_transaction_status === "cancel" ? 'Tidak Melakukan Pembayaran' :
									data.mt_transaction_status === "expire" ? 'Waktu Tunggu Pembayaran Habis' :
									data.mt_transaction_status === "gagal" ? 'Transaksi Dibatalkan' :
									data.mt_transaction_status
								}
							</Tag>
						</div>
					</div>
				</div>
			</div>
		);
	}

};

export default (Card);

import React from 'react';
import { notification, Modal, Button, Divider, Spin } from 'antd';
import { ApiMidtrans } from "../../../utils";
import { separator } from "../../../helpers";
import { useNavigate } from 'react-router-dom';

const Index = ({ data, close }) => {
	const navigate = useNavigate();
    
    return <Detail
        data={data}
        close={close}
        navigate={navigate}
    />;
};

class Detail extends React.Component {

	state = {
		loading: false,
	}

    bayar = () => {
        const { data, navigate } = this.props;

        this.setState({ loading: true }, () => {
            const snap = window.snap;
    
            ApiMidtrans.post(`snap/token`, data)
            .then(result => {
                this.setState({ loading: false });
                snap.pay(result.data, {
                    onSuccess: function(result){
                        /* You may add your own implementation here */
                        // alert("payment success!");
                        // console.log("onSuccess===========", result);
                        navigate(`/dashboard/voucher-saya`);
                    },
                    onPending: function(result){
                        /* You may add your own implementation here */
                        // alert("wating your payment!");
                        // console.log("onPending===========", result);
                        navigate(`/dashboard/voucher-saya`);
                    },
                    onError: function(result){
                        /* You may add your own implementation here */
                        // alert("payment failed!");
                        // console.log("onError===========", result);
                        navigate(`/dashboard/voucher-saya`);
                    },
                    onClose: function(){
                        /* You may add your own implementation here */
                        // alert('you closed the popup without finishing the payment');
                        // console.log("onClose===========");
                        navigate(`/dashboard/voucher-saya`);
                    },
                });
            })
            .catch(error => {
                this.setState({ loading: false });
                notification.error({
                    message: error.response.data,
                });
            });
        });
    }
	
	render() {
		const { data, close } = this.props;
        const { loading } = this.state;
		
		return (
			<>
                <Modal
                    open={true}
                    title={data.name}
                    footer={[
                        <React.Fragment key="Bayar">
                            {
                                loading ?
                                    <Spin size="large" />
                                :
                                    <Button onClick={this.bayar}>Bayar</Button>
                            }

                        </React.Fragment>,
                    ]}
                    onCancel={close}
                >
                    <div className="flex justify-between">
                        <span>Harga:</span>
                        <span>{separator(data.price, 0)}</span>
                    </div>
                    <div className="flex justify-between">
                        <span>PPN 11%:</span>
                        <span>{separator((data.price * (11 / 100)), 0)}</span>
                    </div>
                    <div className="flex justify-between">
                        <span>Biaya Transaksi:</span>
                        <span>{separator(4000, 0)}</span>
                    </div>
                    <Divider />
                    <div className="flex justify-between">
                        <span><b>TOTAL:</b></span>
                        <span><b>{separator((data.price + (data.price * (11 / 100)) + 4000), 0)}</b></span>
                    </div>
                </Modal>
            </>
		);
	}

}

export default (Index);

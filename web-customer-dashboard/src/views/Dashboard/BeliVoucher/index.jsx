import React from 'react';
import { api } from "../../../utils";
import { notification, Skeleton, Divider, Button } from 'antd';
import Detail from "./Detail";
import { separator } from "../../../helpers";

const Card = ({ data, klikDetail }) => (
	<div className="rounded shadow-2xl col-span-1 w-full p-4">
		<div>
			<div>{data.name}</div>
			<br />
			<div className="font-bold text-lg">Rp{separator(data.price, 0)} / {data.sql_date_ket}</div>
		</div>
		<Divider />
		<div
			className="flex justify-between"
		>
			<span>
				<Button type="primary" danger onClick={klikDetail}>BELI</Button>
			</span>
			{/* <span
				className="text-violet-700 font-bold cursor-pointer"
				// onClick={klikDetail}
			>Info Detail &nbsp; {`>`}</span> */}
		</div>
	</div>
);

class Index extends React.Component {

	state = {
		loading: false,
		openDetail: false,

		data: [],
		aktif: {},
	}
	
	componentDidMount() {
		this.setState({ loading: true }, () => {
			api.get(`/profile?limit=all&hapus=false&jenis=1`)
			.then(result => {
				this.setState({ data: result.data, loading: false });
			})
			.catch(error => {
				this.setState({ loading: false });
				notification.error({
					message: error.response.data,
				});
			});
		});
	}

	klikDetail = (row) => {
		this.setState({ aktif: row, openDetail: true });
	}
	
	render() {
		const { loading, openDetail, data, aktif } = this.state;
		
		return (
			<>
				<div className="p-1">
					<div className="font-semibold p-2">PILIH PAKET KAMU</div>
					{
						loading ?
							<Skeleton />
						:
							<div className="grid grid-cols-1 gap-4 md:grid-cols-2 lg:grid-cols-4">
								{
									data.map((row, i) => (
										<Card
											key={i}
											data={row}
											klikDetail={() => this.klikDetail(row)}
										/>
									))
								}
							</div>
					}
				</div>
				{
					openDetail ?
						<Detail
							data={aktif}
							close={() => this.setState({ openDetail: false, aktif: {} })}
						/>
					: null
				}
			</>
		);
	}

}

export default (Index);
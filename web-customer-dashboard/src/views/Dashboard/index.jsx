import React, { useEffect } from 'react';
import { DashboardRoutes } from '../../config';
import { api } from "../../utils";
import { Layout, Menu, Tag } from 'antd';
import { FaAlignJustify, FaTimes } from "react-icons/fa";
import { customerState } from "../../recoil/customer";
import { useRecoilState } from "recoil";
import { useNavigate } from 'react-router-dom';

const { Header, Content, Sider, Footer } = Layout;

const Index = () => {
	const [customer, setCustomer] = useRecoilState(customerState);

	const navigate = useNavigate();

	useEffect(() => {
		window.addEventListener('storage', e => {
			if (e.key === "token" && e.newValue && !e.oldValue) {
				window.location.assign(`${process.env.REACT_APP_PUBLIC_URL}`);
			};
		});
		api.post(`/customer/cektoken`)
		.then(result => {
			const { hp } = result.data;
			setCustomer({ hp });
		})
		.catch(error => {
			window.location.assign(`${process.env.REACT_APP_PUBLIC_URL}`);
		});
	}, []);

	return <Dashboard customer={customer} navigate={navigate} />;
};


class Dashboard extends React.Component {
	
	state = {
		collapse: true,
	}

	onClickSider = (link) => {
		const { navigate } = this.props;
		const { collapse } = this.state;
		this.setState({ collapse: !collapse }, () => {
			navigate(link);
		});
	}
	
	render() {
		const { customer, navigate } = this.props;
		const { collapse } = this.state;
		return (
			<>
				<Layout>
					<Sider
						className={`${collapse ? 'invisible' : 'visible'} lg:visible`}
						width={208}
						style={{
							// overflow: 'auto',
							height: '100vh',
							position: 'fixed',
							left: 0,
							top: 0,
							bottom: 0,
						}}
					>
						<div className="bg-white h-8 m-2 text-center border border-green-300">{customer.hp}</div>
						<div className="flex flex-col h-full pb-32 justify-between border border-green-300">
							<Menu
								theme="dark"
								mode="inline"
								defaultSelectedKeys={['1']}
								items={[
									{
										key: "1",
										label: "Voucher Saya",
										// icon: <UserOutlined />,
										onClick: () => this.onClickSider(`/dashboard/voucher-saya`),
									},
									{
										key: "2",
										label: "Beli Voucher",
										// icon: <UserOutlined />,
										onClick: () => this.onClickSider(`/dashboard/beli-voucher`),
									},
									{
										key: "3",
										label: "Profil",
										// icon: <UserOutlined />,
										onClick: () => this.onClickSider(`/dashboard/profil`),
									},
									{
										key: "4",
										label: "S&K Refund Voucher",
										// icon: <UserOutlined />,
										onClick: () => this.onClickSider(`/dashboard/sk`),
									},
									{
										key: "5",
										label: "Member Saya",
										// icon: <UserOutlined />,
										onClick: () => this.onClickSider(`/dashboard/member-saya`),
									},
								]}
							/>
							<Menu
								theme="dark"
								mode="inline"
								defaultSelectedKeys={['1']}
								items={[
									{
										key: "logout",
										label: "Logout",
										// icon: <UserOutlined />,
										onClick: () => {
											localStorage.clear();
											navigate(`/`);
										},
									},
								]}
							/>
						</div>
					</Sider>
					<Layout
						className={`${!collapse ? 'ml-52' : ''} lg:ml-52`}
					>
						<Header
							className="bg-white"
							style={{
								padding: 0,
								position: 'fixed',
								zIndex: 1,
								width: '100%',
								// right: 0,
							}}
						>
							<div className="h-full flex justify-between">
								<div className="p-5">
									{
										!collapse ?
										<FaTimes
											onClick={() => this.setState({ collapse: !collapse })}
											className={`lg:invisible text-2xl`}
										/>
										:
										<FaAlignJustify
											onClick={() => this.setState({ collapse: !collapse })}
											className={`lg:invisible text-2xl`}
										/>
									}
								</div>
								<div className="mr-3">
									<Tag color="lime" className="text-lg">{customer.hp}</Tag>
								</div>
							</div>
						</Header>
						<Content
							className="bg-white mt-16 p-2 mb-5"
						>
							<DashboardRoutes />
						</Content>
						<Footer
							className="bg-white"
							style={{
								textAlign: 'center',
								padding: 0,
								position: 'fixed',
								zIndex: 1,
								bottom: 0,
								width: '100%',
							}}
						>
							PT. Lintas Persada Optika ©2014 Created by Verd Team
						</Footer>
					</Layout>
				</Layout>
			</>
		);
	}
	
}

export default (Index);

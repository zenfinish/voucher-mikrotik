import React from 'react';
import { customerState } from "../../../recoil/customer";
import { useRecoilValue } from "recoil";
import { Input, Button, Spin, notification } from 'antd';
import { useNavigate } from 'react-router-dom';
import { api } from "../../../utils";

const Index = () => {
	const customer = useRecoilValue(customerState);

	const navigate = useNavigate();

	return <Profile customer={customer} navigate={navigate} />;
};


class Profile extends React.Component {

	state = {
		loading: false,

		password_lama: null,
		password_baru: null,
		password_baru_ulang: null,
	}

	update = () => {
		const { navigate, customer } = this.props;
		const { password_lama, password_baru, password_baru_ulang } = this.state;

		if (!password_lama || !password_baru || !password_baru_ulang) {
			notification.error({
				message: 'Password Masih Kosong',
			});
		} else if (password_baru !== password_baru_ulang) {
			notification.error({
				message: 'Password Baru Tidak Sama Ulang Password Baru',
			});
		} else {
			this.setState({ loading: true }, () => {
				api.post(`customer/gantipassword`, {
					password_lama,
					password_baru,
					hp: customer.hp,
				})
				.then(result => {
					localStorage.clear();
					notification.success({
						message: 'Ganti Password Berhasil. Silahkan Login Ulang',
					});
					navigate("/");
				})
				.catch(error => {
					this.setState({ loading: false });
					notification.error({
						message: `Gagal. ${error.response.data}`,
					});
				});
			});
		};
	}
	
	render() {
		const { customer } = this.props;
		const { loading, password_lama, password_baru, password_baru_ulang } = this.state;

		return (
			<>
				<div>{customer.hp}</div>
				<div>GANTI PASSWORD</div>
				<div>
					<Input
						placeholder="Password Lama"
						type="password"
						value={password_lama}
						onChange={(e) => this.setState({ password_lama: e.target.value })}
					/>
				</div>
				<div>
					<Input
						placeholder="Password Baru"
						type="password"
						value={password_baru}
						onChange={(e) => this.setState({ password_baru: e.target.value })}
					/>
				</div>
				<div>
					<Input
						placeholder="Ulangi Password Baru"
						type="password"
						value={password_baru_ulang}
						onChange={(e) => this.setState({ password_baru_ulang: e.target.value })}
					/>
				</div>
				<div>
					{
						loading ?
							<Spin size="large" />
						:
							<Button onClick={this.update}>Update Password</Button>
					}
				</div>
			</>
		);
	}

}

export default (Index);
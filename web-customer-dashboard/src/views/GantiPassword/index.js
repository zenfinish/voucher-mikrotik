import React, { useState, useEffect } from "react";
import { FiRefreshCcw } from "react-icons/fi";
import { createCaptcha } from "../../helpers";
import { Modal, Spin, notification } from 'antd';
import { api } from "../../utils";
import { useNavigate, useParams } from 'react-router-dom';

const Register = () => {
	const [captcha, setCaptcha] = useState(null);
	const [hp, setHp] = useState(null);
	const [loading, setLoading] = useState(false);
	const [dataSend, setDataSend] = useState({
		password: "",
		ulangiPassword: "",
		valueCaptcha: "",
	});
	const { token } = useParams();

	const classInput = "border border-black w-full p-2 rounded";
	const navigate = useNavigate();
	
	useEffect(() => {
		setCaptcha(createCaptcha());
		api.get(`customer/gantipassword/cektoken/${token}`)
		.then(result => {
			setHp(result.data.hp);
		})
		.catch(error => {
			// console.log("masuk====", error.response.data)
			notification.error({
				message: `Gagal. ${error.response.data}`,
			});
			navigate("/");
		});
	}, []);

	const postGantiPassword = (e) => {
		e.preventDefault();
		const { password, ulangiPassword, valueCaptcha } = dataSend;
		if (password !== ulangiPassword) {
			Modal.error({
				title: 'Password & Ulangi Password Tidak Sama',
				centered: true,
			});
		} else if (captcha !== valueCaptcha) {
			Modal.error({
				title: 'Captcha Tidak Sesuai, Silahkan Ulang Lagi',
				centered: true,
			});
			setCaptcha(createCaptcha());
		} else {
			setLoading(true);
			api.post(`customer/gantipassword`, {
				hp,
				password_baru: dataSend.password,
			})
			.then(result => {
				setLoading(false);
				notification.success({
					message: 'Ganti Password Berhasil, Silahkan Login',
				});
				navigate("/");
			})
			.catch(error => {
				setLoading(false);
				Modal.error({
					title: `Gagal. ${error.response.data}`,
					centered: true,
				});
			});
		};
	};

	return (
		<>
			<div className="grid h-screen place-items-center">
				<form className="grid grid-cols-1 gap-1 w-64" onSubmit={postGantiPassword} autoComplete="off">
					<div>
						<label>No. Handphone</label>
						<div>{hp}</div>
					</div>
					<div>
						<label>Password Baru</label>
						<input
							type="password"
							className={classInput}
							value={dataSend.password}
							onChange={(e) => setDataSend({ ...dataSend, password: e.target.value })}
						/>
					</div>
					<div>
						<label>Ulangi Password Baru</label>
						<input
							type="password"
							className={classInput}
							value={dataSend.ulangiPassword}
							onChange={(e) => setDataSend({ ...dataSend, ulangiPassword: e.target.value })}
						/>
					</div>
					<div>
						<div className="p-1 line-through text-2xl flex justify-between">
							<span>{captcha}</span>
							<FiRefreshCcw onClick={() => setCaptcha(createCaptcha())} />
						</div>
						<input
							type="text"
							className={classInput}
							value={dataSend.valueCaptcha}
							onChange={(e) => setDataSend({ ...dataSend, valueCaptcha: e.target.value })}
							placeholder="Ketik Captcha Diatas"
						/>
					</div>
					<div>
						<button className={classInput}>GANTI PASSWORD</button>
					</div>
				</form>
			</div>
			<Modal open={loading} centered footer={null} closable={false}>
				<Spin size='large'>
				</Spin>
			</Modal>
		</>
	);
};

export default Register;

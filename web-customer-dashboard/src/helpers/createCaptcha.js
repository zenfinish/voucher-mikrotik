export default () => {
    const value = [];
    for (let q = 0; q < 6; q++) {
        if (q % 2 === 0) {
            value[q] = String.fromCharCode(Math.floor(Math.random() * 26 + 65));
        } else {
            value[q] = Math.floor(Math.random() * 10 + 0);
        };
    };
    return (value.join(""));
};

import createCaptcha from "./createCaptcha";

function separator(data, number) {
    if (data === undefined || data === null || data === '' || isNaN(data)) data = 0;
    return Number(data).toFixed(number).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
};

export {
    createCaptcha,
    separator,
};

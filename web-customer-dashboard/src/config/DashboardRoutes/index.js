import React from "react";
import { Routes, Route } from "react-router-dom";

import VoucherSaya from '../../views/Dashboard/VoucherSaya';
import BeliVoucher from '../../views/Dashboard/BeliVoucher';
import Profil from '../../views/Dashboard/Profil';
import Sk from '../../views/Dashboard/Sk';
import MemberSaya from '../../views/Dashboard/MemberSaya';

const DashboardRoutes = () => {
	return (
		<Routes>
			<Route path={`/`} element={<VoucherSaya />} />
			<Route path={`/voucher-saya`} element={<VoucherSaya />} />
			<Route path={`/member-saya`} element={<MemberSaya />} />
			<Route path={`/beli-voucher`} element={<BeliVoucher />} />
			<Route path={`/profil`} element={<Profil />} />
			<Route path={`/sk`} element={<Sk />} />
		</Routes>
	);
};

export default (DashboardRoutes);
const express = require('express');
const midtransClient = require('midtrans-client');
const { Mysql } = require('../helpers');

const router = express.Router();

const getCurrentTimestamp = () => {
    return "" + Math.round(new Date().getTime() / 1000);
};

const cariProfil = (profile_id) => {
    return new Promise((resolve, reject) => {
        Mysql.query(`
            SELECT * FROM profile WHERE id = '${profile_id}' && hapus IS NULL
        `, function(error, result) {
            if (error) {
                reject(error);
            } else {
                if (result.length > 0) {
                    resolve(result[0]);
                } else {
                    reject("Kode Tidak Ditemukan");
                };
            };
        });
    });
};

router.post('/token', (req, res) => {
    const { price, name, profile_id } = req.body;
    const { hp } = req.user;
    const mt_order_id = "V-" + getCurrentTimestamp();

    cariProfil(profile_id)
    .then((result) => {
        Mysql.beginTransaction((error) => {
            if (error) return res.status(500).json(error.sqlMessage);
    
            const query = `
                INSERT INTO transactions (mt_order_id, profile_id, hp, propinsi_id)
                VALUES ('${mt_order_id}', '${profile_id}', '${hp}', 3)
            `;
            // console.log(query)
            Mysql.query(query, function(error, result) {
                if (error) return res.status(500).json(error.sqlMessage);
        
                const query = `
                    SELECT * FROM transactions WHERE mt_order_id = '${mt_order_id}'
                `;
                // console.log(query)
                Mysql.query(query, function(error, result) {
                    if (error) {
                        Mysql.rollback(function(error) {
                            if (error) {
                                return Mysql.rollback(function() {
                                    throw error;
                                });
                            };
                            return res.status(500).json(error.sqlMessage);
                        });
                    };
        
                    if (result.length > 0) {
                        const ppn = price * (11 / 100);
                        const btr = 4000;
                        const gross_amount = price + ppn + btr;
                    
                        const snap = new midtransClient.Snap({
                            isProduction : process.env.MODE === "DEVELOPMENT" ? false : process.env.MODE === "PRODUCTION" ? true : null,
                            serverKey : process.env.MIDTRANS_SERVER_KEY,
                            clientKey : process.env.MIDTRANS_CLIENT_KEY
                        });
                    
                        const parameter = {
                            transaction_details: {
                                order_id: mt_order_id,
                                gross_amount,
                            },
                            item_details: [
                                {
                                    "id": profile_id,
                                    "price": price,
                                    "quantity": 1,
                                    "name": name,
                                    "brand": "Verd Internet",
                                    "category": "VOUCHER",
                                    "merchant_name": "Verd Network",
                                },
                                {
                                    "id": "PPN",
                                    "price": ppn,
                                    "quantity": 1,
                                    "name": "PPN 11%",
                                },
                                {
                                    "id": "BTR",
                                    "price": btr,
                                    "quantity": 1,
                                    "name": "Biaya Transaksi",
                                },
                            ],
                            credit_card: {
                                secure: true
                            },
                            customer_details: {
                                first_name: hp,
                                // last_name: "Kane",
                                // email: "testmidtrans@mailnesia.com",
                                // phone: hp
                            }
                        };
                    
                        snap.createTransactionToken(parameter)
                        .then(snapToken => {
                            
                            Mysql.commit(function(error) {
                                if (error) {
                                    return Mysql.rollback(function() {
                                        throw err;
                                    });
                                };
                                return res.status(200).json(snapToken);
                            });
                        })
                        .catch(error => {
                            // e.message // basic error message string
                            // e.httpStatusCode // HTTP status code e.g: 400, 401, etc.
                            // e.ApiResponse // JSON of the API response 
                            // e.rawHttpClientData // raw Axios response object
                            // console.log("masuk=======", error.message)
                            res.status(500).json(error.message);
                        });
                    } else {
                        res.status(500).json("Gagal Pembuatan Transaksi");
                    };
                });
            });
        });
    })
    .catch((error) => {
        return res.status(500).json(error);
    });
});

module.exports = router;

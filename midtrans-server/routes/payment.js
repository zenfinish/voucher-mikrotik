const express = require('express');
const { ApiServer } = require("../helpers");
const { isMidtrans, isUser, isVersion } = require("../middlewares");
const midtransClient = require('midtrans-client');

const apiClient = new midtransClient.Snap({
    isProduction : false,
    serverKey : process.env.MIDTRANS_SERVER_KEY,
    clientKey : process.env.MIDTRANS_CLIENT_KEY
});

const router = express.Router();

router.get('/', isVersion, isUser, (req, res) => {
    const { order_id } = req.query;

    apiClient.transaction.status(order_id)
    .then(result => {
        res.status(200).json(result);
    })
    .catch(error => {
        res.status(500).json(error);
    });
});

router.post('/notification', isMidtrans, (req, res) => {
    const { order_id, transaction_status, transaction_id } = req.body;

    const updateTransaction = (data, tipe) => {
        // console.log("masuk1===", tipe)
        ApiServer.patch(`/transaction`, {
            mt_order_id: order_id,
            mt_transaction_id: transaction_id,
            mt_transaction_status: transaction_status,
            mt_json: JSON.stringify(req.body),
            kode: data.kode ? data.kode : null,
            profile_name: data.profile_name,
            hp: data.hp,
        })
        .then(result2 => {
            // console.log("result1===", result2.data)
            res.status(200).json({
                status_code: "200",
                status_message: "OK",
            });
        })
        .catch(error => {
            // console.log("error1===", error.response)
            res.status(500).json({
                status_code: "500",
                status_message: `${tipe}: Error Di patch transaction`,
            });
        });
    };

    if (order_id.substring(0, 18) === "payment_notif_test") {
        res.status(200).json({
            status_code: "200",
            status_message: "OK",
        });
    } else {
        if (transaction_status === "settlement") {
            ApiServer.get(`/transaction?mt_order_id=${order_id}`)
            .then(result => {
                if (
                    result.data.mt_transaction_status === "pending" ||
                    !result.data.mt_transaction_status
                ) {
                    updateTransaction(result.data, "settlement");
                } else if (result.data.mt_transaction_status === "settlement") {
                    // res.status(500).json({
                    //     status_code: "500",
                    //     status_message: "Di database kita sudah settlement, gak usah dikirim lagi bang..",
                    // });
                    res.status(200).json({
                        status_code: "200",
                        status_message: "OK",
                    });
                };
            })
            .catch(error => {
                res.status(500).json({
                    status_code: "500",
                    status_message: "settlement: Error Di get transaction",
                });
            });
        } else if (transaction_status === "pending") {
            ApiServer.get(`/transaction?mt_order_id=${order_id}`)
            .then(result => {
                if (
                    !result.data.mt_transaction_status
                ) {
                    updateTransaction(result.data, "pending");
                } else if (result.data.mt_transaction_status === "settlement") {
                    // res.status(500).json({
                    //     status_code: "500",
                    //     status_message: "Di database kita sudah settlement, kok dikirim lagi pending, kan jadi pertanyaan..",
                    // });
                    res.status(200).json({
                        status_code: "200",
                        status_message: "OK",
                    });
                };
            })
            .catch(error => {
                res.status(500).json({
                    status_code: "500",
                    status_message: "pending: Error Di get transaction",
                });
            });
        } else {
            updateTransaction(req.body, transaction_status);
            // res.status(500).json({
            //     status_code: "500",
            //     status_message: "transaction_status !== pending & settlement",
            // });
        };
    };
});

module.exports = router;

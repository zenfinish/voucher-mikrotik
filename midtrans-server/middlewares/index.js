const { verifyToken } = require('../tools/jsonwebtoken');

module.exports = {

	isUser(req, res, next) {
		verifyToken(req.headers.token, function(err, decoded) {
			if (err) {
				res.status(500).json('Token anda tidak sah');
			} else {
				req.user = decoded;
				next();
			};
		})
	},

	isMidtrans(req, res, next) {
		// res.status(500).json('Is Midtrans');
		next();
	},

	isVersion(req, res, next) {
		if (req.headers.version !== process.env.WEB_CUSTOMER_DASHBOARD_VERSION) {
            res.status(500).json('Versi App Terupdate, Silahkan Refresh Browser Anda.');
        } else {
            next();
        };
	},	

};

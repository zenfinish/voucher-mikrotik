const axios = require('axios');

const ApiServer = axios.create({
	baseURL: process.env.SERVER_URL,
    headers: {
        token: process.env.SERVER_TOKEN,
        version: process.env.WEB_CUSTOMER_DASHBOARD_VERSION,
    },
});

// api.interceptors.request.use(
//    (config) => {
//         const token = process.env.;

//         config.headers['token'] = `${token}`;

//       return config;
//    },
// );

module.exports = ApiServer;

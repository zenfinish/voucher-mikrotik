const mysql = require('mysql');

const connMysql = mysql.createConnection({
   host: process.env.MYSQL_HOST,
   user: process.env.MYSQL_USER,
   password: process.env.MYSQL_PASS,
   database: process.env.MYSQL_DB,
   multipleStatements: true,
   connectTimeout: 60000,
});
connMysql.connect();

module.exports = connMysql;

require('dotenv').config();

const express = require('express');
const cors = require('cors');
const https = require('https');
const http = require('http');
const fs = require('fs');

const { isUser, isVersion } = require('./middlewares');

const snapRoute = require('./routes/snap');
const paymentRoute = require('./routes/payment');

const app = express();

const httpsServer = https.createServer({
    key: fs.readFileSync(process.env.PRIVKEY),
    cert: fs.readFileSync(process.env.CERT),
}, app);
const httpServer = http.createServer(app);

const port = process.env.PORT || 8443;

app.use(cors());
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

app.use('/public', express.static('public'));

app.use('/snap', isVersion, isUser, snapRoute);
app.use('/payment', paymentRoute);

app.use('*', function(req, res) {
    res.status(404).json(`Link Tidak Ditemukan`);
});

httpsServer.listen(port, function() {
    console.log(`HTTPS: Running on port ${port}`);
});

httpServer.listen(3005, function() {
    console.log(`HTTP: Running on port 3005`);
});

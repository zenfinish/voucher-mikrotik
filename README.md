# Voucher Mikrotik

shop.lpo.net.id
    ~ voucher-mikrotik/server PORT=7443 PORT=3002
    ~ voucher-mikrotik/server-aceh PORT=3003
    ~ voucher-mikrotik/midtrans-server
    ~ voucher-mikrotik/server-radius PORT=3009
    ~ voucher-mikrotik/web-customer-dashboard

kurir.lpo.net.id
    ~ wabot/baileys-api = 103.144.135.233:3009/whatsapp/masuk
    ~ wabot/web-admin
    ~ wabot/web-client

103.31.249.217
    ~ voucher-mikrotik/web-member-admin

<!--
    Tanggal 01 Terbit Invoice
    Tanggal 10 jika tidak bayar Disable Mikrotik
    Tanggal 15 jika tidak bayar juga ubah status menjadi berhenti langganan
-->


<!--
    {.id: '*1', name: '10Mbps', name-for-users: '10mbps', override-shared-users: 'off', price: '0', …}
    1
    : 
    {.id: '*2', name: '20Mbps', name-for-users: '20Mbps', override-shared-users: 'off', price: '0', …}
    2
    : 
    {.id: '*3', name: '40Mbps', name-for-users: '40Mbps', override-shared-users: 'off', price: '0', …}
    length
    : 
    3

    15 mnenit sekali kirim invoice
-->


import React from 'react';
import Select from 'react-select';
import api from '../config/api.js';

class SelectIdPoli extends React.Component {
   
   state = {
      data: []
   }

   componentDidMount() {
      api.get(`/referensi/poli`)
      .then(result => {
         let hasil = [];
         result.data.forEach(row => {
            hasil.push({ value: row.id_poli, label: row.nama_poli, target: { value: row.id_poli, name: 'id_poli' } });
         });
         this.setState({
            data: hasil
         });
      })
      .catch(error => {
         console.log(error.response)
      });
   }

   render() {
      return (
         <Select
            onChange={this.props.onChange}
            value={this.state.data.filter(option => option.value === this.props.value)}
            options={this.state.data}
            placeholder="Poliklinik"
            isDisabled={this.props.disabled}
         />
      );
   }

}

export default (SelectIdPoli);

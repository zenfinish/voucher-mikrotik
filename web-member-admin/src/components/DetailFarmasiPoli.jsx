import React from 'react';
import { Dialog, DialogActions, DialogContent, Button } from '@material-ui/core';
import { DataGrid, GridColumn, ComboBox, TreeGrid, LinkButton, Tabs, TabPanel } from 'rc-easyui';
import ReactDataGrid from 'react-data-grid';

import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';

import Autocomplete from '../components/input/Autocomplete.jsx';
import api from '../config/api.js';

const defaultState = () => ({
	transaksi: [],
	permintaanFarmasi: {
		open: false,
	},
	
	nama_obat_temp: '',
	id_farstok_temp: '',
	nama_satuan_temp: '',
	qty_temp: '',
	cara_pakai_nama_temp: '',
	cara_pakai_temp: '',
	stok_temp: '',

	loadingPermintaan: false,

});

class DetailFarmasiPoli extends React.Component {
    
   state = {
		...defaultState(),
		rowData: [
			{ make: "Toyota", model: "Celica", price: 35000 },
			{ make: "Ford", model: "Mondeo", price: 32000 },
			{ make: "Porsche", model: "Boxter", price: 72000 }
		]
	}

	simpan = () => {
		this.setState({ loadingPermintaan: true }, () => {
			api.post(`farmasi/permintaan`, {
				data: this.state.transaksi,
				no_rekmedis: this.props.data.dataPasien.no_rekmedis,
				id_checkin: this.props.data.dataPasien.id_rjcheckin,
				id_farpelayanan: '1'
			})
			.then(result => {
				this.props.fetchpermintaan(this.props.data.dataPasien.no_rekmedis);
				this.setState({ ...defaultState() });
			})
			.catch(error => {
				console.log(error)
			});
		});
	}

   render() {
		return (
         <div style={{overflowX: 'auto'}}>
            <Dialog
               open={this.props.open}
               onClose={this.props.close}
               maxWidth="lg"
               fullWidth={true}
            >
					<DialogContent>
						<table style={{ fontSize: 14, marginBottom: 10 }} cellPadding={5}>
							<tbody>
								<tr>
									<td>Nama Pasien</td>
									<td>:</td>
									<td>{this.props.data.dataPasien ? this.props.data.dataPasien.nama_pasien : ''}</td>
									<td>Jenis Kelamin</td>
									<td>:</td>
									<td>{this.props.data.dataPasien ? this.props.data.dataPasien.jkel : ''}</td>
									<td>Tgl. Lahir</td>
									<td>:</td>
									<td>{this.props.data.dataPasien ? this.props.data.dataPasien.tgl_lahir_view : ''}</td>
								</tr>
								<tr>
									<td>No. Rekmedis</td>
									<td>:</td>
									<td>{this.props.data.dataPasien ? this.props.data.dataPasien.no_rekmedis : ''}</td>
									<td>Cara Bayar</td>
									<td>:</td>
									<td>{this.props.data.dataPasien ? this.props.data.dataPasien.nama_cabar : ''}</td>
								</tr>
							</tbody>
						</table>
						<div 
							className="ag-theme-balham"
							style={{ 
								height: '500px', 
								width: "100%"
							}} 
						>
							<AgGridReact
								columnDefs={[
									{ headerName: "Make", field: "make" },
									{ headerName: "Model", field: "model" },
									{ headerName: "Price", field: "price" }
								]}
								rowData={this.state.rowData}
								defaultColDef={{
									resizable: true,
									editable: true,
								}}
								onGridReady={(params) => {
									params.api.sizeColumnsToFit();
								}}
							/>
						</div>
						<DataGrid
							data={this.state.transaksi}
							style={{ height:320, width: '59%', float: 'left', marginRight: '5px' }}
							lazy
							toolbar={
								() => (
									<div style={{ padding: 4 }}>
										<Autocomplete
											placeholder="Cari"
											link="farmasi/search"
											dataTampil={['nama_obat', 'harga_view', 'stok']}
											onenter={
												(data) => {
													this.setState({
														nama_obat_temp: data.nama_obat,
														id_farstok_temp: data.id_farstok,
														nama_satuan_temp: data.nama_satuan,
														stok_temp: data.stok,
													}, () => {
														this.qty.focus();
													});
												}
											}
											refautocomplete={elem => (this.cari_obat = elem)}
											width="50px"
										/>
										<input
											placeholder="Nama Obat"
											type="text"
											value={this.state.nama_obat_temp}
											style={{
												width: '300px',
												padding: '5px 4px 5px 4px',
												borderRadius: '4px',
												border: '1px solid #3987c9',
											}}
											disabled={true}
										/>
										<input
											placeholder="Jml"
											type="number"
											onChange={
												(e) => {
													if ((Number(this.state.stok_temp) - Number(e.target.value)) < 0) {
														this.props.error('Stok Tidak Mencukupi.');
													} else {
														this.setState({ qty_temp: e.target.value });
													}
												}
											}
											onKeyDown={
												(e) => {
													if (e.keyCode === 13) {
														this.cara_pakai.inputRef.focus()
													}
												}
											}
											value={this.state.qty_temp}
											min={1}
											ref={elem => (this.qty = elem)}
											style={{
												width: '55px',
												padding: '5px 4px 5px 4px',
												borderRadius: '4px',
												border: '1px solid #3987c9',
											}}
										/>
										<ComboBox
											data={[
												{ value: "1 x Sehari (06:00)", text: "QD" },
												{ value: "1 x Sehari (22:00)", text: "QD (22:00)" },
												{ value: "2 x Sehari (06:00, 18:00)", text: "BID" },
												{ value: "3 x Sehari (06:00, 12:00, 19:00)", text: "TID" },
												{ value: "4 x Sehari (06:00, 12:00, 18:00, 22:00)", text: "QID" },
											]}
											placeholder="Cara Pakai"
											ref={elem => (this.cara_pakai = elem)}
											value={this.state.cara_pakai_temp}
											onSelectionChange={
												(data) => {
													if (data.value !== '') {
														if (this.state.id_farstok_temp === '') {
															this.props.error('Nama Obat Belum Dicari.');
															this.cari_obat.focus();
														} else if (this.state.qty_temp === '') {
															this.props.error('Jml Belum Diisi.');
															this.qty.focus();
														} else {
															this.setState({
																cara_pakai_nama_temp: data.text,
																cara_pakai_temp: data.value,
															}, () => {
																let transaksi = this.state.transaksi;
																transaksi.push({
																	nama_obat: this.state.nama_obat_temp,
																	id_farstok: this.state.id_farstok_temp,
																	nama_satuan: this.state.nama_satuan_temp,
																	qty: this.state.qty_temp,
																	cara_pakai: this.state.cara_pakai_temp,
																	cara_pakai_nama: this.state.cara_pakai_nama_temp,
																});
																this.setState({
																	transaksi: transaksi,
																	nama_obat_temp: '',
																	id_farstok_temp: '',
																	nama_satuan_temp: '',
																	qty_temp: '',
																	cara_pakai_nama_temp: '',
																	cara_pakai_temp: '',
																}, () => {
																	this.cari_obat.focus();
																});
															});
														}
													}
												}
											}
										/>
										<input
											placeholder="Stok"
											type="number"
											disabled
											value={this.state.stok_temp}
											min={0}
											style={{
												width: '55px',
												padding: '5px 4px 5px 4px',
												borderRadius: '4px',
												border: '1px solid #3987c9',
											}}
										/>
									</div>
								)
							}
							showFooter
							footerData={[{
								nama_obat: (
									<LinkButton iconCls="icon-save" onClick={this.simpan}>
										Simpan
									</LinkButton>
								)
							}]}					
						>
							<GridColumn field="nama_obat" title="Nama Obat" width="180px" />
							<GridColumn field="nama_satuan" title="Satuan" align="center" width="100px" />
							<GridColumn field="qty" title="Jml" width="50px" align="center" />
							<GridColumn field="cara_pakai_nama" title="Cara Pakai" width="100px" align="center" />
						</DataGrid>
						<Tabs style={{ height: 320 }} justified>
							<TabPanel title="Permintaan Pending">
								<TreeGrid
									data={this.props.data.dataPermintaan}
									style={{ height:'100%' }}
									lazy
									onRowExpand={
										(row) => {
											api.get(`farmasi/permintaan/detail/${row.id_farpermintaan}`)
											.then(result => {
												row.children = result.data;
												this.setState({})
											})
											.catch(error => {
												console.log(error.response)
											});
										}
									}
									treeField="tgl_view"
									loading={this.state.loadingPermintaan}
								>
									<GridColumn field="tgl_view" title="Tanggal" width="300px" />
									<GridColumn field="nama_dokter" title="Dokter" width="200px" />
									<GridColumn field="qty" title="Jml" width="50px" align="center" />
									<GridColumn field="cara_pakai" title="Cara Pakai" width="100px" align="center" />
								</TreeGrid>
							</TabPanel>
							<TabPanel title="Riwayat Terapi">
								<TreeGrid
									data={this.props.data.dataTerapi}
									style={{ height:'100%' }}
									lazy
									toolbar={
										() => (
											<div style={{ padding: 5 }}>
												<LinkButton iconCls="icon-back" onClick={() => {}}>
													Copy Ke Resep
												</LinkButton>
											</div>
										)
									}
									onRowExpand={
										(row) => {
											api.get(`farmasi/detail/${row.id_fartransaksi}`)
											.then(result => {
												row.children = result.data;
												this.setState({})
											})
											.catch(error => {
												console.log(error.response)
											});
										}
									}
									treeField="tgl_transaksi_view"
								>
									<GridColumn field="tgl_transaksi_view" title="Tanggal" width="300px" />
									<GridColumn field="nama_dokter" title="Dokter" width="200px" />
									<GridColumn field="qty" title="Jml" width="50px" align="center" />
									<GridColumn field="cara_pakai" title="Cara Pakai" width="100px" align="center" />
								</TreeGrid>
							</TabPanel>
						</Tabs>
               </DialogContent>
               <DialogActions>
                  <Button
							// onClick={this.props.close}
							onClick={() => {
								console.log(this.state.rowData)
							}}
						color="secondary" variant="contained">Tutup</Button>
               </DialogActions>
            </Dialog>
         </div>
		);
   }

};

export default DetailFarmasiPoli;
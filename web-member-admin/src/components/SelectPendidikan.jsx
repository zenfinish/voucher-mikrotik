import React from 'react';
import TextField from '@material-ui/core/TextField';

class SelectPendidikan extends React.Component {

   render() {
      return (
         <TextField
            select
            label="Pendidikan"
            onChange={this.props.onChange}
            SelectProps={{ native: true, }}
            name="pendidikan"
            fullWidth
            InputLabelProps={{ shrink: true }}
            value={this.props.value}
            InputProps={this.props.disabled === true ? { disabled: true, } : { disabled: false, }}
         >
            <option key="" value=""></option>
            <option key="1" value="1">Tidak Sekolah</option>
            <option key="2" value="2">Sd</option>
            <option key="3" value="3">Smp</option>
            <option key="4" value="4">Sma</option>
            <option key="5" value="5">Diploma</option>
            <option key="6" value="6">Sarjana</option>
            <option key="7" value="7">Magister</option>
            <option key="8" value="8">Doktor</option>
            <option key="9" value="9">Profesor</option>
         </TextField>
      );
   }

}

export default SelectPendidikan;
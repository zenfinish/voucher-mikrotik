import React from 'react';
import { ComboBox } from 'rc-easyui';

class SelectLakaLantasKdKecamatan extends React.Component {
   
   render() {
      return (
         <ComboBox
				data={this.props.valueX}
				value={this.props.value}
				onChange={this.props.onChange}
				disabled={this.props.disabled}
			/>
      );
   }

}

export default SelectLakaLantasKdKecamatan;

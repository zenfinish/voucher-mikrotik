import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Chip, TextField, Table, TableBody, TableCell, TableHead, TableRow, Dialog, DialogActions, DialogContent, DialogTitle, Button, Grid } from '@material-ui/core';

import api from '../config/api.js';

const CustomTableCell = withStyles(theme => ({
   head: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
   },
   body: {
      fontSize: 12,
   },
}))(TableCell);

const styles = theme => ({
   row: {
      '&:nth-of-type(odd)': {
         backgroundColor: theme.palette.background.default,
      },
   },
});

class CariDiagnosa extends React.Component {
    
   state = {
      dataDiagnosa: [],
      search: '',
   }

   getDataDiagnosa = (e) => {
      e.preventDefault();
      api.post(`referensi/diagnosa`, {
         search: this.state.search
      })
      .then(result => {
         this.setState({
            dataDiagnosa: result.data,
         });
      })
      .catch(error => {
         this.props.error(JSON.stringify(error.response.data));
      });
   }

   handleChange = (e) => {
      this.setState({
         [e.target.name]: e.target.value,
      });
   }

   getDataAktif = (data) => {
      this.setState({
         open: false,
      }, () => {
         this.props.getDataDiagnosa(data);
      });
   }

   render() {
      const { classes } = this.props;
      return (
         <div style={{overflowX: 'auto'}}>
            <Dialog
               open={this.props.open}
               onClose={this.props.close}
               maxWidth="lg"
               fullWidth={true}
            >
               <DialogTitle>
                  <Grid container spacing={8} style={{ marginBottom: 10 }}>
                     <Grid item xs={6}>
                        <form onSubmit={this.getDataDiagnosa}>
                           <TextField
                              label="Cari Berdasarkan Kode / Nama Diagnosa"
                              name="search"
                              type="text"
                              value={this.state.search}
                              fullWidth
                              onChange={this.handleChange}
                              InputLabelProps={{ shrink: true }}
                              autoComplete="off"
                           />
                        </form>
                     </Grid>
                  </Grid>
               </DialogTitle>
               <DialogContent>
                  <Table className={classes.table}>
                     <TableHead>
                        <TableRow>
                           <CustomTableCell align="center">No.</CustomTableCell>
                           <CustomTableCell align="center">Kode</CustomTableCell>
                           <CustomTableCell align="center">Nama</CustomTableCell>
                        </TableRow>
                     </TableHead>
                     <TableBody>
                        {
                           this.state.dataDiagnosa.map((row, index) => (
                              <TableRow className={classes.row} key={index}>
                                 <CustomTableCell align="center">{index+1}</CustomTableCell>
                                 <CustomTableCell align="center">
                                    <Chip
                                       label={row.kode}
                                       clickable
                                       onClick={() => {this.getDataAktif(row)}}
                                    />
                                 </CustomTableCell>
                                 <CustomTableCell align="center">{row.nama}</CustomTableCell>
                              </TableRow>
                           ))
                        }
                     </TableBody>
                  </Table>
               </DialogContent>
               <DialogActions>
                  <Button onClick={this.props.close} color="primary">Close</Button>
               </DialogActions>
            </Dialog>
         </div>
      );
   }
}

CariDiagnosa.propTypes = {
   classes: PropTypes.object.isRequired,
};

export default (withStyles(styles)(CariDiagnosa));

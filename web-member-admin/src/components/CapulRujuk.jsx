import React, { Fragment } from 'react';
import Grid from '@material-ui/core/Grid';
import CariFaskes from './CariFaskes.jsx';
import TextFieldCatatan from './input/TextFieldCatatan.jsx';
import SelectTipeRujukan from './input/SelectTipeRujukan.jsx';

class CapulRujuk extends React.Component {

   render() {
      return (
         <Fragment>
            <Grid container spacing={8} style={{ marginBottom: 10 }}>
               <Grid item xs={6}><CariFaskes getDataFaskes={this.props.getDataFaskes} /></Grid>
               <Grid item xs={6}><TextFieldCatatan /></Grid>
            </Grid>
            <Grid container spacing={8} style={{ marginBottom: 10 }}>
               <Grid item xs={3}><SelectTipeRujukan value={this.props.data.tipe_rujukan} /></Grid>
            </Grid>
         </Fragment>
      );
   }

}

export default CapulRujuk;

import React from 'react';
import TextField from '@material-ui/core/TextField';

class TextFieldTglKejadian extends React.Component {

   render() {
      return (
         <TextField
            label="Tgl Kejadian"
            name="tgl_kejadian"
            type="date" variant="outlined"
            fullWidth
            onChange={this.handleChange}
            InputLabelProps={{ shrink: true }}
            style={{height: '40px'}}
            value={this.state.tgl_kejadian}
         />
      );
   }

}

export default TextFieldTglKejadian;

import React from 'react';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';

import api from '../config/api.js';
import Date from './input/Date.jsx';

class UpdatePulang extends React.Component {
    
   state = {
      sep: '',
      tgl_pulang: '',
   }

   handleChange = (e) => {
      this.setState({
         [e.target.name]: e.target.value
      });
   }

   UNSAFE_componentWillReceiveProps(nextProps, prevState) {
      this.setState({
         sep: nextProps.data.noSep,
      });
   }
   update = () => {
      api.put(`/bpjs/sep/update-pulang`, {
         ...this.state,
      }, {
         headers: { token: localStorage.getItem('token') }
      })
      .then(result => {
         this.props.success('Data berhasil di delete');
         this.props.close();
      })
      .catch(error => {
         this.props.error(error.response.data);
      });
   }

   render() {
      return (
         <div style={{overflowX: 'auto'}}>
            <Dialog
               open={this.props.open}
               onClose={this.props.close}
               maxWidth="lg"
               fullWidth={true}
            >
               <DialogTitle>Update Tanggal Pulang</DialogTitle>
               <DialogContent>
                  <br />
                  <Date name="tgl_pulang" label="Tanggal Pulang" value={this.state.tgl_pulang} onChange={this.handleChange} />
               </DialogContent>
               <DialogActions>
                  <Button onClick={this.update} color="secondary">Update</Button>
                  <Button onClick={this.props.close} color="secondary">Close</Button>
               </DialogActions>
            </Dialog>
         </div>
      );
   }

};

export default UpdatePulang;
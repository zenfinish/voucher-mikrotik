import React from 'react';
import TextField from '@material-ui/core/TextField';

class TextFieldLakaLantasTglKejadian extends React.Component {

   render() {
      return (
         <TextField
            label="Tgl Kejadian"
            name="laka_lantas_tgl_kejadian"
            type="date"
            fullWidth
            onChange={this.props.onChange}
            InputLabelProps={{ shrink: true }}
            value={this.props.value}
         />
      );
   }

}

export default TextFieldLakaLantasTglKejadian;

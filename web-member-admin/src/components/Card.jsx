import React from 'react';

class Card extends React.Component {

	render() {
		return (
			<div className={`bg-white p-3 rounded ${this.props.className}`} style={this.props.style}>
				{this.props.children}
			</div>
		)
	}

}

export default Card;

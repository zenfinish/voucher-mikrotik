import React from 'react';
import Select from 'react-select';

class SelectPelayanan extends React.Component {
   
   state = {
      data: [
         { value: '2', label: 'Rawat Jalan', target: { value: '2', name: 'jenis_pelayanan' } },
         { value: '1', label: 'Rawat Inap', target: { value: '1', name: 'jenis_pelayanan' } }
      ]
   }

   render() {
      return (
         <Select
            onChange={this.props.onChange}
            value={this.state.data.filter(option => option.value === this.props.value)}
            options={this.state.data}
            placeholder="Pelayanan"
            isDisabled={this.props.disabled}
         />
      );
   }

}

export default (SelectPelayanan);

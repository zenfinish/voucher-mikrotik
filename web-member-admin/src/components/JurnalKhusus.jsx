import React from 'react';
import { Dialog, DialogContent, DialogActions, Button, Grid } from '@material-ui/core';
import { TreeGrid, GridColumn, TextBox, NumberBox, DataGrid, DateBox } from 'rc-easyui';
import api from '../config/api.js';
import { separator, tglSql } from '../config/helpers.js';

const defaultState = () => ({
	judul: 'Header',
	kode_jurnal: 'JU',
	kode_kantor: '01',
	referensi: '',
	tanggal: tglSql(new Date()),
	keterangan: '',

	nama_transaksi: '',
	kode1: '',
	nama1: '',
	kode2: '',
	nama2: '',
	debet1: 0,
	kredit2: 0,
});

class JurnalKhusus extends React.Component {

   state = {
      ...defaultState(),
   }

	clearDataKodePerk = () => {
		this.setState({
			dataKodePerk: []
		});
   }

	simpan = () => {
		if (this.state.debet1 === '' || this.state.debet1 === 0 || this.state.debet1 === '0') {
			this.props.error('Nilai Transaksi Masih Kosong !');
		} else if (this.state.kode1 === '' || this.state.kode2 === '') {
			this.props.error('Nama Transaksi Belum Dipilih !');
		} else {
			let tanggal = this.state.tanggal;
			api.post(`keuangan/jurnal/save`,
				{
					kode_jurnal: this.state.kode_jurnal,
					kode_kantor: this.state.kode_kantor,
					referensi: this.state.referensi,
					tanggal: tanggal,
					keterangan: this.state.keterangan,
					kode_zainal: this.props.kode_zainal,
					dataAktif: [
						{
							KODE_PERK: this.state.kode1,
							NAMA_PERK: this.state.nama1,
							debet: this.state.debet1,
							kredit: 0,
						},
						{
							KODE_PERK: this.state.kode2,
							NAMA_PERK: this.state.nama2,
							debet: 0,
							kredit: this.state.kredit2,
						},
					],
				},
				{
					headers: { token: localStorage.getItem('token') }
				}
			)
			.then(result => {
				this.props.success('Data Berhasil Disimpan.');
				this.setState({ ...defaultState() });
			})
			.catch(error => {
				console.log(error.response)
				// this.props.error(error.response.data);
			});
		}
	}

	reset = () => {
		this.setState({
			...defaultState(),
		});
	}

   render() {
      return (
         <Dialog
				open={this.props.open}
				onClose={this.props.close}
				maxWidth="lg"
				fullWidth={true}
			>
            <DialogContent>
					<Grid container spacing={8} style={{ marginBottom: 10 }}>
						<Grid item xs={4}>
							<TreeGrid 
								style={{ height: 350 }}
								data={this.props.data}
								treeField="name"
							>
								<GridColumn
									field="name"
									render={({ row }) => (
										<span
											onClick={() => {
												if (row.parent !== undefined) {
													this.setState({
														judul: row.parent.name,
														nama_transaksi: row.name,
														kode1: row.kode1,
														nama1: row.nama1,
														kode2: row.kode2,
														nama2: row.nama2,
													});
												}
											}}
										>{row.name}</span>
									)}
								/>
							</TreeGrid>
						</Grid>
						<Grid item xs={8}>
							<div>{this.state.judul}</div><br/>
							<table style={{ fontSize: 14 }}>
								<tbody>
									<tr>
										<td>Nama Transaksi</td>
										<td>:</td>
										<td>
											<TextBox
												disabled
												value={this.state.nama_transaksi}
												style={{ width: 300 }}
											/>
										</td>
									</tr>
									<tr>
										<td>Nilai Transaksi</td>
										<td>:</td>
										<td>
											<NumberBox
												onChange={
													(value) => {
														this.setState({
															debet1: value,
															kredit2: value,
														});
													}
												}
												style={{ width: 300 }}
												precision={2}
											/>
										</td>
									</tr>
									<tr>
										<td>Kode Jurnal</td>
										<td>:</td>
										<td>
											<TextBox
												style={{ width: 45, marginRight: 2 }}
												disabled
												value={this.state.kode_jurnal}
											/>
											<TextBox
												disabled
												value="Jurnal Umum"
											/>
										</td>
									</tr>
									<tr>
										<td>Referensi</td>
										<td>:</td>
										<td>
											<TextBox
												style={{ width: 155 }}
												onChange={
													(value) => {
														this.setState({
															referensi: value,
														});
													}
												}
												value={this.state.referensi}
											/>
										</td>
									</tr>
									<tr>
										<td>Tanggal</td>
										<td>:</td>
										<td>
											<input
												type="date"
												onChange={
													(e) => {
														this.setState({
															tanggal: e.target.value,
														});
													}
												}
												value={this.state.tanggal}
											/>
										</td>
									</tr>
									<tr>
										<td>Keterangan</td>
										<td>:</td>
										<td colSpan={3}>
											<TextBox
												style={{ width: 450 }}
												onChange={
													(value) => {
														this.setState({
															keterangan: value,
														});
													}
												}
												value={this.state.keterangan}
											/>
										</td>
									</tr>
								</tbody>
							</table>
							<DataGrid data={[
								{
									kode_perkiraan: this.state.kode1,
									nama_perkiraan: this.state.nama1,
									debet: separator(this.state.debet1),
									kredit: 0,
								},
								{
									kode_perkiraan: this.state.kode2,
									nama_perkiraan: this.state.nama2,
									debet: 0,
									kredit: separator(this.state.kredit2),
								}
							]} style={{height:100}}>
								<GridColumn field="kode_perkiraan" title="Kode Perkiraan" width="15%"></GridColumn>
								<GridColumn field="nama_perkiraan" title="Nama Perkiraan" width="55%"></GridColumn>
								<GridColumn field="debet" title="Debet" align="right" width="15%"></GridColumn>
								<GridColumn field="kredit" title="Kredit" align="right" width="15%"></GridColumn>
							</DataGrid>
						</Grid>
					</Grid>
            </DialogContent>
				<DialogActions>
					<Button onClick={this.simpan} color="primary" variant="contained">Simpan</Button>
					<Button onClick={this.reset} color="secondary" variant="contained">Reset Form</Button>
				</DialogActions>
         </Dialog>
      );
   }

}

export default JurnalKhusus;

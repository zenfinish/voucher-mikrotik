import React from 'react';
import TextField from '@material-ui/core/TextField';

class TextFieldSep extends React.Component {

   render() {
      return (
         <TextField
            label="No. SEP"
            name="no_bpjs"
            type="text" variant="outlined"
            value={this.props.value}
            fullWidth
            InputProps={this.props.disabled === true ? { disabled: true, } : { disabled: false, }}
            onChange={this.props.onChange}
            InputLabelProps={{ shrink: true }}
            style={{height: '40px'}}
         />
      );
   }

}

export default TextFieldSep;

import React from 'react';
import TextField from '@material-ui/core/TextField';

class SelectWilayah extends React.Component {

   render() {
      return (
         <TextField
            select
            label="Wilayah"
            onChange={this.props.onChange}
            SelectProps={{ native: true, }}
            name="wilayah"
            fullWidth
            InputLabelProps={{ shrink: true }}
            value={this.props.value}
            InputProps={this.props.disabled === true ? { disabled: true, } : { disabled: false, }}
         >
            <option key="" value=""></option>
            <option key="1" value="1">Dalam Banda Aceh</option>
            <option key="2" value="2">Luar Banda Aceh</option>
         </TextField>
      );
   }

}

export default SelectWilayah;
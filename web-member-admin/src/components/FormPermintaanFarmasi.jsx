import React from 'react';
import { Dialog, Grid, DialogActions, DialogContent, Button } from '@material-ui/core';
import { ComboBox, Dialog as DialogAlert } from 'rc-easyui';
import NumberBox from './input/NumberBox.jsx';
import TextBox from './input/TextBox.jsx';
import Select from 'react-select';
import api from '../config/api.js';
import FarmasiTableTransaksi from './farmasi/TableTransaksi.jsx';

const defaultState = () => ({
	nama_obat: '',
	id_farstok: '',
	harga: 0,
	hargajual: '0',
	qty: '',
	kronis: '',
	stok: '0',

	total: 0,

	data: [],
	dataSearch: [],
});

class FormPermintaanFarmasi extends React.Component {
	
	state = {
		...defaultState(),
		id_checkin: this.props.id_checkin,
		id_farpelayanan: this.props.id_farpelayanan,
	}

	componentDidMount() {
		this.dlg.close();
	}

	UNSAFE_componentWillReceiveProps(nextProps, prevState) {
      this.setState({
			id_checkin: nextProps.id_checkin,
			id_farpelayanan: nextProps.id_farpelayanan,
      });
	}

	addObat = (e) => {
		e.preventDefault();
		if (this.state.qty === '0' || this.state.qty === 0 || this.state.qty === '') {
			this.props.error('Qty Masih Kosong !');
			this.refs.qty.state.ref.current.focus();
		} else {
			let hasil = this.state.data;
			hasil.push({
				qty: this.state.qty,
				kronis: this.state.kronis,
				nama_obat: this.state.nama_obat,
				nama_satuan: this.state.nama_satuan,
				id_farstok: this.state.id_farstok,
				harga: this.state.harga,
				jml_harga: this.state.qty * this.state.harga,
			});
			let total = 0;
			for (let i = 0; i < hasil.length; i++) {
				total += Number(hasil[i].qty) * Number(hasil[i].harga);
			}
			this.setState({
				...defaultState(),
				data: hasil,
				total: total,
			}, () => {
				this.refSearch.focus();
			});
		}
	}

	getSearch = (e) => {
		if (e.keyCode === 13) {
			api.get(`farmasi/search/${e.target.value}`)
			.then(result => {
				let hasil = [];
				result.data.forEach(row => {
					hasil.push({
						label: `${row.nama_obat} - Stok: ${row.stok} - Exp: ${row.tgl_kadaluarsa_view}`,
						...row,
					});
				});
				this.setState({
					dataSearch: hasil
				});
			})
			.catch(error => {
				this.props.error(JSON.stringify(error.response.data));
			});
		}
	}

	simpan = () => {
		api.post(`farmasi/permintaan`, this.state)
		.then(result => {
			this.close();
		})
		.catch(error => {
			// this.props.error(JSON.stringify(error.response.data));
			console.log(error.response)
		});
	}

	onChange = (data) => {
		this.setState({
			...data
		}, () => {
			this.refs.qty.state.ref.current.focus();
		});
	}

	close = () => {
		this.setState({
			...defaultState()
		}, () => {
			this.props.close();
		});
	}

	deleteTransaksi = (id_farstok) => {
		let hasil = this.state.data;
		let index = hasil.map(function(e) { return e.id_farstok }).indexOf(id_farstok);
		hasil.splice(index, 1);
		let total = 0;
		for (let i = 0; i < hasil.length; i++) {
			total += Number(hasil[i].qty) * Number(hasil[i].harga);
		}
      this.setState({
			data: hasil,
			total: total,
      }, () => {
			this.refSearch.focus();
		});
	}
	
	validationQty = (e) => {
		if (e.target.value !== '') {
			let qty = Number(this.state.stok) - Number(e.target.value);
			if (qty < 0) {
				this.dlg.open();
				// this.refs.qty.state.ref.current.focus();
			} 
		}
	}

   render() {
      return (
         <div style={{overflowX: 'auto'}}>
            <Dialog
               open={this.props.open}
               onClose={this.close}
               maxWidth="lg"
               fullWidth={true}
            >
               <DialogContent>
                  <Grid container spacing={8} style={{ marginBottom: 5 }}>
                     <Grid item xs={4} style={{ fontSize: 12 }}>
								<Grid container spacing={8}>
									<Grid item xs={12}>
										<Select
											onKeyDown={this.getSearch}
											options={this.state.dataSearch}
											placeholder="Cari Obat"
											onInputChange={() => {this.setState({ dataSearch: [] })}}
											onChange={this.onChange}
											value=""
											ref={node => this.refSearch = node}
										/>
									</Grid>
								</Grid>
								<form onSubmit={this.addObat}>
									<Grid container spacing={8}>
										<Grid item xs={12}>
											<div style={{ marginBottom: 4 }}>Nama Obat</div>
											<TextBox
												disabled
												style={{width:'100%'}}
												placeholder="Null"
												value={this.state.nama_obat}
												name="nama_obat"
											/>
										</Grid>
									</Grid>
									<Grid container spacing={8}>
										<Grid item xs={4}>
											<div style={{ marginBottom: 4 }}>ID Stok</div>
											<TextBox
												disabled
												style={{width:'100%'}}
												placeholder="Null"
												value={this.state.id_farstok}
												name="id_farstok"
											/>
										</Grid>
										<Grid item xs={6}>
											<div style={{ marginBottom: 4 }}>Harga</div>
											<NumberBox
												min={0}
												disabled
												value={this.state.harga}
												name="harga"
												onChange={this.handleChange}
											/>
										</Grid>
									</Grid>
									<Grid container spacing={8}>
										<Grid item xs={4}>
											<div style={{ marginBottom: 4 }}>Qty</div>
											<NumberBox
												placeholder="0"
												onChange={(e) => {this.setState({ qty: e.target.value })}}
												value={this.state.qty}
												min={0}
												ref="qty"
												onKeyDown={this.validationQty}
											/>
										</Grid>
										<Grid item xs={4}>
											<div style={{ marginBottom: 4 }}>Kronis</div>
											<NumberBox
												placeholder="0"
												onChange={(e) => {this.setState({ kronis: e.target.value })}}
												value={this.state.kronis}
												min={0}
											/>
										</Grid>
										<Grid item xs={4}>
											<div style={{ marginBottom: 4 }}>Stok</div>
											<NumberBox
												style={{width:'100%'}}
												placeholder="0"
												onChange={(e) => {this.setState({ stok: e.target.value })}}
												value={this.state.stok}
												min={0}
												disabled
											/>
										</Grid>
									</Grid>
									<Grid container spacing={8}>
										<Grid item xs={8}>
											<ComboBox
												data={[
													{ value: "1", text: "KAPSUL" },
													{ value: "2", text: "KERTAS" },
													{ value: "3", text: "BOTOL" },
													{ value: "4", text: "POT" },
												]}
												style={{width:'100%'}}
												placeholder="Bungkus"
											/>
										</Grid>
										<Grid item xs={4}>
											<TextBox name="fname" style={{width:'100%'}} placeholder="0" />
										</Grid>
									</Grid>
									<Grid container spacing={8}>
										<Grid item xs={4}>
											<ComboBox
												data={[
													{ value: "TABLET", text: "TABLET" },
													{ value: "TETES", text: "TETES" },
													{ value: "KAPSUL", text: "KAPSUL" },
													{ value: "BUNGKUS", text: "BUNGKUS" },
													{ value: "AMPUL", text: "AMPUL" },
													{ value: "VIAL", text: "VIAL" },
													{ value: "UNIT", text: "UNIT" },
													{ value: "SENDOK TAKAR", text: "SENDOK TAKAR" },
												]}
												placeholder="Sediaan"
												style={{width:'100%'}}
											/>
										</Grid>
										<Grid item xs={4}>
											<ComboBox
												data={[
													{ value: "1 x Sehari (06:00)", text: "QD" },
													{ value: "1 x Sehari (22:00)", text: "QD (22:00)" },
													{ value: "2 x Sehari (06:00, 18:00)", text: "BID" },
													{ value: "3 x Sehari (06:00, 12:00, 19:00)", text: "TID" },
													{ value: "4 x Sehari (06:00, 12:00, 18:00, 22:00)", text: "QID" },
												]}
												placeholder="Cara Pakai"
												style={{width:'100%'}}
											/>
										</Grid>
										<Grid item xs={4}>
											<ComboBox
												data={[
													{ value: "1 Tablet", text: "1 Tablet" },
													{ value: "2 Tablet", text: "2 Tablet" },
													{ value: "3 Tablet", text: "3 Tablet" },
													{ value: "4 Tablet", text: "4 Tablet" },
													{ value: "1/2 sdt", text: "1/2 sdt" },
													{ value: "1/4 sdt", text: "1/4 sdt" },
													{ value: "1 sdm", text: "1 sdt" },
													{ value: "1/2 sdm", text: "1/2 sdm" },
													{ value: "1/4 sdm", text: "1/4 sdm" },
													{ value: "1 sdm", text: "1 sdm" },
												]}
												placeholder="Tablet"
												style={{width:'100%'}}
											/>
										</Grid>
									</Grid>
									<Grid container spacing={8}>
										<Grid item xs={4}>
											<ComboBox
												data={[
													{ value: "Mata Kanan", text: "OD" },
													{ value: "Mata Kiri", text: "OS" },
													{ value: "Telinga Kanan", text: "AD" },
													{ value: "Telinga Kiri", text: "AS" },
													{ value: "Penggunaan Luar", text: "OE" },
													{ value: "Sebelum Makan", text: "AC" },
													{ value: "Sesudah Makan", text: "PC" },
													{ value: "Bila Diperlukan", text: "PRN" },
													{ value: "Sebelum Tidur", text: "HS" },
													{ value: "Kedua Mata", text: "OU" },
													{ value: "Kedua Telinga", text: "AU" },
													{ value: "Melalui Mulut", text: "PO" },
													{ value: "Dibawah Lidah", text: "SL" },
													{ value: "Melalui Anus", text: "PR" },
													{ value: "Melalui Vagina", text: "PV" },
													{ value: "Melalui Perawat", text: "IMM" },
												]}
												placeholder="Ket 1"
												style={{width:'100%'}}
											/>
										</Grid>
										<Grid item xs={4}>
											<ComboBox
												data={[
													{ value: "Mata Kanan", text: "OD" },
													{ value: "Mata Kiri", text: "OS" },
													{ value: "Telinga Kanan", text: "AD" },
													{ value: "Telinga Kiri", text: "AS" },
													{ value: "Penggunaan Luar", text: "OE" },
													{ value: "Sebelum Makan", text: "AC" },
													{ value: "Sesudah Makan", text: "PC" },
													{ value: "Bila Diperlukan", text: "PRN" },
													{ value: "Sebelum Tidur", text: "HS" },
													{ value: "Kedua Mata", text: "OU" },
													{ value: "Kedua Telinga", text: "AU" },
													{ value: "Melalui Mulut", text: "PO" },
													{ value: "Dibawah Lidah", text: "SL" },
													{ value: "Melalui Anus", text: "PR" },
													{ value: "Melalui Vagina", text: "PV" },
													{ value: "Melalui Perawat", text: "IMM" },
												]}
												placeholder="Ket 2"
												style={{width:'100%'}}
											/>
										</Grid>
										<Grid item xs={4}>
											<ComboBox
												data={[
													{ value: "Mata Kanan", text: "OD" },
													{ value: "Mata Kiri", text: "OS" },
													{ value: "Telinga Kanan", text: "AD" },
													{ value: "Telinga Kiri", text: "AS" },
													{ value: "Penggunaan Luar", text: "OE" },
													{ value: "Sebelum Makan", text: "AC" },
													{ value: "Sesudah Makan", text: "PC" },
													{ value: "Bila Diperlukan", text: "PRN" },
													{ value: "Sebelum Tidur", text: "HS" },
													{ value: "Kedua Mata", text: "OU" },
													{ value: "Kedua Telinga", text: "AU" },
													{ value: "Melalui Mulut", text: "PO" },
													{ value: "Dibawah Lidah", text: "SL" },
													{ value: "Melalui Anus", text: "PR" },
													{ value: "Melalui Vagina", text: "PV" },
													{ value: "Melalui Perawat", text: "IMM" },
												]}
												placeholder="Ket 3"
												style={{width:'100%'}}
											/>
											<button type="submit" style={{ display: "none" }}></button>
										</Grid>
									</Grid>
								</form>
							</Grid>
							<Grid item xs={8}>
								<FarmasiTableTransaksi
									data={this.state.data}
									total={this.state.total}
									delete={this.deleteTransaksi}
								/>
                     </Grid>
                  </Grid>
               </DialogContent>
               <DialogActions>
                  <Button onClick={this.simpan} color="primary" variant="contained">Simpan Permintaan</Button>
						<Button onClick={this.close} color="secondary" variant="contained">Tutup</Button>
               </DialogActions>
            </Dialog>
				<DialogAlert
					title="Basic Dialog"
					style={{width:'400px',height:'200px'}}
					modal
					ref={ref => this.dlg = ref}
				>
					<p style={{textAlign:'center',margin:'50px 0',fontSize:'16px'}}>The Dialog Content.</p>
				</DialogAlert>
         </div>
      );
   }

};

export default FormPermintaanFarmasi;

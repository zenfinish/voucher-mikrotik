import React from 'react';
import { notification } from 'antd';
import Dropdown from 'components/inputs/Dropdown.jsx';
import Button from 'components/inputs/Button.jsx';
import DateBox from 'components/inputs/DateBox.jsx';
import api from 'config/api.js';
import Autocomplete from 'components/Autocomplete.jsx';
import TextArea from 'components/inputs/TextArea.jsx';
import TextField from 'components/inputs/TextField.jsx';

class Index extends React.Component {

	state = {
		loading: false,

		dataCabar: [],
		dataDokter: [],
		dataPoli: [],
		dataSearchDiagnosa: [],
		dataCapul: [],
		dataRs: [],

		id_pelcabar: '',
		id_pelkelas: '',
		dokter: '',
		id_pelpoli: '',
		tgl_checkin: '',
		tgl_checkout: '',
		diagnosa_akhir: '',
		nama_diagnosa_akhir: '',
		id_pelcapul: 0,
		capul_inacbg: 0,
		id_peldafrs: 0,
		kondisi_rujuk: '',
	}

	UNSAFE_componentWillMount() {
		this.fetchData();
		this.getCabar();
		this.getKelas();
		this.getDokter();
		this.getPoli();
		this.getCapul();
		this.getRs();
	}

	fetchData = () => {
		api.get(`/pelayanan/${this.props.data.tipe === 1 ? 'jalan' : this.props.data.tipe === 2 ? 'inap' : ''}/${this.props.data.tipe === 1 ? this.props.data.id_peljalan : this.props.data.tipe === 2 ? this.props.data.id_pelinap : ''}`)
		.then(result => {
			this.setState(result.data);
		})
		.catch(error => {
			console.log(error.response)
		});
	}

	getCabar = () => {
		api.get(`/pelayanan/cabar`)
		.then(result => {
			const dataCabar = result.data.map((data) => { return { value: data.id_pelcabar, label: data.nama_pelcabar } });
			this.setState({ dataCabar: dataCabar });
		})
		.catch(error => {
			console.log(error.response)
		});
	}

	getKelas = () => {
		api.get(`/pelayanan/kelas`)
		.then(result => {
			const dataKelas = result.data.map((data) => { return { value: data.id_pelkelas, label: data.nama_pelkelas } });
			this.setState({ dataKelas: dataKelas });
		})
		.catch(error => {
			console.log(error.response)
		});
	}

	getDokter = () => {
		api.get(`/pelayanan/user/dokter`)
		.then(result => {
			const dataDokter = result.data.map((data) => { return { value: data.id_peluser, label: data.nama_peluser } });
			this.setState({ dataDokter: dataDokter });
		})
		.catch(error => {
			console.log(error.response)
		});
	}

	getPoli = () => {
		api.get(`/pelayanan/poli/data`)
		.then(result => {
			const dataPoli = result.data.map((data) => { return { value: data.id_pelpoli, label: data.nama_pelpoli, kode_bpjs: data.kode_bpjs } });
			this.setState({ dataPoli: dataPoli });
		})
		.catch(error => {
			console.log(error.response)
		});
	}

	getCapul = () => {
		api.get(`/pelayanan/capul`)
		.then(result => {
			const dataCapul = result.data.reduce(function(result, option) {
				if (option.id_pelcapul !== 8 && option.id_pelcapul !== 9 && option.id_pelcapul !== 11 && option.id_pelcapul !== 3) {
					result.push({
						value: option.id_pelcapul,
						label: option.nama_pelcapul,
						capul_inacbg: option.inacbg_kode,
					});
				}
				return result;
			}, []);
			this.setState({ dataCapul: dataCapul });
		})
		.catch(error => {
			console.log(error.response)
		});
	}

	getRs = () => {
		api.get(`/pelayanan/rs`)
		.then(result => {
			const dataRs = result.data.map((data) => { return { value: data.id_peldafrs, label: data.nama_peldafrs } });
			this.setState({ dataRs: dataRs });
		})
		.catch(error => {
			console.log(error.response)
		});
	}

	simpan = () => {
		this.setState({ loading: true }, () => {
			api.put(`/pelayanan/${this.props.data.tipe === 1 ? 'jalan' : this.props.data.tipe === 2 ? 'inap' : ''}/admmedis`, {
				id_peljalan: this.props.data.id_peljalan,
				id_pelinap: this.props.data.id_pelinap,
				id_pelcabar: this.state.id_pelcabar,
				id_pelpoli: this.state.id_pelpoli,
				tgl_checkin: this.state.tgl_checkin,
				tgl_checkout: this.state.tgl_checkout,
				dokter: this.state.dokter,
				id_pelcapul: this.state.id_pelcapul,
				id_peldafrs: this.state.id_peldafrs,
				kondisi_rujuk: this.state.kondisi_rujuk,
				diagnosa_akhir: this.state.diagnosa_akhir,
				nama_diagnosa_akhir: this.state.nama_diagnosa_akhir,
				id_pelkelas: this.state.id_pelkelas,
			})
			.then(result => {
				this.props.closeRefresh({ ...this.props.data, ...this.state });
			})
			.catch(error => {
				this.setState({ loading: false });
				notification.error({
					message: 'Error',
					description: JSON.stringify(error.response.data),
				});
			});
		});
	}

	render() {
		return (
			<div>
				<div className="grid grid-cols-6">
					{
						this.props.data.tipe === 1 ?
							<div className="pr-2 col-span-2"> {/* Cara Bayar */}
								<div className="text-sm">Cara Bayar</div>
								<Dropdown
									data={this.state.dataCabar}
									value={this.state.id_pelcabar}
									id="id_pelcabar"
									onChange={(value) => {
										this.setState({ id_pelcabar: Number(value.value) });
									}}
									className="mb-2 w-full"
								/>
							</div>
						: null
					}
				</div>
				<div className="grid grid-cols-3">
					{
						this.props.data.tipe === 2 && !this.props.belumCheckout ?
							<div className="pr-2"> {/* Kelas Daftar */}
								<div className="text-sm">Kelas Daftar</div>
								<Dropdown
									data={this.state.dataKelas}
									value={this.state.id_pelkelas}
									id="id_pelkelas"
									onChange={(value) => {
										this.setState({ id_pelkelas: Number(value.value) });
									}}
									className="mb-2 w-full"
								/>
							</div>
						: null
					}
					{
						this.props.data.tipe === 2 && !this.props.belumCheckout ?
							<div className="pr-2"> {/* Tgl. Checkout */}
								<div className="text-sm">Tgl. Checkout</div>
								<DateBox
									value={this.state.tgl_checkout.substring(0, 10)}
									onChange={(e) => { this.setState({ tgl_checkout: e.target.value }); }}
									className="mb-2 w-full"
								/>
							</div>
						: null
					}
					{
						this.props.data.tipe === 1 ?
							<>
								<div className="pr-2"> {/* Tgl. Checkin */}
									<div className="text-sm">Tgl. Checkin</div>
									<DateBox
										value={this.state.tgl_checkin.substring(0, 10)}
										onChange={(e) => { this.setState({ tgl_checkin: e.target.value }); }}
										className="mb-2 w-full"
									/>
								</div>
								<div className="pr-2"> {/* Poliklinik */}
									<div className="text-sm">Poliklinik</div>
									<Dropdown
										data={this.state.dataPoli}
										value={this.state.id_pelpoli}
										onChange={(value) => {
											this.setState({ id_pelpoli: Number(value.value) });
										}}
										className="mb-2 w-full"
									/>
								</div>
							</>
						: null
					}
					
					<div className="pr-2"> {/* Dokter DPJP */}
						<div className="text-sm">Dokter DPJP</div>
						<Dropdown
							data={this.state.dataDokter}
							value={this.state.dokter}
							id="dokter"
							onChange={(data) => { 
								this.setState({ dokter: data.value });
							}}
							className="mb-2 w-full"
						/>
					</div>

					<div> {/* Capul */}
						<div className="text-sm">Cara Pulang</div>
						{
							this.state.id_pelcapul !== 3 ?
								<>
									<Dropdown
										data={this.state.dataCapul}
										value={this.state.id_pelcapul}
										id="id_pelcapul"
										onChange={(data) => { 
											this.setState({ id_pelcapul: data.value, capul_inacbg: data.capul_inacbg });
										}}
										className="mb-2 w-full"
									/>
									<div>
										<div>Nama Rumah Sakit Rujukan</div>
										<Dropdown
											data={this.state.dataRs}
											className="w-full mb-2"
											onChange={(value) => {this.setState({ id_peldafrs: value.value })}}
											value={this.state.id_peldafrs}
											disabled={this.state.id_pelcapul !== 2}
										/>
									</div>
									<div>
										<div>Kondisi Penderita Waktu Dirujuk</div>
										<TextField
											data={this.state.data}
											className="w-full"
											onChange={(e) => {this.setState({ kondisi_rujuk: e.target.value })}}
											disabled={this.state.id_pelcapul !== 2}
											value={!this.state.kondisi_rujuk ? '' : this.state.kondisi_rujuk}
										/>
									</div>
								</>
							: <div>Rawat Inap</div>
						}
					</div>

				</div>
				<div> {/* Diagnosa */}
					<div>Diagnosa Akhir</div>
					<div className="flex">
						<Autocomplete
							className="w-16 mr-2"
							placeholder="Cari..."
							onEnter={(value) => {
								this.setState({ loadingSearchDiagnosa: true }, () => {
									api.get(`/inacbg/diagnosa/search`, {
										headers: { search: value }
									})
									.then(result => {
										const result2 = result.data.map((row) => { return { value: row[1], kode: row[1], nama: `${row[1]} - ${row[0]}` } })
										this.setState({ dataSearchDiagnosa: result2 });
									})
									.catch(error => {
										console.log('Error: ', error.response);
										this.setState({ loadingSearchDiagnosa: false });
									});
								});
							}}
							data={this.state.dataSearchDiagnosa}
							list={(row) => (
								<div>{row.nama}</div>
							)}
							onSelect={(row) => {
								this.setState({ nama_diagnosa_akhir: row.nama, diagnosa_akhir: row.kode });
							}}
							loading={this.state.loadingSearchDiagnosa}
						/>
						<TextArea
							className="mb-2 w-full"
							disabled
							value={`${this.state.nama_diagnosa_akhir}`}
						/>
					</div>
				</div>
				<div><Button onClick={this.simpan} color="primary" loading={this.state.loading}>Update</Button></div>
			</div>
		);
	}

};

export default Index;

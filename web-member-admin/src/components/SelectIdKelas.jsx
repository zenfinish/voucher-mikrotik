import React from 'react';
import TextField from '@material-ui/core/TextField';
import api from '../config/api.js';

class SelectIdKelas extends React.Component {

   state = {
      data: []
   }
   
   componentDidMount() {
      api.get(`/kelas/get-all`)
      .then(result => {
         this.setState({
            data: result.data,
         });
      })
      .catch(error => {
         console.log(error);
      });
   }
   
   render() {
      return (
         <TextField
            label="Kelas Daftar"
            style={{height: '40px'}}
            variant="outlined"
            InputLabelProps={{ shrink: true }}
            SelectProps={{ native: true }}
            fullWidth select
            onChange={this.handleChangePoli}
            name="id_kelas"
            value={this.props.value}
            InputProps={this.props.disabled === true ? { disabled: true, } : { disabled: false, }}
         >
            <option key="" value=""></option>
            {this.state.data.map(row => (
               <option key={row.id_kelas} value={row}>
                  {row.nama_kelas}
               </option>
            ))}
         </TextField>
      );
   }

}

export default SelectIdKelas;

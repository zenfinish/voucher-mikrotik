import React from 'react';

class KopSurat extends React.Component {

	render() {
		return (
			<>
				<table className="w-full text-sm">
          <tbody>
            <tr>
              <td><b>{process.env.REACT_APP_NAMA_RS}</b></td>
              <td rowSpan='3' align="right">
                <img src={require('assets/imgs/logo.png')} alt="" className="h-12 w-auto" />
              </td>
            </tr>
            <tr>
              <td>{process.env.REACT_APP_ALAMAT_RS}</td>
            </tr>
            <tr>
              <td>Telp: {process.env.REACT_APP_TELP_RS} | Email: {process.env.REACT_APP_EMAIL_RS} | Website: {process.env.REACT_APP_WEB_RS}</td>
            </tr>
          </tbody>
        </table>
        <div className="border border-black mb-3"></div> 
			</>
		)
	}

}

export default KopSurat;

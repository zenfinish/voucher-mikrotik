import React from 'react';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import { Chip, InputAdornment, IconButton } from '@material-ui/core';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';

import ListIcon from '@material-ui/icons/List';

import api from '../config/api.js';

const styles = theme => ({
   head: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
   },
   body: {
      fontSize: 14,
   },
   row: {
      '&:nth-of-type(odd)': {
         backgroundColor: theme.palette.background.default,
      },
   },
   td: {
      padding: 5,
   },
});

class FormPilihSep extends React.Component {
    
   state = {
      data: [],
   }

   cariKartu = () => {
      api.get(`bpjs/monitoring/history-percheckin/${this.props.no_bpjs}/${this.props.tgl_checkin}`, {
         headers: { token: localStorage.getItem('token') }
      })
      .then(result => {
         this.setState({
            data: result.data,
         });
      })
      .catch(error => {
         // this.props.error(error.response.data);
         console.log(error.response)
      });
   }

   getDataSep = (data) => {
      let kirim = {
         no_rujukan: data.noRujukan,
         sep: data.noSep,
         id_checkin: this.props.id_checkin,
      }
      api.post(`admmedis/updatesep/${this.props.rj}`, kirim, {
         headers: { token: localStorage.getItem('token') }
      })
      .then(result => {
         this.props.success();
      })
      .catch(error => {
         // this.props.error(error.response.data);
         console.log(error.response)
      });
   }

   handleChange = (e) => {
      this.setState({
         [e.target.name]: e.target.value,
      });
   };

   close = () => {
      this.setState({
         data: [],
      }, () => {
         this.props.close();
      });
   }

   render() {
      const { classes } = this.props;
      return (
         <div style={{overflowX: 'auto'}}>
            <Dialog
               open={this.props.open}
               onClose={this.close}
               maxWidth="lg"
               fullWidth={true}
            >
               <DialogTitle>
                  <Grid container spacing={8} style={{ marginBottom: 10 }}>
                     <Grid item xs={12}>
                        <TextField
                           label="Cari Berdasarkan No. Kartu"
                           name="no_bpjs"
                           type="text"
                           value={this.props.no_bpjs}
                           fullWidth disabled
                           onChange={this.handleChange}
                           InputLabelProps={{ shrink: true }}
                           autoComplete="off"
                           InputProps={{
                              endAdornment: (
                                 <InputAdornment position="end">
                                    <IconButton
                                       onClick={this.cariKartu}
                                       title="Cari History"
                                    ><ListIcon /></IconButton>
                                 </InputAdornment>
                              )
                           }}
                        />
                        | RJ : {this.props.rj} | ID. CHECKIN : {this.props.id_checkin}
                     </Grid>
                  </Grid>
               </DialogTitle>
               <DialogContent>
                  <div style={{ overflowX: 'auto', overflowY: 'auto' }}>
                     <table className={classes.body} style={{ width: 2800 }}>
                        <thead className={classes.head}>
                           <tr>
                              <td className={classes.td} style={{ width: 150 }} align="center">No. Kartu</td>
                              <td className={classes.td} style={{ width: 200 }} align="center">Nama Peserta</td>
                              <td className={classes.td} style={{ width: 140 }} align="center">Tgl. SEP</td>
                              <td className={classes.td} style={{ width: 140 }} align="center">Tgl. Pulang SEP</td>
                              <td className={classes.td} style={{ width: 180 }} align="center">No. SEP</td>
                              <td className={classes.td} style={{ width: 300 }} align="center">PPK Pelayanan</td>
                              <td className={classes.td} style={{ width: 150 }} align="center">Poliklinik</td>
                              <td className={classes.td} style={{ width: 180 }} align="center">No. Rujukan</td>
                              <td className={classes.td} style={{ width: 130 }} align="center">Kelas Rawat</td>
                              <td className={classes.td} style={{ width: 150 }} align="center">Jenis Pelayanan</td>
                              <td className={classes.td}>Diagnosa</td>
                           </tr>
                        </thead>
                        <tbody>
                           {
                              this.state.data.map((row, index) => (
                                 <tr className={classes.row} key={index}>
                                    <td className={classes.td} align="center">{row.noKartu}</td>
                                    <td className={classes.td} align="center">{row.namaPeserta}</td>
                                    <td className={classes.td} align="center">{row.tglSep}</td>
                                    <td className={classes.td} align="center">{row.tglPlgSep}</td>
                                    <td className={classes.td} align="center">
                                       <Chip
                                          label={row.noSep}
                                          clickable
                                          onClick={() => {this.getDataSep(row)}}
                                       />
                                    </td>
                                    <td className={classes.td} align="center">{row.ppkPelayanan}</td>
                                    <td className={classes.td} align="center">{row.poli}</td>
                                    <td className={classes.td} align="center">{row.noRujukan}</td>
                                    <td className={classes.td} align="center">{row.kelasRawat}</td>
                                    <td className={classes.td} align="center">{row.jnsPelayanan}</td>
                                    <td className={classes.td}>{row.diagnosa}</td>
                                 </tr>
                              ))
                           }
                        </tbody>
                     </table>
                  </div>
               </DialogContent>
               <DialogActions>
                  <Button onClick={this.close} color="primary">Close</Button>
               </DialogActions>
            </Dialog>
         </div>
      );
   }

};

FormPilihSep.propTypes = {
   classes: PropTypes.object.isRequired,
};

export default (withStyles(styles)(FormPilihSep));

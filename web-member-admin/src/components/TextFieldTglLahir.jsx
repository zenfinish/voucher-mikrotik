import React from 'react';
import TextField from '@material-ui/core/TextField';

class TextFieldTglLahir extends React.Component {

   render() {
      return (
         <TextField
            label="Tgl Lahir"
            name="tgl_lahir"
            type="date"
            value={this.props.value}
            InputProps={this.props.disabled === true ? { disabled: true, } : { disabled: false, }}
            onChange={this.props.onChange}
            InputLabelProps={{ shrink: true }}
            fullWidth
         />
      );
   }

}

export default TextFieldTglLahir;

import React from 'react';
import TextField from '@material-ui/core/TextField';
import api from '../config/api.js';

class SelectIdCabar extends React.Component {

   state = {
      data: [],
   }

   componentDidMount() {
      api.get(`/referensi/cabar`)
      .then(result => {
         this.setState({
            data: result.data,
         });
      })
      .catch(error => {
         console.log(error.response)
      });
   }
   
   render() {
      return (
         <TextField
            label="Cara Bayar"
            name="id_cabar"
            InputLabelProps={{ shrink: true }}
            SelectProps={{ native: true }}
            fullWidth select
            onChange={this.props.onChange}
            value={this.props.value}
         >
            <option key="" value=""></option>
            {this.state.data.map(row => (
               <option key={row.id_cabar} value={row.id_cabar}>
                  {row.nama_cabar}
               </option>
            ))}
         </TextField>
      );
   }

}

export default SelectIdCabar;
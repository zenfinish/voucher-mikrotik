import React from 'react';
import api from '../config/api.js';
import { ComboBox } from 'rc-easyui';

class SelectLakaLantasKdPropinsi extends React.Component {
   
   state = {
      data: [],
   }
   
   componentDidMount() {
      api.get(`/referensi/propinsi`)
      .then(result => {
         this.setState({
            data: result.data,
         });
      })
      .catch(error => {
         console.log(error);
      });
   }
   
   render() {
      return (
         <ComboBox
				data={this.state.data}
				value={this.props.value}
				onChange={this.props.onChange}
				disabled={this.props.disabled}
			/>
      );
   }

}

export default SelectLakaLantasKdPropinsi;

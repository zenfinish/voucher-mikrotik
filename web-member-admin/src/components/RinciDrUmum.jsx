import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

import { Paper, TextField } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import api from '../config/api.js';

const styles = theme => ({
   paper: {
      position: 'absolute',
      zIndex: 1,
      marginTop: theme.spacing.unit,
      left: 0,
      right: 0,
   },
 });

class RinciDrUmum extends React.Component {
    
   state = {
      openSearch: false,
      data: [],
      search: '',
   }

   getData = (e) => {
      e.preventDefault();
      api.get(`/dokter/search/umum/${this.state.search}`)
      .then(result => {
         this.setState({
            data: result.data,
            openSearch: true,
         });
      })
      .catch(error => {
         console.log(error)
      });
   }

   getDataBeneran = (data) => {
      this.props.getData(data);
      this.setState({
         data: [],
         openSearch: false,
         search: '',
      });
   }

   handleChange = (e) => {
      this.setState({
         [e.target.name]: e.target.value,
         data: [],
         openSearch: false,
      });
   }
   
   render() {
      return (
         <Fragment>
            <form onSubmit={this.getData}>
               <TextField
                  label="Pemeriksaan Dokter Umum"
                  style={{height: '40px'}}
                  variant="outlined"
                  InputLabelProps={{ shrink: true }}
                  SelectProps={{ native: true }}
                  fullWidth
                  onChange={this.handleChange}
                  name="search" autoComplete="off"
                  value={this.state.search}
               />
               {
                  this.state.openSearch ?
                  <div style={{ overflowX: 'auto', maxHeight: 100 }}>
                     <Paper>
                        {this.state.data.map((data, index) =>
                           <div key={index} onClick={() => {this.getDataBeneran(data)}}>{data.nama_karyawan}</div>
                        )}
                     </Paper>
                  </div> : ''
               }
            </form>
            <div style={{ overflowX: 'auto', overflowY: 'auto', height: 100 }}>
               <table border="1" style={{width: '100%'}}>
                  <thead>
                     <tr>
                        <td>No.</td>
                        <td>Nama</td>
                     </tr>
                  </thead>
                  <tbody>
                     {
                        this.props.data.map((row, index) => (
                           <tr key={index}>
                              <td align="center">{index+1}</td>
                              <td align="center">{row.nama_karyawan}</td>
                           </tr>
                        ))
                     }
                  </tbody>
               </table>
            </div>
         </Fragment>
      );
   }
}

RinciDrUmum.propTypes = {
   classes: PropTypes.object.isRequired,
};
 
 export default withStyles(styles)(RinciDrUmum);

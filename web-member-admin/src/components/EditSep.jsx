import React, { Fragment } from 'react';

import { Dialog, DialogActions, DialogContent, DialogTitle, Button, Grid } from '@material-ui/core';

import CariDiagnosa from './CariDiagnosa.jsx';
import TextFieldCatatan from './input/TextFieldCatatan.jsx';
import SelectLakaLantas from './input/SelectLakaLantas.jsx';
import TextFieldLakaLantasTglKejadian from './input/TextFieldLakaLantasTglKejadian.jsx';
import TextFieldLakaLantasKeterangan from './input/TextFieldLakaLantasKeterangan.jsx';
import SelectLakaLantasSuplesi from './input/SelectLakaLantasSuplesi.jsx';
import TextFieldLakaLantasNoSepSuplesi from './input/TextFieldLakaLantasNoSepSuplesi.jsx';
import SelectLakaLantasKdPropinsi from './input/SelectLakaLantasKdPropinsi.jsx';
import SelectLakaLantasKdKabupaten from './input/SelectLakaLantasKdKabupaten.jsx';
import SelectLakaLantasKdKecamatan from './input/SelectLakaLantasKdKecamatan.jsx';
import CheckboxLakaLantasPenjamin from './input/CheckboxLakaLantasPenjamin.jsx';

import api from '../config/api.js';

class EditSep extends React.Component {
    
   state = {
      status_checkin: '',

      nama_pasien: '',
      no_rekmedis: '',
      tgl_checkin: '',
      nama_cabar: '',
      id_cabar: '',

      sep: '',
      tgl_sep: '',
      nama_poli: '',
      asal_rujukan: '',
      ppk_rujukan: '',
      tgl_rujukan: '',
      no_rujukan: '',

      no_icd: '',
      nama_diagnosis: '',
      id_diagnosis: '',
      cariDiagnosa: '',

      hp: '',

      laka_lantas: '0',
      laka_lantas_tgl_kejadian: '',
      laka_lantas_keterangan: '',
      laka_lantas_suplesi: '0',
      laka_lantas_no_sep_suplesi: '',
      laka_lantas_kd_propinsi: '',
      laka_lantas_kd_kabupaten: '',
      laka_lantas_kd_kecamatan: '',
      laka_lantas_penjamin: [],
   }

   UNSAFE_componentWillReceiveProps(nextProps, prevState) {
      this.setState({
         status_checkin: nextProps.data.status_checkin,

         nama_pasien: nextProps.data.nama_pasien,
         no_rekmedis: nextProps.data.no_rekmedis,
         tgl_checkin: nextProps.data.tgl_checkin,
         nama_cabar: nextProps.data.nama_cabar,
         id_cabar: nextProps.data.id_cabar,

         sep: nextProps.data.noSep,
         tgl_sep: nextProps.data.tgl_checkin,
         nama_poli: nextProps.data.nama_poli,
         asal_rujukan: nextProps.data.asal_rujukan,
         ppk_rujukan: nextProps.data.ppk_rujukan,
         tgl_rujukan: nextProps.data.tgl_rujukan,
         no_rujukan: nextProps.data.no_rujukan,

         hp: nextProps.data.hp,
      });
   }

   handleChange = (e) => {
      if (e.target.name === 'laka_lantas_penjamin') {
         let arr = this.state.laka_lantas_penjamin;
         if (e.target.checked) {
            arr.push(e.target.value);
         } else {
            let index = arr.indexOf(e.target.value);
            arr.splice(index, 1);
         }
      } else {
         this.setState({
            [e.target.name]: e.target.value,
         });
      }
   }
      

   update = () => {
      api.put(`/bpjs/sep`, { ...this.state, laka_lantas_penjamin: this.state.laka_lantas_penjamin.join() })
      .then(result => {
         this.props.success('Data berhasil di update');
      })
      .catch(error => {
         this.props.error(error.response.data);
      });
   }

   getDataDiagnosa = (data) => {
      api.get(`/diagnosa/get-id-diagnosis/${data.kode}/${data.nama}`)
      .then(result => {
         this.setState({
            no_icd: data.kode,
            nama_diagnosis: data.nama,
            id_diagnosis: result.data.id,
            cariDiagnosa: `${data.kode} | ${data.nama}`
         });
      })
      .catch(error => {
         console.log(error.response.data)
      });
   }

   render() {
      return (
         <div style={{overflowX: 'auto'}}>
            <Dialog
               open={this.props.open}
               onClose={this.props.close}
               maxWidth="lg"
               fullWidth={true}
            >
               <DialogTitle>Edit SEP {this.state.bpjs_sep}</DialogTitle>
               <DialogContent>
                  <br />
                  <Grid container spacing={8} style={{ marginBottom: 10 }}>
                     <Grid item xs={6}>
                        <CariDiagnosa value={this.state.cariDiagnosa} getDataDiagnosa={this.getDataDiagnosa} />
                     </Grid>
                     <Grid item xs={6}>
                        <TextFieldCatatan value={this.state.catatan} onChange={this.handleChange} />
                     </Grid>
                  </Grid>
                  <Grid container spacing={8} style={{ marginBottom: 10 }}>
                     <Grid item xs={3}>
                        <SelectLakaLantas value={this.state.laka_lantas} onChange={this.handleChange} />
                     </Grid>
                  </Grid>
                  {
                     this.state.laka_lantas === '1' ?
                     <Fragment>
                        <Grid container spacing={8} style={{ marginBottom: 10 }}>
                           <Grid item xs={3}>
                              <TextFieldLakaLantasTglKejadian value={this.state.laka_lantas_tgl_kejadian} onChange={this.handleChange} />
                           </Grid>
                           <Grid item xs={3}>
                              <TextFieldLakaLantasKeterangan value={this.state.laka_lantas_keterangan} onChange={this.handleChange} />
                           </Grid>
                           <Grid item xs={3}>
                              <SelectLakaLantasSuplesi value={this.state.laka_lantas_suplesi} onChange={this.handleChange} />
                           </Grid>
                        </Grid>
                        {
                           this.state.laka_lantas_suplesi === '1' ?
                           <Fragment>
                              <Grid container spacing={8} style={{ marginBottom: 10 }}>
                                 <Grid item xs={3}>
                                    <TextFieldLakaLantasNoSepSuplesi value={this.state.laka_lantas_no_sep_suplesi} onChange={this.handleChange} />
                                 </Grid>
                                 <Grid item xs={3}>
                                    <SelectLakaLantasKdPropinsi value={this.state.laka_lantas_kd_propinsi} onChange={this.handleChange} />
                                 </Grid>
                                 <Grid item xs={3}>
                                    <SelectLakaLantasKdKabupaten value={this.state.laka_lantas_kd_kabupaten} onChange={this.handleChange} />
                                 </Grid>
                                 <Grid item xs={3}>
                                    <SelectLakaLantasKdKecamatan value={this.state.laka_lantas_kd_kecamatan} onChange={this.handleChange} />
                                 </Grid>
                              </Grid>
                              <Grid container spacing={8} style={{ marginBottom: 10 }}>
                                 <Grid item xs={12}>
                                    <CheckboxLakaLantasPenjamin onChange={this.handleChange} />
                                 </Grid>
                              </Grid>
                           </Fragment> : ''
                        }
                     </Fragment> : ''
                  }
               </DialogContent>
               <DialogActions>
                  <Button onClick={this.update} color="primary">Update</Button>
                  <Button onClick={this.props.close} color="primary">Close</Button>
               </DialogActions>
            </Dialog>
         </div>
      );
   }

};

export default EditSep;
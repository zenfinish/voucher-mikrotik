import React from 'react';
import { Modal as ModalAntd, Skeleton } from 'antd';

class Modal extends React.Component {

	render() {
		const { visible, onClose, footer, header, children, size, loading } = this.props;
		return (
			<ModalAntd
				visible={visible}
				onCancel={onClose}
				footer={!footer ? null : footer}
				title={header}
				width={size === "lg" ? "100%" : size === "md" ? "50%" : size === "sm" ? 300 : "50%"}
				centered
			>
				{
					loading ?
						<Skeleton active />
					: children
				}
			</ModalAntd>
		)
	}

}

export default Modal;

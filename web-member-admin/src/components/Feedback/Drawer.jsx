import React from 'react';
import { Drawer as DrawerAntd, Skeleton} from 'antd';

class Drawer extends React.Component {

	render() {
		const { visible, onClose, footer, header, children, onVisible, loading } = this.props;
		return (
			<DrawerAntd
				width="100%"

        // visible
				visible={visible}

        // onClose
				onClose={onClose}

        // footer
				footer={footer}

        // header
				title={header}

        // onVisible
        afterVisibleChange={(visible) => {
          if (visible) {
            onVisible();
          };
        }}

			>
				{
					loading ?
						<Skeleton active />
					: children
				}
			</DrawerAntd>
		)
	}

}

export default Drawer;

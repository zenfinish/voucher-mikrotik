import React from 'react';
import TextField from '@material-ui/core/TextField';

class TextFieldTglSep extends React.Component {

   render() {
      return (
         <TextField
            label="Tgl Sep"
            name="tgl_sep"
            type="date" variant="outlined"
            fullWidth
            onChange={this.props.onChange}
            InputLabelProps={{ shrink: true }}
            style={{height: '40px'}}
            value={this.props.value}
         />
      );
   }

}

export default TextFieldTglSep;

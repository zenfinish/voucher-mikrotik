import React from 'react';
import PropTypes from 'prop-types';
import ReactToPrint from 'react-to-print';

import { withStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import Table from '@material-ui/core/Table';
import TableCell from '@material-ui/core/TableCell';
import TableBody from '@material-ui/core/TableBody';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

import FormRujukan from './FormRujukan.jsx';

const CustomTableCell = withStyles(theme => ({
   head: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
   },
   body: {
      fontSize: 12,
   },
}))(TableCell);

const styles = theme => ({
   table: {
      minWidth: 700,
   },
   row: {
      '&:nth-of-type(odd)': {
         backgroundColor: theme.palette.background.default,
      },
   },
});

class CetakRujukan extends React.Component {
   render() {
      const { classes } = this.props;
      return (
         <div style={{overflowX: 'auto'}}>
            <Dialog
               open={true}
               onClose={this.props.closeCetakRujukan}
               maxWidth="lg"
               fullWidth={true}
            >
               <DialogTitle>Data Berhasil Disimpan</DialogTitle>
               <DialogContent>
                  <table>
                     <tbody>
                        <tr>
                           <td>No. Rekmedis</td>
                           <td>:</td>
                           <td>{Object.keys(this.props.dataRujukan).length === 0 ? '' : this.props.dataRujukan.peserta.noMr}</td>
                        </tr>
                        <tr>
                           <td>Nama Pasien</td>
                           <td>:</td>
                           <td>{Object.keys(this.props.dataRujukan).length === 0 ? '' : this.props.dataRujukan.peserta.nama}</td>
                        </tr>
                        <tr>
                           <td>Action</td>
                           <td>:</td>
                           <td>
                              <ReactToPrint
                                 trigger={() => <button>Cetak Rujukan</button>}
                                 content={() => this.componentRef}
                                 pageStyle="@page { size: A5 landscape}"
                              />
                           </td>
                        </tr>
                     </tbody>
                  </table>
                  <div>
                     <Table className={classes.table}>
                        <TableHead>
                           <TableRow>
                              <CustomTableCell align="center">Tgl. Checkin</CustomTableCell>
                              <CustomTableCell align="center">Poli</CustomTableCell>
                              <CustomTableCell align="center">Cara Bayar</CustomTableCell>
                              <CustomTableCell align="center">Diagnosa</CustomTableCell>
                              <CustomTableCell align="center">Dokter</CustomTableCell>
                              <CustomTableCell align="center">No. Rujukan</CustomTableCell>
                              <CustomTableCell align="center">Asal Rujukan</CustomTableCell>
                              <CustomTableCell align="center">Tgl Rujukan</CustomTableCell>
                              <CustomTableCell align="center">No. Rujukan</CustomTableCell>
                              <CustomTableCell align="center">Ppk Rujukan</CustomTableCell>
                           </TableRow>
                        </TableHead>
                        <TableBody>
                           <TableRow className={classes.row} key="">
                              <CustomTableCell align="center">Tgl. Checkin</CustomTableCell>
                              <CustomTableCell align="center">Poli</CustomTableCell>
                              <CustomTableCell align="center">Cara Bayar</CustomTableCell>
                              <CustomTableCell align="center">Diagnosa</CustomTableCell>
                              <CustomTableCell align="center">Dokter</CustomTableCell>
                              <CustomTableCell align="center">No. Rujukan</CustomTableCell>
                              <CustomTableCell align="center">Asal Rujukan</CustomTableCell>
                              <CustomTableCell align="center">Tgl Rujukan</CustomTableCell>
                              <CustomTableCell align="center">No. Rujukan</CustomTableCell>
                              <CustomTableCell align="center">Ppk Rujukan</CustomTableCell>
                           </TableRow>
                        </TableBody>
                     </Table>
                  </div>
               </DialogContent>
               <DialogActions>
                  <Button onClick={this.props.closeCetakRujukan} color="primary">Close</Button>
               </DialogActions>
            </Dialog>
            <FormRujukan ref={el => (this.componentRef = el)} dataRujukan={this.props.dataRujukan} tipeRujukan={this.props.tipeRujukan} />
         </div>
      );
   }

};

CetakRujukan.propTypes = {
   classes: PropTypes.object.isRequired,
};

export default (withStyles(styles)(CetakRujukan));

import React from 'react';
import { Grid, Card, CardActionArea, CardContent, Typography } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

import api from 'config/api.js';
import Edit from './Edit.jsx';
import TambahCheckout from './TambahCheckout.jsx';
import TambahRuangan from './TambahRuangan';
import Icon from 'components/Icon.jsx';

const styles = theme => ({
	root: {
		maxWidth: 345,
	},
});

class Index extends React.Component {

	state = {
		menus: [],
		loading: false,
		openEdit: false,
		aktif: {},
		jml_checkout: null,
		openCheckout: false,
		openRuangan: false,
	}

	componentDidMount() {
		this.fetchApi();
		this.fetchCheckout();
	}

	fetchApi = () => {
		this.setState({ loading: true }, () => {
			api.get(`/module/sub/${this.props.data.id_mdl}/countuser`)
			.then(result => {
				this.setState({ menus: result.data, loading: false });
			})
			.catch(error => {
				console.log('Error: ', error.response);
				this.setState({ loading: false });
			});
		});
	}

	fetchCheckout = () => {
		api.get(`/farmasi/hak/checkout/count`)
		.then(result => {
			this.setState({ jml_checkout: result.data.jml });
		})
		.catch(error => {
			console.log('Error: ', error.response);
		});
	}
	
	render() {
		const { classes } = this.props;
		return (
			<>
				<h3>Jumlah Orang Pemakai Module {this.props.data.nama}</h3>
				<Grid container spacing={3}>
				{
					this.state.menus.map((row, i) => (
						<Grid item sm={2} key={i}>
							<Card className={classes.root} onClick={() => {
								this.setState({ aktif: row, openEdit: true });
							}}>
								<CardActionArea>
									<CardContent style={{ textAlign: 'center' }}>
										<Icon name={row.icon} />
										<Typography gutterBottom variant="h5" component="h2">{row.jml}</Typography>
										<Typography variant="body2" color="textSecondary" component="p">{row.nama_mdlsub}</Typography>
									</CardContent>
								</CardActionArea>
							</Card>
						</Grid>
					))
				}
				{
					this.props.data.id_mdl === '6' ?
						<Grid item sm={2}>
							<Card className={classes.root} onClick={() => {
								this.setState({ openCheckout: true });
							}}>
								<CardActionArea>
									<CardContent style={{ textAlign: 'center' }}>
										<Icon name="EditColor">edit</Icon>
										<Typography gutterBottom variant="h5" component="h2">{this.state.jml_checkout}</Typography>
										<Typography variant="body2" color="textSecondary" component="p">Tambah Checkout</Typography>
									</CardContent>
								</CardActionArea>
							</Card>
						</Grid>
					: null
				}
					<Grid item sm={2}>
						<Card className={classes.root} onClick={() => {
							this.setState({ openRuangan: true });
						}}>
							<CardActionArea>
								<CardContent style={{ textAlign: 'center' }}>
									<Typography variant="body2" color="textSecondary" component="p">Ruangan</Typography>
								</CardContent>
							</CardActionArea>
						</Card>
					</Grid>
				</Grid>
				
				{
					this.state.openEdit ?
					<Edit
						data={this.state.aktif} close={() => {
							this.setState({ openEdit: false });
							this.fetchApi();
						}}
					/> : null
				}
				{
					this.state.openCheckout ?
					<TambahCheckout
						close={() => {
							this.setState({ openCheckout: false });
							this.fetchCheckout();
						}}
					/> : null
				}
				{
					this.state.openRuangan ?
					<TambahRuangan
						close={() => {
							this.setState({ openRuangan: false });
						}}
					/> : null
				}

			</>
		)
	}

};

export default withStyles(styles)(Index);

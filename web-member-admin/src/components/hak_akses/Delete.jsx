import React from 'react';
import { notification, Modal } from 'antd';
import { Button, CircularProgress } from '@material-ui/core';
import SaveIcon from '@material-ui/icons/Save';
import api from 'config/api.js';

class Delete extends React.Component {
	
	state = {
		data: [],

		id_peluser: '',
		nama_peluser: '',
		searchUser: '',
		loadingSearchUser: false,
		optionSearchUser: [],
	}

	componentDidMount() {
		this.fetchApi();
	}

	fetchApi = () => {
		api.get(`/farmasi/hak/${this.props.data.id_mdlsub}`)
		.then(result => {
			this.setState({ data: result.data });
		})
		.catch(error => {
			console.log(error);
		});
	}
	
	render() {
		return (
			<>
				<Modal
					centered
					width="100%"
					visible={true}
					onCancel={this.props.close}
					footer={[
						<div style={{ position: 'relative' }} key="simpan">
							<Button variant="outlined" color="primary"
								onClick={() => {
									this.setState({ disabledSave: true }, () => {
										api.delete(`/farmasi/hak/${this.props.data.id_mdlhak}`)
										.then(result => {
											this.setState({
												disabledSave: false,
											}, () => {
												this.props.close();
											});
										})
										.catch(error => {
											notification.error({
												message: 'Error',
												description: JSON.stringify(error.response.data),
										 });
											this.setState({
												disabledSave: false,
											});
										});
									});
							}} disabled={this.state.disabledSave} startIcon={<SaveIcon />}>Hapus</Button>
							{
								this.state.disabledSave &&
								<CircularProgress size={24}
									style={{ position: 'absolute', right: '35%', top: '20%' }}
								/>
							}
						</div>
					]}
					title={`Yakin Delete ?`}
				>
						<table style={{ whiteSpace: 'nowrap' }} cellPadding={5}>
							<tbody>
								<tr>
									<td>Nama User</td>
									<td>:</td>
									<td>{this.props.data.nama_peluser} [{this.props.data.id_peluser}]</td>
								</tr>
							</tbody>
						</table>
				</Modal>
			</>
		)
	}

};

export default (Delete);

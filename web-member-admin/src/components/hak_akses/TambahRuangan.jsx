import React from 'react';
import { notification, Modal, Select, Table, Button } from 'antd';
import api from 'config/api.js';

const { Option } = Select;

class TambahRuangan extends React.Component {
	
	state = {
		dataHakRuangan: [],
        dataPoli: [],
        dataUser: [],

		id_peluser: null,
        id_pelruangan: null,

		loading: false,
	}

	componentDidMount() {
		// this.fetchData();
		this.getUser();
        this.getRuangan();
		// console.log(this.props.data)
	}
  
	fetchData = () => {
		const { id_pelruangan } = this.state;
		this.setState({ loading: true }, () => {
			api.get(`/hak/ruangan?id_pelruangan=${id_pelruangan}`)
			.then(result => {
				this.setState({ dataHakRuangan: result.data, loading: false });
			})
			.catch(error => {
				console.log(error.response.data);
				this.setState({ loading: false });
			});
		});
	}

	getUser = () => {
		api.get(`/pelayanan/user/all`)
		.then(result => {
			const dataUser = result.data;
            this.setState({ dataUser });
		})
		.catch(error => {
			console.log(error.response)
		});
	}

	getRuangan = () => {
		api.get(`/pelayanan/ruangan`)
		.then(result => {
			const dataPoli = result.data;
            this.setState({ dataPoli });
		})
		.catch(error => {
			console.log(error.response)
		});
	}

	hapus = (id_hakruangan) => {
		this.setState({ loading: true }, () => {
            api.delete(`/hak/ruangan/${id_hakruangan}`)
            .then(result => {
				this.fetchData();
            })
            .catch(error => {
                notification.error({
                    message: 'Error',
                    description: JSON.stringify(error.response.data),
                });
                this.setState({ loading: false });
            });
        });
	}

	simpan = () => {
        const { id_peluser, id_pelruangan } = this.state;

		this.setState({ loading: true }, () => {
            api.post(`/hak/ruangan`, {
                id_peluser,
                id_pelruangan,
            })
            .then(result => {
                this.fetchData();
            })
            .catch(error => {
                notification.error({
                    message: 'Error',
                    description: JSON.stringify(error.response.data),
                });
                this.setState({ loading: false });
            });
        });
    }
			
	render() {
		return (
			<Modal
				centered
				visible={true}
				onCancel={this.props.close}
				width="80%"
				footer={false}
				title={`Hak Akses Ruangan`}
			>
				<div className="flex mb-1">
                    <div className="mr-2 w-full"> {/* User */}
                        <label>User</label>
						<Select
							onChange={(id_peluser) => this.setState({ id_peluser })}
							className="w-full"
							placeholder="-Pilih-"
							value={this.state.id_peluser}
							showSearch
							optionFilterProp="children"
							filterOption={(input, option) =>
								option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
							}
							filterSort={(optionA, optionB) =>
								optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
							}
						>
							{this.state.dataUser.map((row, i) => (<Option value={row.id_peluser} key={i}>
								<div className="flex justify-between">
									<span>{row.nama_peluser}</span>
									<span>{row.email}</span>
								</div>
							</Option>))}
						</Select>
                    </div>
                    <div className="mr-2 w-full"> {/* Ruangan */}
                        <label>Ruangan</label>
						<Select
							onChange={(id_pelruangan) => {
								this.setState({ id_pelruangan }, () => {
									this.fetchData();
								})
							}}
							className="w-full"
							placeholder="-Pilih-"
							value={this.state.id_pelruangan}
							showSearch
							optionFilterProp="children"
							filterOption={(input, option) =>
								option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
							}
							filterSort={(optionA, optionB) =>
								optionA.children.toLowerCase().localeCompare(optionB.children.toLowerCase())
							}
						>
							{this.state.dataPoli.map((row, i) => (<Option value={row.id_pelruangan} key={i}>{row.nama_pelruangan}</Option>))}
						</Select>
                    </div>
					<div>
						<div className="text-white">TEST</div>
						<Button
							onClick={this.simpan}
							disabled={this.state.loading}
						>Simpan</Button>
					</div>
				</div>
				<Table
					dataSource={this.state.dataHakRuangan}
					size="small"
					pagination={false}
					rowKey={(row) => row.id_hakruangan}
					columns={[
						{ title: 'Nama User', dataIndex: 'nama_peluser' },
						{ title: 'Email', dataIndex: 'email' },
						{ title: 'Ruangan', dataIndex: 'nama_pelruangan' },
						{ title: 'Action', render: (dataIndex, row) => <Button color="primary" onClick={() => this.hapus(row.id_hakruangan)}>Hapus</Button> },
					]}
					bordered
				/>
			</Modal>
		)
	}

};

export default TambahRuangan;

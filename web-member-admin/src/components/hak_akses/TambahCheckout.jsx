import React from 'react';
import { Dialog, DialogTitle, DialogContent, DialogActions, IconButton, Divider, Button, CircularProgress, Tooltip, TextField, Icon } from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
import CloseIcon from '@material-ui/icons/Close';
import SaveIcon from '@material-ui/icons/Save';

import api from 'config/api.js';
import Alert  from 'components/Alert.jsx';
import DeleteCheckout from './DeleteCheckout.jsx';

class TambahCheckout extends React.Component {
	
	state = {
		data: [],

		id_peluser: '',
		nama_peluser: '',
		searchUser: '',
		loadingSearchUser: false,
		optionSearchUser: [],

		openDelete: false,
		dataDelete: {},

		text: '',
		variant: '',
		openAlert: false,
	}

	componentDidMount() {
		this.fetchApi();
	}

	fetchApi = () => {
		api.get(`/farmasi/hak/checkout/all`)
		.then(result => {
			this.setState({ data: result.data });
		})
		.catch(error => {
			console.log(error);
		});
	}
	
	render() {
		return (
			<Dialog open={true} maxWidth="xl" fullWidth scroll="paper">
				<DialogTitle>
					<span style={{ fontFamily: 'Gadugib' }}>Detail Hak Tambah Checkout</span>
					<IconButton style={{
						position: 'absolute',
						color: 'grey',
						right: 5,
						top: 5,
					}} onClick={this.props.close} disabled={this.state.disabledSave}><CloseIcon /></IconButton>
					<Divider style={{ marginTop: 10 }} />
				</DialogTitle>
				<DialogContent>
					<table style={{ whiteSpace: 'nowrap', borderCollapse: 'collapse' }} cellPadding={5} border="1">
						<thead>
							<tr>
								<th>Tgl</th>
								<th>Nama User</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							{
								this.state.data.map((row, i) => (
									<tr key={i}>
										<td>{row.tgl}</td>
										<td>{row.nama_karyawan} [{row.id_peluser}]</td>
										<td>
											<Tooltip title="Hapus">
												<IconButton
													color="secondary"
													style={{
														padding: 5, minHeight: 0, minWidth: 0
													}}
													onClick={() => {
														this.setState({
															dataDelete: row,
															openDelete: true
														});
													}}
												><Icon fontSize="small">delete_forever_outlined</Icon></IconButton>
											</Tooltip>
										</td>
									</tr>
								))
							}
						</tbody>
					</table>
				</DialogContent>
				<DialogActions>
					<Autocomplete
						options={this.state.optionSearchUser}
						getOptionLabel={option => `${option.nama_karyawan} [${option.karyawan}]`}
						style={{ width: 300 }}
						onChange={(e, value) => {
							if (value) {
								this.setState({
									id_peluser: value.karyawan,
									nama_peluser: value.nama_karyawan,
								});
							}
						}}
						loading={this.state.loadingSearchUser}
						loadingText="Please Wait..."
						inputValue={this.state.searchUser}
						renderInput={params => (
							<Tooltip
								title="Berdasarkan Nama User / Id User (Min. 3 Karakter)"
							>
								<TextField
									{...params} fullWidth
									label="Cari User.."
									variant="outlined"
									size="small"
									onKeyPress={(e) => {
										if (e.key === 'Enter' && this.state.searchUser.length >= 3) {
											this.setState({ loadingSearchUser: true }, () => {
												api.get(`/user/karyawan/search`, {
													headers: { search: this.state.searchUser }
												})
												.then(result => {
													this.setState({
														optionSearchUser: result.data,
														loadingSearchUser: false
													});
												})
												.catch(error => {
													this.setState({ loadingSearchUser: false });
												});
											});
										}
									}}
									onChange={(e) => {
										this.setState({ searchUser: e.target.value });
									}}
									InputProps={{
										...params.InputProps,
										endAdornment: (
											<React.Fragment>
												{this.state.loadingSearchUser ? <CircularProgress color="inherit" size={20} /> : null}
												{params.InputProps.endAdornment}
											</React.Fragment>
										),
									}}
								/>
							</Tooltip>
						)}
					/>
					<TextField
						label="Nama User.."
						variant="outlined"
						size="small"
						disabled
						value={`${this.state.nama_peluser} [${this.state.id_peluser}]`}
						style={{ width: 400 }}
					/>
					<div style={{ position: 'relative' }}>
						<Button variant="outlined" color="primary"
							onClick={() => {
							if (this.state.id_peluser === '' || this.state.id_peluser === undefined) {
								this.showAlert('Nama User Masih Kosong', 'error');
							} else {
								this.setState({ disabledSave: true }, () => {
									api.post(`/farmasi/hak/checkout`, {
										id_peluser: this.state.id_peluser,
									})
									.then(result => {
										this.setState({
											disabledSave: false,
											searchUser: '',
											nama_peluser: '',
											id_peluser: '',
										}, () => {
											this.fetchApi();
										});
									})
									.catch(error => {
										this.showAlert(JSON.stringify(error.response.data), 'error');
										this.setState({
											disabledSave: false,
											searchUser: '',
											nama_peluser: '',
											id_peluser: '',
										});
									});
								});
							}
						}} disabled={this.state.disabledSave} startIcon={<SaveIcon />}>Tambah</Button>
						{
							this.state.disabledSave &&
							<CircularProgress size={24}
								style={{ position: 'absolute', right: '35%', top: '20%' }}
							/>
	 					}
					</div>
				</DialogActions>
				{
					this.state.openDelete ?
					<DeleteCheckout data={this.state.dataDelete} close={() => {
						this.setState({ openDelete: false });
						this.fetchApi();
					}} /> : null
				}
				<Alert
					open={this.state.openAlert}
					text={this.state.text}
					variant={this.state.variant}
					close={() => {this.setState({ openAlert: false })}}
				/>
			</Dialog>
		)
	}

	showAlert = (text, variant) => {
		this.setState({
			text: text,
			variant: variant,
			openAlert: true,
		});
	}

};

export default (TambahCheckout);

import React from 'react';
import { notification, Modal } from 'antd';

import { IconButton, Button, CircularProgress, Tooltip, TextField } from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
import SaveIcon from '@material-ui/icons/Save';
import DeleteForeverOutlinedIcon from '@material-ui/icons/DeleteForeverOutlined';

import api from 'config/api.js';
import Delete from './Delete.jsx';

class Edit extends React.Component {
	
	state = {
		data: [],

		id_peluser: '',
		nama_peluser: '',
		searchUser: '',
		loadingSearchUser: false,
		optionSearchUser: [],

		openDelete: false,
		dataDelete: {},
	}

	componentDidMount() {
		this.fetchApi();
	}

	fetchApi = () => {
		api.get(`/farmasi/hak/${this.props.data.id_mdlsub}`)
		.then(result => {
			this.setState({ data: result.data });
		})
		.catch(error => {
			console.log(error);
		});
	}
	
	render() {
		return (
			<>
				<Modal
					centered
					width="100%"
					visible={true}
					onCancel={this.props.close}
					footer={[
						<div key="simpan">
							<Autocomplete
								options={this.state.optionSearchUser}
								getOptionLabel={option => `${option.nama_peluser} [${option.id_peluser}]`}
								style={{ width: 300 }}
								onChange={(e, value) => {
									if (value) {
										this.setState({
											id_peluser: value.id_peluser,
											nama_peluser: value.nama_peluser,
										});
									}
								}}
								loading={this.state.loadingSearchUser}
								loadingText="Please Wait..."
								inputValue={this.state.searchUser}
								renderInput={params => (
									<Tooltip
										title="Berdasarkan Nama User / Id User (Min. 3 Karakter)"
									>
										<TextField
											{...params} fullWidth
											label="Cari User.."
											variant="outlined"
											size="small"
											onKeyPress={(e) => {
												if (e.key === 'Enter' && this.state.searchUser.length >= 3) {
													this.setState({ loadingSearchUser: true }, () => {
														api.get(`/pelayanan/user/search`, {
															headers: { search: this.state.searchUser }
														})
														.then(result => {
															this.setState({
																optionSearchUser: result.data,
																loadingSearchUser: false
															});
														})
														.catch(error => {
															console.log(error.response)
															this.setState({ loadingSearchUser: false });
														});
													});
												}
											}}
											onChange={(e) => {
												this.setState({ searchUser: e.target.value });
											}}
											InputProps={{
												...params.InputProps,
												endAdornment: (
													<React.Fragment>
														{this.state.loadingSearchUser ? <CircularProgress color="inherit" size={20} /> : null}
														{params.InputProps.endAdornment}
													</React.Fragment>
												),
											}}
										/>
									</Tooltip>
								)}
							/>
							<TextField
								label="Nama User.."
								variant="outlined"
								size="small"
								disabled
								value={`${this.state.nama_peluser} [${this.state.id_peluser}]`}
								style={{ width: 400 }}
							/>
							<div style={{ position: 'relative' }}>
								<Button variant="outlined" color="primary"
									onClick={() => {
									if (this.state.id_peluser === '' || this.state.id_peluser === undefined) {
										notification.error({
											message: 'Error',
											description: 'Nama User Masih Kosong',
									 });
									} else {
										this.setState({ disabledSave: true }, () => {
											api.post(`/farmasi/hak`, {
												id_peluser: this.state.id_peluser,
												id_mdlsub: this.props.data.id_mdlsub,
											})
											.then(result => {
												this.setState({
													disabledSave: false,
													searchUser: '',
													nama_peluser: '',
													id_peluser: '',
												}, () => {
													this.fetchApi();
												});
											})
											.catch(error => {
												notification.error({
													message: 'Error',
													description: JSON.stringify(error.response.data),
												});
												this.setState({
													disabledSave: false,
													searchUser: '',
													nama_peluser: '',
													id_peluser: '',
												});
											});
										});
									}
								}} disabled={this.state.disabledSave} startIcon={<SaveIcon />}>Tambah</Button>
								{
									this.state.disabledSave &&
									<CircularProgress size={24}
										style={{ position: 'absolute', right: '35%', top: '20%' }}
									/>
								}
							</div>
						</div>
					]}
					title={`Detail Hak Akses`}
				>
					<table style={{ whiteSpace: 'nowrap' }} cellPadding={5}>
						<tbody>
							<tr>
								<td>Nama Module</td>
								<td>:</td>
								<td>{this.props.data.nama_mdlsub}</td>
							</tr>
						</tbody>
					</table>
					<table style={{ whiteSpace: 'nowrap', borderCollapse: 'collapse' }} cellPadding={5} border="1">
						<thead>
							<tr>
								<th className="border border-black">Tgl</th>
								<th className="border border-black">Nama User</th>
								<th className="border border-black">Email</th>
								<th className="border border-black">Action</th>
							</tr>
						</thead>
						<tbody>
							{
								this.state.data.map((row, i) => (
									<tr key={i}>
										<td className="border border-black">{row.tgl}</td>
										<td className="border border-black">{row.nama_peluser} [{row.id_peluser}]</td>
										<td className="border border-black">{row.email}</td>
										<td className="border border-black">
											<Tooltip title="Hapus">
												<IconButton
													color="secondary"
													style={{
														padding: 5, minHeight: 0, minWidth: 0
													}}
													onClick={() => {
														this.setState({
															dataDelete: row,
															openDelete: true
														});
													}}
												><DeleteForeverOutlinedIcon /></IconButton>
											</Tooltip>
										</td>
									</tr>
								))
							}
						</tbody>
					</table>
				</Modal>

				{
					this.state.openDelete ?
					<Delete
						data={this.state.dataDelete} close={() => {
							this.setState({ openDelete: false });
							this.fetchApi();
						}}
					/> : null
				}
				
			</>
		)
	}

};

export default (Edit);

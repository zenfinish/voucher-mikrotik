import React from 'react';

class Label extends React.Component {

	render() {
		return (
      <span
        className={`
          px-2 rounded text-sm border border-black
          ${this.props.className}
          ${this.props.color === 'primary' ? 'bg-blue-900 text-white' : ''}
        `}
        onClick={this.props.onClick}
      >{this.props.children}</span>
		)
	}

}

export default Label;

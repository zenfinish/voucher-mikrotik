import React, { Component } from 'react';
import Select from 'react-select';
import api from '../config/api.js';

export default class CariDiagnosa3 extends Component {
	
	state = {
		data: [],
		value: '',
	};

	handleInputChange = (e) => {
		if (e.keyCode === 13 && this.state.value !== '') {
			api.get(`admmedis/referensi/diagnosa/${this.state.value}`)
			.then(result => {
				let hasil = [];
				result.data.forEach(row => {
					hasil.push({
						value: row.no_icd,
						label: row.no_icd + ' | ' + row.nama_diagnosa,
						name: row.nama_diagnosa
					});
				});
				this.setState({
					value: '',
					data: hasil
				});
			})
			.catch(error => {
				this.props.error(JSON.stringify(error.response.data));
			});
		}
	};

	onChange = (data) => {
		api.get(`referensi/diagnosa/insert/${data.value}/${data.name}`)
      .then(result => {
         this.setState({
            open: false,
         }, () => {
            this.props.getDataDiagnosa(data);
         });
      })
      .catch(error => {
         console.log(error.response)
      });
	}

	render() {
		return (
			<Select
				onKeyDown={this.handleInputChange}
				options={this.state.data}
				placeholder="Cari Diagnosa"
				onInputChange={(e) => {this.setState({ value: e, data: [] })}}
				onChange={this.onChange}
				inputValue={this.state.value}
			/>
		);
	}
	
}

import React from 'react';
import TextField from '@material-ui/core/TextField';

class TextFieldIdIdentitas extends React.Component {

   render() {
      return (
         <TextField
            label="NIK"
            name="id_identitas"
            type="text"
            value={this.props.value}
            fullWidth
            InputProps={this.props.disabled === true ? { disabled: true, } : { disabled: false, }}
            onChange={this.props.onChange}
            InputLabelProps={{ shrink: true }}
         />
      );
   }

}

export default TextFieldIdIdentitas;

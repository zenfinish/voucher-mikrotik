import React from 'react';
import Select from 'react-select';
import api from '../config/api.js';

class SelectIdCapul extends React.Component {
   
   state = {
      data: []
   }

   componentDidMount() {
      api.get(`/referensi/capul`)
      .then(result => {
         let hasil = [];
         result.data.forEach(row => {
            hasil.push({ value: row.id_capul, label: row.nama_capul, target: { value: row.id_capul, name: 'id_capul' } });
         });
         this.setState({
            data: hasil
         });
      })
      .catch(error => {
         console.log(error.response)
      });
   }

   render() {
      return (
         <Select
            onChange={this.props.onChange}
            value={this.state.data.filter(option => option.value === this.props.value)}
            options={this.state.data}
            placeholder="Cara Pulang"
            isDisabled={this.props.disabled}
         />
      );
   }

}

export default (SelectIdCapul);

import React from 'react';
import socketIOClient from "socket.io-client";
import ReactAudioPlayer from 'react-audio-player';
import Button from 'components/inputs/Button.jsx';
import Card from 'components/Card.jsx';

class Notif extends React.Component {

	state = {
		notifikasi: [],
		socket: socketIOClient(process.env.REACT_APP_SERVER_URL),
		soundNotif: false,
	}

	componentWillUnmount() {
		this.state.socket.disconnect();
	}
  
	UNSAFE_componentWillMount() {
		this.props.id.map((row) => {
			return this.state.socket.on(row, result => {
				let datas = this.state.notifikasi;
				datas.push(result);
				this.setState({ notifikasi: datas, soundNotif: true });
				this.props.onNotif(result);
			});
		});
	}
  
	render() {
		return (
			<>
				<div className="relative ml-1">
					<Button
						color="primary"
						badge={this.state.notifikasi.length}
						onClick={() => {
							this.setState({ openNotif: true });
						}}
						onBlur={() => {
							this.setState({ openNotif: false, notifikasi: [] });
						}}
					>Notifikasi</Button>
					{
						this.state.openNotif ?
							<Card className="absolute z-30 w-64 h-64 overflow-auto p-3 right-0">
								{this.state.notifikasi.map((row, i) => (
									<li key={i}>{row.ket}</li>
								))}
							</Card>
						: null
					}
					{
						this.state.soundNotif ?
							<div className="absolute z-30 right-0">
								<ReactAudioPlayer
									src={require('assets/sounds/test.ogg')}
									autoPlay
									controls
									onEnded={() => {
										this.setState({ soundNotif: false });
									}}
								/>
							</div>
						: null
					}
				</div>
			</>
		)
	}

}

export default Notif;

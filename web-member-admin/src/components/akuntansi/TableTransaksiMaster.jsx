import React, { Fragment } from 'react';
import { DataGrid, GridColumn, LinkButton } from 'rc-easyui';
import { Messager } from 'rc-easyui';
import api from 'config/api.js';
import DetailTransaksiMaster from './DetailTransaksiMaster.jsx';

const defaultState = () => ({
	data: [],
	total: 0,
	pageNumber: 0,
	pageSize: 20,
	loading: false,
	openDetailTransaksiMaster: false,
	dataAktif: [],
});

class TableTransaksiMaster extends React.Component {

	state = {
		...defaultState(),
	}
	
	componentDidMount() {
		this.fetchData(this.state.pageNumber);
	}

	UNSAFE_componentWillReceiveProps(nextProps) {
		if (nextProps.reload) {
			this.fetchData(this.state.pageNumber);
			this.props.falseReload();
		}
	}

	fetchData = (pageNumber) => {
		this.setState({ loading: true });
		api.get(`keuangan/akuntansi/all/limit20/${this.props.kodeZainal}/${pageNumber}`)
    .then(result => {
			
			api.get(`keuangan/akuntansi/all/count/${this.props.kodeZainal}`)
			.then(result2 => {
				this.setState({
					data: result.data,
					total: result2.data.jml,
					loading: false,
				});
			})
			.catch(error => {
				console.log(error.response)
			});
    })
		.catch(error => {
			console.log(error.response)
		});
	}

	delete = (data) => {
		this.messager.confirm({
			title: "Error",
			msg: `Yakin Delete Uraian: ${data.URAIAN}, Tgl. Transaksi: ${data.TGL_TRANS} ?`,
			result: (r) => {
				if (r) {
					api.delete(`keuangan/akuntansi/transaksimaster/${data.TRANS_ID}`)
					.then(result => {
						this.props.trueReload();
					})
					.catch(error => {
						console.log(error.response)
					});
				}
			}
		});
	}

	detail = (TRANS_ID) => {
		api.get(`keuangan/akuntansi/transaksidetail/${TRANS_ID}`)
		.then(result => {
			this.setState({
				dataAktif: result.data,
				openDetailTransaksiMaster: true,
			});
		})
		.catch(error => {
			console.log(error.response)
		});
	}

	error = (data) => {
		this.messager.alert({
			title: "Error",
			icon: "error",
			msg: data
		});
	}

   render() {
      return (
        <DataGrid
					style={{ height: 400 }}
					pagination
					lazy
					total={this.state.total}
					pageNumber={this.state.pageNumber}
					pageSize={this.state.pageSize}
					data={this.state.data}
					onPageChange={(e) => {this.fetchData(e.pageNumber-1)}}
					loading={this.state.loading}
					toolbar={
						this.props.add === true ?
						() => (
							<div style={{ padding: 4 }}>
								<LinkButton iconCls="icon-add" plain onClick={this.props.openAdd}>Add</LinkButton>
							</div>
						) : null
					}
				>
				<GridColumn width="30px"
					title="Action"
					render={({ row }) => (
						<Fragment>
							<LinkButton
								iconCls="icon-print"
								onClick={() => {this.detail(row.TRANS_ID)}}
								plain
							/>
							<LinkButton iconCls="icon-remove" plain onClick={() => {this.delete(row)}} />
						</Fragment>
					)}
				/>
				<GridColumn field="URAIAN" title="Uraian" width="200px"></GridColumn>
				<GridColumn field="TGL_TRANS" title="Tgl. Transaksi" width="100px" align="center"></GridColumn>
				<GridColumn field="KODE_JURNAL" title="Kode Jurnal" width="100px" align="center" />
				<Messager ref={ref => this.messager = ref}></Messager>
				<DetailTransaksiMaster
					open={this.state.openDetailTransaksiMaster}
					close={() => {
						this.setState({ openDetailTransaksiMaster: false });
					}}
					data={this.state.dataAktif}
				/>
			</DataGrid>
      );
	}

}

export default (TableTransaksiMaster);

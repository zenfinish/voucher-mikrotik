import React from 'react';
import TextField from '@material-ui/core/TextField';

class SelectLakaLantasSuplesi extends React.Component {
   
   render() {
      return (
         <TextField
            select
            label="Suplesi"
            onChange={this.props.onChange}
            SelectProps={{ native: true }}
            name="laka_lantas_suplesi"
            fullWidth
            InputLabelProps={{ shrink: true }}
            value={this.props.value}
         >
            <option key="0" value="0">TIDAK</option>
            <option key="1" value="1">YA</option>
         </TextField>
      );
   }

}

export default SelectLakaLantasSuplesi;

import React from 'react';
import Loading from 'components/Loading.jsx';

class Popover extends React.Component {

	render() {
		return (
      <div className={`relative inline-block`}>
        {this.props.children}
        <div className={`absolute border border-black bg-white z-10 ${this.props.open ? '' : 'hidden'}`}>
          <div className="text-sm whitespace-nowrap p-2">
            {this.props.content()}
          </div>
          {
            this.props.loading ?
              <Loading />
            : null
          }
        </div>
      </div>
		)
	}

}

export default Popover;

import React from 'react';
import Button from 'components/inputs/Button.jsx';
import TextField from 'components/inputs/TextField.jsx';

class Table extends React.Component {

	render() {
		return (
			<>
        <div className="mb-2 flex items-center">
          {this.props.add ? <Button className="mr-1" onClick={this.props.add.onClick} icon="Add">Add</Button> : null}
          <Button icon="Bin">Delete</Button>
          <TextField placeholder="Search" className="ml-auto" />
        </div>
        <table className="w-full text-sm">
          {
            this.props.headers ?
              <thead>
                <tr>
                  {
                    this.props.headers.map((row, i) => (
                      <th
                        key={i}
                        className="border border-gray-400 px-2 py-1 bg-gray-100"
                      >{JSON.stringify(row)}</th>
                    ))}
                </tr>
              </thead>
            : null
          }
          {
            this.props.data ?
              <tbody>
                <tr>
                  {this.props.data.map((row, i) => (<td key={i} className="border border-gray-400 px-2 py-1">{JSON.stringify(row)}</td>))}
                </tr>
              </tbody>
            : null
          }
        </table>
      </>
		)
	}

}

export default Table;

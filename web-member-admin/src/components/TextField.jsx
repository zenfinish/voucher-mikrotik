import React from 'react';
import TextField from '@material-ui/core/TextField';

class TextFieldMurni extends React.Component {

   render() {
      return (
         <TextField
            label={this.props.label}
            name={this.props.name}
            type="text"
            value={this.props.value}
            fullWidth
            InputProps={this.props.disabled === true ? { disabled: true, } : { disabled: false, }}
            onChange={this.props.onChange}
            InputLabelProps={{ shrink: true }}
         />
      );
   }

}

export default TextFieldMurni;

import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Autocomplete from '../components/input/Autocomplete.jsx';
import api from '../config/api.js';

const styles = theme => ({
   row: {
      '&:nth-of-type(odd)': {
         backgroundColor: theme.palette.background.default,
      },
   },
});

class CariPasienLama extends React.Component {
    
   state = {
		dataPasienLama: [],
		dataTampil: [],
		search: '',
	}
	
	clearData = () => {
		this.setState({
			dataPasienLama: [],
			dataTampil: [],
		});
	}
	
	enter = (data) => {
      this.props.getDataPasien(data);
	}
   
   getDataPasienLama = () => {
		api.get(`/pasien/search/${this.state.search}`)
      .then(result => {
			let dataTampil = [];
			for (let i = 0; i < result.data.length; i++) {
				dataTampil.push({
					no_rekmedis: result.data[i].no_rekmedis,
					nama_pasien: result.data[i].nama_pasien,
					alamat: result.data[i].alamat,
				});
			}
			this.setState({
				dataTampil: dataTampil,
				dataPasienLama: result.data
         });
      })
      .catch(error => {
         console.log(error.response)
      });
   }

   render() {
      return (
         <Autocomplete
				placeholder="Cari Pasien Lama Berdasarkan Nama / No. Rekmedis"
				clear={this.clearData}
				onenter={this.enter}
				width="400px"
				link="pasien/search"
				dataTampil={['no_rekmedis', 'nama_pasien', 'tgl_lahir']}
				disabled={this.props.disabled}
			/>
      );
   }
}

CariPasienLama.propTypes = {
   classes: PropTypes.object.isRequired,
};

export default (withStyles(styles)(CariPasienLama));

import React from 'react';
import TextField from '@material-ui/core/TextField';

class SelectAgama extends React.Component {

   render() {
      return (
         <TextField
            select label="Agama"
            onChange={this.props.onChange}
            SelectProps={{ native: true, }}
            name="agama"
            fullWidth
            InputLabelProps={{ shrink: true }}
            value={this.props.value}
            InputProps={this.props.disabled === true ? { disabled: true, } : { disabled: false, }}
         >
            <option key="" value=""></option>
            <option key="ISLAM" value="ISLAM">ISLAM</option>
            <option key="KATOLIK" value="KATOLIK">KATOLIK</option>
            <option key="PROTESTAN" value="PROTESTAN">PROTESTAN</option>
            <option key="BUDHA" value="BUDHA">BUDHA</option>
            <option key="HINDU" value="HINDU">HINDU</option>
            <option key="KONGHUCHU" value="KONGHUCHU">KONGHUCHU</option>
         </TextField>
      );
   }

}

export default SelectAgama;

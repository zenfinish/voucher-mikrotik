import React from 'react';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';

import api from '../config/api.js';

class DeleteSep extends React.Component {
    
   state = {
      sep: '',
   }

   UNSAFE_componentWillReceiveProps(nextProps, prevState) {
      this.setState({
         sep: nextProps.data.noSep,
      });
   }
   delete = () => {
      api.delete(`/bpjs/sep/${this.state.bpjs_sep}`)
      .then(result => {
         this.props.success('Data berhasil di delete');
         this.props.close();
      })
      .catch(error => {
         this.props.error(error.response.data);
      });
   }

   render() {
      return (
         <div style={{overflowX: 'auto'}}>
            <Dialog
               open={this.props.open}
               onClose={this.props.close}
               maxWidth="lg"
               fullWidth={true}
            >
               <DialogTitle>Yakin Delete SEP</DialogTitle>
               <DialogContent>

               </DialogContent>
               <DialogActions>
                  <Button onClick={this.delete} color="secondary">Delete</Button>
                  <Button onClick={this.props.close} color="secondary">Close</Button>
               </DialogActions>
            </Dialog>
         </div>
      );
   }

};

export default DeleteSep;
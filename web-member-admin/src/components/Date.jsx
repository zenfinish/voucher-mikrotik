import React from 'react';
import TextField from '@material-ui/core/TextField';

class Date extends React.Component {

   render() {
      return (
         <TextField
            label={this.props.label}
            name={this.props.name}
            type="date" variant="outlined"
            fullWidth
            onChange={this.props.onChange}
            InputLabelProps={{ shrink: true }}
            style={{height: '40px'}}
            value={this.props.value}
         />
      );
   }

}

export default Date;

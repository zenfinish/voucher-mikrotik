import React from 'react';

import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import TextField from '@material-ui/core/TextField';

import SearchIcon from '@material-ui/icons/Search';

import api from '../config/api.js';

class CariSep extends React.Component {
    
   state = {
      value: '',
   }

   handleChange = (e) => {
      this.setState({
         [e.target.name]: e.target.value,
      });
   }

   cariPasienKartu = (e) => {
      e.preventDefault();
      api.get(`bpjs/sep/search/${this.state.value}`)
      .then(result => {
         this.props.result(result.data);
      })
      .catch(error => {
         this.props.error(error.response.data);
      });
   }
       
   render() {
      return (
         <form onSubmit={this.cariPasienKartu}>
            <TextField
               label="Cari Sep"
               InputLabelProps={{ shrink: true }}
               fullWidth
               value={this.state.value}
               InputProps={{
                  endAdornment: (
                     <InputAdornment position="end">
                        <IconButton
                           onClick={this.cariSep}
                        ><SearchIcon /></IconButton>
                     </InputAdornment>
                  )
               }}
               name="value"
               onChange={this.handleChange}
               autoComplete="off"
            />
         </form>
      );
   }
}

export default (CariSep);

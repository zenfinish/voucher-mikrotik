import React from 'react';
import TextField from '@material-ui/core/TextField';

class SelectStatusPasien extends React.Component {
   
   render() {
      return (
         <TextField
            label="Status Pasien"
            InputLabelProps={{ shrink: true }} SelectProps={{ native: true }}
            fullWidth select
            onChange={this.props.onChange}
            name="status_pasien"
            value={this.props.value}
            InputProps={this.props.disabled === true ? { disabled: true, } : { disabled: false, }}
         >
            <option key="" value=""></option>
            <option key="0" value="0">Pasien Baru</option>
            <option key="1" value="1">Pasien Lama</option>
         </TextField>
      );
   }

}

export default SelectStatusPasien;
import React from 'react';
import TextField from '@material-ui/core/TextField';

class TextFieldNoRujukan extends React.Component {

   render() {
      return (
         <TextField
            label="No. Rujukan"
            name="no_rujukan"
            type="text"
            value={this.props.value}
            fullWidth
            InputProps={this.props.disabled === true ? { disabled: true, } : { disabled: false, }}
            onChange={this.props.onChange}
            InputLabelProps={{ shrink: true }}
         />
      );
   }

}

export default TextFieldNoRujukan;

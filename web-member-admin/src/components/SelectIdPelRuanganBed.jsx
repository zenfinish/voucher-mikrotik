import React from 'react';
import TextField from '@material-ui/core/TextField';

class SelectIdPelRuanganBed extends React.Component {

   state = {
      data: []
   }
   
   render() {
      return (
         <TextField
            label="Bed"
            style={{height: '40px'}}
            variant="outlined"
            InputLabelProps={{ shrink: true }}
            SelectProps={{ native: true }}
            fullWidth select
            onChange={this.handleChangePoli}
            name="id_pelruanganbed"
            value={this.props.value}
            InputProps={this.props.disabled === true ? { disabled: true, } : { disabled: false, }}
         >
            <option key="" value=""></option>
            {this.state.data.map(row => (
               <option key={row.id_pelruanganbed} value={row}>
                  {row.nama_ruangan}
               </option>
            ))}
         </TextField>
      );
   }

}

export default SelectIdPelRuanganBed;

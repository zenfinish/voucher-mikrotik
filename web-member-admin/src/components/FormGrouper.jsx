import React, { Fragment } from 'react';
import { Dialog, Grid, Divider, DialogActions, DialogContent, Button } from '@material-ui/core';
import { DataGrid, GridColumn, ComboBox, TextBox } from 'rc-easyui';
import api from '../config/api.js';
import { separator } from '../config/helpers.js';
import CariProsedur from './CariProsedur.jsx';
import CariDiagnosa3 from './CariDiagnosa3.jsx';

const defaultState = () => ({
   no_bpjs: '',
   sep: '',
   no_rekmedis: '',
   nama_pasien: '',
   tgl_lahir: '',
   tgl_checkin: '',
   tgl_checkout: '',
   id_capul: '',
   diagnosa_awal: '',
   jkel: '',
   id_rj: '',
   TRANS_ID: '',

   id_checkin: '',
   jenis_pelayanan: '2',
   status_pelayanan: '',
   
   diagnosa_inacbg: [],
   prosedur_inacbg: [],

   inacbg_tarif: '',
   inacbg_code: '',
   inacbg_description: '',

   total_akomodasi: 0,
   total_obat: 0,
   total_prosedur_non_bedah: 0,
   total_prosedur_bedah: 0,
   total_konsultasi: 0,
   total_tenaga_ahli: 0,
   total_keperawatan: 0,
   total_penunjang: 0,
   total_radiologi: 0,
   total_laboratorium: 0,
   total_pelayanan_darah: 0,
   total_rehabilitasi: 0,
   total_kamar: 0,
   total_rawat_intensif: 0,
   total_obat_kronis: 0,
   total_obat_kemoterapi: 0,
   total_alkes: 0,
   total_bmhp: 0,
	total_sewa_alat: 0,

	id_dokter: '',
	nama_dokter: '',

   response_inacbg: null,
	klaim_status_cd: null,
});

class FormGrouper extends React.Component {
    
   state = {
      ...defaultState(),
	}

   UNSAFE_componentWillReceiveProps(nextProps, prevState) {
		this.setState({
         no_bpjs: nextProps.data.no_bpjs,
         sep: nextProps.data.bpjs_sep,
         no_rekmedis: nextProps.data.no_rekmedis,
         nama_pasien: nextProps.data.nama_pasien,
         tgl_lahir: nextProps.data.tgl_lahir,
         tgl_checkin: nextProps.data.tgl_checkin,
         tgl_checkout: nextProps.data.tgl_checkout,
         jkel: nextProps.data.jkel,
         id_rj: nextProps.data.id_rj,
         TRANS_ID: `${nextProps.data.TRANS_ID}`,

         diagnosa_awal: nextProps.data.nama_diagnosis,
         kode_diagnosa_awal: nextProps.data.no_icd,
         diagnosa_inacbg: nextProps.data.diagnosa_inacbg,

         prosedur_inacbg: nextProps.data.prosedur_inacbg,

         inacbg_code: nextProps.data.inacbg_code,
         inacbg_description: nextProps.data.inacbg_description,
         inacbg_tarif: nextProps.data.inacbg_tarif,

         id_checkin: nextProps.data.id_checkin,
         status_pelayanan: nextProps.statusPelayanan,

         total_akomodasi: nextProps.data.total_akomodasi,
         total_obat: nextProps.data.total_obat,
         total_prosedur_non_bedah: nextProps.data.total_prosedur_non_bedah + nextProps.data.total_lain,
         total_prosedur_bedah: nextProps.data.total_prosedur_bedah === undefined ? 0 : nextProps.data.total_prosedur_bedah,
         total_konsultasi: nextProps.data.total_konsultasi,
         total_tenaga_ahli: nextProps.data.total_tenaga_ahli,
         total_keperawatan: nextProps.data.total_keperawatan,
         total_penunjang: nextProps.data.total_penunjang,
         total_radiologi: nextProps.data.total_radiologi,
         total_laboratorium: nextProps.data.total_laboratorium,
         total_pelayanan_darah: nextProps.data.total_pelayanan_darah,
         total_rehabilitasi: nextProps.data.total_rehabilitasi,
         total_kamar: nextProps.data.total_kamar,
         total_rawat_intensif: nextProps.data.total_rawat_intensif,
         total_obat_kronis: nextProps.data.total_obat_kronis,
         total_obat_kemoterapi: nextProps.data.total_obat_kemoterapi,
         total_alkes: nextProps.data.total_alkes,
         total_bmhp: nextProps.data.total_bmhp,
         total_sewa_alat: nextProps.data.total_sewa_alat,
			
			nama_dokter: nextProps.data.nama_dokter,
			klaim_status_cd: nextProps.data.klaim_status_cd,
      });
   }

   grouper = () => {
      let diagnosa_inacbg = [];
      for (let i = 0; i < this.state.diagnosa_inacbg.length; i++) {
         diagnosa_inacbg.push(this.state.diagnosa_inacbg[i].no_icd);
      }
      let prosedur_inacbg = [];
      for (let i = 0; i < this.state.prosedur_inacbg.length; i++) {
         prosedur_inacbg.push(this.state.prosedur_inacbg[i].no_icd);
      }
      let link;
      if (this.state.id_rj === '1') {
         link = `poli`;
      } else if (this.state.id_rj === '2') {
         link = `igd`;
      }
      api.post(`admmedis/groupingstage1/${link}`, {
         ...this.state,
         diagnosa_inacbg: diagnosa_inacbg,
         prosedur_inacbg: prosedur_inacbg,
      }, {
         headers: { token: localStorage.getItem('token') }
      })
      .then(result => {
         this.setState({
            inacbg_code: result.data.response.cbg.code,
            inacbg_description: result.data.response.cbg.description,
            inacbg_tarif: result.data.response.cbg.tariff,
            TRANS_ID: result.data.TRANS_ID,
         });
      })
      .catch(error => {
			this.props.error(JSON.stringify(error.response.data));
      });
   }

   final = () => {
      let link;
      if (this.state.id_rj === '1') {
         link = `poli`;
      } else if (this.state.id_rj === '2') {
         link = `igd`;
      }
      api.post(`admmedis/final/${link}/${this.state.id_checkin}/${this.state.bpjs_sep}`, { ...this.state }, {
         headers: { token: localStorage.getItem('token') }
      })
      .then(result => {
         this.setState({
            klaim_status_cd: 'final'
         });
         // this.props.suksesFinal();
      })
      .catch(error => {
         this.props.error(JSON.stringify(error.response.data));
      });
   }

   cetak = () => {
      api.get(`admmedis/print/${this.state.bpjs_sep}`, {
         headers: { token: localStorage.getItem('token') }
      })
      .then(result => {
         // let pdfWindow = window.open("");
         // pdfWindow.document.write("<iframe src='data:application/pdf;base64, " + encodeURI(result.data) + "'></iframe>");

         // var file = new Blob([result.data], {type: 'application/pdf'});
         // var fileURL = URL.createObjectURL(file);
         // window.open(fileURL);

         window.open("data:application/pdf;base64,"+result.data);
      })
      .catch(error => {
         // this.props.error(error.response.data);
         console.log(error.response)
      });
   }

   editKlaim = () => {
      let link;
      if (this.state.id_rj === '1') {
         link = `poli`;
      } else if (this.state.id_rj === '2') {
         link = `igd`;
      }
      api.get(`admmedis/edit/${link}/${this.state.id_checkin}/${this.state.bpjs_sep}`, {
         headers: { token: localStorage.getItem('token') }
      })
      .then(result => {
         this.setState({
            klaim_status_cd: ''
         });
      })
      .catch(error => {
         // this.props.error(error.response.data);
         console.log(error.response)
      });
   }

   getDataDiagnosa = (data) => {
		let tmp = this.state.diagnosa_inacbg;
		tmp.push({
         no_icd: data.value,
         nama_diagnosa: data.label,
		});
		this.setState({
			diagnosa_inacbg: tmp
		});
   }

   getDataProsedur = (data) => {
		let tmp = this.state.prosedur_inacbg;
		tmp.push({
         no_icd: data.value,
         nama_prosedur: data.label,
		});
		this.setState({
			prosedur_inacbg: tmp
		});
   }

   deleteDiagnosa = (data) => {
      let arrDiagnosa = this.state.diagnosa_inacbg;
      let index = arrDiagnosa.indexOf(data);
      arrDiagnosa.splice(index, 1);
      this.setState({
         diagnosa_inacbg: arrDiagnosa
      });
   }

   deleteProsedur = (data) => {
      let arrProsedur = this.state.prosedur_inacbg;
      let index = arrProsedur.indexOf(data);
      arrProsedur.splice(index, 1);
      this.setState({
         prosedur_inacbg: arrProsedur
      });
   }

   close = () => {
      this.setState({
         ...defaultState()
      }, () => {
         this.props.close();
      });
   }

   render() {
      const { data } = this.props;
      return (
         <div style={{overflowX: 'auto'}}>
            <Dialog
               open={this.props.open}
               onClose={this.close}
               maxWidth="lg"
               fullWidth={true}
            >
               <DialogContent>
                  <span>Data Pasien</span><br /><br />
                  <Grid container spacing={8} style={{ marginBottom: 5 }}>
                     <Grid item xs={4}>
                        <table cellPadding={5} style={{ fontSize: 12 }}>
                           <tbody>
                              <tr>
                                 <td valign="top">No. Rekmedis</td>
                                 <td valign="top">:</td>
                                 <td valign="top">{this.state.no_rekmedis}</td>
                              </tr>
                              <tr>
                                 <td valign="top">Nama Pasien</td>
                                 <td valign="top">:</td>
                                 <td valign="top">{this.state.nama_pasien}</td>
                              </tr>
                              <tr>
                                 <td valign="top">Jkel</td>
                                 <td valign="top">:</td>
                                 <td valign="top">{this.state.jkel}</td>
                              </tr>
                              <tr>
                                 <td valign="top">Tgl. Lahir</td>
                                 <td valign="top">:</td>
                                 <td valign="top">{this.state.tgl_lahir}</td>
                              </tr>
                              <tr>
                                 <td valign="top">TRANS_ID</td>
                                 <td valign="top">:</td>
                                 <td valign="top">{this.state.TRANS_ID}</td>
                              </tr>
                           </tbody>
                        </table>
                     </Grid>
                     <Grid item xs={4}>
                        <table cellPadding={5} style={{ fontSize: 12 }}>
                           <tbody>
                              <tr>
                                 <td valign="top">Tgl. Masuk</td>
                                 <td valign="top">:</td>
                                 <td valign="top">{data.tgl_checkin}</td>
                              </tr>
                              <tr>
                                 <td valign="top">Tgl. Pulang</td>
                                 <td valign="top">:</td>
                                 <td valign="top">{data.tgl_checkout}</td>
                              </tr>
                              <tr>
                                 <td valign="top">No. SEP</td>
                                 <td valign="top">:</td>
                                 <td valign="top">{this.state.bpjs_sep}</td>
                              </tr>
                              <tr>
                                 <td valign="top">No. Kartu BPJS</td>
                                 <td valign="top">:</td>
                                 <td valign="top">{data.no_bpjs}</td>
                              </tr>
                              <tr>
                                 <td valign="top">Diagnosa Awal</td>
                                 <td valign="top">:</td>
                                 <td valign="top">{this.state.kode_diagnosa_awal + ' | ' + this.state.diagnosa_awal}</td>
                              </tr>
                           </tbody>
                        </table>
                     </Grid>
                     <Grid item xs={4}>
                        <table cellPadding={5} style={{ fontSize: 12 }}>
                           <tbody>
                              <tr>
                                 <td valign="top">Umur</td>
                                 <td valign="top">:</td>
                                 <td valign="top">{data.umur} tahun</td>
                              </tr>
                              <tr>
                                 <td valign="top">Cara Pulang</td>
                                 <td valign="top">:</td>
                                 <td valign="top">{data.nama_capul}</td>
                              </tr>
                              <tr>
                                 <td valign="top">DPJP</td>
                                 <td valign="top">:</td>
                                 <td valign="top">
												<ComboBox
													data={[
														{ text: 'Cari', value: '' },
														...this.props.datadpjp,
													]}
													value={this.state.id_dokter}
													onSelectionChange={
														(data) => {
															if (data.value !== "") {
																let link = '';
																if (this.state.id_rj === '1') {
																	link = `poli/update/dpjp`;
																} else if (this.state.id_rj === '2') {
																	link = `igd/update/dpjp`;
																}
																api.put(`${link}`, { id_dokter: data.value, id_checkin: this.state.id_checkin })
																.then(result => {
																	this.setState({
																		id_dokter: '',
																		nama_dokter: data.text,
																	});
																})
																.catch(error => {
																	this.props.error(JSON.stringify(error.response));
																});
															}
														}
													}
													style={{ marginRight: 4, width: '70px' }}
													panelStyle={{ width: '250px' }}
												/>
												<TextBox disabled value={this.state.nama_dokter} style={{ marginRight: 4, width: '250px' }} />
											</td>
                              </tr>
                           </tbody>
                        </table>
                     </Grid>
                  </Grid>

                  <Divider /><br />
                  <span>Total Biaya Rumah Sakit</span><br /><br />
                  <Grid container spacing={8} style={{ marginBottom: 5 }}>
                     <Grid item xs={4}>
                        <table cellPadding={5} style={{ fontSize: 12 }}>
                           <tbody>
                              <tr>
                                 <td valign="top">Total Akomodasi / Pendaftaran</td>
                                 <td valign="top">:</td>
                                 <td valign="top" align="right">{separator(data.total_akomodasi)}</td>
                              </tr>
                              <tr>
                                 <td valign="top">Total Radiologi</td>
                                 <td valign="top">:</td>
                                 <td valign="top" align="right">{separator(data.total_radiologi)}</td>
                              </tr>
										<tr>
                                 <td valign="top"><b>TOTAL KESELURUHAN</b></td>
                                 <td valign="top">:</td>
											<td valign="top" align="right">
												{
													separator(
														Number(data.total_akomodasi) +
														Number(this.state.total_obat) +
														Number(this.state.total_prosedur_non_bedah) +
														// Number(data.total_prosedur_bedah)
														Number(this.state.total_konsultasi) +
														// Number(data.total_tenaga_ahli) +
														// Number(data.total_keperawatan) +
														// Number(data.total_penunjang) +
														Number(data.total_radiologi) +
														Number(data.total_laboratorium)
														// Number(data.total_pelayanan_darah) +
														// Number(data.total_rehabilitasi) +
														// Number(data.total_kamar) +
														// Number(data.total_rawat_intensif) +
														// Number(data.total_obat_kronis) +
														// Number(data.total_obat_kemoterapi) +
														// Number(data.total_alkes) +
														// Number(data.total_bmhp) +
														// Number(data.total_sewa_alat)
													)
												}
											</td>
                              </tr>
                           </tbody>
                        </table>
                     </Grid>
                     <Grid item xs={4}>
                        <table cellPadding={5} style={{ fontSize: 12 }}>
                           <tbody>
                              <tr>
                                 <td valign="top">Total Prosedur Non Bedah</td>
                                 <td valign="top">:</td>
                                 <td valign="top" align="right">{separator(this.state.total_prosedur_non_bedah)}</td>
                              </tr>
                              <tr>
                                 <td valign="top">Total Konsultasi</td>
                                 <td valign="top">:</td>
                                 <td valign="top" align="right">{separator(this.state.total_konsultasi)}</td>
                              </tr>
                           </tbody>
                        </table>
                     </Grid>
                     <Grid item xs={4}>
                        <table cellPadding={5} style={{ fontSize: 12 }}>
                           <tbody>
                              <tr>
                                 <td valign="top">Total Obat</td>
                                 <td valign="top">:</td>
                                 <td valign="top" align="right">{separator(this.state.total_obat)}</td>
                              </tr>
                              <tr>
                                 <td valign="top">Total Laboratorium</td>
                                 <td valign="top">:</td>
                                 <td valign="top" align="right">{separator(data.total_laboratorium)}</td>
                              </tr>
                           </tbody>
                        </table>
                     </Grid>
                  </Grid>
                  <Divider /><br />
                  <span>Grouping</span><br /><br />
                  <Grid container spacing={8} style={{ marginBottom: 5 }}>
                     <Grid item xs={12}>
                        <table cellPadding={5} style={{ fontSize: 12 }}>
                           <tbody>
                              <tr>
                                 <td width="200">
												<CariDiagnosa3
													error={this.props.error}
													getDataDiagnosa={this.getDataDiagnosa}
												/>
											</td>
                                 <td>:</td>
                                 <td>
                                    {
													this.state.diagnosa_inacbg
													?
													this.state.diagnosa_inacbg.map((data, index) => (
														<span
															key={index}
															onClick={() => {this.deleteDiagnosa(data)}}
															title="Hapus Data"
															style={{
																cursor: 'pointer',
																background: '#40E0D0',
																borderRadius: '10px',
																padding: '8px',
																marginRight: 4,
															}}
														>{data.nama_diagnosa}</span>
													))
													: ''
												}
                                 </td>
                              </tr>
										<tr>
                                 <td width="200">
												<CariProsedur
													error={this.props.error}
													getDataProsedur={this.getDataProsedur}
												/>
											</td>
                                 <td>:</td>
                                 <td>
                                    {
                                       this.state.prosedur_inacbg
                                       ?
                                          this.state.prosedur_inacbg.map((data, index) => (
															<span
																key={index}
																onClick={() => {this.deleteProsedur(data)}}
																title="Hapus Data"
																style={{
																	cursor: 'pointer',
																	background: '#F08080',
																	borderRadius: '10px',
																	padding: '8px',
																	marginRight: 4,
																}}
															>{data.nama_prosedur}</span>
                                          ))
                                       : ''
                                    }
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                     </Grid>
                  </Grid>
						<DataGrid
							style={{ height: 360 }}
							data={[
								{
									field1: 'Info 0',
									field2: '',
									field3: '',
									field4: '',
									field5: '',
								},
								{
									field1: 'Jenis Rawat',
									field2: '',
									field3: '',
									field4: '',
									field5: '',
								},
								{
									field1: 'Group',
									field2: this.state.inacbg_description,
									field3: this.state.inacbg_code,
									field4: 'Rp.',
									field5: separator(this.state.inacbg_tarif),
								},
								{
									field1: 'Sub Acute',
									field2: '',
									field3: '',
									field4: '',
									field5: '',
								},
								{
									field1: 'Chronic',
									field2: '',
									field3: '',
									field4: '',
									field5: '',
								},
								{
									field1: 'Special Prosedur',
									field2: '',
									field3: '',
									field4: '',
									field5: '',
								},
								{
									field1: 'Special Prosthesis',
									field2: '',
									field3: '',
									field4: '',
									field5: '',
								},
								{
									field1: 'Special Investigation',
									field2: '',
									field3: '',
									field4: '',
									field5: '',
								},
								{
									field1: 'Special Drug',
									field2: '',
									field3: '',
									field4: '',
									field5: '',
								},
								{
									field1: 'Total',
									field2: '',
									field3: '',
									field4: '',
									field5: '',
								},
							]}
						>
							<GridColumn field="field1" width="150px" />
							<GridColumn field="field2" />
							<GridColumn field="field3" width="80px" align="center" />
							<GridColumn field="field4" width="35px" align="center" />
							<GridColumn field="field5" width="150px" align="right" />
						</DataGrid>

               </DialogContent>
               <DialogActions>
                  {
                     this.state.klaim_status_cd === 'final'
                     ?
                     <Fragment>
                        <Button onClick={this.cetak} color="primary" variant="contained" style={{ marginRight: 4 }}>Cetak</Button>
                        <Button onClick={this.editKlaim} color="primary" variant="contained">Edit Klaim</Button>
                     </Fragment>
                     :
                     <Fragment>
                        <Button onClick={this.final} color="secondary" variant="contained" style={{ marginRight: 4 }}>Final</Button>
                        <Button onClick={this.grouper} color="secondary" variant="contained">Grouper</Button>
                     </Fragment>
                  }
						<Button onClick={this.close} color="secondary" variant="contained">Tutup</Button>
               </DialogActions>
            </Dialog>
         </div>
      );
   }

};

export default FormGrouper;
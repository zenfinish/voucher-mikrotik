import React from 'react';
import TextField from '@material-ui/core/TextField';

class Select extends React.Component {

   render() {
      return (
         <TextField
            select name={this.props.name}
            label={this.props.label}
            onChange={this.props.onChange}
            SelectProps={{ native: true, }}
            InputLabelProps={{ shrink: true }}
            fullWidth
            value={this.props.value}
         >
            {this.props.option.map((row, index) => (
               <option key={index} value={row.value}>
                  {row.nama}
               </option>
            ))}
         </TextField>
      );
   }

}

export default Select;

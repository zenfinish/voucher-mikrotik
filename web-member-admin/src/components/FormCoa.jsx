import React from 'react';

import { Button } from '@material-ui/core';
import { TreeGrid, GridColumn } from 'rc-easyui';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';

import api from '../config/api.js';

export default class FormCoa extends React.Component {
    
	state = {
      data: [],
	}

	componentDidMount() {
		api.get(`keuangan/coa/`, {
			headers: { token: localStorage.getItem('token') }
		})
		.then(result => {
			for (let i = 0; i < result.data.length; i++) {
				if (result.data[i].G_OR_D === 'G') {
					result.data[i]['children'] = [];
				}
			}
			this.setState({
				data: result.data,
			});
		})
		.catch(error => {
			console.log(error.response)
		});
	}
	
	render() {
		return (
			<Dialog
				open={this.props.open}
				onClose={this.props.close}
				maxWidth="lg"
            fullWidth={true}
			>
				<DialogContent>
					<TreeGrid
						style={{ height: '100%' }}
						data={this.state.data}
						treeField="NAMA_PERK"
					>
						<GridColumn
							field="NAMA_PERK"
							render={({ row }) => (
								<span
									onDoubleClick={() => {
										this.props.getdata(row.KODE_PERK);
										this.props.close();
									}}
								>{row.NAMA_PERK}</span>
							)}
							title="Nama Perkiraan"
							width="40%"
						/>
						<GridColumn field="KODE_PERK" title="Kode Perkiraan" width="15%" />
						<GridColumn field="KODE_ALTERNATIF" title="Kode Alternatif" width="15%" />
						<GridColumn field="type_perk" title="Jenis Rekening" width="15%" />
						<GridColumn field="SALDO_AKHIR" title="Saldo" width="15%" align="right" />
					</TreeGrid>
				</DialogContent>
				<DialogActions>
					<Button
						variant="contained"
						color="primary"
						style={{marginRight: 5}}
					>Klik 2 Kali Di Nama Perkiraan Untuk Memilih</Button>
					<Button
						variant="contained"
						color="secondary"
						style={{marginRight: 5}}
						onClick={this.props.close}
					>Batal</Button>
				</DialogActions>
			</Dialog>
		);
	}

}
import React from 'react';
import TextField from '@material-ui/core/TextField';

class TextFieldKtpElek extends React.Component {

   render() {
      return (
         <TextField
            label="KTP Elektronik"
            name="ktp_elek"
            type="text"
            value={this.props.value}
            fullWidth
            onChange={this.props.onChange}
            InputLabelProps={{ shrink: true }}
            InputProps={this.props.disabled === true ? { disabled: true, } : { disabled: false, }}
         />
      );
   }

}

export default TextFieldKtpElek;

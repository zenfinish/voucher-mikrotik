import React from 'react';
import TextField from '@material-ui/core/TextField';

class TextFieldTempatLahir extends React.Component {

   render() {
      return (
         <TextField
            label="Tempat Lahir"
            name="tempat_lahir"
            type="text"
            value={this.props.value}
            fullWidth
            InputProps={this.props.disabled === true ? { disabled: true, } : { disabled: false, }}
            onChange={this.props.onChange}
            InputLabelProps={{ shrink: true }}
         />
      );
   }

}

export default TextFieldTempatLahir;

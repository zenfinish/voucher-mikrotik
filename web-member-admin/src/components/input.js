import React from 'react';
import Select from 'react-select';
import Icon from 'components/Icon.jsx';

const style = {
  input: `
    rounded border border-gray-300 text-xs h-9 py-1 px-2 text-gray-700
    focus:outline-none focus:ring-2 focus:ring-blue-500 focus:border-transparent
  `,
};

class TextField extends React.Component {

	render() {
    return (
      <input
        type={this.props.type ? this.props.type : 'text'}
        placeholder={this.props.placeholder}
        className={style.input + this.props.className}
        onKeyPress={this.props.onKeyPress}
        disabled={this.props.loading === true ? true : this.props.disabled}
        value={this.props.value}
        onChange={this.props.onChange}
        onKeyUp={
          this.props.onEnter ?
            (e) => {
              if (e.keyCode === 13) {
                this.props.onEnter(e.target.value);
              }
            }
          : null
        }
        name={this.props.name}
        title={this.props.title}
      />
		)
	}

};

class DateBox extends React.Component {

	render() {
		return (
      <input
        type="date"
        className={style.input + this.props.className}
        onChange={this.props.onChange}
        value={this.props.value}
        disabled={this.props.disabled}
      />
		)
	}

}

class Dropdown extends React.Component {

	render() {
		return (
      <Select
        options={this.props.data ? this.props.data.filter(option => {return option}) : null}
        onChange={this.props.onChange ? this.props.onChange : null}
        value={this.props.data ? this.props.data.filter(option => option && option.value === this.props.value) : ''}
        styles={{
          control: (styles) => ({
            ...styles,
            fontSize: '12px',
            width: this.props.width,
          }),
          option: (styles) => ({
            ...styles,
            fontSize: '12px',
          }),
          container: styles => ({
            ...styles,
          }),
        }}
        isDisabled={this.props.disabled}
        className={this.props.className}
        placeholder={this.props.placeholder}
      />
		)
	}

}

class TextArea extends React.Component {

	render() {
		return (
      <textarea
        className={style.input + 'h-auto ' + this.props.className}
        placeholder={this.props.placeholder}
        onChange={this.props.onChange}
        value={this.props.value}
        disabled={this.props.disabled}
        style={this.props.style}
      />
		)
	}

}

class Button extends React.Component {
  
  render() {
		return (
      <button
        className={style.input + 'hover:bg-blue-500 hover:text-white ' + this.props.className}
        onClick={this.props.onClick}
        disabled={this.props.loading ? true : this.props.disabled}
        title={this.props.title}
        onBlur={this.props.onBlur}
      >
        <div className="flex items-center">
          {
            this.props.loading ?
              <img src={require('assets/imgs/loading2.gif')} alt="" className="w-6" />
            : 
              <>
                {this.props.icon  ? <Icon name={this.props.icon} className={`fill-current w-4 ${this.props.children ? 'mr-2' : ''}`} /> : null}
                {this.props.children ? <span>{this.props.children}</span> : null}
              </>
          }
        </div>
        {
          this.props.badge ?
            <span className="absolute rounded-full bg-red-900 p-1" style={{ top : -20, right: -10 }}>{this.props.badge}</span>
          : null
        }
      </button>
		)
	}

}

export { TextField, DateBox, Dropdown, TextArea, Button };

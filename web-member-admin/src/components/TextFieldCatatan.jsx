import React from 'react';
import TextField from '@material-ui/core/TextField';

class TextFieldCatatan extends React.Component {

   render() {
      return (
         <TextField
            label="Catatan"
            name="catatan"
            type="text"
            value={this.props.value}
            fullWidth
            onChange={this.props.onChange}
            InputLabelProps={{ shrink: true }}
         />
      );
   }

}

export default TextFieldCatatan;

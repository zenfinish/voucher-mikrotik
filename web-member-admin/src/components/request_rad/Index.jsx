import React from 'react';
import { CircularProgress, Button } from '@material-ui/core';
import { notification, Modal, Checkbox } from 'antd';
import api from 'config/api.js';
import { separator } from 'config/helpers.js';
import DataGrid from 'components/table/DataGrid.jsx';
import GridColumn from 'components/table/GridColumn.jsx';
import Search from "../../views/MainApp/Administrasi/Registrasi/_jalan/Form/Content/Search";
import { useRecoilValue } from "recoil";
import { userState } from '../../recoil/user';

const Index = ({ close, closeRefresh, data }) => {
	const user = useRecoilValue(userState);
	return <RequestRad
		close={close}
		closeRefresh={closeRefresh}
		user={user}
		data={data}
	/>;
};

class RequestRad extends React.Component {
	
	state = {
		disableSearch: false,
		optionCapak: [],
		searchPaket: '',
		loadingPaket: false,
		optionCariTindakan: [],
		disabledSimpan: false,

		id_radtarif: '',
		nama_radtarif: '',
		tarif: '',
		id_radtarifdet: '',
		diagnosa: null,
		diagnosa_nama: null,
		
		dataTable: [],
		data: [],
	}

	componentDidMount() {
		this.refreshTable();
	}

	refreshTable = () => {
		this.setState({ loading: true }, () => {
			api.get(`/rad/data/aktif`)
			.then(result => {
				this.setState({ data: result.data, loading: false });
			})
			.catch(error => {
				console.log('Error: ', error.response);
				this.setState({ loading: false });
			});
		});
	}

	pilih = (checked, value, index) => {
		let data = this.state.dataTable;
		if (checked) {
			data.push({ ...value, index });
		} else {
			data = data.filter((item) => item.index !== index );
		}
		this.setState({ dataTable: data });
	}
			
	render() {
		const { data, close, closeRefresh, user } = this.props;

		return (
			<Modal
				centered
				width="100%"
				bodyStyle={{ height: '60vh' }}
				visible={true}
				onCancel={close}
				footer={[
					<div style={{ position: 'relative' }} key="simpan"> {/* tombol_simpan */}
						<Button variant="outlined" color="primary"
							onClick={() => {
								if (this.state.dataTable.length === 0) {
									notification.error({
										message: 'Error',
										description: 'Paket Belum Dipilih',
									});
								} else {
									this.setState({ disabledSimpan: true }, () => {
										api.post(`/rad/transaksi`, {
											no_rekmedis: data.no_rekmedis,
											nama_pelpasien: data.nama_pelpasien,
											tipe: data.tipe,
											id_checkin: data.tipe === 1 ? data.id_peljalan : data.tipe === 2 ? data.id_pelinap : '',
											data: this.state.dataTable,
											diagnosa: this.state.diagnosa,
											diagnosa_nama: this.state.diagnosa_nama,
											dokter: user.id_peluser,
										})
										.then(result => {
											this.setState({ disabledSimpan: false }, () => {
												closeRefresh();
											});
										})
										.catch(error => {
											let error2 = error.response.data;
											if (error2 === "Cannot add or update a child row: a foreign key constraint fails (`rspur3`.`rad_transaksi_jalan`, CONSTRAINT `rad_transaksi_jalan_DIAGNOSA` FOREIGN KEY (`diagnosa`) REFERENCES `diagnosa` (`kode`))") {
												error2 = "Diagnosa Harus Diisi"
											};
											notification.error({
												message: 'Error',
												description: JSON.stringify(error2),
											});
											this.setState({ disabledSimpan: false });
										});
									});
								}
							}}
							disabled={this.state.disabledSimpan}
						>Simpan</Button>
						{
							this.state.disabledSimpan &&
							<CircularProgress size={24}
								style={{ position: 'absolute', right: '35%', top: '20%' }}
							/>
	 					}
					</div>
				]}
				title="Tambah Transaksi Radiologi"
			>
				<table className="whitespace-nowrap w-full text-sm mb-4" cellPadding={1}>
					<tbody>
						<tr>
							<td>Nama Pasien</td>
							<td>:</td>
							<td>{data.nama_pelpasien} [{data.no_rekmedis}]</td>
							<td>Tgl Checkin</td>
							<td>:</td>
							<td>{data.tgl_checkin}</td>
						</tr>
						<tr>
							<td>Cara Bayar</td>
							<td>:</td>
							<td>{data.nama_pelcabar}</td>
							<td>Tipe</td>
							<td>:</td>
							<td>{data.tipe === 1 ? 'Rawat Jalan' : data.tipe === 2 ? 'Rawat Inap' : ''}</td>
						</tr>
					</tbody>
				</table>
				<Search
					onSelected={(data) => this.setState({ diagnosa_nama: data[0], diagnosa: data[1] })}
					fetchData={(value) => {
						return api.get(`/inacbg/diagnosa/search`, { headers: { search: value } })
					}}
					customOption={(data) => `${data[1]} - ${data[0]}`}
					value={(data) => `${data[1]} - ${data[0]}`}
					outsideValue={data.diagnosaOutsideValue}
				/>
				<DataGrid
					data={this.state.data}
				>
					<GridColumn
						title="Nama"
						render={(row, index) => <Checkbox onChange={(e) => this.pilih(e.target.checked, row, index)}>{row.nama_radtarif}</Checkbox>}
					/>
					<GridColumn title="ID" field="id_radtarif" />
					<GridColumn
						title="Tarif"
						field="tarif"
						render={(row) => (<span>{separator(row.tarif)}</span>)}
						right
					/>
				</DataGrid>
			</Modal>
		)
	}

};

export default (Index);

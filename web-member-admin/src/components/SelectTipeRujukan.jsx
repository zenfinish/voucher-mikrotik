import React from 'react';
import TextField from '@material-ui/core/TextField';

class SelectJkel extends React.Component {

   render() {
      return (
         <TextField
            select label="Tipe Rujukan"
            onChange={this.props.onChange}
            SelectProps={{ native: true }}
            InputLabelProps={{ shrink: true }}
            style={{height: '40px'}}
            value={this.props.value}
            variant="outlined" name="tipe_rujukan" fullWidth
            InputProps={this.props.disabled === true ? { disabled: true, } : { disabled: false, }}
         >
            <option key="0" value="0">Penuh</option>
            <option key="1" value="1">Partial</option>
            <option key="2" value="2">Rujuk Balik (Non PRB)</option>
         </TextField>
      );
   }

}

export default SelectJkel;

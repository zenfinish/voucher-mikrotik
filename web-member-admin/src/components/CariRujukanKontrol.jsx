import React, { Fragment } from 'react';
import { ComboBox } from 'rc-easyui';

import api from '../config/api.js';

class CariRujukan extends React.Component {
    
   state = {
      nomor: '',
      nama_nomor: 'bpjs',
   }

   handleChange = (e) => {
      this.setState({
         [e.target.name]: e.target.value,
      });
   }

   cariPasien = (e) => {
		e.preventDefault();
		this.setState({
			nomor: this.state.nomor.padStart(13, '0'),
		}, () => {
			let link = '';
			if (this.state.nama_nomor === 'bpjs' && this.props.status_pasien === '0') {
				link = 'bpjs/rujukan/manual/bpjs/baru'
			} else if (this.state.nama_nomor === 'bpjs' && this.props.status_pasien === '1') {
				link = 'bpjs/rujukan/manual/bpjs/lama'
			}
			api.get(`${link}/${this.state.nomor}`)
			.then(result => {
				api.get(`inap/sep/teratas/${result.data.no_rekmedis}`)
				.then(result2 => {
					result.data['nama_fpk'] = 'RS PERTAMEDIKA UMMI ROSNATI';
					result.data['ppk_rujukan'] = '0017R017';
					result.data['no_kontrol'] = result2.data.id_inap;
					result.data['no_rujukan'] = result2.data.bpjs_sep;
					this.props.getDataRujuk(result.data);
				})
				.catch(error => {
					this.props.error(error.response.data);
				});
			})
			.catch(error => {
				this.props.error(error.response.data);
			});
		});
   }
       
   render() {
      return (
         <Fragment>
            <ComboBox
					data={[
						{ value: 'bpjs', text: 'BPJS' },
						{ value: 'nik', text: 'NIK' },
					]}
					value={this.state.nama_nomor}
					onChange={
						(value) => {
							this.setState({
								nama_nomor: value,
							});
						}
					}
					style={{ marginRight: '4px', float: 'left', }}
					disabled={this.props.id_cabar === '2' ? false : true}
				/>
				<form onSubmit={this.cariPasien} style={{ float: 'left', }}>
					<input type="text"
						placeholder	="Ketik Nomor Lalu Enter"
						onChange={
							(e) => {
								this.setState({
									nomor: e.target.value,
								});
							}
						}
						onKeyDown={(e) => { 
							if (e.keyCode === 9) {
								this.setState({
									nomor: this.state.nomor.padStart(13, '0'),
								});
							}
						 }}
						disabled={this.props.id_cabar === '2' ? false : true}
						value={this.state.nomor}
					/>
				</form>
			</Fragment>
      );
   }
}

export default (CariRujukan);

import React, { Fragment } from 'react';
import { TextBox, Tooltip } from 'rc-easyui';
import { Chip, } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';

import api from '../config/api.js';

const CustomTableCell = withStyles(theme => ({
   head: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
   },
   body: {
      fontSize: 12,
   },
}))(TableCell);

const styles = theme => ({
   row: {
      '&:nth-of-type(odd)': {
         backgroundColor: theme.palette.background.default,
      },
   },
});

class CariFaskes extends React.Component {
    
   state = {
      open: false,
      dataFaskes: [],
      search: '',
      jenis_faskes: '1',
   }
   
   cariFaskes = () => {
      this.setState({
         open: true,
      });
   }

   closeFaskes = () => {
      this.setState({
         open: false,
      });
   }

   getDataFaskes = (e) => {
      e.preventDefault();
      api.get(`bpjs/referensi/faskes/${this.state.search}/${this.state.jenis_faskes}`)
      .then(result => {
         this.setState({
            dataFaskes: result.data,
         });
      })
      .catch(error => {
         console.log('keluar==', error)
      });
   }

   handleChange = (e) => {
      this.setState({
         [e.target.name]: e.target.value,
      });
   }

   getDataAktif = (data) => {
      this.setState({
         open: false,
      }, () => {
         data['asal_rujukan'] = this.state.jenis_faskes;
         this.props.getDataFaskes(data);
      });
   }

   render() {
      const { classes } = this.props;
      return (
         <Fragment>
            <TextBox
					addonRight={
						() => {
							return (
								<Tooltip content="Klik Disini Untuk Cari Faskes">
									<span
										className="textbox-icon icon-search"
										onClick={this.cariFaskes}
									/>
								</Tooltip>
							)
						}
					}
					style={{width:'250px'}}
					disabled
					value={this.props.value}
				/>
            <div style={{overflowX: 'auto'}}>
               <Dialog
                  open={this.state.open}
                  onClose={this.closeFaskes}
                  maxWidth="lg"
                  fullWidth={true}
               >
                  <DialogTitle>
                     <Grid container spacing={8} style={{ marginBottom: 10 }}>
                        <Grid item xs={6}>
                           <TextField
                              select label="Jenis Faskes"
                              onChange={this.handleChange}
                              SelectProps={{ native: true }}
                              InputLabelProps={{ shrink: true }}
                              value={this.state.jenis_faskes}
                              name="jenis_faskes" fullWidth
                           >
                              <option key="1" value="1">Faskes 1</option>
                              <option key="2" value="2">Faskes 2 / RS</option>
                           </TextField>
                        </Grid>
                        <Grid item xs={6}>
                           <form onSubmit={this.getDataFaskes}>
                              <TextField
                                 label="Cari Berdasarkan Kode / Nama Faskes"
                                 name="search"
                                 type="text"
                                 value={this.state.search}
                                 fullWidth
                                 onChange={this.handleChange}
                                 InputLabelProps={{ shrink: true }}
                                 autoComplete="off"
                              />
                           </form>
                        </Grid>
                     </Grid>
                  </DialogTitle>
                  <DialogContent>
                     <Table className={classes.table}>
                        <TableHead>
                           <TableRow>
                              <CustomTableCell align="center">No.</CustomTableCell>
                              <CustomTableCell align="center">Kode</CustomTableCell>
                              <CustomTableCell align="center">Nama</CustomTableCell>
                           </TableRow>
                        </TableHead>
                        <TableBody>
                           {
                              this.state.dataFaskes.map((row, index) => (
                                 <TableRow className={classes.row} key={index}>
                                    <CustomTableCell align="center">{index+1}</CustomTableCell>
                                    <CustomTableCell align="center">
                                       <Chip
                                          label={row.kode}
                                          clickable
                                          onClick={() => {this.getDataAktif(row)}}
                                       />
                                    </CustomTableCell>
                                    <CustomTableCell align="center">{row.nama}</CustomTableCell>
                                 </TableRow>
                              ))
                           }
                        </TableBody>
                     </Table>
                  </DialogContent>
                  <DialogActions>
                     <Button onClick={this.closeFaskes} color="primary">Close</Button>
                  </DialogActions>
               </Dialog>
            </div>
         </Fragment>
      );
   }
}

CariFaskes.propTypes = {
   classes: PropTypes.object.isRequired,
};

export default (withStyles(styles)(CariFaskes));

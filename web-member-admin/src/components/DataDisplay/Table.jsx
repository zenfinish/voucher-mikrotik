import React from 'react';
import { Table as TableAntd } from 'antd';

class Table extends React.Component {
	
	render() {
		return (
      <div className="overflow-scroll">
        <TableAntd
          columns={this.props.columns}
          dataSource={this.props.data}
          bordered
          scroll={{ x: 1000, y: 750 }}
          loading={this.props.loading}
          size="small"
          rowKey={this.props.rowKey}
          pagination={this.props.pagination}
        />
      </div>
		);
	}

}

export default Table;

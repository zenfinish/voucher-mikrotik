import React from 'react';
import Select from 'react-select';

class SelectKatarak extends React.Component {
   
   state = {
      data: [
         { value: '0', label: 'Tidak', target: { value: '0', name: 'katarak' } },
         { value: '1', label: 'Ya', target: { value: '1', name: 'katarak' } }
      ]
   }

   render() {
      return (
         <Select
            onChange={this.props.onChange}
            value={this.state.data.filter(option => option.value === this.props.value)}
            options={this.state.data}
            placeholder="Katarak"
            isDisabled={this.props.disabled}
         />
      );
   }

}

export default (SelectKatarak);

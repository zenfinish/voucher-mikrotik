import React from 'react';
import { notification, Modal } from 'antd';
import Button from 'components/inputs/Button.jsx';
import api from 'config/api.js';

class AktifkanList extends React.Component {

	state = {
		loading: false,
	}

  delete = () => {
    this.setState({ loading: true }, () => {
			api.put(`/tarif/${this.props.data.id}/aktif/${this.props.data[`id_tar${this.props.data.id}`]}`)
			.then(result => {
        this.props.closeRefresh();
			})
			.catch(error => {
				this.setState({ loading: false });
				notification.error({
					message: 'Error',
					description: JSON.stringify(error.response.data),
				});
			});
		});
  }
	
	render() {
    return (
			<Modal
				centered
				width="100%"
				visible={true}
				onCancel={this.props.close}
				footer={[
					<Button color="primary" onClick={this.delete} loading={this.state.loading} key="simpan">Simpan</Button>
				]}
				title={`Aktifkan Status`}
			>
        <div className="relative">
          <div>Nama Tindakan : {this.props.data[`nama_tar${this.props.data.id}`]}</div>
					<div>ID : {this.props.data[`id_tar${this.props.data.id}`]}</div>
				</div>
      </Modal>
		);
	}

};

export default AktifkanList;

import React from 'react';
import { notification, Modal } from 'antd';
import Button from 'components/inputs/Button.jsx';
import api from 'config/api.js';

class UpdateTarif extends React.Component {

	state = {
    loading: false,
    tarif: ''
	}

	componentDidMount() {
    this.setState({ ...this.props.data, tarif: !this.props.data.tarif ? 0 : this.props.data.tarif });
  }

  update = () => {
    this.setState({ loading: true }, () => {
			api.post(`tarif/${this.props.data.id}/detil`, {
        [`id_tar${this.props.data.id}`]: this.props.data[`id_tar${this.props.data.id}`],
        tarif: this.state.tarif,
      })
			.then(result => {
        this.setState({ loading: false }, () => {
          this.props.closeRefresh();
        });
			})
			.catch(error => {
				notification.error({
          message: 'Error',
          description: JSON.stringify(error.response.data),
        });
        this.setState({ loading: false });
			});
		});
  }
	
	render() {
    return (
			<Modal
        centered
        width="100%"
        visible={true}
        onCancel={this.props.close}
        footer={[
          <Button color="primary" onClick={this.update} loading={this.state.loading} key="simpan">Update</Button>
        ]}
        title={`Update Tarif`}
      >
        <div className="relative">
          <div>Nama : {this.props.data[`nama_tar${this.props.data.id}`]}</div>
					<input type="number" onChange={(e) => {this.setState({tarif: e.target.value})}} value={this.state.tarif} />
				</div>
      </Modal>
		);
	}

};

export default UpdateTarif;

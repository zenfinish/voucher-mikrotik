import React from 'react';
import { notification, Table, Input, Space, Button as ButtonAntd } from 'antd';
import { AppleFilled, SearchOutlined } from '@ant-design/icons';
import Button from 'components/inputs/Button.jsx';
import Label from 'components/Label.jsx';
import Add from './Add.jsx';
import UpdateTarif from './UpdateTarif.jsx';
import HapusList from './HapusList.jsx';
import AktifkanList from './AktifkanList.jsx';
import api from 'config/api.js';
import { separator } from 'config/helpers.js';

class Index extends React.Component {

	state = {
		data: [],
		loading: false,
		loadingSearch: false,
		dataActive: {},
		openAdd: false,
		openDelete: false,
		openUpdateTarif: false,
		openHapusList: false,
		openAktifkanList: false,
	}

	UNSAFE_componentWillMount() {
		this.refreshTable();
	}

	refreshTable = () => {
		this.setState({ loading: true }, () => {
			api.get(`/tarif/${this.props.data.id}/all`)
			.then(result => {
				this.setState({ data: result.data, loading: false });
			})
			.catch(error => {
				console.log('Error: ', error.response);
				this.setState({ loading: false });
			});
		});
	}

	getColumnSearchProps = dataIndex => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={node => {
            this.searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
          style={{ width: 188, marginBottom: 8, display: 'block' }}
        />
        <Space>
          <ButtonAntd
            type="primary"
            onClick={() => {
              this.handleSearch(selectedKeys, confirm, dataIndex);
            }}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >Search</ButtonAntd>
          <ButtonAntd onClick={() => this.handleReset(clearFilters)} size="small" style={{ width: 90 }}>
            Reset
          </ButtonAntd>
          <ButtonAntd
            type="link"
            size="small"
            onClick={() => {
              confirm({ closeDropdown: false });
              this.setState({
                searchText: selectedKeys[0],
                searchedColumn: dataIndex,
              });
            }}
          >
            Filter
          </ButtonAntd>
        </Space>
      </div>
    ),
    filterIcon: filtered => <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />,
    onFilter: (value, record) =>
      record[dataIndex]
        ? record[dataIndex].toString().toLowerCase().includes(value.toLowerCase())
        : '',
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => this.searchInput.select(), 100);
      }
    },
    render: text => 
      ( text ),
  });

  handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    this.setState({
      searchText: selectedKeys[0],
      searchedColumn: dataIndex,
    });
  };

  handleReset = clearFilters => {
    clearFilters();
    this.setState({ searchText: '' });
  };
	
	render() {
		return (
			<div className="w-full h-full">
				<div className="flex justify-between">
					<h5>Tarif Pelayanan {this.props.data.nama}</h5>
					<div>
						<Button icon="Add"
							onClick={() => {
								this.setState({ openAdd: true });
							}}
						>Tambah</Button>
					</div>
				</div>
				<Table
					columns={[
						{
							title: 'Action',
							key: 'action',
							render: (dataIndex, row) => (
								<>
									<span
										className="rounded px-2 py-1 text-indigo-700 shadow-inner cursor-pointer hover:bg-indigo-700 hover:text-white"
										onClick={() => {
											if (row.hapus && row.hapus !== 0 && row.hapus !== '' && row.hapus !== '0') {
												notification.error({
													message: 'Error',
													description: 'Data Non Aktif',
												});
											} else {
												this.props.data[`id_tar${this.props.data.id}`] = row[`id_tar${this.props.data.id}`];
												this.props.data[`nama_tar${this.props.data.id}`] = row[`nama_tar${this.props.data.id}`];
												this.props.data['tarif'] = row.tarif;
												this.setState({ openUpdateTarif: true });
											}
										}}
										title="Update Tarif"
									><AppleFilled /></span>
								</>
							),
							width: 10,
							fixed: 'left',
						},
						{
							title: 'Nama Tindakan',
							key: "nama_tindakan",
							dataIndex: `nama_tar${this.props.data.id}`,
							width: 50,
							fixed: 'left',
							...this.getColumnSearchProps(`nama_tar${this.props.data.id}`),
						},
						{
							title: 'Kode',
							key: "kode_tindakan",
							dataIndex: `id_tar${this.props.data.id}`,
							width: 10,
						},
						{
							title: 'Status',
							key: `status`,
							render: (dataIndex, row) => (
								<>
									{
										row.hapus && row.hapus !== 0 && row.hapus !== '' && row.hapus !== '0' ?
											<Label
												className="mr-1 cursor-pointer"
												onClick={() => {
													this.props.data[`id_tar${this.props.data.id}`] = row[`id_tar${this.props.data.id}`];
													this.props.data[`nama_tar${this.props.data.id}`] = row[`nama_tar${this.props.data.id}`];
													this.setState({ openAktifkanList: true });
												}}
											>Non Aktif</Label>
										: 
											<Label
												className="mr-1 cursor-pointer"
												onClick={() => {
													this.props.data[`id_tar${this.props.data.id}`] = row[`id_tar${this.props.data.id}`];
													this.props.data[`nama_tar${this.props.data.id}`] = row[`nama_tar${this.props.data.id}`];
													this.setState({ openHapusList: true });
												}}
											>Aktif</Label>
									}
								</>
							),
							width: 50,
						},
						{
							title: 'Tarif',
							key: `tarif`,
							render: (dataIndex, row) => (
								<><span>{separator(row.tarif)}</span></>
							),
							width: 50,
						},
					]}
					dataSource={this.state.data}
					bordered
					scroll={{ x: 1000, y: 400 }}
					loading={this.state.loading}
					rowClassName={(row) => row.hapus && row.hapus !== 0 && row.hapus !== '' && row.hapus !== '0' ? 'bg-red-200' : ''}
					size="small"
				/>

				{
					this.state.openAdd ?
						<Add
							close={() => { this.setState({ openAdd: false }) }}	
							closeRefresh={() => { this.setState({ openAdd: false }, () => { this.refreshTable() }) }}
							data={this.props.data}
						/>
					: null
				}
				{
					this.state.openUpdateTarif ?
						<UpdateTarif
							close={() => { this.setState({ openUpdateTarif: false }) }}
							closeRefresh={() => { this.setState({ openUpdateTarif: false }, () => { this.refreshTable() }) }}
							data={this.props.data}
						/>
					: null
				}
				{
					this.state.openHapusList ?
						<HapusList
							close={() => { this.setState({ openHapusList: false }) }}
							closeRefresh={() => { this.setState({ openHapusList: false }, () => { this.refreshTable() }) }}
							data={this.props.data}
						/>
					: null
				}
				{
					this.state.openAktifkanList ?
						<AktifkanList
							close={() => { this.setState({ openAktifkanList: false }) }}
							closeRefresh={() => { this.setState({ openAktifkanList: false }, () => { this.refreshTable() }) }}
							data={this.props.data}
						/>
					: null
				}
	
			</div>
		);
	}

}

export default Index;

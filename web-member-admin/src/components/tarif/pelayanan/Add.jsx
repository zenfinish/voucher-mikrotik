import React from 'react';
import { notification, Modal } from 'antd';
import Button from 'components/inputs/Button.jsx';
import TextField from 'components/inputs/TextField.jsx';

import api from 'config/api.js';

class Add extends React.Component {

	state = {
    loading: false,
    [`nama_tar${this.props.data.id}`]: '',
  }

  simpan = () => {
    this.setState({ loading: true }, () => {
      api.post(`tarif/${this.props.data.id}`, this.state)
			.then(result => {
        this.setState({ loading: false }, () => {
          this.props.closeRefresh();
        });
			})
			.catch(error => {
        this.setState({ loading: false });
        notification.error({
          message: 'Error',
          description: JSON.stringify(error.response.data),
        });
			});
		});
  }
	
	render() {
    return (
			<Modal
        centered
        width="100%"
        visible={true}
        onCancel={this.props.close}
        footer={[
          <Button color="primary" onClick={this.simpan} loading={this.state.loading} key="simpan">Tambah</Button>
        ]}
        title={`Tambah ${this.props.data.nama}`}
      >
        <div className="relative">
          <div>Nama {this.props.data.nama}</div>
					<TextField
            onChange={(e) => {
              this.setState({ [`nama_tar${this.props.data.id}`]: e.target.value });
            }}
          />
				</div>
      </Modal>
		);
	}

};

export default Add;

import React from 'react';
import TextField from '@material-ui/core/TextField';

class TextFieldTotalLab extends React.Component {

   render() {
      return (
         <TextField
            label="Pemeriksaan Lab"
            name="total_lab"
            type="number" variant="outlined"
            value={this.props.value}
            fullWidth
            InputProps={this.props.disabled === true ? { disabled: true, } : { disabled: false, }}
            onChange={this.props.onChange}
            InputLabelProps={{ shrink: true }}
            style={{height: '40px'}}
         />
      );
   }

}

export default TextFieldTotalLab;

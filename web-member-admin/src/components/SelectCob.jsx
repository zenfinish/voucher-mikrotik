import React from 'react';
import TextField from '@material-ui/core/TextField';

class SelectCob extends React.Component {
   
   render() {
      return (
         <TextField
            select
            label="Cob"
            onChange={this.props.onChange}
            SelectProps={{ native: true }} InputLabelProps={{ shrink: true }}
            name="cob"
            fullWidth
            value={this.props.value}
         >
            <option key="0" value="0">TIDAK</option>
            <option key="1" value="1">YA</option>
         </TextField>
      );
   }

}

export default SelectCob;

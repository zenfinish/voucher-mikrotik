import React from 'react';
import TextField from '@material-ui/core/TextField';

class SelectRujukan extends React.Component {

   render() {
      return (
         <TextField
            label="Rujukan"
            InputLabelProps={{ shrink: true }}
            SelectProps={{ native: true }}
            fullWidth select
            onChange={this.props.onChange}
            name="rujukan"
            value={this.props.value}
         >
            <option key="sistem" value="sistem">Sistem</option>
            <option key="manual" value="manual">Manual</option>
         </TextField>
      );
   }

}

export default SelectRujukan;
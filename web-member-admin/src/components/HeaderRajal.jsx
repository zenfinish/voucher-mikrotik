import React from 'react';
import Grid from '@material-ui/core/Grid';

class HeaderRajal extends React.Component {

   render() {
      return (
         <Grid container spacing={8} style={{fontSize: 14, fontWeight: 'bold'}}>
            <Grid item xs={4}>
               <table>
                  <tbody>
                     <tr>
                        <td>Nama</td>
                        <td>:</td>
                        <td>{this.props.data.nama_pasien}</td>
                     </tr>
                     <tr>
                        <td>No. Rekmedis</td>
                        <td>:</td>
                        <td>{this.props.data.no_rekmedis}</td>
                     </tr>
                     <tr>
                        <td>Total Farmasi</td>
                        <td>:</td>
                        <td>{this.props.data.no_rekmedis}</td>
                     </tr>
                  </tbody>
               </table>
            </Grid>
            <Grid item xs={4}>
               <table>
                  <tbody>
                     <tr>
                        <td>Cara Bayar</td>
                        <td>:</td>
                        <td>{this.props.data.nama_cabar}</td>
                     </tr>
                     <tr>
                        <td>Poliklinik</td>
                        <td>:</td>
                        <td>{this.props.data.nama_poli}</td>
                     </tr>
                  </tbody>
               </table>
            </Grid>
            <Grid item xs={4}>
               <table>
                  <tbody>
                     <tr>
                        <td>Tgl Checkin</td>
                        <td>:</td>
                        <td>{this.props.data.tgl_checkin}</td>
                     </tr>
                     <tr>
                        <td>Sep BPJS</td>
                        <td>:</td>
                        <td>{this.props.data.bpjs_sep}</td>
                     </tr>
                  </tbody>
               </table>
            </Grid>
         </Grid>
      );
   }

}

export default HeaderRajal;

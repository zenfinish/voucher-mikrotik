import React from 'react';

class Radio extends React.Component {

	render() {
		return (
      <label className={`${this.props.className}`}>
        <input
          type="radio"
          checked={this.props.checked}
          name={this.props.name}
          onChange={this.props.onChange}
          value={this.props.value}
        />
        <span>{this.props.text}</span>
      </label>
		)
	}

}

export default Radio;

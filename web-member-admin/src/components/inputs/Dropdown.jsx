import React from 'react';
import Select from 'react-select';

class Dropdown extends React.Component {

	render() {
		return (
      <Select
        options={this.props.data ? this.props.data.filter(option => {return option}) : null}
        onChange={this.props.onChange ? this.props.onChange : null}
        value={this.props.data ? this.props.data.filter(option => option && option.value === this.props.value) : ''}
        styles={{
          control: (styles) => ({
            ...styles,
            fontSize: '12px',
            width: this.props.width,
          }),
          option: (styles) => ({
            ...styles,
            fontSize: '12px',
          }),
          container: styles => ({
            ...styles,
          }),
        }}
        isDisabled={this.props.disabled}
        className={this.props.className}
        placeholder={this.props.placeholder}
      />
		)
	}

}

export default Dropdown;

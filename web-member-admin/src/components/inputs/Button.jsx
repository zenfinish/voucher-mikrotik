import React from 'react';
import Icon from '../Icon.jsx';

class Button extends React.Component {
  
	render() {
		return (
			<button
				className={
					`
						px-2 py-1 rounded focus:outline-none text-sm hover:bg-gray-400 shadow
						${this.props.className}
						${
							this.props.color === 'primary' ?
								'bg-blue-700 text-white'
							: this.props.color === 'secondary' ?
								'bg-red-700 text-white'
							: this.props.color === 'transparent' ?
								'bg-transparent text-black'
							: 'bg-white text-black'
						}
						${this.props.disabled ? 'bg-gray-400' : ''}
					`
				}
				onClick={this.props.onClick}
				disabled={this.props.loading ? true : this.props.disabled}
				title={this.props.title}
				onBlur={this.props.onBlur}
			>
				<div className="flex items-center">
					{
						this.props.loading ?
						<img src={require('assets/imgs/loading2.gif')} alt="" className="w-6" />
						: 
						<>
							{this.props.icon  ? <Icon name={this.props.icon} className={`fill-current w-4 ${this.props.children ? 'mr-2' : ''}`} /> : null}
							{this.props.children ? <span>{this.props.children}</span> : null}
						</>
					}
				</div>
				{
					this.props.badge ?
						<span className="absolute rounded-full bg-red-900 p-1" style={{ top : -20, right: -10 }}>{this.props.badge}</span>
					: null
				}
			</button>
		)
	}

}

export default Button;

import React from 'react';

class DateBox extends React.Component {

	render() {
		return (
      <input
        type="date"
        className={`text-sm shadow rounded py-1 px-2 text-gray-700 focus:outline-none ${this.props.className}`}
        onChange={this.props.onChange}
        value={this.props.value}
        disabled={this.props.disabled}
      />
		)
	}

}

export default DateBox;

import React from 'react';
import { TextField } from '@material-ui/core';

class TextFieldNamaPasien extends React.Component {

   render() {
      return (
         <TextField
            label="Nama Pasien"
            name="nama_pasien"
            type="text"
            value={this.props.value}
            onChange={this.props.onChange}
            InputLabelProps={{ shrink: true }}
            InputProps={this.props.disabled === true ? { disabled: true, } : { disabled: false, }}
            fullWidth
         />
      );
   }

}

export default TextFieldNamaPasien;

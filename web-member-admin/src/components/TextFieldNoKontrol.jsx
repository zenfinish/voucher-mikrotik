import React from 'react';
import TextField from '@material-ui/core/TextField';

class TextFieldNoKontrol extends React.Component {

   render() {
      return (
         <TextField
            label="No. Kontrol"
            name="no_kontrol"
            type="text"
            value={this.props.value}
            fullWidth
            InputProps={this.props.disabled === true ? { disabled: true, } : { disabled: false, }}
            onChange={this.props.onChange}
            InputLabelProps={{ shrink: true }}
         />
      );
   }

}

export default TextFieldNoKontrol;

import React from 'react';

class Loading extends React.Component {

	render() {
		return (
			<div className="absolute w-full h-full flex items-center justify-center bg-black bg-opacity-25 z-30 top-0 left-0">
				<div className="bg-white rounded-lg shadow">
					<img src={require('assets/imgs/loading2.gif')} alt="" className="h-12 w-auto" />
				</div>
			</div>
		)
	}

}

export default Loading;

import React, { Component } from "react";

class Autocomplete extends Component {

  static defaultProps = {
    suggestions: []
  };

  constructor(props) {
    super(props);

    this.state = {
      activeSuggestion: -1,
      filteredSuggestions: [],
      showSuggestions: false,
      userInput: "",
      loading: false,
      active: '',
    };
  }

  onKeyDown = async (e) => {
    const { activeSuggestion } = this.state;
    const value = e.currentTarget.value;
    if (e.keyCode === 13) { /* Enter */
      if (activeSuggestion === -1) {
        await this.props.onEnter(value);
        if (this.props.data.length > 0) {
          this.setState({
            activeSuggestion: 0,
            showSuggestions: true,
            active: this.props.data[0].value,
          });
        } else {
          this.setState({
            activeSuggestion: -1,
            showSuggestions: true,
          });
        }
      } else if (activeSuggestion !== -1) {
        const wkwk = this.props.data.filter((row) => {return row.value === this.state.active});
        await this.props.onSelect(wkwk[0]);
        this.setState({
          activeSuggestion: -1,
          showSuggestions: false
        });
      }
    }
    else if (e.keyCode === 38) { /* Up */
      if (activeSuggestion > -1) {
        this.setState({
          activeSuggestion: activeSuggestion - 1,
        }, () => {
          if (this.state.activeSuggestion === -1) { return; }
          this.setState({ active: this.props.data[this.state.activeSuggestion].value });
        });
      }
    }
    else if (e.keyCode === 40) { /* Down */
      this.setState({
        showSuggestions: true
      }, () => {
        if (activeSuggestion < this.props.data.length - 1) {
          this.setState({
            activeSuggestion: activeSuggestion + 1,
          }, () => {
            if (this.state.activeSuggestion === -1) { return; }
            this.setState({ active: this.props.data[this.state.activeSuggestion].value });
          });
        }
      });
    }
    else if (e.keyCode === 8) { /* Backspace */
      this.setState({
        activeSuggestion: -1,
        showSuggestions: false
      });
    }
    else if (e.keyCode === 46) { /* Delete */
      this.setState({
        activeSuggestion: -1,
        showSuggestions: false
      });
    }
    else if (e.keyCode === 27) { /* Esc */
      this.setState({
        activeSuggestion: -1,
        showSuggestions: false
      });
    }
  };

  onChange = () => {
    this.setState({
      activeSuggestion: -1,
      showSuggestions: false
    });
  }

  render() {
    const {
      onChange, onKeyDown,
      state: { showSuggestions, active }
    } = this;

    let suggestionsListComponent;

    if (showSuggestions) {
      if (this.props.data.length) {
        suggestionsListComponent = (
          <div
            className="absolute mt-2 py-2 bg-white rounded-lg shadow-xl h-40 overflow-auto"
            style={{ width: '600px', zIndex: 100 }}
          >
            <ul className="overflow-y-auto pl-0">
              {this.props.data.map((row, i) => {
                let className;
                if (row.value === active) { className = "bg-black text-white"; }
                return (
                  <li
                    className={`${className} hover:bg-black hover:text-white p-1 text-sm`}
                    key={i}
                    onClick={() => {
                      this.setState({
                        activeSuggestion: -1,
                        showSuggestions: false
                      }, () => {
                        this.props.onSelect(row);
                      });
                    }}
                  >{this.props.list(row)}</li>);
              })}
            </ul>
          </div>
        );
      } else {
        suggestionsListComponent = (
          <div className="text-gray-600 absolute mt-2 py-2 bg-white rounded-lg shadow-xl w-64 z-30">
            <em>Data Kosong</em>
          </div>
        );
      }
    }

    return (
      <div className="relative">
        <input
          type="text"
          onChange={onChange}
          onKeyDown={onKeyDown}
          className={`text-sm shadow rounded py-1 px-2 text-gray-700 focus:outline-none ${this.props.className}`}
          placeholder={this.props.placeholder}
          disabled={this.state.loading ? true : this.props.disabled}
          style={this.props.style}
        />
        {suggestionsListComponent}
      </div>
    );
  }
}

export default Autocomplete;
import React from 'react';
import TextField from '@material-ui/core/TextField';

class SelectJkel extends React.Component {

   render() {
      return (
         <TextField
            select
            label="Jenis Kelamin"
            onChange={this.props.onChange}
            SelectProps={{ native: true }}
            InputLabelProps={{ shrink: true }}
            value={this.props.value}
            name="jkel"
            fullWidth
            InputProps={this.props.disabled === true ? { disabled: true, } : { disabled: false, }}
         >
            <option key="" value=""></option>
            <option key="LAKI - LAKI" value="LAKI - LAKI">LAKI - LAKI</option>
            <option key="PEREMPUAN" value="PEREMPUAN">PEREMPUAN</option>
         </TextField>
      );
   }

}

export default SelectJkel;
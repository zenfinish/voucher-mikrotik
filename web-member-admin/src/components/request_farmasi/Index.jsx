import React, { useState, useRef } from 'react';
import { IconButton, Tooltip, Button } from '@material-ui/core';
import DeleteForeverOutlinedIcon from '@material-ui/icons/DeleteForeverOutlined';
import api from 'config/api.js';
import { Table, Input, Select, Modal, notification } from 'antd';
import Search from './Search';

const { Option } = Select;

const CapakComponent = (row, handleSave, optionCapak) => {
	const [value, setValue] = useState(row.cara_pakai);
	const refCapak = useRef();

	const cekValue = () => {
		handleSave({ ...row, cara_pakai: value });
	}

	return <Input
		onPressEnter={cekValue}
		onBlur={cekValue}
		addonAfter={(
			<Select
				value=""
				dropdownStyle={{ minWidth: '50%' }}
				onChange={(value) => {
					setValue(value);
					refCapak.current.focus();
				}}
			>
				<Option value="">Klik</Option>
				{
					optionCapak.map((row, i) => (
						<Option key={i} value={row.keterangan}>{row.keterangan}</Option>
					))
				}
			</Select>
		)}
		value={value}
		onChange={(e) => setValue(e.target.value)}
		ref={refCapak}
	/>
}

const KeteranganComponent = (row, handleSave) => {
	const [value, setValue] = useState(row.keterangan);
	const refKeterangan = useRef();

	const cekValue = () => {
		handleSave({ ...row, keterangan: value });
	}

	return <Input
		onPressEnter={cekValue}
		onBlur={cekValue}
		value={value}
		onChange={(e) => setValue(e.target.value)}
		ref={refKeterangan}
	/>
}

class Index extends React.Component {
	
	state = {
		disableSearch: false,
		optionCapak: [],
		searchObat: '',
		loadingObat: false,
		optionCariObat: [],
		disabledSimpan: false,
		id_fardepo: this.props.pelayanan.id === 'igd' ? '1' : this.props.pelayanan.id === 'poli' || this.props.pelayanan.id === 'inap' ? '2' : '',
		dataTable: [],
	}

	componentDidMount() {
		api.get(`/farmasi/capak/all`)
		.then(result => {
			this.setState({ optionCapak: result.data });
		})
		.catch(error => {
			console.log(error);
		});

		if (this.props.dataExpand) {
			this.setState({ dataTable: this.props.dataExpand });
		};


	}

	handleAdd = (data) => {
    	const { dataTable } = this.state;
		const key = this.state.id_fardepo === '1' ? data.id_fardepoigd : this.state.id_fardepo === '2' ? data.id_fardepoapotek : null;
		if (dataTable.length > 0) {
			const guguk = dataTable.filter((row) => key === row.key);
			if (guguk.length > 0) {
				return notification.error({
					message: 'Error',
					description: "Obat Tidak Boleh Lebih Dari Satu No. Stok",
				});
			}
		}
		const newData = {
			key: key,
			id_fardepoapotek: data.id_fardepoapotek,
			id_fardepoigd: data.id_fardepoigd,
			id_farfakturdet: data.id_farfakturdet,
			nama_obat: data.nama_farobat,
			id_farobat: data.id_farobat,
			nama_farsatuan: data.nama_farsatuan,
			qty: '',
			cara_pakai: '',
			keterangan: '',
			stok: data.stok,
		};
		this.setState({ dataTable: [...dataTable, newData], });
	}

	handleSave = (row) => {
		const newData = [...this.state.dataTable];
		const index = newData.findIndex((item) => row.key === item.key);
		const item = newData[index];
		newData.splice(index, 1, { ...item, ...row });
		this.setState({ dataTable: newData, });
	}

	handleDelete = (key) => {
		const dataTable = [...this.state.dataTable];
		this.setState({ dataTable: dataTable.filter((item) => item.key !== key), });
	};

	QtyComponent = (row, handleSave) => {
		const [value, setValue] = useState(row.qty);
		const refQty = useRef();
		const cekValue = () => {
			if (Number(row.stok) - Number(value) < 0) {
				setValue("");
				refQty.current.focus();
				return notification.error({
					message: 'Error',
					description: "Stok Tidak Mencukupi",
				});
			};
			handleSave({ ...row, qty: value });
		}
		return <Input
			onPressEnter={cekValue}
			onBlur={cekValue}
			type="number"
			placeholder="0"
			value={value}
			onChange={(e) => setValue(e.target.value)}
			ref={refQty}
		/>
	}

	render() {
		const { dataTable } = this.state;
		
		return (
			<>
				<Modal
					centered
					width="100%"
					visible={true}
					onCancel={this.props.close}
					footer={[
						<Button variant="outlined" color="primary" key="simpan"
							onClick={() => {
								if (this.state.id_fardepo !== '1' && this.state.id_fardepo !== '2') {
									notification.error({
										message: 'Error',
										description: "Depo Penjualan Belum Dipilih",
									});
								} else if (this.state.dataTable.length === 0) {
									notification.error({
										message: 'Error',
										description: "Obat Belum Dipilih",
									});
								} else {
									const guguk = this.state.dataTable.filter((row) => row.qty === 0 || row.qty === "" || row.qty === "0");
									if (guguk.length > 0) {
										return notification.error({
											message: 'Error',
											description: `Qty ${guguk[0].nama_obat} [${guguk[0].id_farobat}]  Nol.`,
										});
									}
									this.setState({ disabledSimpan: true }, () => {
										api.post(`/farmasi/permintaan/${this.state.id_fardepo === '1' ? 'igd' : this.state.id_fardepo === '2' ? 'apotek' : ''}`, {
											id_checkin: this.props.pelayanan.id === 'inap' ? this.props.data.id_pelinap : this.props.data.id_peljalan,
											no_rekmedis: this.props.data.no_rekmedis,
											nama_pelpasien: this.props.data.nama_pelpasien,
											id_pelcabar: this.props.data.id_pelcabar,
											id_farresep: this.props.data.id_farresep,
											id_fardepo: this.state.id_fardepo,
											data: this.state.dataTable,
										})
										.then(result => {
											this.setState({ disabledSimpan: false }, () => {
												this.props.closeRefresh(this.state.dataTable);
											});
										})
										.catch(error => {
											notification.error({
												message: 'Error',
												description: JSON.stringify(error.response.data),
											});
											this.setState({ disabledSimpan: false });
										});
									});
								}
							}}
							disabled={this.state.disabledSimpan}
						>Simpan</Button>
					]}
				>
					<table className="whitespace-nowrap w-full text-sm mb-2" cellPadding={1}>
						<tbody>
							<tr>
								<td>Nama Pasien</td>
								<td>:</td>
								<td>{this.props.data.nama_pelpasien} [{this.props.data.no_rekmedis}]</td>
								<td>Tanggal Masuk</td>
								<td>:</td>
								<td>{this.props.data.tgl_checkin}</td>
							</tr>
							<tr>
								<td>Cara Bayar</td>
								<td>:</td>
								<td>{this.props.data.nama_pelcabar}</td>
								<td>Unit</td>
								<td>:</td>
								<td>{this.props.data.unit}</td>
							</tr>
						</tbody>
					</table>
					<Search
						onSelected={(data) => {
							this.handleAdd(data);
						}}
						link={`/farmasi/obat/${this.state.id_fardepo === '1' ? 'igd' : this.state.id_fardepo === '2' ? 'apotek' : ''}/search`}
					/>
					<Table
						columns={[
							{
								title: 'Nama Obat',
								render: (text, record) => `${record.nama_obat} [${record.id_farobat}] [Stok: ${record.stok}]`,
							},
							{ title: 'Satuan', dataIndex: 'nama_farsatuan' },
							{
								title: 'Qty',
								dataIndex: 'qty',
								render: (dataIndex, row) => this.QtyComponent(row, this.handleSave),
								width: 100
							},
							{
								title: 'Cara Pakai',
								dataIndex: 'cara_pakai',
								render: (dataIndex, row) => CapakComponent(row, this.handleSave, this.state.optionCapak),
							},
							{
								title: 'Keterangan',
								dataIndex: 'keterangan',
								render: (dataIndex, row) => KeteranganComponent(row, this.handleSave),
							},
							{
								title: 'Action',
								dataIndex: 'action',
								render: (dataIndex, row) => (
									<Tooltip title="Hapus">
										<IconButton
											color="secondary"
											style={{
												padding: 5, minHeight: 0, minWidth: 0
											}}
											onClick={() => {
												this.handleDelete(row.key)
											}}
										><DeleteForeverOutlinedIcon fontSize="small" /></IconButton>
									</Tooltip>
								)
							},
						]}
						dataSource={dataTable}
						size="small"
						pagination={false}
						className="whitespace-nowrap"
						bordered
					/>
				</Modal>
			</>
		)
	}

};

export default (Index);

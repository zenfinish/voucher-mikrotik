import React from 'react';
import { Select } from 'antd';
import { api } from '../../helpers';
import { tglIndo } from 'config/helpers.js';

const { Option } = Select;

let timeout;
let currentValue;
function fetchDatas(value, link, callback) {
  if (timeout) {
    clearTimeout(timeout);
    timeout = null;
  }
  currentValue = value;

  function fake() {
    api.get(link, {
      headers: { search: value }
    })
		.then(result => {
      if (currentValue === value) {
				callback(result)
			}
		})
		.catch(error => console.log('error=======', error));
  }

  timeout = setTimeout(fake, 300);
}

class Search extends React.Component {

	state = {
		dataPasien: [],
    id_fardepoapotek: '',
    id_fardepoigd: '',
		loading: false,
	}

	handleSearchPasien = value => {
    if (value && value.length > 2) {
      fetchDatas(value, this.props.link, (data) => {
				this.setState({ dataPasien: data })
			});
    } else {
      this.setState({ dataPasien: [] });
    }
  };

  handleChangePasien = (value, option) => {
    this.setState({ id_fardepoapotek: "", id_fardepoigd: "" }, () => {
      this.props.onSelected(option.data);
    });
  };
	
	render() {
		const { link } = this.props;
    const { id_fardepoapotek, id_fardepoigd } = this.state;
    const optionsPasien = this.state.dataPasien.map(d =>
      <Option key={link === "/farmasi/obat/igd/search" ? d.id_fardepoigd : link === "/farmasi/obat/apotek/search" ? d.id_fardepoapotek : ""} data={d}>
        <div className="grid grid-cols-4 gap-1">
          <span>{d.nama_farobat} [{d.id_farobat}]</span>
          <span>{d.id_farfakturdet}</span>
          <span>{d.stok}</span>
          <span>{tglIndo(d.tgl_kadaluarsa)}</span>
        </div>
      </Option>
    )
		return (
			<>
        <Select
          showSearch
          value={link === "/farmasi/obat/igd/search" ? id_fardepoigd : link === "/farmasi/obat/apotek/search" ? id_fardepoapotek : ""}
          defaultActiveFirstOption={false}
          showArrow={false}
          filterOption={false}
          onSearch={this.handleSearchPasien}
          onChange={this.handleChangePasien}
          notFoundContent={null}
          className="w-full"
          placeholder="Tambah Obat | Cari Disini"
        >
          <Option disabled>
            <div className="grid grid-cols-4 gap-1">
              <span>Nama Obat</span>
              <span>No. Stok</span>
              <span>Stok</span>
              <span>Tgl. Kadaluarsa</span>
            </div>
          </Option>
          {optionsPasien}
        </Select>
			</>
		);
	}

};

export default Search;

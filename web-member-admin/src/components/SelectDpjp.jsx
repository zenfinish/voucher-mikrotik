import React from 'react';
import Select from 'react-select';
import api from '../config/api.js';

class SelectDpjp extends React.Component {
   
   state = {
      data: []
   }

   componentDidMount() {
      api.get(`/referensi/dokter`)
      .then(result => {
         let hasil = [];
         result.data.forEach(row => {
            hasil.push({ value: row.id, label: row.nama_karyawan, target: { value: row.id_poli, name: 'id_dokter' } });
         });
         this.setState({
            data: hasil
         });
      })
      .catch(error => {
         console.log(error.response)
      });
   }

   render() {
      return (
         <Select
            onChange={this.props.onChange}
            value={this.state.data.filter(option => option.value === this.props.value)}
            options={this.state.data}
            placeholder="Dokter DPJP"
            isDisabled={this.props.disabled}
         />
      );
   }

}

export default (SelectDpjp);

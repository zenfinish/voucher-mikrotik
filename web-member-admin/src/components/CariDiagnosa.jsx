import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { TextBox } from 'rc-easyui';
import Autocomplete from '../components/input/Autocomplete.jsx';

import api from '../config/api.js';

const styles = theme => ({
   row: {
      '&:nth-of-type(odd)': {
         backgroundColor: theme.palette.background.default,
      },
   },
});

class CariDiagnosa extends React.Component {
    
   state = {
		dataDiagnosa: [],
		dataTampilDiagnosa: [],
      search: '',
   }
   
   getDataDiagnosa = () => {
		api.post(`referensi/diagnosa`, {
         search: this.state.search
      })
      .then(result => {
			// let dataTampilDiagnosa = [];
			// for (let i = 0; i < result.data.length; i++) {
			// 	dataTampilDiagnosa.push({
			// 		kode: result.data[i].kode,
			// 	});
			// }
			this.setState({
				dataDiagnosa: result.data,
				dataTampilDiagnosa: result.data,
			});
      })
      .catch(error => {
         this.props.error(error.response.data);
      });
   }

   clearData = () => {
		this.setState({
			dataDiagnosa: [],
			dataTampilDiagnosa: [],
		});
	}
	
	enter = (data) => {
      this.props.getDataDiagnosa(data);
	}

   render() {
      return (
         <Fragment>
            <Autocomplete
					placeholder="Cari"
					clear={this.clearData}
					onenter={this.enter}
					width="40px"
					link="referensi/diagnosa"
				/>
				<TextBox
					style={{ width:'200px', marginLeft: '4px' }}
					disabled
					value={this.props.value}
				/>
         </Fragment>
      );
   }
}

CariDiagnosa.propTypes = {
   classes: PropTypes.object.isRequired,
};

export default (withStyles(styles)(CariDiagnosa));

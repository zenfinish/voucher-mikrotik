import React from 'react';
import { Select } from 'antd';
import api from 'config/api.js';

const { Option } = Select;

class AutoComplete extends React.Component {

	state = {
		data: [],
        value: null,
		loading: false,
        disabled: false,
	}

    componentDidMount() {
        if (this.props.disabled) {
            this.setState({ disabled: true });
        };
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevProps.outsideValue !== this.props.outsideValue) {
            this.setState({ value: this.props.outsideValue });
        };
        if (prevProps.disabled !== this.props.disabled) {
            this.setState({ disabled: this.props.disabled });
        };
    }

    handleSearch = value => {
        if (value && value.length > 2) {
            this.olahFetch(value, data => {
                this.setState({ data });
            });
        } else {
            this.setState({ data: [] });
        };
    };

    olahFetch = (search, callback) => {
        let timeout;
        let currentValue;
        if (timeout) {
            clearTimeout(timeout);
            timeout = null;
        };
        currentValue = search;
    
        const fake = async () => {
            await api.get(this.props.link, {
                headers: { search, }
            })
            .then(result => {
                const data = result.data;
                if (data === "EMPTY") {
                    callback([]);
                } else {
                    if (currentValue === search) {
                        callback(data);
                    };
                };
            })
            .catch(error => {
                console.log('Error: ', error.response);
            });
        }
    
        timeout = setTimeout(fake, 300);
    }

    handleChange = (value, option) => {
        this.setState({ value: this.props.value ? this.props.value(option.data) : null }, () => this.props.onSelected(option.data));
    };
	
	render() {
		const optionsData =
            this.state.data.map((data, i) =>
                <Option key={i} data={data}>
                    {
                        this.props.customOption ?
                            this.props.customOption(data)
                        :
                            data
                    }
                </Option>
            );

		return (
			<>
                <Select
                    showSearch
                    value={this.state.value}
                    defaultActiveFirstOption={true}
                    showArrow={false}
                    filterOption={false}
                    onSearch={this.handleSearch}
                    onChange={this.handleChange}
                    // notFoundContent={null}
                    className="w-full"
                    placeholder={this.props.placeholder ? this.props.placeholder : "-Cari-"}
                    disabled={this.state.disabled}
                    style={this.props.style}
                    size={this.props.size}
                >
                    {optionsData}
                </Select>
			</>
		);
	}

};

export default AutoComplete;

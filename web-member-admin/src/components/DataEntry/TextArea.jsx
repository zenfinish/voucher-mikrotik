import React from 'react';
import { Input, Spin } from 'antd';

class TextArea extends React.Component {
	
	render() {
		return (
            <Input.TextArea
                onChange={() => {}}
                value={<Spin />}
            />
		);
	}

}

export default TextArea;

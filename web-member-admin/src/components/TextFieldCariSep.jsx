import React from 'react';
import TextField from '@material-ui/core/TextField';

class TextFieldCariSep extends React.Component {

   render() {
      return (
         <TextField
            label="Ketik Nomor Sep"
            name="cari_sep"
            type="text" variant="outlined"
            value={this.props.value}
            fullWidth
            onChange={this.props.onChange}
            InputLabelProps={{ shrink: true }}
            autoComplete="off"
            style={{height: '40px'}}
         />
      );
   }

}

export default TextFieldCariSep;

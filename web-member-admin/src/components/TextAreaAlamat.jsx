import React from 'react';
import TextField from '@material-ui/core/TextField';

class TextAreaAlamat extends React.Component {

   render() {
      return (
         <TextField
            label="Alamat"
            name="alamat"
            type="textarea"
            value={this.props.value}
            fullWidth
            onChange={this.props.onChange}
            multiline={true}
            InputLabelProps={{ shrink: true }}
            InputProps={this.props.disabled === true ? { disabled: true, } : { disabled: false, }}
         />
      );
   }

}

export default TextAreaAlamat;

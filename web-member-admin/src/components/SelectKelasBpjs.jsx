import React from 'react';
import TextField from '@material-ui/core/TextField';

class SelectKelasBpjs extends React.Component {
   
   render() {
      return (
         <TextField
            label="Kelas Bpjs"
            style={{height: '40px'}}
            variant="outlined"
            InputLabelProps={{ shrink: true }}
            SelectProps={{ native: true }}
            fullWidth select
            onChange={this.props.onChange}
            name="kelas_bpjs"
            value={this.props.value}
            InputProps={this.props.disabled === true ? { disabled: true, } : { disabled: false, }}
         >
            <option key="" value=""></option>
            <option key="1" value="1">Kelas  1</option>
            <option key="2" value="2">Kelas 2</option>
            <option key="3" value="3">Kelas 3</option>
         </TextField>
      );
   }

}

export default SelectKelasBpjs;

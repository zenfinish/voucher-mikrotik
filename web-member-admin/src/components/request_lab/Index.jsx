import React from 'react';
import { CircularProgress, Button } from '@material-ui/core';
import { Modal, notification, Checkbox } from 'antd';
import api from 'config/api.js';
import { separator, tglIndo } from 'config/helpers.js';
import DataGrid from 'components/table/DataGrid.jsx';
import GridColumn from 'components/table/GridColumn.jsx';
import Search from "../../views/MainApp/Administrasi/Registrasi/_jalan/Form/Content/Search";

class Index extends React.Component {
	
	state = {
		disableSearch: false,
		optionCapak: [],
		searchPaket: '',
		loadingPaket: false,
		optionCariPaket: [],
		disabledSimpan: false,

		id_labpaket: '',
		nama_labpaket: '',
		tarif: '',
		diagnosa: null,
		diagnosa_nama: null,

		data: [],
		
		dataTable: [],
	}

	componentDidMount() {
		this.refreshTable();
	}

	refreshTable = () => {
		this.setState({ loading: true }, () => {
			api.get(`/lab/data/paket/aktif`)
			.then(result => {
				this.setState({ data: result.data, loading: false });
			})
			.catch(error => {
				console.log('Error: ', error.response);
				this.setState({ loading: false });
			});
		});
	}

	pilih = (checked, value, index) => {
		let data = this.state.dataTable;
		if (checked) {
			data.push({ ...value, index });
		} else {
			data = data.filter((item) => item.index !== index );
		}
		this.setState({ dataTable: data });
	}
			
	render() {
		return (
			<>
				<Modal
					centered
					width="80%"
					bodyStyle={{ height: '60vh' }}
					visible={true}
					onCancel={this.props.close}
					title="Tambah Permintaan Laboratorium"
					footer={[
						<div style={{ position: 'relative' }} key="simpan"> {/* tombol_simpan */}
							<Button variant="outlined" color="primary"
								onClick={() => {
									if (this.state.dataTable.length === 0) {
										notification.error({
											message: 'Error',
											description: 'Paket Belum Dipilih',
										});
									} else {
										this.setState({ disabledSimpan: true }, () => {
											api.post(`/lab/transaksi`, {
												no_rekmedis: this.props.data.no_rekmedis,
												nama_pelpasien: this.props.data.nama_pelpasien,
												tipe: this.props.data.tipe,
												id_checkin: this.props.data.tipe === 1 ? this.props.data.id_peljalan : this.props.data.tipe === 2 ? this.props.data.id_pelinap : '',
												data: this.state.dataTable,
												diagnosa: this.state.diagnosa,
												diagnosa_nama: this.state.diagnosa_nama,
											})
											.then(result => {
												this.setState({ disabledSimpan: false }, () => {
													this.props.closeRefresh();
												});
											})
											.catch(error => {
												notification.error({
													message: 'Error',
													description: JSON.stringify(error.response.data),
												});
												this.setState({ disabledSimpan: false });
											});
										});
									}
								}}
								disabled={this.state.disabledSimpan}
							>Simpan</Button>
							{
								this.state.disabledSimpan &&
								<CircularProgress size={24}
									style={{ position: 'absolute', right: '35%', top: '20%' }}
								/>
							}
						</div>
					]}
				>
					<table className="whitespace-nowrap w-full text-sm mb-4" cellPadding={1}>
						<tbody>
							<tr>
								<td>Nama Pasien</td>
								<td>:</td>
								<td>{this.props.data.nama_pelpasien} [{this.props.data.no_rekmedis}]</td>
								<td>Tgl Checkin</td>
								<td>:</td>
								<td>{tglIndo(this.props.data.tgl_checkin)}</td>
							</tr>
							<tr>
								<td>Cara Bayar</td>
								<td>:</td>
								<td>{this.props.data.nama_pelcabar}</td>
								<td>Tipe</td>
								<td>:</td>
								<td>{this.props.data.tipe === 1 ? 'Rawat Jalan' : this.props.data.tipe === 2 ? 'Rawat Inap' : ''}</td>
							</tr>
						</tbody>
					</table>
					<Search
						onSelected={(data) => this.setState({ diagnosa_nama: data[0], diagnosa: data[1] })}
						fetchData={(value) => {
							return api.get(`/inacbg/diagnosa/search`, { headers: { search: value } })
						}}
						customOption={(data) => `${data[1]} - ${data[0]}`}
						value={(data) => `${data[1]} - ${data[0]}`}
						outsideValue={this.props.data.diagnosaOutsideValue}
					/>
					<DataGrid
						data={this.state.data}
					>
						<GridColumn
							title="Nama"
							render={(row, index) => <Checkbox onChange={(e) => this.pilih(e.target.checked, row, index)}>{row.nama_labpaket}</Checkbox>}
						/>
						<GridColumn title="ID" field="id_labpaket" />
						<GridColumn
							title="Tarif"
							field="tarif"
							render={(row) => (<span>{separator(row.tarif)}</span>)}
							right
						/>
					</DataGrid>
				</Modal>
			</>
		)
	}

};

export default (Index);

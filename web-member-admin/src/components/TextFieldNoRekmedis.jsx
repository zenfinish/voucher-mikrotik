import React from 'react';
import TextField from '@material-ui/core/TextField';

class TextFieldNoRekmedis extends React.Component {

   render() {
      return (
         <TextField
            label="No. Rekmedis"
            name="no_rekmedis"
            type="text"
            value={this.props.value}
            fullWidth
            InputProps={this.props.disabled === true ? { disabled: true, } : { disabled: false, }}
            onChange={this.props.onChange}
            InputLabelProps={{ shrink: true }}
         />
      );
   }

}

export default TextFieldNoRekmedis;

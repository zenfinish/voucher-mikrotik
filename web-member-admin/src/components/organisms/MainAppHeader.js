import React from 'react';
// import { Menu, Dropdown, Button } from 'antd';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { userState } from "../../recoil";
import { useRecoilValue } from "recoil";

const MainAppHeader = ({ history }) => {
	const user = useRecoilValue(userState);

	return <Index
		history={history}
		userState={user}
	/>;
};
class Index extends React.Component {
	
	render() {
		const {
			userState,
			// headerMenus,
			history
		} = this.props;

		return (
			<>
				<header className="fixed bg-yellow-400 text-white w-full">
					<nav className="flex justify-between items-center h-12">
						<div>
							<span className="ml-2">Web Member Admin</span>
						</div>
						<div>
							{/* {
								headerMenus.map((row, i) => (
									row.menu3.length > 0 ?
									<Dropdown
										overlay={(
											<Menu>
												{
													row.menu3.map((row2, j) => (
														<Menu.Item 
															key={j}
															onClick={() => {
																this.props.history.push(`/main${row2.link}`);
															}}
														>{row2.nama}</Menu.Item>
													))
												}
											</Menu>
										)}
										key={i}
									>
										<Button
											onClick={() => {
												this.props.history.push(`/main${row.link}`);
											}}
											type="text"
										>{row.nama}</Button></Dropdown>
									:
									<Button
										key={i}
										onClick={() => {
											if (row.menu3.length === 0) {
												this.props.history.push(`/main${row.link}`);
											}
										}}
										type="text"
									>{row.nama}</Button>
								))
							} */}
						</div>
						<div>
							<span
								className="cursor-pointer rounded p-2 hover:bg-yellow-600"
								onClick={() => {
									this.props.history.push(`/main/profil`);
								}}
							>{userState.name}</span>
							<span
								className="cursor-pointer rounded p-2 hover:bg-yellow-600 mr-2"
								onClick={() => {
									localStorage.clear();
									history.push("/");
								}}
							>Logout</span>
						</div>
					</nav>
				</header>
			</>
		)
	}

}

const mapStateToProps = (state) => ({
	user_active: state.user_active,
	headerMenus: state.menus.headerMenus,
});

export default connect(mapStateToProps)(withRouter(MainAppHeader));

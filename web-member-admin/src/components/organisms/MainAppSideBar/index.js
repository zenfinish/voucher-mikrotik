import React from 'react';
import Tooltip from 'antd/lib/tooltip';
import { icon } from "../../../utils";
import "./style.css";
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { MENUS_SET_HEADER_MENUS, MENUS_SET_SUB_MENUS } from '../../../config/redux/menus/action';
import { mdlState } from "../../../recoil";
import { useRecoilValue } from "recoil";

const Index = ({ dispatch, subMenus, history, }) => {
	const mdl = useRecoilValue(mdlState);

	return <MainAppSideBar
		dispatch={dispatch}
		subMenus={subMenus}
		history={history}
		mdlState={mdl}
	/>;
};

class MainAppSideBar extends React.Component {
  
	state = {
		openMenu: false,
	}
  
	render() {
		const { openMenu } = this.state;
		const { subMenus, history, dispatch, mdlState } = this.props;

		return (
			<>
				<nav className="w-24 h-screen bg-white fixed flex items-center justify-center" style={{ top: '3rem', }}>
					<div className="flex flex-col gap-2 justify-center">
						{
							mdlState.map((row, i) => (
								<Tooltip title={row.name} placement="right" key={i}>
									<button
										className="w-12 h-12 border-none text-gray-300 rounded-md hover:text-yellow-400 hover:bg-gray-100"
										onClick={() => {
											dispatch(MENUS_SET_SUB_MENUS({
												nama: row.name,
												menus: row.mdl_sub,
											}));
											this.setState({ openMenu: true, });
										}}
									>{icon(row.icon)}</button>
								</Tooltip>
							))
						}
						{/* <Tooltip title={`Pelanggan`} placement="right">
							<button
								className="w-12 h-12 border-none text-gray-300 rounded-md hover:text-yellow-400 hover:bg-gray-100"
								onClick={() => {
									dispatch(MENUS_SET_SUB_MENUS({
										nama: `Pelanggan`,
										menus: [
											{
												nama: 'Master Data',
												link: '/pelanggan/master-data',
												menu2: [],
											},
										],
									}));
									this.setState({ openMenu: true, });
								}}
							>{icon(`TeamOutlined`)}</button>
						</Tooltip> */}
					</div>
				</nav>
				<div
					className={`
						fixed border-l w-80 z-30 h-screen bg-white p-8
						${openMenu ? 'nav-menu active' : 'nav-menu'}
					`}
					style={{ top: "3rem" }}
				>
					<Tooltip title="Close" placement="top">
						<div
						className="pl-5 uppercase text-gray-400 mb-4 cursor-pointer"
						onClick={() => {
							this.setState({ openMenu: false });
						}}
						>{subMenus.nama}</div>
					</Tooltip>
					{
						subMenus.menus.map((row, i) => (
						<button
							className="group w-full border-none text-gray-300 rounded-md hover:text-yellow-400 hover:bg-gray-100 text-left mb-2 p-2"
							key={i}
							size="large"
							onClick={() => {
							history.push(`/main${row.link}`);
							dispatch(MENUS_SET_HEADER_MENUS(row.menu2));
							this.setState({ openMenu: false });
							}}
						>{icon('SnippetsFilled')} <span className="text-sm text-black font-extralight ml-2 group-hover:text-yellow-400">{row.nama}</span></button>
						))
					}
					<div className="w-full mt-5 border-t"></div>
				</div>
			</>
		)
	}

}

const mapStateToProps = (state) => ({
	mainMenus: state.menus.mainMenus,
	subMenus: state.menus.subMenus,
});

export default connect(mapStateToProps)(withRouter(Index));

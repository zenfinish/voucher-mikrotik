import React from 'react'

const MainAppContent = (props) => {
  return (
    <div
      className="px-6	py-3 bg-gray-100 left-24 fixed"
      style={{ top: '3rem', bottom: 5, right: 0 }}
    >
      {props.children}
    </div>
  )
}

export default MainAppContent

import React from 'react';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import { Table, Chip, } from '@material-ui/core';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';

import api from '../config/api.js';

const CustomTableCell = withStyles(theme => ({
   head: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
   },
   body: {
      fontSize: 12,
   },
}))(TableCell);

const styles = theme => ({
   row: {
      '&:nth-of-type(odd)': {
         backgroundColor: theme.palette.background.default,
      },
   },
});

class TableRujukan extends React.Component {
    
   state = {
      no_bpjs: '',
      dataRujuk: [],
   }

   cariKartu = (e) => {
		e.preventDefault();
		this.setState({
			no_bpjs: this.state.no_bpjs.padStart(13, '0'),
		}, () => {
			let link = '';
			if (this.props.asal_rujukan === '1') {
				link = 'bpjs/rujukan/multi/pcare/';
			} else if (this.props.asal_rujukan === '2') {
				link = 'bpjs/rujukan/multi/rs/';
			}
			api.post(link, {
				no_bpjs: this.state.no_bpjs,
			}, {
				headers: { token: localStorage.getItem('token') }
			})
			.then(result => {
				this.setState({
					dataRujuk: result.data,
				});
			})
			.catch(error => {
				this.props.error(error.response.data);
			});
		});
   }

   handleChange = (e) => {
      this.setState({
         [e.target.name]: e.target.value,
      });
   };

   getDataRujuk = (data) => {
		let link = '';
		if (this.props.status_pasien === '0' && this.props.asal_rujukan === '1') {
			link = 'bpjs/rujukan/norujuk/pcare/baru/';
		} else if (this.props.status_pasien === '1' && this.props.asal_rujukan === '1') {
			link = 'bpjs/rujukan/norujuk/pcare/lama/';
		} else if (this.props.status_pasien === '0' && this.props.asal_rujukan === '2') {
			link = 'bpjs/rujukan/norujuk/rs/baru/';
		} else if (this.props.status_pasien === '1' && this.props.asal_rujukan === '2') {
			link = 'bpjs/rujukan/norujuk/rs/lama/';
		}
		if (link === '') return this.props.error('Asal Rujukan Belum Dipilih');
      api.post(link, {
         no_rujukan: data.no_rujukan,
      }, {
         headers: { token: localStorage.getItem('token') }
      })
      .then(result => {
			result.data['asal_rujukan'] = this.props.asal_rujukan;
         result.data['cariDiagnosa'] = `${result.data.no_icd} | ${result.data.nama_diagnosis}`;
			this.props.getDataRujuk(result.data);
			this.props.close();
      })
      .catch(error => {
			this.props.error(error.response.data);
		});
   }
   
   render() {
      const { classes } = this.props;
      return (
         <div style={{overflowX: 'auto'}}>
            <Dialog
               open={this.props.open}
               onClose={this.props.close}
               maxWidth="lg"
               fullWidth={true}
            >
               <DialogTitle>{this.props.nama_asal_rujukan}</DialogTitle>
               <DialogContent>
						<form onSubmit={this.cariKartu} style={{ float: 'left', }}>
							<input type="text"
								placeholder	="Ketik Nomor Lalu Enter"
								onChange={
									(e) => {
										this.setState({
											no_bpjs: e.target.value,
										});
									}
								}
								onKeyDown={(e) => { 
									if (e.keyCode === 9) {
										this.setState({
											no_bpjs: this.state.no_bpjs.padStart(13, '0'),
										});
									}
								}}
								value={this.state.no_bpjs}
							/>
						</form>
						<Table className={classes.table}>
                     <TableHead>
                        <TableRow>
                           <CustomTableCell align="center">No.</CustomTableCell>
                           <CustomTableCell align="center">No. Rujukan</CustomTableCell>
                           <CustomTableCell align="center">Tgl. Rujukan</CustomTableCell>
                           <CustomTableCell align="center">Diagnosa</CustomTableCell>
                           <CustomTableCell align="center">Nama</CustomTableCell>
                           <CustomTableCell align="center">PPK Perujuk</CustomTableCell>
                           <CustomTableCell align="center">Sub/Spesialis</CustomTableCell>
                        </TableRow>
                     </TableHead>
                     <TableBody>
                        {
                           this.state.dataRujuk.map((row, index) => (
                              <TableRow className={classes.row} key={index}>
                                 <CustomTableCell align="center">{index+1}</CustomTableCell>
                                 <CustomTableCell align="center">
                                    <Chip
                                       label={row.no_rujukan}
                                       clickable
                                       onClick={() => {this.getDataRujuk(row)}}
                                    />
                                 </CustomTableCell>
                                 <CustomTableCell align="center">{row.tgl_rujukan}</CustomTableCell>
                                 <CustomTableCell align="center">{row.no_icd} | {row.nama_diagnosis}</CustomTableCell>
                                 <CustomTableCell align="center">{row.nama_pasien}</CustomTableCell>
                                 <CustomTableCell align="center">{row.ppk_rujukan + ' | ' + row.nama_fpk}</CustomTableCell>
                                 <CustomTableCell align="center">{row.poli_bpjs + ' | ' + row.nama_poli}</CustomTableCell>
                              </TableRow>
                           ))
                        }
                     </TableBody>
                  </Table>
               </DialogContent>
               <DialogActions>
                  <Button onClick={this.props.close} color="primary">Close</Button>
               </DialogActions>
            </Dialog>
         </div>
      );
   }

};

TableRujukan.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default (withStyles(styles)(TableRujukan));

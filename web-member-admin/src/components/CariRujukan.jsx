import React, { Fragment } from 'react';
import { ComboBox, TextBox, Tooltip } from 'rc-easyui';
import TableRujukan from './TableRujukan.jsx';
import api from '../config/api.js';

const defaultState = () => ({
	asal_rujukan: '',
	nama_asal_rujukan: '',
   nomor: '',
   nomorValue: '',
   open: false,
});

class CariRujukan extends React.Component {
    
   state = {
      ...defaultState(),
   }

   cariRujukan = () => {
		if (this.state.asal_rujukan === '') return this.props.error('Asal Rujukan Belum Dipilih!');
		this.setState({
         open: true,
      });
   }

   closeRujukan = () => {
      this.setState({
         open: false,
      });
   }

   getDataRujuk = (e) => {
		e.preventDefault();
		let link = '';
		if (this.props.status_pasien === '0' && this.state.asal_rujukan === '1') {
			link = 'bpjs/rujukan/norujuk/pcare/baru/';
		} else if (this.props.status_pasien === '1' && this.state.asal_rujukan === '1') {
			link = 'bpjs/rujukan/norujuk/pcare/lama/';
		} else if (this.props.status_pasien === '0' && this.state.asal_rujukan === '2') {
			link = 'bpjs/rujukan/norujuk/rs/baru/';
		} else if (this.props.status_pasien === '1' && this.state.asal_rujukan === '2') {
			link = 'bpjs/rujukan/norujuk/rs/lama/';
		}
		if (link === '') return this.props.error('Asal Rujukan Belum Dipilih');
      api.post(link, {
         no_rujukan: this.state.nomorValue,
      }, {
         headers: { token: localStorage.getItem('token') }
      })
      .then(result => {
			result.data['asal_rujukan'] = this.state.asal_rujukan;
         result.data['cariDiagnosa'] = `${result.data.no_icd} | ${result.data.nama_diagnosis}`;
         this.props.getDataRujuk(result.data);
      })
      .catch(error => {
			this.props.error(error.response.data);
		});
   }

   render() {
      return (
         <Fragment>
				<ComboBox
					data={[
						{ value: '', text: '- Asal Rujukan -' },
						{ value: '1', text: 'Faskes Tingkat 1' },
						{ value: '2', text: 'Faskes Tingkat 2' },
					]}
					value={this.state.asal_rujukan}
					onSelectionChange={
						(data) => {
							this.setState({
								asal_rujukan: data.value,
								nama_asal_rujukan: data.text,
							});
						}
					}
					style={{ marginRight: '4px', float: 'left', }}
					disabled={this.props.id_cabar === '2' ? false : true}
				/>
				<form onSubmit={this.getDataRujuk} style={{ float: 'left', }}>
					<TextBox
						addonRight={
							() => {
								return (
									<Tooltip content="Cari Berdasarkan No. Kartu">
										<span
											className="textbox-icon icon-search"
											onClick={this.cariRujukan}
										/>
									</Tooltip>
								)
							}
						}
						placeholder="Ketik Nomor Rujukan Faskes"
						style={{width:'250px'}}
						onChange={
							(value) => {
								this.setState({
									nomorValue: value,
								});
							}
						}
						disabled={this.props.id_cabar === '2' ? false : true}
					/>
				</form>
				<TableRujukan
               open={this.state.open}
               close={this.closeRujukan}
               getDataRujuk={this.props.getDataRujuk}
               error={this.props.error}
               status_pasien={this.props.status_pasien}
					no_bpjs={this.state.no_bpjs}
					asal_rujukan={this.state.asal_rujukan}
					nama_asal_rujukan={this.state.nama_asal_rujukan}
            />
			</Fragment>
      );
   }
}

export default (CariRujukan);

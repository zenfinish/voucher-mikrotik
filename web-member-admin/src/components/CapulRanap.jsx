import React, { Fragment } from 'react';
import Grid from '@material-ui/core/Grid';
import SelectIdKelas from './input/SelectIdKelas';
import SelectNaikIdKelas from './input/SelectNaikIdKelas';
import SelectTitipIdKelas from './input/SelectTitipIdKelas';
import SelectIdRuangan from './input/SelectIdRuangan';
import SelectIdPelRuanganKamar from './input/SelectIdPelRuanganKamar';
import SelectIdPelRuanganBed from './input/SelectIdPelRuanganBed';

class CapulRanap extends React.Component {

   render() {
      return (
         <Fragment>
            <Grid container spacing={8} style={{ marginBottom: 10 }}>
               <Grid item xs={4}><SelectIdKelas /></Grid>
               <Grid item xs={4}><SelectNaikIdKelas /></Grid>
               <Grid item xs={4}><SelectTitipIdKelas /></Grid>
            </Grid>
            <Grid container spacing={8} style={{ marginBottom: 10 }}>
               <Grid item xs={6}><SelectIdRuangan /></Grid>
               <Grid item xs={3}><SelectIdPelRuanganKamar /></Grid>
               <Grid item xs={3}><SelectIdPelRuanganBed /></Grid>
            </Grid>
         </Fragment>
      );
   }

}

export default CapulRanap;

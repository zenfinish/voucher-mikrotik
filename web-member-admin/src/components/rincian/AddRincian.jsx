import React from 'react';
import { notification } from 'antd';
import Dropdown from 'components/inputs/Dropdown.jsx';
import Autocomplete from 'components/Autocomplete.jsx';
import Button from 'components/inputs/Button.jsx';
import DateBox from 'components/inputs/DateBox.jsx';
import TextField from 'components/inputs/TextField.jsx';
import { separator } from 'config/helpers.js';
import api from 'config/api.js';

class AddRincian extends React.Component {

	state = {
		id_peltarif: '',
		dataDokter: [],
		dataPoli: [],

		dataTarifAdministrasi: [],
		id_taradministrasidet: '',

		dataKelas: [],
		dataRuangan: [],
		dataRuanganKamar: [],
		dataRuanganKamarBed: [],
		tgl_checkin: '',
		tgl_checkout: '',
		id_pelkelas: '',
		id_pelruangan: '',
		id_pelruangankamar: '',
		id_pelruangankamarbed: '',
		
		dataTarifNo: [],
		id_tarno: '',
		nama_tarno: '',
		id_tarnodet: '',

		dataKonsultasi: [],
		dokterKonsultasi: '',
		id_tarkonsultasidet: '',
		id_pelpoli: '',

		dataTarifAhli: [],
		id_tarahli: '',
		nama_tarahli: '',
		id_tarahlidet: '',

		dataTarifKeperawatan: [],
		id_tarkeperawatan: '',
		nama_tarkeperawatan: '',
		id_tarkeperawatandet: '',

		dataTarifPenunjang: [],
		id_tarpenunjang: '',
		nama_tarpenunjang: '',
		id_tarpenunjangdet: '',

		dataTarifDarah: [],
		id_tardarah: '',
		nama_tardarah: '',
		id_tardarahdet: '',

		dataTarifRehabilitasi: [],
		id_tarrehabilitasi: '',
		nama_tarrehabilitasi: '',
		id_tarrehabilitasidet: '',

		dataTarifAlkes: [],
		id_taralkes: '',
		nama_taralkes: '',
		id_taralkesdet: '',

		dataTarifBmhp: [],
		id_tarbmhp: '',
		nama_tarbmhp: '',
		id_tarbmhpdet: '',

		dataTarifAlat: [],
		id_taralat: '',
		nama_taralat: '',
		id_taralatdet: '',
	}

	componentDidMount() {
		this.getDokter();
		this.getTarifKonsultasi();
		this.getTarifAdministrasi();
		this.getPoli();
		this.getKelas();
	}

	getDokter = () => {
		api.get(`/pelayanan/user/dokter`)
		.then(result => {
			const dataDokter = result.data.map((data) => { return { value: data.id_peluser, label: data.nama_peluser, dokter: data.dokter } });
			this.setState({ dataDokter: dataDokter });
		})
		.catch(error => {
			console.log(error.response)
		});
	}

	getTarifKonsultasi = () => {
		api.get(`/tarif/konsultasi/all`)
		.then(result => {
			const dataKonsultasi = result.data.filter((data) => {
				if (this.props.data.tipe === 1) {
					return data.jenis === this.props.data.poli;
				} else if (this.props.data.tipe === 2) {
					if (this.props.data.kelas_sekarang === 1) {
						return data.jenis === '3';
					} else if (this.props.data.kelas_sekarang === 2) {
						return data.jenis === '2';
					} else if (this.props.data.kelas_sekarang === 3) {
						return data.jenis === '1';
					} else if (this.props.data.kelas_sekarang === 4) {
						return data.jenis === 'vip';
					} else if (this.props.data.kelas_sekarang === 7 || this.props.data.kelas_sekarang === 8) {
						return data.jenis === 'vvip';
					}
				}
				return data;
			}).map((data2) => { return {value: data2.id_tarkonsultasidet, label: `[${data2.jenis}] ${data2.nama_tarkonsultasi} [Rp. ${separator(data2.tarif)}]`, tarif: data2.tarif} });
			this.setState({ dataKonsultasi: dataKonsultasi });
		})
		.catch(error => {
			console.log(error.response)
		});
	}

	getTarifAdministrasi = () => {
		api.get(`/tarif/administrasi/aktif/all`)
		.then(result => {
			const dataTarifAdministrasi = result.data.map((data) => { return { value: data.id_taradministrasidet, label: `${data.nama_taradministrasi} [Rp. ${separator(data.tarif)}]` } });
			this.setState({ dataTarifAdministrasi: dataTarifAdministrasi });
		})
		.catch(error => {
			console.log(error.response)
		});
	}

	getPoli = () => {
		api.get(`/pelayanan/poli/data`)
		.then(result => {
			const dataPoli = result.data.map((data) => { return { value: data.id_pelpoli, label: data.nama_pelpoli, kode_bpjs: data.kode_bpjs } });
			this.setState({ dataPoli: dataPoli });
		})
		.catch(error => {
			console.log(error.response)
		});
	}

	getKelas = () => {
		api.get(`/tarif/akomodasi/all`)
		.then(result => {
			const dataKelas = result.data.map((data) => { return { value: data.id_pelkelas, label: data.nama_pelkelas, id_tarakomodasi: data.id_tarakomodasi } });
			this.setState({ dataKelas: dataKelas });
		})
		.catch(error => {
			console.log(error.response)
		});
	}

	getRuangan = () => {
		api.get(`/pelayanan/ruangan/${this.state.id_pelkelas}`)
		.then(result => {
			const dataRuangan = result.data.map((data) => { return { value: data.id_pelruangan, label: data.nama_pelruangan } });
			this.setState({ dataRuangan: dataRuangan });
		})
		.catch(error => {
			console.log(error.response)
		});
	}

	getRuanganKamar = () => {
		api.get(`/pelayanan/ruangan/kamar/${this.state.id_pelkelas}/${this.state.id_pelruangan}`)
		.then(result => {
			const dataRuanganKamar = result.data.map((data) => { return { value: data.id_pelruangankamar, label: data.nama_pelruangankamar } });
			this.setState({ dataRuanganKamar: dataRuanganKamar });
		})
		.catch(error => {
			console.log(error.response)
		});
	}

	getRuanganKamarBed = () => {
		api.get(`/pelayanan/ruangan/kamar/bed/${this.state.id_pelruangankamar}`)
		.then(result => {
			const dataRuanganKamarBed = result.data.map((data) => { return { value: data.id_pelruangankamarbed, label: data.nama_pelruangankamarbed } });
			this.setState({ dataRuanganKamarBed: dataRuanganKamarBed });
		})
		.catch(error => {
			console.log(error.response)
		});
	}
	
	render() {
		return (
			<>
				<div>Pilih Rincian</div>
				<Dropdown
					data={[
						{ value: '1', label: 'Administrasi'},
						this.props.data.tipe === 2 && this.props.data.aktifAkomodasi ? { value: '2', label: 'Akomodasi'} : null,
						{ value: '3', label: 'Prosedur Non Bedah'},
						{ value: '5', label: 'Konsultasi'},
						{ value: '6', label: 'Tenaga Ahli'},
						{ value: '7', label: 'Keperawatan'},
						{ value: '8', label: 'Penunjang'},
						{ value: '9', label: 'Pelayanan Darah'},
						{ value: '10', label: 'Rehabilitasi'},
						{ value: '12', label: 'Alkes'},
						{ value: '13', label: 'BMHP'},
						{ value: '14', label: 'Sewa Alat'},
					]}
					className="w-full mb-2"
					onChange={(value) => {this.setState({ id_peltarif: value.value })}}
					value={this.state.id_peltarif}
				/>
				<div>
				{
						this.state.id_peltarif === '1' ?
						<> {/* Tarif Administrasi */}
							<div>Tarif Administrasi</div>
							<Dropdown
								data={this.state.dataTarifAdministrasi}
								className="mb-2"
								onChange={(value) => {this.setState({ id_taradministrasidet: value.value })}}
								value={this.state.id_taradministrasidet}
							/>
							<Button onClick={() => {
								api.post(`/rincian/${this.props.data.tipe === 1 ? 'jalan' : this.props.data.tipe === 2 ? 'inap' : ''}/administrasi`, {
									id_taradministrasidet: this.state.id_taradministrasidet,
									id_pelinap: this.props.data.id_pelinap,
									id_peljalan: this.props.data.id_peljalan,
								})
								.then(result => {
									this.props.wasSuccessful();
									this.setState({
										id_taradministrasidet: '',
									});
								})
								.catch(error => {
									notification.error({
										message: 'Error',
										description: JSON.stringify(error.response.data),
									});
								});
							}}>Simpan</Button>
						</>
						: null
					}
					{
						this.state.id_peltarif === '2' ?
						<> {/* Akomodasi */}
							<div>Tgl Checkin</div>
							<DateBox
								className="mb-2 w-full"
								onChange={(e) => { this.setState({ tgl_checkin: e.target.value }) }}
								value={this.state.tgl_checkin}
							/>
							<div>Tgl Checkout</div>
							<DateBox
								className="mb-2 w-full"
								onChange={(e) => { this.setState({ tgl_checkout: e.target.value }) }}
								value={this.state.tgl_checkout}
							/>
							<div>Kelas</div>
							<Dropdown
								data={this.state.dataKelas}
								className="w-full mb-2"
								onChange={(value) => {
									this.setState({
										dataRuangan: [],
										dataRuanganKamar: [],
										dataRuanganKamarBed: [],
										id_pelkelas: value.value,
										id_tarakomodasi: value.id_tarakomodasi,
										id_pelruangan: '',
										id_pelruangankamar: '',
										id_pelruangankamarbed: '',
									}, () => {
										this.getRuangan();
									})
								}}
								value={this.state.id_pelkelas}
							/>
							<div>Ruangan</div>
							<Dropdown
								data={this.state.dataRuangan}
								className="w-full mb-2"
								onChange={(value) => {
									this.setState({
										id_pelruangan: value.value,
										id_pelruangankamar: '',
										dataRuanganKamar: [],
										id_pelruangankamarbed: '',
										dataRuanganKamarBed: [],
									}, () => {
										this.getRuanganKamar();
									})
								}}
								value={this.state.id_pelruangan}
							/>
							<div>Kamar</div>
							<Dropdown
								data={this.state.dataRuanganKamar}
								className="w-full mb-2"
								onChange={(value) => {
									this.setState({
										id_pelruangankamar: value.value,
										id_pelruangankamarbed: '',
										dataRuanganKamarBed: [],
									}, () => {
										this.getRuanganKamarBed();
									})
								}}
								value={this.state.id_pelruangankamar}
							/>
							<div>Bed</div>
							<Dropdown
								data={this.state.dataRuanganKamarBed}
								className="w-full mb-2"
								onChange={(value) => {this.setState({ id_pelruangankamarbed: value.value })}}
								value={this.state.id_pelruangankamarbed}
							/>
							<Button
								onClick={() => {
									api.post(`/rincian/inap/akomodasi`, {
										tgl_checkout: this.state.tgl_checkout,
										tgl: this.state.tgl_checkin,
										id_pelinap: this.props.data.id_pelinap,
										id_pelkelas: this.state.id_pelkelas,
										id_tarakomodasi: this.state.id_tarakomodasi,
										id_pelruangankamar: this.state.id_pelruangankamar,
										id_pelruangankamarbed: this.state.id_pelruangankamarbed,
									})
									.then(result => {
										this.props.wasSuccessful();
										this.setState({
											dataRuangan: [],
											dataRuanganKamar: [],
											dataRuanganKamarBed: [],
											tgl_checkin: '',
											tgl_checkout: '',
											id_pelkelas: '',
											id_tarakomodasi: '',
											id_pelruangan: '',
											id_pelruangankamar: '',
											id_pelruangankamarbed: '',
										});
									})
									.catch(error => {
										notification.error({
											message: 'Error',
											description: JSON.stringify(error.response.data),
										});
									});
								}}
							>Simpan</Button>
						</>
						: null
					}
					{
						this.state.id_peltarif === '3' ?
						<>  {/* Prosedur Non Bedah */}
							<Autocomplete
								className="w-full mr-2"
								placeholder="Cari Prosedur Non Bedah..."
								onEnter={(value) => {
									api.get(`/tarif/no/search`, {
										headers: { search: value }
									})
									.then(result => {
										const result2 = result.data.map((row) => {return {value: row.id_tarnodet, ...row }})
										this.setState({ dataTarifNo: result2 });
									})
									.catch(error => {
										console.log('Error: ', error.response);
									});
								}}
								data={this.state.dataTarifNo}
								list={(row) => (
									<div>{row.nama_tarno} [Id: {row.id_tarno}] [Tarif: {separator(row.tarif)}]</div>
								)}
								onSelect={(row) => {
									this.setState({
										id_tarno: row.id_tarno,
										nama_tarno: row.nama_tarno,
										id_tarnodet: row.id_tarnodet,
									});
								}}
							/>
							<TextField disabled className="w-full mb-2" placeholder="Nama Prosedur Non Bedah" value={this.state.nama_tarno} />
							<TextField disabled className="w-full mb-2" placeholder="Id Prosedur Non Bedah" value={this.state.id_tarno} />
							<Button onClick={() => {
								api.post(`/rincian/${this.props.data.tipe === 1 ? 'jalan' : this.props.data.tipe === 2 ? 'inap' : ''}/nonop`, {
									id_tarno: this.state.id_tarno,
									id_tarnodet: this.state.id_tarnodet,
									id_pelinap: this.props.data.id_pelinap,
									id_peljalan: this.props.data.id_peljalan,
								})
								.then(result => {
									this.props.wasSuccessful();
								})
								.catch(error => {
									notification.error({
										message: 'Error',
										description: JSON.stringify(error.response.data),
								 });
									this.setState({
										id_tarno: '',
										nama_tarno: '',
										id_tarnodet: '',
									});
								});
							}}>Simpan</Button>
						</>
						: null
					}
					{
						this.state.id_peltarif === '5' ?
						<> {/* Tarif Konsultasi */}
							<div>Tarif Konsultasi</div>
							<Dropdown
								data={this.state.dataKonsultasi}
								className="mb-2"
								onChange={(value) => {this.setState({ id_tarkonsultasidet: value.value })}}
								value={this.state.id_tarkonsultasidet}
							/>
							<div>Dokter</div>
							<Dropdown
								data={this.state.dataDokter}
								className="mb-2"
								onChange={(value) => {this.setState({ dokterKonsultasi: value.value })}}
								value={this.state.dokterKonsultasi}
							/>
							{
								this.props.data.tipe === 1 ?
									<>
										<div>Pilih Poli</div>
										<Dropdown
											data={this.state.dataPoli}
											className="mb-2"
											onChange={(value) => {this.setState({ id_pelpoli: value.value })}}
											value={this.state.id_pelpoli}
										/>
									</>
								: null
							}
							<Button onClick={() => {
								api.post(`/rincian/${this.props.data.tipe === 1 ? 'jalan' : this.props.data.tipe === 2 ? 'inap' : ''}/konsultasi`, {
									id_tarkonsultasidet: this.state.id_tarkonsultasidet,
									dokter: this.state.dokterKonsultasi,
									id_pelpoli: this.state.id_pelpoli,
									id_pelinap: this.props.data.id_pelinap,
									id_peljalan: this.props.data.id_peljalan,
								})
								.then(result => {
									this.props.wasSuccessful();
									this.setState({
										id_tarkonsultasidet: '',
										id_pelpoli: '',
										dokterKonsultasi: '',
									});
								})
								.catch(error => {
									notification.error({
										message: 'Error',
										description: JSON.stringify(error.response.data),
									});
								});
							}}>Simpan</Button>
						</>
						: null
					}
					{
						this.state.id_peltarif === '6' ?
						<> {/* Tenaga Ahli */}
							<Autocomplete
								className="w-full mr-2"
								placeholder="Cari Tenaga Ahli..."
								onEnter={(value) => {
									api.get(`/tarif/ahli/search`, {
										headers: { search: value }
									})
									.then(result => {
										const result2 = result.data.map((row) => {return {value: row.id_tarahlidet, ...row }})
										this.setState({ dataTarifAhli: result2 });
									})
									.catch(error => {
										console.log('Error: ', error.response);
									});
								}}
								data={this.state.dataTarifAhli}
								list={(row) => (
									<div>{row.nama_tarahli} [Id: {row.id_tarahli}] [Tarif: {separator(row.tarif)}]</div>
								)}
								onSelect={(row) => {
									this.setState({
										id_tarahli: row.id_tarahli,
										nama_tarahli: row.nama_tarahli,
										id_tarahlidet: row.id_tarahlidet,
									});
								}}
							/>
							<TextField disabled className="w-full mb-2" placeholder="Nama Tenaga Ahli" value={this.state.nama_tarahli} />
							<TextField disabled className="w-full mb-2" placeholder="Id Tenaga Ahli" value={this.state.id_tarahli} />
							<Button onClick={() => {
								api.post(`/rincian/${this.props.data.tipe === 1 ? 'jalan' : this.props.data.tipe === 2 ? 'inap' : ''}/ahli`, {
									id_tarahli: this.state.id_tarahli,
									id_tarahlidet: this.state.id_tarahlidet,
									id_pelinap: this.props.data.id_pelinap,
									id_peljalan: this.props.data.id_peljalan,
								})
								.then(result => {
									this.props.wasSuccessful();
								})
								.catch(error => {
									notification.error({
										message: 'Error',
										description: JSON.stringify(error.response.data),
									});
									this.setState({
										id_tarahli: '',
										nama_tarahli: '',
										id_tarahlidet: '',
									});
								});
							}}>Simpan</Button>
						</>
						: null
					}
					{
						this.state.id_peltarif === '7' ?
						<> {/* Keperawatan */}
							<Autocomplete
								className="w-full mr-2"
								placeholder="Cari Keperawatan..."
								onEnter={(value) => {
									api.get(`/tarif/keperawatan/search`, {
										headers: { search: value }
									})
									.then(result => {
										const result2 = result.data.map((row) => {return {value: row.id_tarkeperawatandet, ...row }})
										this.setState({ dataTarifKeperawatan: result2 });
									})
									.catch(error => {
										console.log('Error: ', error.response);
									});
								}}
								data={this.state.dataTarifKeperawatan}
								list={(row) => (
									<div>{row.nama_tarkeperawatan} [Id: {row.id_tarkeperawatan}] [Tarif: {separator(row.tarif)}]</div>
								)}
								onSelect={(row) => {
									this.setState({
										id_tarkeperawatan: row.id_tarkeperawatan,
										nama_tarkeperawatan: row.nama_tarkeperawatan,
										id_tarkeperawatandet: row.id_tarkeperawatandet,
									});
								}}
							/>
							<TextField disabled className="w-full mb-2" placeholder="Nama Keperawatan" value={this.state.nama_tarkeperawatan} />
							<TextField disabled className="w-full mb-2" placeholder="Id Keperawatan" value={this.state.id_tarkeperawatan} />
							<Button onClick={() => {
								api.post(`/rincian/${this.props.data.tipe === 1 ? 'jalan' : this.props.data.tipe === 2 ? 'inap' : ''}/keperawatan`, {
									id_tarkeperawatan: this.state.id_tarkeperawatan,
									id_tarkeperawatandet: this.state.id_tarkeperawatandet,
									id_pelinap: this.props.data.id_pelinap,
									id_peljalan: this.props.data.id_peljalan,
								})
								.then(result => {
									this.props.wasSuccessful();
								})
								.catch(error => {
									notification.error({
										message: 'Error',
										description: JSON.stringify(error.response.data),
									});
									this.setState({
										id_tarkeperawatan: '',
										nama_tarkeperawatan: '',
										id_tarkeperawatandet: '',
									});
								});
							}}>Simpan</Button>
						</>
						: null
					}
					{
						this.state.id_peltarif === '8' ?
						<> {/* Penunjang */}
							<Autocomplete
								className="w-full mr-2"
								placeholder="Cari Penunjang..."
								onEnter={(value) => {
									api.get(`/tarif/penunjang/search`, {
										headers: { search: value }
									})
									.then(result => {
										const result2 = result.data.map((row) => {return {value: row.id_tarpenunjangdet, ...row }})
										this.setState({ dataTarifPenunjang: result2 });
									})
									.catch(error => {
										console.log('Error: ', error.response);
									});
								}}
								data={this.state.dataTarifPenunjang}
								list={(row) => (
									<div>{row.nama_tarpenunjang} [Id: {row.id_tarpenunjang}] [Tarif: {separator(row.tarif)}]</div>
								)}
								onSelect={(row) => {
									this.setState({
										id_tarpenunjang: row.id_tarpenunjang,
										nama_tarpenunjang: row.nama_tarpenunjang,
										id_tarpenunjangdet: row.id_tarpenunjangdet,
									});
								}}
							/>
							<TextField disabled className="w-full mb-2" placeholder="Nama Penunjang" value={this.state.nama_tarpenunjang} />
							<TextField disabled className="w-full mb-2" placeholder="Id Penunjang" value={this.state.id_tarpenunjang} />
							<Button onClick={() => {
								api.post(`/rincian/${this.props.data.tipe === 1 ? 'jalan' : this.props.data.tipe === 2 ? 'inap' : ''}/penunjang`, {
									id_tarpenunjang: this.state.id_tarpenunjang,
									id_tarpenunjangdet: this.state.id_tarpenunjangdet,
									id_pelinap: this.props.data.id_pelinap,
									id_peljalan: this.props.data.id_peljalan,
								})
								.then(result => {
									this.props.wasSuccessful();
								})
								.catch(error => {
									notification.error({
										message: 'Error',
										description: JSON.stringify(error.response.data),
									});
									this.setState({
										id_tarpenunjang: '',
										nama_tarpenunjang: '',
										id_tarpenunjangdet: '',
									});
								});
							}}>Simpan</Button>
						</>
						: null
					}
					{
						this.state.id_peltarif === '9' ?
						<> {/* Darah */}
							<Autocomplete
								className="w-full mr-2"
								placeholder="Cari Darah..."
								onEnter={(value) => {
									api.get(`/tarif/darah/search`, {
										headers: { search: value }
									})
									.then(result => {
										const result2 = result.data.map((row) => {return {value: row.id_tardarahdet, ...row }})
										this.setState({ dataTarifDarah: result2 });
									})
									.catch(error => {
										console.log('Error: ', error.response);
									});
								}}
								data={this.state.dataTarifDarah}
								list={(row) => (
									<div>{row.nama_tardarah} [Id: {row.id_tardarah}] [Tarif: {separator(row.tarif)}]</div>
								)}
								onSelect={(row) => {
									this.setState({
										id_tardarah: row.id_tardarah,
										nama_tardarah: row.nama_tardarah,
										id_tardarahdet: row.id_tardarahdet,
									});
								}}
							/>
							<TextField disabled className="w-full mb-2" placeholder="Nama Darah" value={this.state.nama_tardarah} />
							<TextField disabled className="w-full mb-2" placeholder="Id Darah" value={this.state.id_tardarah} />
							<Button onClick={() => {
								api.post(`/rincian/${this.props.data.tipe === 1 ? 'jalan' : this.props.data.tipe === 2 ? 'inap' : ''}/darah`, {
									id_tardarah: this.state.id_tardarah,
									id_tardarahdet: this.state.id_tardarahdet,
									id_pelinap: this.props.data.id_pelinap,
									id_peljalan: this.props.data.id_peljalan,
								})
								.then(result => {
									this.props.wasSuccessful();
								})
								.catch(error => {
									notification.error({
										message: 'Error',
										description: JSON.stringify(error.response.data),
									});
									this.setState({
										id_tardarah: '',
										nama_tardarah: '',
										id_tardarahdet: '',
									});
								});
							}}>Simpan</Button>
						</>
						: null
					}
					{
						this.state.id_peltarif === '10' ?
						<> {/* Rehabilitasi */}
							<Autocomplete
								className="w-full mr-2"
								placeholder="Cari rehabilitasi..."
								onEnter={(value) => {
									api.get(`/tarif/rehabilitasi/search`, {
										headers: { search: value }
									})
									.then(result => {
										const result2 = result.data.map((row) => {return {value: row.id_tarrehabilitasidet, ...row }})
										this.setState({ dataTarifRehabilitasi: result2 });
									})
									.catch(error => {
										console.log('Error: ', error.response);
									});
								}}
								data={this.state.dataTarifRehabilitasi}
								list={(row) => (
									<div>{row.nama_tarrehabilitasi} [Id: {row.id_tarrehabilitasi}] [Tarif: {separator(row.tarif)}]</div>
								)}
								onSelect={(row) => {
									this.setState({
										id_tarrehabilitasi: row.id_tarrehabilitasi,
										nama_tarrehabilitasi: row.nama_tarrehabilitasi,
										id_tarrehabilitasidet: row.id_tarrehabilitasidet,
									});
								}}
							/>
							<TextField disabled className="w-full mb-2" placeholder="Nama rehabilitasi" value={this.state.nama_tarrehabilitasi} />
							<TextField disabled className="w-full mb-2" placeholder="Id rehabilitasi" value={this.state.id_tarrehabilitasi} />
							<Button onClick={() => {
								api.post(`/rincian/${this.props.data.tipe === 1 ? 'jalan' : this.props.data.tipe === 2 ? 'inap' : ''}/rehabilitasi`, {
									id_tarrehabilitasi: this.state.id_tarrehabilitasi,
									id_tarrehabilitasidet: this.state.id_tarrehabilitasidet,
									id_pelinap: this.props.data.id_pelinap,
									id_peljalan: this.props.data.id_peljalan,
								})
								.then(result => {
									this.props.wasSuccessful();
								})
								.catch(error => {
									notification.error({
										message: 'Error',
										description: JSON.stringify(error.response.data),
									});
									this.setState({
										id_tarrehabilitasi: '',
										nama_tarrehabilitasi: '',
										id_tarrehabilitasidet: '',
									});
								});
							}}>Simpan</Button>
						</>
						: null
					}
					{
						this.state.id_peltarif === '12' ?
						<> {/* Alkes */}
							<Autocomplete
								className="w-full mr-2"
								placeholder="Cari alkes..."
								onEnter={(value) => {
									api.get(`/tarif/alkes/search`, {
										headers: { search: value }
									})
									.then(result => {
										const result2 = result.data.map((row) => {return {value: row.id_taralkesdet, ...row }})
										this.setState({ dataTarifAlkes: result2 });
									})
									.catch(error => {
										console.log('Error: ', error.response);
									});
								}}
								data={this.state.dataTarifAlkes}
								list={(row) => (
									<div>{row.nama_taralkes} [Id: {row.id_taralkes}] [Tarif: {separator(row.tarif)}]</div>
								)}
								onSelect={(row) => {
									this.setState({
										id_taralkes: row.id_taralkes,
										nama_taralkes: row.nama_taralkes,
										id_taralkesdet: row.id_taralkesdet,
									});
								}}
							/>
							<TextField disabled className="w-full mb-2" placeholder="Nama alkes" value={this.state.nama_taralkes} />
							<TextField disabled className="w-full mb-2" placeholder="Id alkes" value={this.state.id_taralkes} />
							<Button onClick={() => {
								api.post(`/rincian/${this.props.data.tipe === 1 ? 'jalan' : this.props.data.tipe === 2 ? 'inap' : ''}/alkes`, {
									id_taralkes: this.state.id_taralkes,
									id_taralkesdet: this.state.id_taralkesdet,
									id_pelinap: this.props.data.id_pelinap,
									id_peljalan: this.props.data.id_peljalan,
								})
								.then(result => {
									this.props.wasSuccessful();
								})
								.catch(error => {
									notification.error({
										message: 'Error',
										description: JSON.stringify(error.response.data),
									});
									this.setState({
										id_taralkes: '',
										nama_taralkes: '',
										id_taralkesdet: '',
									});
								});
							}}>Simpan</Button>
						</>
						: null
					}
					{
						this.state.id_peltarif === '13' ?
						<> {/* Bmhp */}
							<Autocomplete
								className="w-full mr-2"
								placeholder="Cari bmhp..."
								onEnter={(value) => {
									api.get(`/tarif/bmhp/search`, {
										headers: { search: value }
									})
									.then(result => {
										const result2 = result.data.map((row) => {return {value: row.id_tarbmhpdet, ...row }})
										this.setState({ dataTarifBmhp: result2 });
									})
									.catch(error => {
										console.log('Error: ', error.response);
									});
								}}
								data={this.state.dataTarifBmhp}
								list={(row) => (
									<div>{row.nama_tarbmhp} [Id: {row.id_tarbmhp}] [Tarif: {separator(row.tarif)}]</div>
								)}
								onSelect={(row) => {
									this.setState({
										id_tarbmhp: row.id_tarbmhp,
										nama_tarbmhp: row.nama_tarbmhp,
										id_tarbmhpdet: row.id_tarbmhpdet,
									});
								}}
							/>
							<TextField disabled className="w-full mb-2" placeholder="Nama bmhp" value={this.state.nama_tarbmhp} />
							<TextField disabled className="w-full mb-2" placeholder="Id bmhp" value={this.state.id_tarbmhp} />
							<Button onClick={() => {
								api.post(`/rincian/${this.props.data.tipe === 1 ? 'jalan' : this.props.data.tipe === 2 ? 'inap' : ''}/bmhp`, {
									id_tarbmhp: this.state.id_tarbmhp,
									id_tarbmhpdet: this.state.id_tarbmhpdet,
									id_pelinap: this.props.data.id_pelinap,
									id_peljalan: this.props.data.id_peljalan,
								})
								.then(result => {
									this.props.wasSuccessful();
								})
								.catch(error => {
									notification.error({
										message: 'Error',
										description: JSON.stringify(error.response.data),
									});
									this.setState({
										id_tarbmhp: '',
										nama_tarbmhp: '',
										id_tarbmhpdet: '',
									});
								});
							}}>Simpan</Button>
						</>
						: null
					}
					{
						this.state.id_peltarif === '14' ?
						<> {/* Alat */}
							<Autocomplete
								className="w-full mr-2"
								placeholder="Cari alat..."
								onEnter={(value) => {
									api.get(`/tarif/alat/search`, {
										headers: { search: value }
									})
									.then(result => {
										const result2 = result.data.map((row) => {return {value: row.id_taralatdet, ...row }})
										this.setState({ dataTarifAlat: result2 });
									})
									.catch(error => {
										console.log('Error: ', error.response);
									});
								}}
								data={this.state.dataTarifAlat}
								list={(row) => (
									<div>{row.nama_taralat} [Id: {row.id_taralat}] [Tarif: {separator(row.tarif)}]</div>
								)}
								onSelect={(row) => {
									this.setState({
										id_taralat: row.id_taralat,
										nama_taralat: row.nama_taralat,
										id_taralatdet: row.id_taralatdet,
									});
								}}
							/>
							<TextField disabled className="w-full mb-2" placeholder="Nama alat" value={this.state.nama_taralat} />
							<TextField disabled className="w-full mb-2" placeholder="Id alat" value={this.state.id_taralat} />
							<Button onClick={() => {
								api.post(`/rincian/${this.props.data.tipe === 1 ? 'jalan' : this.props.data.tipe === 2 ? 'inap' : ''}/alat`, {
									id_taralat: this.state.id_taralat,
									id_taralatdet: this.state.id_taralatdet,
									id_pelinap: this.props.data.id_pelinap,
									id_peljalan: this.props.data.id_peljalan,
								})
								.then(result => {
									this.props.wasSuccessful();
								})
								.catch(error => {
									notification.error({
										message: 'Error',
										description: JSON.stringify(error.response.data),
									});
									this.setState({
										id_taralat: '',
										nama_taralat: '',
										id_taralatdet: '',
									});
								});
							}}>Simpan</Button>
						</>
						: null
					}
				</div>
			</>
		)
	}

}

export default AddRincian;

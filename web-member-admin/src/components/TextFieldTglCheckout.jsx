import React from 'react';
import TextField from '@material-ui/core/TextField';

class TextFieldTglCheckout extends React.Component {

   render() {
      return (
         <TextField
            label="Tgl Checkout"
            name="tgl_checkout"
            type="date" variant="outlined"
            fullWidth
            onChange={this.props.onChange}
            InputLabelProps={{ shrink: true }}
            style={{height: '40px'}}
            value={this.props.value}
         />
      );
   }

}

export default TextFieldTglCheckout;

import React from 'react';

class NumberBox extends React.Component {

	state = {
		focus: false,
		ref: React.createRef()
	}

	render() {
		let styleFocus;
		if (this.state.focus) {
			styleFocus = {
				border: '1px solid',
			};
		} else {
			styleFocus = {
				border: '1px solid #3987c9',
			};
		}
		return (
         <input
				min={this.props.min}
				disabled={this.props.disabled ? true : false}
				value={this.props.value}
				name={this.props.name}
				type="number"
				onChange={this.props.onChange}
				onKeyDown={this.props.onKeyDown}
				style={{
					width: this.props.width,
					padding: '5px 4px 5px 4px',
					borderRadius: '4px',
					...styleFocus
				}}
				onFocus={() => {this.setState({ focus: true })}}
				onBlur={() => {this.setState({ focus: false })}}
				ref={this.state.ref}
				placeholder={this.props.placeholder}
			/>
      );
   }

};

export default NumberBox;
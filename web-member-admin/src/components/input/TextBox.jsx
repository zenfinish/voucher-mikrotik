import React from 'react';

class TextBox extends React.Component {
	
	render() {
		return (
         <input
				disabled={this.props.disabled ? true : false}
				value={this.props.value}
				name={this.props.name}
				placeholder={this.props.placeholder}
				type="text"
				onChange={this.props.onChange}
				style={{
					width: this.props.width ? this.props.width : '100%',
					padding: '5px 4px 5px 4px',
					borderRadius: '4px',
					border: '1px solid #3987c9',
					marginRight: '4px',
				}}
				autoComplete="off"
			/>
      );
   }

};

export default TextBox;
import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import api from '../../config/api.js';

const styles = () => ({
	autocomplete: {
		padding: '10px',
		cursor: 'pointer',
		backgroundColor: '#fff',
		border: '1px solid #d4d4d4',
		borderBottom: 'none',
		'&:hover': {
			backgroundColor: '#e9e9e9',
		},
	},
	autocompleteActive: {
		backgroundColor: 'DodgerBlue !important',
		color: '#ffffff',
		padding: '10px',
	}
});

class Autocomplete extends React.Component {
	
	state = {
		rowActive: 0,
		data: [],
		dataTampil: [],
		value: '',
	}

	keyDown = (e) => {
		if (e.keyCode === 40) {
			if (this.state.rowActive < this.state.dataTampil.length - 1) {
				this.setState({
					rowActive: this.state.rowActive + 1
				});
			}
		} else if (e.keyCode === 38) {
			if (this.state.rowActive > 0) {
				this.setState({
					rowActive: this.state.rowActive - 1
				});
			}
		} else if (e.keyCode === 13) {
			if (this.state.dataTampil.length === 0) {
				this.fetchData();
			} else {
				this.props.onenter(this.state.data[this.state.rowActive]);
				this.clear();
				this.setState({
					rowActive: 0,
					value: '',
				});
			}
		}
	}

	onMouseClick = (index) => {
		this.props.onenter(this.state.data[index]);
		this.clear();
		this.setState({
			rowActive: 0,
			value: '',
		});
	}

	fetchData = () => {
		api.get(`${this.props.link}/${this.state.value}`, {
         headers: { token: localStorage.getItem('token') }
      })
      .then(result => {
			if (this.props.dataTampil !== undefined) {
				this.setState({
					data: result.data,
				}, () => {
					let guguk = [];
					result.data.forEach((value) => {
						let dataTampil = {};
						for (let j = 0; j < this.props.dataTampil.length; j++) {
							dataTampil[this.props.dataTampil[j]] = value[this.props.dataTampil[j]];
						}
						guguk.push(dataTampil);
					});
					this.setState({ dataTampil: guguk });
				});
			} else {
				this.setState({
					data: result.data,
					dataTampil: result.data,
				});
			}
      })
      .catch(error => {
         console.log(error.response)
      });
	}

	clear = () => {
		this.setState({
			data: [],
			dataTampil: [],
		});
   }
	
	render() {
		const { classes } = this.props;
		return (
			<Fragment>
				<input
					type="text"
					placeholder={this.props.placeholder}
					style={{
						width: this.props.width,
						padding: '0 4px',
						borderRadius: '5px',
						fontSize:'14px',
						border: '1px solid #95B8E7',
						height: '28px',
						':focus': {
							borderColor: '#6b9cde',
						},
					}}
					onKeyDown={this.keyDown}
					onChange={
						(e) => {
							this.setState({
								data: [],
								dataTampil: [],
								value: e.target.value
							});
						}
					}
					name={this.props.name}
					autoComplete="off"
					value={this.state.value}
					ref={this.props.refautocomplete}
					disabled={this.props.disabled === true ? true : false}
				/>
				{/* {
					this.state.dataTampil !== undefined ?
						this.state.dataTampil.length !== 0 ?
						<DataGrid
							data={this.state.dataTampil}
							style={{
								height:200, width: '500px%',
								overflowY: 'auto',
								zIndex: 99,
								position: 'fixed',
							}}
						>
							{
								this.state.dataTampil.map((data, index) => (
									<GridColumn field={data} title="Nama Obat" key={index} />
								))
							}
						</DataGrid> : ''
					: ''
				} */}
				{
					this.state.dataTampil !== undefined ?
						this.state.dataTampil.length !== 0 ?
						<div style={{
							height: 200,
							overflowY: 'auto',
							zIndex: 99,
							position: 'fixed',
							width: `500px`,
						}}>
							<table
								style={{
									width: '100%',
									borderCollapse: 'collapse',
									border: '1px solid black',
								}}
								cellPadding="5"
							>
								<tbody>
									{
										this.state.dataTampil.map((data, index) => (
											<tr
												key={index}
												className={this.state.rowActive === index ? classes.autocompleteActive : classes.autocomplete}
												onMouseDown={() => {this.onMouseClick(index)}}
											>
												{
													Object.values(data).map((data2, index2) => (
														<td key={index2}>{data2}</td>
													))
												}
											</tr>
										))
									}
								</tbody>
							</table>
						</div> : ''
					: ''
				}
			</Fragment>
      );
   }

};

Autocomplete.propTypes = {
   classes: PropTypes.object.isRequired,
};

export default (withStyles(styles)(Autocomplete));
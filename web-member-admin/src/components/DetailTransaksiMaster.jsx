import React from 'react';
import { DataGrid, GridColumn } from 'rc-easyui';
import { Dialog, DialogContent, DialogTitle, DialogActions, Button } from '@material-ui/core';
import { separator } from '../config/helpers.js';

class DetailTransaksiMaster extends React.Component {
    
   render() {
      return (
			<Dialog
				open={this.props.open}
				onClose={this.props.close}
				maxWidth="lg"
				fullWidth={true}
			>
				<DialogTitle>Detail Transaksi Master</DialogTitle>
				<DialogContent>
					<DataGrid
						data={this.props.data}
					>
						<GridColumn field="KODE_PERK" title="KODE_PERK" width="100px" align="center"></GridColumn>
						<GridColumn field="NAMA_PERK" title="NAMA_PERK" width="400px"></GridColumn>
						<GridColumn field="DEBET" title="DEBET" width="100px" align="right"
							render={({ row }) => (
								<span>{separator(row.DEBET)}</span>
							)}
						/>
						<GridColumn field="KREDIT" title="KREDIT" width="100px" align="right"
							render={({ row }) => (
								<span>{separator(row.KREDIT)}</span>
							)}
						/>
					</DataGrid>
				</DialogContent>
				<DialogActions>
					<Button onClick={this.props.close} color="secondary" variant="contained">Close</Button>
				</DialogActions>
			</Dialog>
      );
   }

};

export default DetailTransaksiMaster;
import React from 'react';

class DataGrid extends React.Component {

	render() {
		return (
			<div className="overflow-auto" style={{ height: '90%' }}>
				<table className={`text-sm w-full overflow-auto whitespace-nowrap`} style={{ maxHeight: '100%', maxWidth: '99%' }}>
					<thead>
						<tr>
							{
								React.Children.map(this.props.children, (child, index) => {
									return React.cloneElement(child, {
										index,
										status: 'head',
									});
								})
							}
						</tr>
					</thead>
					<tbody>
						{
							this.props.data.map((row, i) => (
								<tr
									key={i}
									className={`odd:bg-gray-200 even:bg-white`}
									style={this.props.styleTr ? this.props.styleTr(row) : {}}
								>
								{/* <tr key={i} className="hover:bg-gray-200 even:bg-white"> */}
									{
										React.Children.map(this.props.children, (child, index) => {
											return React.cloneElement(child, {
												index: i,
												status: 'body',
												data: row
											});
										})
									}
								</tr>
							))
						}
					</tbody>
				</table>
				{
					this.props.loading ?
						<div className="absolute w-full h-full flex items-center justify-center bg-black bg-opacity-25 z-30 top-0 left-0">
							<div className="bg-white rounded-lg shadow">
								<img src={require('assets/imgs/loading2.gif')} alt="" className="h-12 w-auto" />
							</div>
						</div>
					: null
				}
			</div>
		)
	}

}

export default DataGrid;

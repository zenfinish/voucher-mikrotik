import React from 'react';

class GridColumn extends React.Component {
	
	render() {
		return (
      this.props.status === 'head' ?
				<th className="border-dotted border border-gray-600 px-2 bg-white sticky top-0">
					<div
						className={`
							${this.props.center ? 'text-center' : this.props.right ? 'text-right' : 'text-left'}
						`}
					>{this.props.title}</div>
				</th>
			:
				<td className={`border-dotted border border-gray-600 ${this.props.className}`}>
					<div
						className={`
							px-2
							${this.props.center ? 'text-center' : this.props.right ? 'text-right' : 'text-left'}
						`}
					>
						{
							this.props.render ?
								this.props.render(this.props.data, this.props.index)
							:
								this.props.data[`${this.props.field}`]
						}
					</div>
				</td>
		)
	}

}

export default GridColumn;

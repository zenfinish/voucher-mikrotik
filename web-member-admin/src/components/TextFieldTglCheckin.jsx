import React from 'react';
import { TextField } from '@material-ui/core';

class TextFieldTglCheckin extends React.Component {

   render() {
      return (
         <TextField
            name="tgl_checkin"
            type="date"
            onChange={this.props.onChange}
            value={this.props.value}
            label="Tgl Checkin"
            InputLabelProps={{ shrink: true }}
            InputProps={this.props.disabled === true ? { disabled: true, } : { disabled: false, }}
            fullWidth
         />
      );
   }

}

export default TextFieldTglCheckin;

import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

import { TextField } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
   paper: {
      position: 'absolute',
      zIndex: 1,
      marginTop: theme.spacing.unit,
      left: 0,
      right: 0,
   },
 });

class RinciLain extends React.Component {
    
   state = {
      openSearch: false,
      data: [],
      search: '',
   }

   getData = (e) => {
      e.preventDefault();
   }

   getDataBeneran = (data) => {
      this.props.getData(data);
      this.setState({
         data: [],
         openSearch: false,
         search: '',
      });
   }

   handleChange = (e) => {
      this.setState({
         [e.target.name]: e.target.value,
         data: [],
         openSearch: false,
      });
   }
   
   render() {
      return (
         <Fragment>
            <form onSubmit={this.getData}>
               <TextField
                  label="Lain - Lain"
                  style={{height: '40px'}}
                  variant="outlined"
                  InputLabelProps={{ shrink: true }}
                  SelectProps={{ native: true }}
                  fullWidth
                  onChange={this.handleChange}
                  name="search" autoComplete="off"
                  value={this.state.search}
               />
            </form>
            <div style={{ overflowX: 'auto', overflowY: 'auto', height: 100 }}>
               <table border="1" style={{width: '100%'}}>
                  <thead>
                     <tr>
                        <td>No.</td>
                        <td>Nama</td>
                     </tr>
                  </thead>
                  <tbody>
                     {
                        this.props.data.map((row, index) => (
                           <tr key={index}>
                              <td align="center">{index+1}</td>
                              <td align="center">{row.nama_karyawan}</td>
                           </tr>
                        ))
                     }
                  </tbody>
               </table>
            </div>
         </Fragment>
      );
   }
}

RinciLain.propTypes = {
   classes: PropTypes.object.isRequired,
};
 
 export default withStyles(styles)(RinciLain);

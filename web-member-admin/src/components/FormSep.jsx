import React from 'react';
import ReactToPrint from 'react-to-print';
import { Dialog, DialogActions, DialogContent, Button } from '@material-ui/core';

class FormSep extends React.Component {
   
   render() {
      let data = JSON.parse(localStorage.getItem('sep'));
      return (
         <div style={{overflowX: 'auto'}}>
            <Dialog
               open={this.props.open}
               onClose={this.props.close}
               maxWidth="lg"
               fullWidth={true}
            >
               <DialogContent>
                  <table ref={el => (this.componentRef = el)}>
                     <tbody>
                        <tr>
                           <td rowSpan={2}>
                              <img
                                 src={require('../config/logo-bpjs.png')}
                                 style={{width: 200, height: 'auto',}}
                                 alt=""
                              />
                           </td>
                           <td>SURAT ELEGIBILITAS PESERTA</td>
                           <td rowSpan={2}>{data && (data.prb !== 'null' || data.prb) !== `PRB : ` + data.prb ? '' : ''}</td>
                        </tr>
                        <tr>
                           <td>RS. PERTAMEDIKA UMMI ROSNATI</td>
                        </tr>
                        <tr>
                           <td colSpan={3}>
                              <table style={{width: '100%'}}>
                                 <tbody>
                                    <tr>
                                       <td>No.bpjs_sep</td>
                                       <td>:</td>
                                       <td>{data ? data.bpjs_sep : ''}</td>
                                       <td colSpan={3}></td>
                                    </tr>
                                    <tr>
                                       <td>Tgl.bpjs_sep</td>
                                       <td>:</td>
                                       <td>{data ? data.tgl_checkin : ''}</td>
                                       <td>Peserta</td>
                                       <td>:</td>
                                       <td>{data ? data.jenis_peserta : ''}</td>
                                    </tr>
                                    <tr>
                                       <td>No.Kartu</td>
                                       <td>:</td>
                                       <td>{data ? data.no_bpjs + ` (No. RM: ${data.no_rekmedis})` : ''}</td>
                                       <td>COB</td>
                                       <td>:</td>
                                       <td>{data && data.cob === '0' ? data.cob : '-'}</td>
                                    </tr>
                                    <tr>
                                       <td>Nama Peserta</td>
                                       <td>:</td>
                                       <td>{data ? data.nama_pasien : ''}</td>
                                       <td>Jns.Rawat</td>
                                       <td>:</td>
                                       <td>{data ? (data.jenis_pelayanan === '1' ? 'R.Inap' : 'R.Jalan') : ''}</td>
                                    </tr>
                                    <tr>
                                       <td>Tgl.Lahir</td>
                                       <td>:</td>
                                       <td>{data ? data.tgl_lahir : ''}</td>
                                       <td>Kls.Rawat</td>
                                       <td>:</td>
                                       <td>{data ? data.kelas_bpjs : ''}</td>
                                    </tr>
                                    <tr>
                                       <td>No.Telepon</td>
                                       <td>:</td>
                                       <td>{data ? data.hp : ''}</td>
                                       <td>Penjamin</td>
                                       <td>:</td>
                                       <td>{data ? data.penjamin : ''}</td>
                                    </tr>
                                    <tr>
                                       <td>Sub/Spesialis</td>
                                       <td>:</td>
                                       <td>{data ? data.nama_poli : ''}</td>
                                       <td colSpan={3}></td>
                                    </tr>
                                    <tr>
                                       <td>Faskes Perujuk</td>
                                       <td>:</td>
                                       <td>{data ? data.nama_fpk : ''}</td>
                                       <td colSpan={3}></td>
                                    </tr>
                                    <tr>
                                       <td>Diagnosa Awal</td>
                                       <td>:</td>
                                       <td>{data ? data.nama_diagnosis : ''}</td>
                                       <td colSpan={3}></td>
                                    </tr>
                                    <tr>
                                       <td>Catatan</td>
                                       <td>:</td>
                                       <td>{data ? data.catatan : ''}</td>
                                       <td colSpan={3}></td>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                        </tr>
                        <tr>
                           <td colSpan={2}>
                              *Saya Menyetujui BPJS Kesehatan menggunakan informasi medis pasien jika diperlukan.<br />
                              *SEP Bukan sebagai bukti penjaminan peserta. 
                           </td>
                           <td align="right">Pasien/Keluarga Pasien</td>
                        </tr>
                        <tr>
                           <td colSpan={2}>
                              Cetakan : {new Date().toISOString()}
                           </td>
                           <td align="right">____________________</td>
                        </tr>
                     </tbody>
                  </table>
               </DialogContent>
               <DialogActions>
                  <ReactToPrint
                     trigger={() => <Button color="primary" variant="contained">Cetak</Button>}
                     content={() => this.componentRef}
                  />
                  <Button onClick={this.props.close} color="primary" variant="contained">Close</Button>
               </DialogActions>
            </Dialog>
         </div>
      );
   }
}

export default (FormSep);

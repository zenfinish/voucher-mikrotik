import React from 'react';
import { DataGrid, GridColumn, GridColumnGroup, GridHeaderRow, LinkButton, ButtonGroup, Tooltip } from 'rc-easyui';

class TableTransaksi extends React.Component {
	
   render() {
      return (
			<DataGrid
				data={this.props.data}
				style={{height:320}}
				lazy
				showFooter
				footerData={[
					{ nama_obat: "Pembungkus / Jasa Pelayanan:", harga: null, jml_harga: null },
					{ nama_obat: "Total:", jml_harga: this.props.total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") },
				]}
			>
				<GridColumnGroup frozen align="left" width="450px">
					<GridHeaderRow>
						<GridColumn field="action" title="Action" width="50px" align="center"
							render={({ row }) => (
								<ButtonGroup>
									<Tooltip content="Hapus">
										<LinkButton iconCls="icon-trash" onClick={() => {this.props.delete(row.id_farstok)}} />
									</Tooltip>
								</ButtonGroup>
							)}
						/>
						<GridColumn field="qty" title="Qty" width="50px" align="center"></GridColumn>
						<GridColumn field="kronis" title="Kronis" width="50px" align="center"></GridColumn>
						<GridColumn field="nama_obat" title="Nama Obat"
							render={({ row }) => (
								row.nama_obat + ` (${row.id_farstok})`
							)}
						/>
					</GridHeaderRow>
				</GridColumnGroup>
				<GridColumn field="nama_satuan" title="Satuan" align="center"></GridColumn>
				<GridColumn field="harga" title="Harga" align="right"
					render={({ row }) => (
						row.harga.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
					)}
				/>
				<GridColumn field="jml_harga" title="Jml. Harga" align="right"
					render={({ row }) => (
						row.jml_harga.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
					)}
				/>
			</DataGrid>
      );
   }

};

export default TableTransaksi;

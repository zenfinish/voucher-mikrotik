import React from 'react';
import { Dialog, DialogContent, DialogActions } from '@material-ui/core';
import ReactToPrint from 'react-to-print';
import Button from 'components/inputs/Button.jsx';
import QRCode from 'qrcode.react';
import { tglIndo } from 'config/helpers.js';

class CetakNoStok extends React.Component {
  
  render() {
    return (
			<Dialog open maxWidth="xl" fullWidth scroll="paper">
        <DialogContent ref={print => (this.componentRef = print)}>
          {
            this.props.data.map((row, i) => (
              <div key={i} style={{ pageBreakAfter: 'always' }} className="flex mb-10">
								<div className="mr-2">
									<div>
										<QRCode
											value={`${row.id_farfakturdet}`}
										/>
									</div>
								</div>
								<div className="flex flex-col justify-between font-medium text-xl">
									<div>
										<div className="text-6xl">{row.id_farfakturdet}</div>
										<div>Ed. {tglIndo(row.tgl_kadaluarsa)}</div>
									</div>
									<div>{row.nama_farobat} [{row.id_farobat}]</div>
								</div>
							</div>
            ))
          }
        </DialogContent>
        <DialogActions>
          <ReactToPrint
						trigger={() => (
							<Button>Print</Button>
						)}
            content={() => this.componentRef}
					/>
          <Button onClick={this.props.close}>Close</Button>
        </DialogActions>
			</Dialog>
		)
	}

}

export default CetakNoStok;

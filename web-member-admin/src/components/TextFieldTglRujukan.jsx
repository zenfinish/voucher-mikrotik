import React from 'react';
import TextField from '@material-ui/core/TextField';

class TextFieldTglRujukan extends React.Component {

   render() {
      return (
         <TextField
            label="Tgl Rujukan"
            name="tgl_rujukan"
            type="date"
            fullWidth
            onChange={this.props.onChange}
            InputLabelProps={{ shrink: true }}
            value={this.props.value}
         />
      );
   }

}

export default TextFieldTglRujukan;

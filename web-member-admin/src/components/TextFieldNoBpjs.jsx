import React from 'react';
import TextField from '@material-ui/core/TextField';

class TextFieldNoBpjs extends React.Component {

   render() {
      return (
         <TextField
            label="No. Kartu Bpjs"
            name="no_bpjs"
            type="text"
            value={this.props.value}
            fullWidth maxLength="13"
            InputProps={this.props.disabled === true ? { disabled: true, } : { disabled: false, }}
            onChange={this.props.onChange}
            InputLabelProps={{ shrink: true }}
         />
      );
   }

}

export default TextFieldNoBpjs;

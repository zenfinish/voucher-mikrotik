import React from 'react';
import TextField from '@material-ui/core/TextField';

class SelectStatus extends React.Component {

   render() {
      return (
         <TextField
            select
            label="Status Pernikahan"
            onChange={this.props.onChange}
            SelectProps={{ native: true, }}
            InputLabelProps={{ shrink: true }}
            name="status"
            fullWidth
            value={this.props.value}
            InputProps={this.props.disabled === true ? { disabled: true, } : { disabled: false, }}
         >
            <option key="" value=""></option>
            <option key="MENIKAH" value="MENIKAH">Menikah</option>
            <option key="BELUM MENIKAH" value="BELUM MENIKAH">Belum Menikah</option>
         </TextField>
      );
   }

}

export default SelectStatus;
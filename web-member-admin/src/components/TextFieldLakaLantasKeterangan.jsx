import React from 'react';
import TextField from '@material-ui/core/TextField';

class TextFieldLakaLantasKeterangan extends React.Component {

   render() {
      return (
         <TextField
            label="Keterangan"
            name="laka_lantas_keterangan"
            type="text"
            fullWidth
            onChange={this.props.onChange}
            InputLabelProps={{ shrink: true }}
            value={this.props.value}
         />
      );
   }

}

export default TextFieldLakaLantasKeterangan;

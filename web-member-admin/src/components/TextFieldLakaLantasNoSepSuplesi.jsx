import React from 'react';
import TextField from '@material-ui/core/TextField';

class TextFieldLakaLantasNoSepSuplesi extends React.Component {

   render() {
      return (
         <TextField
            label="No. Sep Suplesi"
            onChange={this.props.onChange}
            SelectProps={{ native: true }}
            name="laka_lantas_no_sep_suplesi"
            fullWidth
            InputLabelProps={{ shrink: true }}
            value={this.props.value}
         />
      );
   }

}

export default TextFieldLakaLantasNoSepSuplesi;

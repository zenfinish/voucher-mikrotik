import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import { withStyles } from '@material-ui/core/styles';
import Fab from '@material-ui/core/Fab';
import Button from '@material-ui/core/Button';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';

import AddIcon from '@material-ui/icons/Add';
import DeleteIcon from '@material-ui/icons/Delete';
import SaveIcon from '@material-ui/icons/Save';

import FormAddProfile from '../components/FormAddProfile';
import api from '../config/api.js';

const styles = theme => ({
    root: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.paper,
        width: '50%',
    },
    button: {
        margin: theme.spacing.unit,
    },
});

class Profile extends React.Component {
    
    state = {
        openFormAddProfile: false,
        aktif: '',
        profileId: '',
        name: '',
        nameForUsers: '',
        owner: '',
        validity: '',
        price: '',
        limitationId: '',
        limitName: '',
        profileLimitationId: '',
    };

    handleChangeFormProfile = (e, item) => {
        this.setState({
            aktif: item.props.value,
            profileId: item.key,
        }, () => {
            api.get(`/mikrotik/get-profile/${item.key}`)
                .then(result => {
                    this.setState({
                        name: result.data.name,
                        nameForUsers: result.data.nameForUsers,
                        owner: result.data.owner,
                        validity: result.data.validity,
                        price: result.data.price,
                        limitName: result.data.limitation.name,
                        limitationId: result.data.limitation.limitId,
                        profileLimitationId: result.data.limitation.limitId,
                    });
                })
                .catch(error => {
                    console.log(error);
                });
        });
    };

    updateProfile = () => {
        if (Object.keys(this.state.aktif).length !== '') {
            api.put(`/mikrotik/update-profile`, {
                name: this.state.name,
                numbers: this.state.profileId,
                nameUsers: this.state.nameForUsers,
                validity: this.state.validity,
                price: this.state.price,
                limitationId: this.state.limitationId,
                profileLimitationId: this.state.profileLimitationId,
                limitName: this.state.limitName,
            }, {
                headers: { token: localStorage.getItem('token') }
            })
                .then(result => {
                    console.log(result.data)
                })
                .catch(error => {
                    console.log(error)
                });
        }
    }

    deleteProfile = () => {
        if (Object.keys(this.state.aktif).length !== '') {
            api.delete(`/mikrotik/delete-profile/${this.state.profileId}`, {
                headers: { token: localStorage.getItem('token') }
            })
                .then(result => {
                    this.props.getVoucher();
                })
                .catch(error => {
                    console.log(error)
                });
        }
    }

    closeFormAddProfile = () => {
        this.setState({ openFormAddProfile: false });
    };

    openFormAddProfile = () => {
        this.setState({ openFormAddProfile: true });
    };

    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value,
        });
    };

    render() {
        const { classes, dataVoucher, dataLimit } = this.props;
        return (
            <div>
                <Fab
                    color="primary"
                    aria-label="Add"
                    size="small"
                    className={classes.fab}
                    style={{marginBottom: '30px'}}
                    onClick={this.openFormAddProfile}
                ><AddIcon /></Fab>
                <TextField
                    select fullWidth
                    onChange={this.handleChangeFormProfile}
                    label="Select"
                    helperText="Please select your profile"
                    variant="outlined"
                    style={{marginBottom: '30px'}}
                    name="aktif"
                    value={this.state.aktif}
                    inputProps={{
                        value: this.state.aktif
                    }}
                >
                    {
                        dataVoucher.map(result => (
                            <MenuItem key={result.profileId} value={result.name}>{result.name}</MenuItem>
                        ))
                    }
                </TextField>
                <TextField
                    label="Name"
                    className={classes.textField}
                    variant="outlined"
                    fullWidth
                    style={{marginBottom: '30px'}}
                    onChange={this.handleChange}
                    name="name"
                    value={this.state.name}
                />
                <TextField
                    label="Name For Users"
                    className={classes.textField}
                    variant="outlined"
                    fullWidth
                    style={{marginBottom: '30px'}}
                    onChange={this.handleChange}
                    name="nameForUsers"
                    value={this.state.nameForUsers}
                />
                <TextField
                    label="Owner"
                    className={classes.textField}
                    variant="outlined"
                    fullWidth
                    style={{marginBottom: '30px'}}
                    disabled={true}
                    value={this.state.owner}
                />
                <TextField
                    label="Validity"
                    className={classes.textField}
                    variant="outlined"
                    fullWidth
                    style={{marginBottom: '30px'}}
                    onChange={this.handleChange}
                    name="validity"
                    value={this.state.validity}
                />
                <TextField
                    label="Price"
                    className={classes.textField}
                    variant="outlined"
                    fullWidth
                    style={{marginBottom: '30px'}}
                    type="number"
                    onChange={this.handleChange}
                    name="price"
                    value={this.state.price}
                />
                <TextField
                    select
                    name="limitName"
                    value={this.state.limitName}
                    label="Limitation"
                    helperText="Please select your limitation"
                    variant="outlined"
                    fullWidth
                    style={{marginBottom: '30px'}}
                    onChange={this.handleChange}
                >
                    {
                        dataLimit.map(result => (
                            <MenuItem key={result.limitId} value={result.name}>{result.name}</MenuItem>
                        ))
                    }
                </TextField>
                <Button variant="contained" color="primary" className={classes.button} onClick={this.updateProfile}>
                    <SaveIcon className={classNames(classes.leftIcon, classes.iconSmall)} />
                    Update
                </Button>
                <Button variant="contained" color="secondary" className={classes.button} onClick={this.deleteProfile}>
                    <DeleteIcon className={classes.rightIcon} />
                    Delete
                </Button>
                <FormAddProfile 
                    openFormAddProfile={this.state.openFormAddProfile}
                    closeFormAddProfile={this.closeFormAddProfile}
                    dataLimit={dataLimit}
                    getVoucher={this.props.getVoucher}
                />
            </div>
        );
    }
}

Profile.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default (withStyles(styles)(Profile));

import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { ListItem, Typography } from '@material-ui/core';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import List from '@material-ui/core/List';
import { withStyles } from '@material-ui/core/styles';

import AccessibleIcon from '@material-ui/icons/Accessible';
// import InsertDriveFileIcon from '@material-ui/icons/InsertDriveFile';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import AddAlarmIcon from '@material-ui/icons/AddAlarm';
// import AccessTimeIcon from '@material-ui/icons/AccessTime';
// import AccessibleForwardIcon from '@material-ui/icons/AccessibleForward';
// import AirlineSeatFlatIcon from '@material-ui/icons/AirlineSeatFlat';
import DashboardIcon from '@material-ui/icons/Dashboard';
import BorderColorIcon from '@material-ui/icons/BorderColor';
// import AccountBalanceIcon from '@material-ui/icons/AccountBalance';
// import AddLocationIcon from '@material-ui/icons/AddLocation';
// import AssignmentIndIcon from '@material-ui/icons/AssignmentInd';
// import SpeakerNotesIcon from '@material-ui/icons/SpeakerNotes';
// import ArchiveIcon from '@material-ui/icons/Archive';
import PeopleIcon from '@material-ui/icons/People';

import FormLogout from './FormLogout.jsx';

const styles = theme => ({
   nested: {
      paddingLeft: theme.spacing.unit * 4,
   },
});

class ListItem2 extends React.Component {

   state = {
      openFormLogout: false,
      open: false,
      openRegistrasi: false,
      openPelayanan: false,
      openDataMedis: false,
      openMasterData: false,
		openAdmmedis: false,
		openKeuangan: false,
		openAkuntansi: false,
		hak_menu1: [],
		hak_menu2: [],
	}
	
	componentDidMount() {
		let hak_menu1 = JSON.parse(localStorage.getItem('dataUser')).hak_menu1.split(',');
		let hak_menu2 = JSON.parse(JSON.parse(localStorage.getItem('dataUser')).hak_menu2);
		this.setState({
			hak_menu1: hak_menu1,
			hak_menu2: hak_menu2,
		});
	}

   closeFormLogout = () => {
      this.setState({ openFormLogout: false });
   };

   openFormLogout = () => {
      this.setState({ openFormLogout: true });
   };

   logout = () => {
      this.props.logout();
   }

   handleClick = () => {
      this.setState(state => ({ open: !state.open }));
   };

   handleClickRegistrasi = () => {
      this.setState(state => ({ openRegistrasi: !state.openRegistrasi }));
   };

   handleClickPelayanan = () => {
      this.setState(state => ({ openPelayanan: !state.openPelayanan }));
   };

   handleClickDataMedis = () => {
      this.setState(state => ({ openDataMedis: !state.openDataMedis }));
   };

   handleClickMasterData = () => {
      this.setState(state => ({ openMasterData: !state.openMasterData }));
   };

   handleClickAdmmedis = () => {
      this.setState(state => ({ openAdmmedis: !state.openAdmmedis }));
	};
	
	handleClickKeuangan = () => {
      this.setState(state => ({ openKeuangan: !state.openKeuangan }));
	};
	
	handleClickAkuntansi = () => {
      this.setState(state => ({ openAkuntansi: !state.openAkuntansi }));
   };

   render() {
		const { classes } = this.props;
		const items = [];
		for (let i = 0; i < this.state.hak_menu1.length; i++) {
			if (this.state.hak_menu1[i] === '1') {
				const tmp1 = [];
				for (let j = 0; j < this.state.hak_menu2.length; j++) {
					if (this.state.hak_menu2[j]['1'] !== undefined) {
						let guguk = this.state.hak_menu2[j]['1'].split(',');
						for (let k = 0; k < guguk.length; k++) {
							if (guguk[k] === '1') {
								tmp1.push(
									<ListItem button className={classes.nested} component={Link} to="/main/registrasi/dashboard" key={guguk[k]}>
										<ListItemIcon><DashboardIcon /></ListItemIcon><ListItemText inset primary={<Typography>Dashboard</Typography>} />
									</ListItem>
								);
							}
							if (guguk[k] === '2') {
								tmp1.push(
									<ListItem button className={classes.nested} component={Link} to="/main/registrasi/poliklinik" key={guguk[k]}>
										<ListItemIcon><AddAlarmIcon /></ListItemIcon><ListItemText inset primary={<Typography>Poliklinik</Typography>} />
									</ListItem>
								);
							}
							if (guguk[k] === '3') {
								tmp1.push(
									<ListItem button className={classes.nested} component={Link} to="/main/registrasi/igd" key={guguk[k]}>
										<ListItemIcon><AddAlarmIcon /></ListItemIcon><ListItemText inset primary={<Typography>IGD</Typography>} />
									</ListItem>
								);
							}
							if (guguk[k] === '4') {
								tmp1.push(
									<ListItem button className={classes.nested} component={Link} to="/main/registrasi/edit-data" key={guguk[k]}>
										<ListItemIcon><BorderColorIcon /></ListItemIcon><ListItemText inset primary={<Typography>Edit Data</Typography>} />
									</ListItem>
								);
							}
							if (guguk[k] === '5') {
								tmp1.push(
									<ListItem button className={classes.nested} component={Link} to="/main/registrasi/online" key={guguk[k]}>
										<ListItemIcon><BorderColorIcon /></ListItemIcon><ListItemText inset primary={<Typography>Pendaftaran Online</Typography>} />
									</ListItem>
								);
							}
						}
					}
				}
				items.push(
					<Fragment key={this.state.hak_menu1[i]}>
						<ListItem button onClick={this.handleClickRegistrasi}>
							<ListItemIcon><AccessibleIcon /></ListItemIcon><ListItemText primary={<Typography>Registrasi Pasien</Typography>} />
							{this.state.openRegistrasi ? <ExpandLess /> : <ExpandMore />}
						</ListItem>
						<Collapse in={this.state.openRegistrasi} timeout="auto" unmountOnExit>
							<List component="div" disablePadding>{tmp1}</List>
						</Collapse>
					</Fragment>
				);
			}
			if (this.state.hak_menu1[i] === '2') {
				const tmp2 = [];
				for (let j = 0; j < this.state.hak_menu2.length; j++) {
					if (this.state.hak_menu2[j]['2'] !== undefined) {
						let guguk = this.state.hak_menu2[j]['2'].split(',');
						for (let k = 0; k < guguk.length; k++) {
							if (guguk[k] === '1') {
								tmp2.push(
									<ListItem button className={classes.nested} component={Link} to="/main/pelayanan/poliklinik" key={guguk[k]}>
										<ListItemIcon><PeopleIcon /></ListItemIcon><ListItemText inset primary={<Typography>Poliklinik</Typography>} />
									</ListItem>
								)
							}
							if (guguk[k] === '2') {
								tmp2.push(
									<ListItem button className={classes.nested} component={Link} to="/main/pelayanan/igd" key={guguk[k]}>
										<ListItemIcon><PeopleIcon /></ListItemIcon><ListItemText inset primary={<Typography>IGD</Typography>} />
									</ListItem>
								)
							}
							if (guguk[k] === '3') {
								tmp2.push(
									<ListItem button className={classes.nested} component={Link} to="/main/pelayanan/inap" key={guguk[k]}>
										<ListItemIcon><PeopleIcon /></ListItemIcon><ListItemText inset primary={<Typography>Rawat Inap</Typography>} />
									</ListItem>
								)
							}
							if (guguk[k] === '4') {
								tmp2.push(
									<ListItem button className={classes.nested} component={Link} to="/main/pelayanan/ok" key={guguk[k]}>
										<ListItemIcon><PeopleIcon /></ListItemIcon><ListItemText inset primary={<Typography>Operation Komer</Typography>} />
									</ListItem>
								)
							}
						}
					}
				}
				items.push(
					<Fragment key={this.state.hak_menu1[i]}>
						<ListItem button onClick={this.handleClickPelayanan}>
							<ListItemIcon><ExitToAppIcon /></ListItemIcon><ListItemText primary={<Typography>Pelayanan</Typography>} />
							{this.state.openPelayanan ? <ExpandLess /> : <ExpandMore />}
						</ListItem>
						<Collapse in={this.state.openPelayanan} timeout="auto" unmountOnExit>
							<List component="div" disablePadding>{tmp2}</List>
						</Collapse>
					</Fragment>
				);
			}
			if (this.state.hak_menu1[i] === '3') {
				const tmp3 = [];
				for (let j = 0; j < this.state.hak_menu2.length; j++) {
					if (this.state.hak_menu2[j]['3'] !== undefined) {
						let guguk = this.state.hak_menu2[j]['3'].split(',');
						for (let k = 0; k < guguk.length; k++) {
							if (guguk[k] === '1') {
								tmp3.push(
									<ListItem button className={classes.nested} component={Link} to="/main/bpjs/sep-rawat-inap" key={guguk[k]}>
										<ListItemIcon><PeopleIcon /></ListItemIcon><ListItemText inset primary={<Typography>SEP Rawat Inap</Typography>} />
									</ListItem>
								);
							}
							if (guguk[k] === '2') {
								tmp3.push(
									<ListItem button className={classes.nested} component={Link} to="/main/bpjs/rujukan" key={guguk[k]}>
										<ListItemIcon><PeopleIcon /></ListItemIcon><ListItemText inset primary={<Typography>Rujukan</Typography>} />
									</ListItem>
								);
							}
							if (guguk[k] === '3') {
								tmp3.push(
									<ListItem button className={classes.nested} component={Link} to="/main/bpjs/persetujuan-sep" key={guguk[k]}>
										<ListItemIcon><PeopleIcon /></ListItemIcon><ListItemText inset primary={<Typography>Persetujuan SEP</Typography>} />
									</ListItem>
								);
							}
							if (guguk[k] === '4') {
								tmp3.push(
									<ListItem button className={classes.nested} component={Link} to="/main/bpjs/monitoring" key={guguk[k]}>
										<ListItemIcon><PeopleIcon /></ListItemIcon><ListItemText inset primary={<Typography>Monitoring</Typography>} />
									</ListItem>
								);
							}
							if (guguk[k] === '5') {
								tmp3.push(
									<ListItem button className={classes.nested} component={Link} to="/main/bpjs/aplicares" key={guguk[k]}>
										<ListItemIcon><PeopleIcon /></ListItemIcon><ListItemText inset primary={<Typography>Aplicares</Typography>} />
									</ListItem>
								);
							}
						}
					}
				}
				items.push(
					<Fragment key={this.state.hak_menu1[i]}>
						<ListItem button onClick={this.handleClickMasterData}>
							<ListItemIcon><ExitToAppIcon /></ListItemIcon>
							<ListItemText
								primary={<Typography>Bpjs</Typography>}
							/>
							{this.state.openMasterData ? <ExpandLess /> : <ExpandMore />}
						</ListItem>
						<Collapse in={this.state.openMasterData} timeout="auto" unmountOnExit>
							<List component="div" disablePadding>{tmp3}</List>
						</Collapse>
					</Fragment>
				);
			}
			if (this.state.hak_menu1[i] === '4') {
				const tmp4 = [];
				for (let j = 0; j < this.state.hak_menu2.length; j++) {
					if (this.state.hak_menu2[j]['4'] !== undefined) {
						let guguk = this.state.hak_menu2[j]['4'].split(',');
						for (let k = 0; k < guguk.length; k++) {
							if (guguk[k] === '1') {
								tmp4.push(
									<Fragment key={guguk[k]}>
										<ListItem button className={classes.nested} onClick={this.handleClickAkuntansi}>
											<ListItemIcon><PeopleIcon /></ListItemIcon>
											<ListItemText
												primary={<Typography>Akuntansi</Typography>}
											/>
											{this.state.openAkuntansi ? <ExpandLess /> : <ExpandMore />}
										</ListItem>
										<Collapse in={this.state.openAkuntansi} timeout="auto" unmountOnExit>
											<List component="div" disablePadding>
												<ListItem button className={classes.nested} component={Link} to="/main/keuangan/akuntansi/penjurum">
													<ListItemIcon><PeopleIcon /></ListItemIcon>
													<ListItemText
														primary={<Typography>Penc. Jurnal Umum</Typography>}
													/>
												</ListItem>
												<ListItem button className={classes.nested} component={Link} to="/main/keuangan/akuntansi/pipenas">
													<ListItemIcon><PeopleIcon /></ListItemIcon>
													<ListItemText
														primary={<Typography>Piutang & Penagihan Asuransi</Typography>}
													/>
												</ListItem>
												<ListItem button className={classes.nested} component={Link} to="/main/keuangan/akuntansi/pidake">
													<ListItemIcon><PeopleIcon /></ListItemIcon>
													<ListItemText
														primary={<Typography>Piutang Bpjs & Data Keuangan</Typography>}
													/>
												</ListItem>
												<ListItem button className={classes.nested} component={Link} to="/main/keuangan/akuntansi/hutang">
													<ListItemIcon><PeopleIcon /></ListItemIcon>
													<ListItemText
														primary={<Typography>Hutang</Typography>}
													/>
												</ListItem>
												<ListItem button className={classes.nested} component={Link} to="/main/keuangan/akuntansi/kasir">
													<ListItemIcon><PeopleIcon /></ListItemIcon>
													<ListItemText
														primary={<Typography>Kasir</Typography>}
													/>
												</ListItem>
											</List>
										</Collapse>
									</Fragment>
								);
							}
						}
					}
				}
				items.push(
					<Fragment key={this.state.hak_menu1[i]}>
						<ListItem button onClick={this.handleClickKeuangan}>
							<ListItemIcon><ExitToAppIcon /></ListItemIcon>
							<ListItemText
								primary={<Typography>Keuangan</Typography>}
							/>
							{this.state.openKeuangan ? <ExpandLess /> : <ExpandMore />}
						</ListItem>
						<Collapse in={this.state.openKeuangan} timeout="auto" unmountOnExit>
							<List component="div" disablePadding>{tmp4}</List>
						</Collapse>
					</Fragment>
				);
			}
			if (this.state.hak_menu1[i] === '5') {
				const tmp5 = [];
				for (let j = 0; j < this.state.hak_menu2.length; j++) {
					if (this.state.hak_menu2[j]['5'] !== undefined) {
						let guguk = this.state.hak_menu2[j]['5'].split(',');
						for (let k = 0; k < guguk.length; k++) {
							if (guguk[k] === '1') {
								tmp5.push(
									<ListItem button className={classes.nested} component={Link} to="/main/admmedis/grouper-jalan" key={guguk[k]}>
										<ListItemIcon><PeopleIcon /></ListItemIcon>
										<ListItemText
											primary={<Typography>Grouper Jalan</Typography>}
										/>
									</ListItem>
								);
							}
						}
					}
				}
				items.push(
					<Fragment key={this.state.hak_menu1[i]}>
						<ListItem button onClick={this.handleClickAdmmedis}>
							<ListItemIcon><ExitToAppIcon /></ListItemIcon>
							<ListItemText
								primary={<Typography>Adm. Medis</Typography>}
							/>
							{this.state.openAdmmedisGrouper ? <ExpandLess /> : <ExpandMore />}
						</ListItem>
						<Collapse in={this.state.openAdmmedis} timeout="auto" unmountOnExit>
							<List component="div" disablePadding>{tmp5}</List>
						</Collapse>
					</Fragment>
				);
			}
		}
      return (
         <div>
            {items}

				<ListItem button onClick={this.openFormLogout}>
               <ListItemIcon><ExitToAppIcon /></ListItemIcon>
					<ListItemText
						primary={<Typography>Logout</Typography>}
					/>
            </ListItem>
            <FormLogout
               openFormLogout={this.state.openFormLogout}
               closeFormLogout={this.closeFormLogout}
               logout={this.logout}
            />

         </div>
      );

   }

}

const mapStateToProps = (state) => {
   return {
      ...state,
   }
}

ListItem2.propTypes = {
   classes: PropTypes.object.isRequired,
};

export default connect(mapStateToProps, null)(withStyles(styles)(ListItem2));

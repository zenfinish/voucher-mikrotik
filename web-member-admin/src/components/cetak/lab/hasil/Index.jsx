import React from 'react';
import KopSurat from 'components/KopSurat.jsx';
import { Dialog, DialogContent, DialogActions } from '@material-ui/core';
import ReactToPrint from 'react-to-print';
import Button from 'components/inputs/Button.jsx';
import api from 'config/api.js';
import { tglIndo } from 'config/helpers.js';

class Index extends React.Component {

  state = {
    data: {
      nama_pelpasien: '',
      no_rekmedis: '',
      tgl_lahir: '',
      umur: '',
      jkel: '',
      tipe: '',
      id_labtransaksi: '',
      nama_dokter: '',
      tgl: '',
      tgl_keluar: '',
      data: [],
    }
  }
  
  componentDidMount() {
    // console.log("masuk===", this.props.data)
    api.get(`/lab/${this.props.data.tipe === 1 ? 'jalan' : this.props.data.tipe === 2 ? 'inap': ''}/hasil/single/${this.props.data.id_labtransaksi}`)
		.then(result => {
      // console.log(result.data)
      this.setState({ data: result.data });
		})
		.catch(error => {
			console.log(error.response);
		});
  }
  
  render() {
		return (
			<Dialog open maxWidth="xl" fullWidth scroll="paper">
        <DialogContent ref={print => (this.componentRef = print)}>
          <KopSurat />
          <div className="text-sm flex justify-between mb-2">
            <div className="border border-black px-4 py-2"><b>Penanggung Jawab Laboratorium :<br />dr. Andy Arfan, Sp.PK</b></div>
            <div className="border border-black px-4 py-2"><b>RM.1.16.2/LAB/2016</b></div>
          </div>
          <table className="mb-2 text-sm w-full">
            <tbody>
              <tr>
                <td colSpan='6' align='center'><b>HASIL PEMERIKSAAN LABORATORIUM PATOLOGI KLINIK PER TRANSAKSI</b></td>
              </tr>
              <tr>
                <td>Nama Pasien</td>
                <td>:</td>
                <td>{this.state.data.nama_pelpasien}</td>
                <td>No. Rekmedis</td>
                <td>:</td>
                <td>{this.state.data.no_rekmedis}</td>
              </tr>
              <tr>
                <td>Tgl Lahir</td>
                <td>:</td>
                <td>{tglIndo(this.state.data.tgl_lahir)}</td>
                <td>Umur</td>
                <td>:</td>
                <td>{this.state.data.umur} Tahun</td>
              </tr>
              <tr>
                <td>Jenis Kelamin</td>
                <td>:</td>
                <td>{this.state.data.jkel === 'L' ? 'Laki - Laki' : this.state.data.jkel === 'P' ? 'Perempuan' : ''}</td>
                <td>Tipe Pelayanan</td>
                <td>:</td>
                <td>{this.state.data.tipe === 1 ? 'Rawat Jalan' : this.state.data.tipe === 2 ? 'Rawat Inap' : ''}</td>
              </tr>
              <tr>
                <td>No. Reg. Lab</td>
                <td>:</td>
                <td>{this.state.data.id_labtransaksi}</td>
                <td>Dokter Pengirim</td>
                <td>:</td>
                <td>{this.state.data.nama_dokter}</td>
              </tr>
              <tr>
                <td>Waktu Sample Datang</td>
                <td>:</td>
                <td>{tglIndo(this.state.data.tgl)}</td>
                <td>Waktu Hasil Keluar</td>
                <td>:</td>
                <td>{tglIndo(this.state.data.tgl_keluar)}</td>
              </tr>
            </tbody>
          </table>
          <table cellPadding="2" className="mb-2 w-full text-sm">
            {
              this.state.data.data.map((row2, j) => (
                <tbody key={j}>
                  <tr>
                    <td align='center' className="border border-black" colSpan='6'><b>{row2.nama_labjenis}</b></td>
                  </tr>
                  <tr>
                    <td align='center' className="border border-black"><b>No</b></td>
                    <td align='center' className="border border-black"><b>Pemeriksaan</b></td>
                    <td align='center' className="border border-black"><b>Hasil</b></td>
                    <td align='center' className="border border-black"><b>Normal</b></td>
                    <td align='center' className="border border-black"><b>Satuan</b></td>
                    <td align='center' className="border border-black"><b>Keterangan</b></td>
                  </tr>
                  {
                    row2.data.map((row3, k) => (
                      <tr key={k} className={JSON.stringify(row3.kritis) === '1' ? 'bg-red-300' : ''}>
                        <td align='center' className="border border-black">{k+1}</td>
                        <td className="border border-black">{row3.nama_labpaketdet}</td>
                        <td className="border border-black" align='center'>{row3.hasil}</td>
                        <td className="border border-black" align='center'>{row3.normal}</td>
                        <td className="border border-black" align='center'>{row3.satuan}</td>
                        <td className="border border-black" align='center'></td>
                      </tr>
                    ))
                  }
                </tbody>
              ))
            }
          </table>
          <table className="w-full text-sm">
            <tbody>
              <tr>
                <td align="center">Pemeriksa<br /><br /><br /></td>
                <td align="center">Dokter Konsultan<br /><br /><br /></td>
              </tr>
              <tr>
                <td align="center">{this.state.data.nama_peluser}</td>
                <td align="center">(dr. Andy Arfan, Sp.PK)</td>
              </tr>
            </tbody>
          </table>
        </DialogContent>
        <DialogActions>
          <ReactToPrint
						trigger={() => (
							<Button>Print</Button>
						)}
						content={() => this.componentRef}
					/>
          <Button onClick={this.props.close}>Close</Button>
        </DialogActions>
      </Dialog>
		)
	}

}

export default Index;

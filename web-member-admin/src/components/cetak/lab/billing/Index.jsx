import React from 'react';
import { Dialog, DialogContent, DialogActions } from '@material-ui/core';
import ReactToPrint from 'react-to-print';
import Button from 'components/inputs/Button.jsx';
import KopSurat from 'components/KopSurat.jsx';
import api from 'config/api.js';
import { tglIndo, separator } from 'config/helpers.js';

class Index extends React.Component {

	state = {
		data: {
			nama_pelpasien: '',
			no_rekmedis: '',
			tgl_lahir: '',
			nama_pelcabar: '',
			jkel: '',
			tipe: '',
			tgl: '',
			id_labtransaksi: '',
			diagnosa: null,
			alamat: null,
			perinci: null,
			data: [],
		}
	}
  
	componentDidMount() {
		api.get(`/lab/${this.props.data.tipe === 1 ? 'jalan' : this.props.data.tipe === 2 ? 'inap': ''}/single/${this.props.data.id_labtransaksi}`)
			.then(result => {
		this.setState({ data: result.data });
			})
			.catch(error => {
				console.log(error.response);
			});
	}
  
  render() {
    let total = 0;
    return (
			<Dialog open maxWidth="xl" fullWidth scroll="paper">
        <DialogContent ref={print => (this.componentRef = print)}>
          <KopSurat />
          <h3 align="center" className="mb-2">KWITANSI LABORATORIUM PER TRANSAKSI</h3>
          <table className="w-full text-sm mb-2">
            <tbody>
              <tr>
                <td><b>Nama Pasien</b></td>
                <td><b>:</b></td>
                <td>{this.state.data.nama_pelpasien}</td>
                <td><b>No. Rekmedis</b></td>
                <td><b>:</b></td>
                <td>{this.state.data.no_rekmedis}</td>
              </tr>
              <tr>
                <td><b>Tgl Lahir</b></td>
                <td><b>:</b></td>
                <td>{tglIndo(this.state.data.tgl_lahir)}</td>
                <td><b>Cara Bayar</b></td>
                <td><b>:</b></td>
                <td>{this.state.data.nama_pelcabar}</td>
              </tr>
              <tr>
                <td><b>Jenis Kelamin</b></td>
                <td><b>:</b></td>
                <td>{this.state.data.jkel === 'L' ? 'Laki - Laki' : this.state.data.jkel === 'P' ? 'Perempuan' : '-'}</td>
                <td><b>Tipe Pelayanan</b></td>
                <td><b>:</b></td>
                <td>{this.state.data.tipe === 1 ? 'Rawat Jalan' : this.state.data.tipe === 2 ? 'Rawat Inap': ''}</td>
              </tr>
              <tr>
                <td><b>Tanggal</b></td>
                <td><b>:</b></td>
                <td>{tglIndo(this.state.data.tgl)}</td>
                <td><b>No. Transaksi</b></td>
                <td><b>:</b></td>
                <td>{this.state.data.id_labtransaksi}</td>
              </tr>
              <tr>
                <td><b>Diagnosa</b></td>
                <td><b>:</b></td>
                <td>{this.state.data.diagnosa}</td>
                <td><b>Alamat</b></td>
                <td><b>:</b></td>
                <td>{this.state.data.alamat}</td>
              </tr>
            </tbody>
          </table>
          <table className="text-sm w-full mb-2">
            <tbody>
              <tr>
                <td className="border border-black"><b>No</b></td>
                <td className="border border-black"><b>Pemeriksaan</b></td>
                <td className="border border-black" align="right"><b>Jumlah</b></td>
              </tr>
              {
                this.state.data.data.map((row, i) => {
                  total += row.tarif;
                  return (
                    <tr key={i}>
                      <td className="border border-black">{i+1}</td>
                      <td className="border border-black">{row.nama_labpaket}</td>
                      <td className="border border-black" align="right">{separator(row.tarif)}</td>
                    </tr>
                  )
                })
              }
            </tbody>
          </table>
          <div className="flex justify-between">
            <div className="text-sm">Perinci,<br /><br /><br />{this.state.data.perinci}</div>
            <table className="text-sm">
              <tbody>
                <tr>
                  <td><b>Total Tagihan</b></td>
                  <td><b>:</b></td>
                  <td align="right">{separator(total)}</td>
                </tr>
                <tr>
                  <td><b>Dijamin</b></td>
                  <td><b>:</b></td>
                  <td align="right"> - </td>
                </tr>
                <tr>
                  <td><b>Keringanan & Diskon</b></td>
                  <td><b>:</b></td>
                  <td align="right"> - </td>
                </tr>
                <tr>
                  <td><b>Harus Bayar</b></td>
                  <td><b>:</b></td>
                  <td align="right"><b>Rp. {separator(total)}</b></td>
                </tr>
              </tbody>
            </table>
          </div>
        </DialogContent>
        <DialogActions>
          <ReactToPrint
						trigger={() => (
							<Button>Print</Button>
						)}
						content={() => this.componentRef}
					/>
          <Button onClick={this.props.close}>Close</Button>
        </DialogActions>
			</Dialog>
		)
	}

}

export default Index;

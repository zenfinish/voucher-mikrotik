import React from 'react';
import { Dialog, DialogContent, DialogActions } from '@material-ui/core';
import ReactToPrint from 'react-to-print';
import QRCode from 'qrcode.react';
import _ from "lodash";
import Button from 'components/inputs/Button.jsx';
import api from 'config/api.js';
import KopSurat from 'components/KopSurat.jsx';
import { tglIndo } from 'config/helpers.js';

class Resep extends React.Component {

  state = {
    doctor_name: '',
		data: {
			date: '',
			prescription_request_id: '',
			doctor_name: '',
			sip: "",
			unit: '',
			patient_name: '',
			mr: '',
			age: '',
			birth_date: '',
			address: '',
			hp: '',
			details: [],
		},
  }
  
	componentDidMount() {
		const { id_farpermintaan } = this.props.data;
		this.setState({ loading: true }, () => {
			api.get(`/prescriptions/request/${id_farpermintaan}?include[details]=true`)
			.then(result => {
				const dataDiracik = [];
				const dataTidakDiracik = [];
				result.data.details.map(row => !row.id_farracikan ? dataTidakDiracik.push(row) : dataDiracik.push(row));
				const dataDiracik2 = _(dataDiracik)
				.groupBy('id_farracikan')
				.map((items, data) => {
					return {
						...items[0],
						children: _.map(items)
					};
				}).value();
				const data = [ ...dataTidakDiracik, ...dataDiracik2 ];
				this.setState({ data: { ...result.data, details: data }, loading: false });
			})
			.catch(error => {
				console.log(error.response);
			});
		});
	}
  
	render() {
		const { data } = this.state;
		return (
			<>
				<Dialog open fullScreen scroll="paper">
					<DialogContent ref={print => (this.componentRef = print)}>
						<div className="flex relative">
							<div className="w-full">
								<KopSurat />
								<div className="text-center text-lg font-bold mb-2"><u>R E S E P</u></div>

								<div className="text-center leading-none">
									<span className="text-base font-bold">{this.state.data.doctor_name}</span><br />
									<span className="text-xs font-bold">SIP. {this.state.data.sip}</span>
								</div>
								<div className="text-center text-sm">{this.state.data.unit}</div>

								<table className="w-full">
									<tbody>
										<tr>
											<td>Tanggal</td>
											<td>:</td>
											<td>{tglIndo(this.state.data.date)}</td>
											<td colSpan={3}>
												<h3>No. Antrian: {this.props.data.angkaantrean}</h3>
											</td>
										</tr>
										<tr>
											<td>No. Resep</td>
											<td>:</td>
											<td>{this.state.data.prescription_request_id}</td>
										</tr>
									</tbody>
								</table>

								<table className="text-sm mb-2 w-full">
									<tbody>
										<tr className="border border-black">
											<td align='center'><b>R/</b></td>
											<td><b>Nama Obat</b></td>
											<td align='center'><b>Satuan</b></td>
											<td align='center'><b>Qty</b></td>
										</tr>
										{
											data.details.map((row, i) => (
												<React.Fragment key={i}>
													{
                                                        row.children ?
															<>
																<tr className="bg-gray-200">
																	<th>R/</th>
																	<th colSpan={2} align="left">Racikan. {row.nama_farpembungkus} - {row.cara_pakai_racikan}</th>
																	<th>{row.jml_bungkus}</th>
																</tr>
																{
																	row.children.map((row2, j) => (
																		<tr key={j} className="border border-black">
																			<td align='center'></td>
																			<td>{row2.drug_name}</td>
																			<td align="center">{row2.piece}</td>
																			<td align='center'>{row2.qty}</td>
																		</tr>
																	))
																}
																<tr className="bg-gray-200"><th colSpan={4} align="left"><span className="text-gray-200">R/</span></th></tr>
															</>
														: 
															<tr className="border border-black">
																<td align='center'>R/</td>
																<td>
																	{row.drug_name}
																	{row.how_to_use !== "" ? <><br/><span className="text-xs">Cara Pakai: {row.how_to_use}</span></> : null}
																	{row.note !== "" ? <><br/><span className="text-xs">Keterangan: {row.note}</span></> : null}
																</td>
																<td align="center">{row.piece}</td>
																<td align='center'>{row.qty}</td>
															</tr>
													}
												</React.Fragment>
											))
										}
									</tbody>
								</table>

								<div className="flex justify-between">
									<table>
										<tbody>
											<tr>
												<td>Nama Pasien</td>
												<td>:</td>
												<td><b>{data.patient_name} (No.MR: {data.mr})</b></td>
											</tr>
											<tr>
												<td>Umur</td>
												<td>:</td>
												<td>{data.age} Tahun ({tglIndo(data.birth_date)})</td>
											</tr>
											<tr>
												<td>Alamat</td>
												<td>:</td>
												<td>{data.address} (Hp: {data.hp})</td>
											</tr>
											<tr>
												<td>Cara Bayar</td>
												<td>:</td>
												<td>{data.payment_method_name}</td>
											</tr>
										</tbody>
									</table>
									<div>
										<QRCode
											value={JSON.stringify(data.doctor_name)}
										/>
									</div>
								</div>

							</div>
						</div>
					</DialogContent>
					<DialogActions>
						<ReactToPrint
							trigger={() => (
								<Button>Print</Button>
							)}
							content={() => this.componentRef}
						/>
						<Button onClick={this.props.close}>Close</Button>
					</DialogActions>
				</Dialog>
			</>
		);
	}

}

export default Resep;

import React from 'react';
import { Dialog, DialogContent, DialogActions } from '@material-ui/core';
import ReactToPrint from 'react-to-print';
import Button from 'components/inputs/Button.jsx';
import KopSurat from '../../../components/KopSurat.jsx';
import api from 'config/api.js';
import { tglIndo, separator } from 'config/helpers.js';
import Loading from 'components/Loading.jsx';
import angkaTerbilang from '@develoka/angka-terbilang-js';
import Pdf from "react-to-pdf";

class Pesanan extends React.Component {

  state = {
    data: {
      nama_fardistributor: '',
      no_po: '',
      tgl_po: '',
      telp: '',
      data: [],
    },
    loading: false,
  }
  
  componentDidMount() {
    this.setState({ loading: true }, () => {
      api.get(`/farmasi/pesanan/detail/`, {
        headers: { no_po: this.props.no_po }
      })
      .then(result => {
        this.setState({ data: result.data, loading: false });
      })
      .catch(error => {
        console.log(error.response);
      });
    });
  }
  
  render() {
    let total = 0;
    let jumlah = 0;
    return (
			<Dialog open maxWidth="xl" fullWidth scroll="paper">
        <DialogContent ref={print => (this.componentRef = print)}>
          <KopSurat />
          <div className="flex relative">
            <div className="w-full">
              <table
                cellPadding={3}
                cellSpacing={0}
                className="w-full text-sm mb-2"
              >
                <tbody>
                  <tr>
                    <td colSpan={6}>SURAT PESANAN</td>
                  </tr>
                  <tr>
                    <td>Kepada</td>
                    <td>:</td>
                    <td>{this.state.data.nama_fardistributor}</td>
                    <td>No. Po</td>
                    <td>:</td>
                    <td>{this.state.data.no_po}</td>
                  </tr>
                  <tr>
                    <td>No. Telp</td>
                    <td>:</td>
                    <td>{this.state.data.telp}</td>
                    <td>Tanggal</td>
                    <td>:</td>
                    <td>{tglIndo(this.state.data.tgl_po)}</td>
                  </tr>
                </tbody>
              </table>

              <div
                className="text-sm text-gray-600"
              >PENTING : Syarat dan Kondisi Pesanan seperti tercantum di Halaman Belakang.</div>
              
              <table cellPadding={3} cellSpacing={0} border={1}
                className="w-full text-sm"
              >
                <tbody>
                  <tr>
                    <td className="border border-black" align="center">No</td>
                    <td className="border border-black" align="center">ID Obat</td>
                    <td className="border border-black">Nama Barang</td>
                    <td className="border border-black" align="center">Jumlah</td>
                    <td className="border border-black" align="center">Satuan</td>
                    <td className="border border-black" align="right">Harga (Rp)</td>
                    <td className="border border-black" align="center">Diskon (%)</td>
                    <td className="border border-black" align="right">Jumlah Harga (Rp)</td>
                    <td className="border border-black">Keterangan</td>
                  </tr>
                  {this.state.data.data.map((row, i) => {
                    jumlah = (row.harga - ((row.harga * row.diskon) / 100)) * row.jml;
                    total += jumlah;
                    return (
                      <tr key={i}>
                        <td className="border border-black" align="center">{i+1}</td>
                        <td className="border border-black" align="center">{row.id_farobat}</td>
                        <td className="border border-black">{row.nama_farobat}</td>
                        <td className="border border-black" align="center">{row.jml}</td>
                        <td className="border border-black" align="center">{row.nama_farsatuan}</td>
                        <td className="border border-black" align="right">{separator(row.harga, 0)}</td>
                        <td className="border border-black" align="center">{row.diskon}</td>
                        <td className="border border-black" align="right">{separator(jumlah, 0)}</td>
                        <td className="border border-black">{row.keterangan}</td>
                      </tr>
                    );
                  })}
                  <tr>
                    <td colSpan={7} align="right">TOTAL</td>
                    <td align="right">Rp. {separator(total, 0)}</td>
                    <td></td>
                  </tr>
                </tbody>
              </table>

              <div className="text-sm text-gray-600 border-b border-black">
                Terbilang: {angkaTerbilang(total)} rupiah.
              </div>
              <div className="text-sm text-gray-600 mb-2">
                Kami menyetujui syarat dan kondisi pesanan seperti yang telah tercantum / ditentukan.
              </div>
              
              <table
                className="w-full text-sm"
              >
                <tbody>
                  <tr>
                    <td align="center">Direktur<br /><br /><br /><br /></td>
                    <td align="center" valign="top">Apoteker Penanggung Jawab</td>
                  </tr>
                  <tr>
                    <td align="center">dr. Rudy Harmanda, MPH</td>
                    <td align="center">apt. Aulan Nisa, S.Farm<br />NO. 19960411/SIPA.1171/2021/2.446.50.1</td>
                  </tr>
                </tbody>
              </table>
            </div>
            {this.state.loading ? <Loading /> : null}
          </div>
        </DialogContent>
        <DialogActions>
          <ReactToPrint
						trigger={() => (
							<Button>Print</Button>
						)}
						content={() => this.componentRef}
					/>
          <Pdf
            targetRef={this.componentRef}
            filename="Surat-Pesanan.pdf"
            options={{
              orientation: 'landscape',
              unit: 'mm',
              format: [215, 330],
            }}
          >
            {({ toPdf }) => <Button onClick={toPdf}>Print Pdf</Button>}
          </Pdf>
          {/* <Button onClick={this.props.close}>Excel</Button> */}
          <Button onClick={this.props.close}>Close</Button>
        </DialogActions>
			</Dialog>
		)
	}

}

export default Pesanan;

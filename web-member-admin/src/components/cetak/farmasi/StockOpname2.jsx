import React from 'react';
// import ReactToPrint from 'react-to-print';
import api from 'config/api.js';
import HistoryStok from './HistoryStok.jsx';
import { separator, tglIndo } from 'config/helpers.js';
import { notification, Modal, Table, Tag, Input, Button, Space, Popover } from 'antd';
import SearchOutlined from '@ant-design/icons/lib/icons/SearchOutlined';
import { CSVLink } from "react-csv";

class StockOpname extends React.Component {

  state = {
    loading: false,
    data: [],
    aktif:{},
    openHistoryStok: false,
    openPrint: false,

    searchText: '',
    searchedColumn: '',
  }
  
  componentDidMount() {
    this.fetchData(this.props.id_farso);
  }

  fetchData = (id_farso) => {
    this.setState({ loading: true }, () => {
      api.get(`/farmasi/so/detail/${id_farso}`)
      .then(result => {
        this.setState({ data: result.data, loading: false });
      })
      .catch(error => {
        console.log(error.response);
        this.setState({ loading: false });
        notification.error({
          message: 'Error',
          description: JSON.stringify(error.response.data),
        });
      });
    });
  }

  getColumnSearchProps = dataIndex => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={node => {
            this.searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
          style={{ width: 188, marginBottom: 8, display: 'block' }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => {
              this.handleSearch(selectedKeys, confirm, dataIndex);
            }}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >Search</Button>
          <Button onClick={() => this.handleReset(clearFilters)} size="small" style={{ width: 90 }}>
            Reset
          </Button>
          <Button
            type="link"
            size="small"
            onClick={() => {
              confirm({ closeDropdown: false });
              this.setState({
                searchText: selectedKeys[0],
                searchedColumn: dataIndex,
              });
            }}
          >
            Filter
          </Button>
        </Space>
      </div>
    ),
    filterIcon: filtered => <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />,
    onFilter: (value, record) =>
      record[dataIndex]
        ? record[dataIndex].toString().toLowerCase().includes(value.toLowerCase())
        : '',
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => this.searchInput.select(), 100);
      }
    },
    render: text => 
      dataIndex === "id_farfakturdetlama" ?
        ( <Tag color="volcano">{text}</Tag> )
      : ( text ),
  });

  handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    this.setState({
      searchText: selectedKeys[0],
      searchedColumn: dataIndex,
    });
  };

  handleReset = clearFilters => {
    clearFilters();
    this.setState({ searchText: '' });
  };
  
  render() {
    // let total_rp = 0;
    const { openPrint } = this.state;
    return (
			<>
        <Modal
          visible={true}
          centered
          onCancel={this.props.close}
          title="Hasil Stock Opname"
          width={5000}
          footer={false}
        >
          {/* <DialogContent>
             */}
              {/* <table cellPadding={3} cellSpacing={0} border={1}
                className="w-full mb-2 text-sm overflow-auto whitespace-nowrap"
              >
                <tbody>
                  <tr>
                  </tr>
                  {this.state.data.map((row, i) => {
                    total_rp = total_rp + rp;
                    return (
                      <tr key={i}>
                      </tr>
                    );
                  })}
                  <tr>
                    <td className="border border-black" align="right" colSpan={20}>Total Rp</td>
                    <td className="border border-black" align="right"><b>{separator(total_rp)}</b></td>
                  </tr>
                </tbody>
              </table> */}
          <div ref={print => (this.componentRef = print)}>
            <Table
              size="small"
              loading={this.state.loading}
              dataSource={this.state.data}
              bordered
              scroll={openPrint ? false : { x: 1500, y: 600 }}
              pagination={openPrint ? false : true}
              rowKey={(row) => row.id}
              title={() => (
                <>
                  {/* <ReactToPrint
                    trigger={() => (
                      <Button>Print</Button>
                      )}
                      content={() => this.componentRef}
                      onBeforeGetContent={() => {
                        this.setState({ openPrint: true });
                      }}
                      onAfterPrint={() => {
                        this.setState({ openPrint: false });
                      }}
                      key="print"
                      /> */}
                  <CSVLink
                    data={this.state.data.map((row) => {
                      return {
                        'No. Stok': row.id_farfakturdet,
                        'Tgl. Faktur': row.tgl_faktur,
                        'ID Obat': row.id_farobat,
                        'Jenis': row.nama_farjenis,
                        'Nama Obat': row.nama_farobat,
                        'Harga Satuan': row.har_satuan,
                        'Stok Awal': row.jml,
                        'Stok Sekarang': row.stok,
                        'Jml Pemakaian': Number(row.jml) - Number(row.stok),
                        'Selisih': row.hilang,
                        'Rp': row.stok * row.har_satuan,
                      };
                    })}
                    // className="border border-black"
                  >Download CSV</CSVLink>
                </>
              )}
              columns={[
                {
                  title: 'No. Stok',
                  dataIndex: 'id_farfakturdet',
                  align: 'center',
                  render: (text, record, i) => <Tag color="volcano">{text}</Tag>,
                  fixed: 'left',
                  ...this.getColumnSearchProps('id_farfakturdet'),
                  width: 100,
                },
                {
                  title: 'Tgl Faktur',
                  dataIndex: 'tgl_faktur',
                  align: 'right',
                  width: 150,
                  fixed: 'left',
                  render: (text, record, i) => tglIndo(text),
                },
                {
                  title: 'ID Obat',
                  dataIndex: 'id_farobat',
                  align: 'center',
                  width: 100,
                  fixed: 'left',
                },
                {
                  title: 'Jenis',
                  dataIndex: 'nama_farjenis',
                  align: 'center',
                  width: 100,
                  fixed: 'left',
                },
                {
                  title: 'Nama Obat',
                  key: 'nama_farobat',
                  dataIndex: 'nama_farobat',
                  fixed: 'left',
                  ...this.getColumnSearchProps('nama_farobat'),
                  render: (text, record, i) => (
                    <>
                      <Popover
                        content={(
                          <table>
                            <tbody>
                              <tr>
                                <td>ID Obat</td>
                                <td>:</td>
                                <td align="right">{record.id_farobat}</td>
                              </tr>
                              <tr>
                                <td>Harga Faktur</td>
                                <td>:</td>
                                <td align="right">{separator(record.har_faktur)}</td>
                              </tr>
                              <tr>
                                <td>Harga Satuan</td>
                                <td>:</td>
                                <td align="right">{separator(record.har_satuan)}</td>
                              </tr>
                              <tr>
                                <td>Diskon</td>
                                <td>:</td>
                                <td align="right">{record.diskon}%</td>
                              </tr>
                              <tr>
                                <td>PPN</td>
                                <td>:</td>
                                <td align="right">{record.ppn}%</td>
                              </tr>
                              <tr>
                                <td>Margin</td>
                                <td>:</td>
                                <td align="right">{record.margin}%</td>
                              </tr>
                              <tr>
                                <td>Tgl. Kadaluarsa</td>
                                <td>:</td>
                                <td align="right">{tglIndo(record.tgl_kadaluarsa)}</td>
                              </tr>
                              <tr>
                                <td>Batch</td>
                                <td>:</td>
                                <td align="right">{record.batch}</td>
                              </tr>
                            </tbody>
                          </table>
                        )}
                        title={text}
                      >{text}</Popover>
                    </>
                  )
                },
                { title: 'Harga Satuan', key: 'har_satuan', render: (text, record, i) => <>{separator(record.har_satuan)}</>, align: 'right' },
                {
                  title: 'Stok Awal',
                  dataIndex: 'jml',
                  align: 'center',
                  width: 100,
                },
                {
                  title: 'Stok Sekarang',
                  align: 'center',
                  render: (text, row, i) => <Tag color="default" className="cursor-pointer"
                    // onClick={() => {
                    //   this.setState({
                    //     aktif: row,
                    //     openHistoryStok: true,
                    //   });
                    // }}
                  >{row.stok}</Tag>,
                  width: 100,
                },
                {
                  title: 'Jml Pemakaian',
                  render: (text, row, i) => Number(row.jml) - Number(row.stok),
                  align: 'right',
                  width: 100,
                },
                {
                  title: 'Selisih',
                  align: 'center',
                  render: (text, row, i) => row.hilang ?
                  <Tag color="error" className="cursor-pointer"
                    // onClick={() => {
                    //   this.setState({
                    //     aktif: row,
                    //     openHistoryStok: true,
                    //   });
                    // }}
                  >{row.hilang}</Tag> : null,
                  width: 100,
                },
                { title: 'Rp', key: 'rp', render: (text, record, i) => <>{separator(record.stok * record.har_satuan)}</>, align: 'right' },
              ]}
            />
          </div>
        </Modal>
        {
          this.state.openHistoryStok ?
            <HistoryStok
              data={{
                id_farfakturdet: this.state.aktif.id_farfakturdetlama,
                id_farobat: this.state.aktif.id_farobat,
                nama_farobat: this.state.aktif.nama_farobat,
              }}
              close={() => {
                this.setState({ openHistoryStok: false });
              }}
            />
          : null
        }
      </>
		)
	}

}

export default StockOpname;

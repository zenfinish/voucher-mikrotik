import React from 'react';
import { Dialog, DialogContent, DialogActions } from '@material-ui/core';
import ReactToPrint from 'react-to-print';
import Button from 'components/inputs/Button.jsx';
import KopSurat from 'components/KopSurat.jsx';
import api from 'config/api.js';
import { tglIndo } from 'config/helpers.js';
import { saveAs } from 'file-saver';
import Loading from 'components/Loading.jsx';

import CetakNoStok from '../../../components/farmasi/CetakNoStok.jsx';

class DistribusiDepo extends React.Component {

  state = {
    data: {
      id_fardistribusitransaksi: '',
      tgl: '',
      nama_fardepo: '',
      petugas_gudang: '',
      data: [],
    },
    loading: false,
    openCetakNoStok: false,
  }
  
  componentDidMount() {
    this.setState({ loading: true }, () => {
      api.get(`/farmasi/distribusi/detail/`, {
				headers: { id_fardistribusitransaksi: this.props.id_fardistribusitransaksi }
			})
      .then(result => {
        this.setState({ data: result.data, loading: false });
      })
      .catch(error => {
        console.log(error.response);
      });
    });
  }
  
  render() {
    return (
			<>
        <Dialog open maxWidth="xl" fullWidth scroll="paper">
          <DialogContent ref={print => (this.componentRef = print)}>
            <KopSurat />
            <div className="flex relative">
              <div className="w-full">
                <h3>Detail Distribusi Depo</h3>
                <table cellPadding={3} className="w-full mb-2 text-sm">
                  <tbody>
                    <tr>
                      <td>No. Transaksi</td>
                      <td>:</td>
                      <td>{this.state.data.id_fardistribusitransaksi}</td>
                      <td>Tgl Transaksi</td>
                      <td>:</td>
                      <td>{tglIndo(this.state.data.tgl)}</td>
                    </tr>
                    <tr>
                      <td>Nama Depo</td>
                      <td>:</td>
                      <td>{this.state.data.nama_fardepo}</td>
                      <td>Petugas Gudang</td>
                      <td>:</td>
                      <td>{this.state.data.petugas_gudang}</td>
                    </tr>
                  </tbody>
                </table>
                <table cellPadding={3} cellSpacing={0} border={1}
                  className="w-full mb-2 text-sm"
                >
                  <tbody>
                    <tr>
                      <td className="border border-black" align="center">No</td>
                      <td className="border border-black" align="center">No. Stok</td>
                      <td className="border border-black">Nama Obat</td>
                      <td className="border border-black" align="right">Jml</td>
                    </tr>
                    {this.state.data.data.map((row, i) => {
                      return (
                        <tr key={i}>
                          <td className="border border-black" align="center">{i+1}</td>
                          <td className="border border-black" align="center">{row.id_farfakturdet}</td>
                          <td className="border border-black">{row.nama_farobat} [{row.id_farobat}]</td>
                          <td className="border border-black" align="right">{row.jml} {row.nama_farsatuan}</td>
                        </tr>
                      );
                    })}
                  </tbody>
                </table>
                <table className="w-full text-sm">
                  <tbody>
                    <tr>
                      <td valign="top">Penerima<br /><br /><br /><br /></td>
                    </tr>
                    <tr>
                      <td>{this.state.data.nama_karyawan}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
              {
                this.state.loading ?
                  <Loading />
                : null
              }
            </div>
          </DialogContent>
          <DialogActions>
            <Button
              onClick={() => {
                this.setState({ openCetakNoStok: true });
              }}
            >Cetak No. Stok</Button>
            <ReactToPrint
              trigger={() => (
                <Button>Print</Button>
              )}
              content={() => this.componentRef}
            />
            <Button color="primary"
              onClick={() => {
                api.post(`/pdf/pesanan/`, {
                  data: {
                    ...this.state.data,
                    data: this.state.dataTable,
                  }
                })
                  .then(result => {
                    api.get(`pdf/pesanan`, {
                      responseType: 'blob'
                    })
                      .then(result => {
                        const pdfBlob = new Blob([result.data], { type: 'application/pdf' });
                        saveAs(pdfBlob, 'pesanan.pdf');
                      })
                      .catch(error => {
                        console.log('Error: ', error.response);
                      });
                  })
                  .catch(error => {
                    console.log('Error: ', error.response);
                  });
              }}
            >Print Pdf</Button>
            <Button onClick={this.props.close}>Close</Button>
          </DialogActions>
        </Dialog>
        {
          this.state.openCetakNoStok ?
            <CetakNoStok
              data={this.state.data.data}
              close={() => {
                this.setState({ openCetakNoStok: false });
              }}
            />
          : null
        }
      </>
		)
	}

}

export default DistribusiDepo;

import React from 'react';
import { Dialog, DialogContent, DialogActions } from '@material-ui/core';
import { Button, Tag } from 'antd';
import ReactToPrint from 'react-to-print';
import api from 'config/api.js';
import Loading from 'components/Loading.jsx';
import KopSurat from 'components/KopSurat.jsx';
import { tglIndo, separator } from 'config/helpers.js';

class Pelayanan extends React.Component {

	state = {
		data: [],
		loading: false,
	}
  
	componentDidMount() {
		this.setState({ loading: true }, () => {
			api.get(`/farmasi/transaksi/resep/${this.props.id_farresep === 1 ? 'jalan' : this.props.id_farresep === 2 ? 'inap' : ''}/full/${this.props.id_farresep === 1 ? this.props.id_peljalan : this.props.id_farresep === 2 ? this.props.id_pelinap : ''}`)
			.then(result => {
				this.setState({ data: result.data, loading: false });
			})
			.catch(error => {
				console.log(error.response);
			});
		});
	}
  
	render() {
		let total_sub = 0;
		let total_pelayanan = 0;
		let total_pembungkus = 0;
		let total = 0;
		return (
			<Dialog open maxWidth="xl" fullWidth scroll="paper">
				<DialogContent ref={print => (this.componentRef = print)}>
				<div className="flex relative">
					<div className="w-full">
						<KopSurat />
						{
							this.state.data.map((row, i) => {
								total_sub += row.total_sub;
								total_pelayanan += row.total_pelayanan;
								total_pembungkus += row.total_pembungkus;
								total += row.total;
								return (
									<div key={i}>
										<table className="text-sm mb-2 w-full">
											<tbody>
												<tr>
													<td><b>No. Transaksi</b></td>
													<td><b>:</b></td>
													<td>{row.id_fartransaksi}</td>
													<td><b>Pelayanan</b></td>
													<td><b>:</b></td>
													<td>{row.nama_farpelayanan}</td>
												</tr>
												<tr>
													<td><b>Nama Pasien</b></td>
													<td><b>:</b></td>
													<td>{row.nama_pelpasien} [{row.no_rekmedis}]</td>
													<td><b>Cara Bayar</b></td>
													<td><b>:</b></td>
													<td>{row.nama_pelcabar}</td>
												</tr>
												<tr>
													<td><b>Operator</b></td>
													<td><b>:</b></td>
													<td>{row.operator}</td>
													<td><b>Tanggal</b></td>
													<td><b>:</b></td>
													<td>{tglIndo(row.tgl)}</td>
												</tr>
												<tr>
													<td><b>Dokter</b></td>
													<td><b>:</b></td>
													<td>{row.nama_dokter}</td>
												</tr>
											</tbody>
										</table>
										<table className="text-sm mb-2 w-full">
											<tbody>
												<tr>
													<td className="border border-black" align='center'><b>No</b></td>
													<td className="border border-black" align='center'><b>No. Stok</b></td>
													<td className="border border-black" align='center'><b>Nama Obat</b></td>
													<td className="border border-black" align='center'><b>Qty</b></td>
													<td className="border border-black" align='center'><b>Satuan</b></td>
													<td className="border border-black" align='right'><b>Jumlah</b></td>
												</tr>
												{
													row.data.map((row2, i) => (
														<tr key={i}>
															<td className="border border-black" align='center'>{i+1}</td>
															<td className="border border-black" align='center'>{row2.id_farfakturdet}</td>
															<td className="border border-black">{row2.nama_farobat}</td>
															<td className="border border-black" align='center'>{row2.qty}</td>
															<td className="border border-black" align='center'>{row2.nama_farsatuan}</td>
															<td className="border border-black" align='right'>{separator(row2.j_total)}</td>
														</tr>
													))
												}
											</tbody>
										</table>
									</div>
								);
							})
						}
						<table className="text-sm">
							<tbody>
								<tr>
									<td><b>Diskon / Sub Total</b></td>
									<td><b>:</b></td>
									<td align="right"> - | {separator(total_sub)}</td>
								</tr>
								<tr>
									<td><b>Pembungkus / Jasa Pelayanan</b></td>
									<td><b>:</b></td>
									<td align="right">{separator(total_pembungkus)} | {separator(total_pelayanan)}</td>
								</tr>
								<tr>
									<td><b>Total</b></td>
									<td><b>:</b></td>
									<td align="right"><b>Rp. {separator(total)}</b></td>
								</tr>
							</tbody>
						</table>
					</div>
					{
					this.state.loading ?
						<Loading />
					: null
					}
				</div>
				</DialogContent>
				<DialogActions>
					{
						this.props.panggil_selesai || this.props.panggil_siap || this.props.panggil_akhir ?
							<Tag
								onClick={() => {
									let panggil;
									if (this.props.panggil_selesai) {
										panggil = { panggil_siap: "true" }
									} else if (this.props.panggil_siap) {
										panggil = { panggil_akhir: "true" }
									} else if (this.props.panggil_akhir) {
										panggil = { panggil_ambil: "true" }
									};
									this.setState({ loading: true }, () => {
										api.patch(`/farmasi/permintaan/${this.props.id_farpermintaan}`, panggil)
										.then(result => {
											this.props.closeRefresh();
										})
										.catch(error => {
											console.log(error.response);
										});
									});
								}}
								className="cursor-pointer"
								color={
									this.props.panggil_selesai ?
										"pink"
									:
										this.props.panggil_siap ?
										"blue"
									:
										this.props.panggil_akhir ?
										"green"
									: null
								}
							>Selesaikan &nbsp;
								{
									this.props.panggil_selesai ?
									"Dalam Antrean"
								:
									this.props.panggil_siap ?
									"Dalam Proses"
								:
									this.props.panggil_akhir ?
									"Selesai Dan Bisa Diambil"
								: null
								}
							</Tag>
						: null
					}
					<ReactToPrint
						trigger={() => (
							<Button>Print</Button>
						)}
						content={() => this.componentRef}
					/>
					<Button onClick={this.props.close}>Close</Button>
				</DialogActions>
			</Dialog>
		)
	}

}

export default Pelayanan;

import React from 'react';
import { Dialog, DialogContent, DialogActions } from '@material-ui/core';
import ReactToPrint from 'react-to-print';
import Button from 'components/inputs/Button.jsx';
import KopSurat from 'components/KopSurat.jsx';
import api from 'config/api.js';
import { tglIndo, separator } from 'config/helpers.js';
import { saveAs } from 'file-saver';
import Loading from 'components/Loading.jsx';

import CetakNoStok from 'components/farmasi/CetakNoStok.jsx';

class Faktur extends React.Component {

  state = {
    data: {
      nama_fardistributor: '',
      no_po: '',
      tgl_po: '',
      no_faktur: '',
      tgl_faktur: '',
      data: [],
    },
    loading: false,
    openCetakNoStok: false,
  }
  
  componentDidMount() {
    this.setState({ loading: true }, () => {
      api.get(`/farmasi/faktur/detail/`, {
				headers: { id_farfaktur: this.props.id_farfaktur }
			})
      .then(result => {
        this.setState({ data: result.data, loading: false });
      })
      .catch(error => {
        console.log(error.response);
      });
    });
  }
  
  render() {
    let totalFaktur = 0;
		let totalTotalFaktur = 0;
    return (
			<>
        <Dialog open maxWidth="xl" fullWidth scroll="paper">
          <DialogContent ref={print => (this.componentRef = print)}>
            <KopSurat />
            <div className="flex relative">
              <div className="w-full">
                <table
                  cellPadding={3}
                  cellSpacing={0}
                  className="w-full mb-2 text-sm"
                >
                  <tbody>
                    <tr>
                      <td>Nama Distributor</td>
                      <td>:</td>
                      <td colSpan={4}>{this.state.data.nama_fardistributor}</td>
                    </tr>
                    <tr>
                      <td>No. Po</td>
                      <td>:</td>
                      <td>{this.state.data.no_po}</td>
                      <td>Tgl PO</td>
                      <td>:</td>
                      <td>{tglIndo(this.state.data.tgl_po)}</td>
                    </tr>
                    <tr>
                      <td>No. Faktur</td>
                      <td>:</td>
                      <td>{this.state.data.no_faktur}</td>
                      <td>Tgl Faktur</td>
                      <td>:</td>
                      <td>{tglIndo(this.state.data.tgl_faktur)}</td>
                    </tr>
                  </tbody>
                </table>

                <table cellPadding={3} cellSpacing={0}
                  className="text-sm w-full"
                >
                  <tbody>
                    <tr>
                      <td className="border border-black" align="center">No. Stok</td>
                      <td className="border border-black">Nama Obat</td>
                      <td className="border border-black" align="center">Jml Faktur</td>
                      <td className="border border-black" align="center">Diskon</td>
                      <td className="border border-black" align="right">Harga Faktur</td>
                      <td className="border border-black" align="right">Total Faktur</td>

                      <td className="border border-black" align="center">Stok</td>
                      <td className="border border-black" align="center">PPN</td>
                      <td className="border border-black" align="center">Margin</td>
                      <td className="border border-black" align="right">Harga Satuan Jual</td>
                    </tr>
                    {this.state.data.data.map((row, i) => {
                      totalFaktur = (row.har_faktur * row.jml_faktur) - (((row.har_faktur * row.jml_faktur) * row.diskon) / 100);
                      totalTotalFaktur += totalFaktur;
                      return (
                        <tr key={i}>
                          <td className="border border-black" align="center">{row.id_farfakturdet}</td>
                          <td className="border border-black">{row.nama_farobat} [{row.id_farobat}]</td>
                          <td className="border border-black" align="center">{row.jml_faktur} {row.nama_farsatuan}</td>
                          <td className="border border-black" align="center">{row.diskon} %</td>
                          <td className="border border-black" align="right">{separator(row.har_faktur, 0)}</td>
                          <td className="border border-black" align="right">{separator(totalFaktur, 0)}</td>

                          <td className="border border-black" align="center">{row.jml}</td>
                          <td className="border border-black" align="center">{row.ppn} %</td>
                          <td className="border border-black" align="center">{row.margin} %</td>
                          <td className="border border-black" align="right">{separator(row.har_satuan, 0)}</td>
                        </tr>
                      );	
                    })}
                    <tr>
                      <td className="border border-black" align="right" colSpan={5}><b>Total</b></td>
                      <td className="border border-black" align="right"><b>{separator(totalTotalFaktur, 0)}</b></td>
                      <td className="border border-black" colSpan={4}></td>
                    </tr>
                    <tr>
                      <td className="border border-black" align="right" colSpan={5}><b>PPN Beli</b></td>
                      <td className="border border-black" align="right"><b>{this.state.data.ppn} %</b></td>
                      <td className="border border-black" colSpan={4}></td>
                    </tr>
                    <tr>
                      <td className="border border-black" align="right" colSpan={5}><b>Ext. Diskon</b></td>
                      <td className="border border-black" align="right"><b>{this.state.data.ext_diskon} %</b></td>
                      <td className="border border-black" colSpan={4}></td>
                    </tr>
                  </tbody>
                </table>
              </div>
              {
                this.state.loading ?
                  <Loading />
                : null
              }
            </div>
          </DialogContent>
          <DialogActions>
            <Button
              onClick={() => {
                this.setState({ openCetakNoStok: true });
              }}
            >Cetak No. Stok</Button>
            <ReactToPrint
              trigger={() => (
                <Button>Print</Button>
              )}
              content={() => this.componentRef}
            />
            <Button color="primary"
              onClick={() => {
                api.post(`/pdf/faktur/`, {
                  data: {
                    ...this.state.data,
                    data: this.state.dataTable,
                  }
                })
                  .then(result => {
                    api.get(`pdf/faktur`, {
                      responseType: 'blob'
                    })
                      .then(result => {
                        const pdfBlob = new Blob([result.data], { type: 'application/pdf' });
                        saveAs(pdfBlob, 'pesanan.pdf');
                      })
                      .catch(error => {
                        console.log('Error: ', error.response);
                      });
                  })
                  .catch(error => {
                    console.log('Error: ', error.response);
                  });
              }}
            >Print Pdf</Button>
            <Button onClick={this.props.close}>Close</Button>
          </DialogActions>
        </Dialog>
        {
          this.state.openCetakNoStok ?
            <CetakNoStok
              data={this.state.data.data}
              close={() => {
                this.setState({ openCetakNoStok: false });
              }}
            />
          : null
        }
      </>
		)
	}

}

export default Faktur;

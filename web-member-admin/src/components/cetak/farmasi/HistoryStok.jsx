import React from 'react';
import { Dialog, DialogTitle, DialogContent, IconButton, Divider, Tooltip, } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import api from 'config/api.js';
import PageviewOutlinedIcon from '@material-ui/icons/PageviewOutlined';
import CetakDistribusiDepo from 'components/cetak/farmasi/DistribusiDepo.jsx';
import CetakDistribusiRuangan from 'components/cetak/farmasi/DistribusiRuangan.jsx';
import { tglIndo } from 'config/helpers.js';

class HistoryStok extends React.Component {
	
	state = {
		dataTable: {
			nama_fardistributor: '',
			transaksiGudang: [],
			transaksiDepoApotek: [],
			transaksiDepoIgd: [],
		},
		loading: false,
		aktif: {},
		openCetakDistribusiDepo: false,
		openCetakDistribusiRuangan: false,
	}

	componentDidMount() {
		this.setState({ disabledSearch: true }, () => {
			api.get(`/farmasiX/gudang/stok/history/${this.props.data.id_farfakturdet}`)
			.then(result => {
				this.setState({ dataTable: result.data, disabledSearch: false });
			})
			.catch(error => {
				console.log('Error: ', error.response);
				this.setState({ disabledSearch: false });
			});
		});
	}
	
	render() {
		let total = 0;
		let total_depo_apotek = 0;
		let total_depo_igd = 0;
		return (
			<>
				<Dialog fullScreen open={true} maxWidth="xl" fullWidth scroll="paper">
					<DialogTitle>
						<span style={{ fontFamily: 'Gadugib' }}>History Stok {this.props.data.nama_farobat} [{this.props.data.id_farobat}] | No. Stok : {this.props.data.id_farfakturdet}</span>
						<IconButton style={{
							position: 'absolute',
							color: 'grey',
							right: 5,
							top: 5,
						}} onClick={this.props.close}>
							<CloseIcon />
						</IconButton>
						<Divider style={{ marginTop: 10 }} />
					</DialogTitle>
					<DialogContent ref={el => (this.componentRef = el)}>
						<table className="text-sm w-full">
							<tbody>
								<tr>
									<td>Tanggal</td>
									<td>:</td>
									<td>{tglIndo(this.state.dataTable.tgl)}</td>
								</tr>
								<tr>
									<td>No. PO</td>
									<td>:</td>
									<td>{this.state.dataTable.no_po}</td>
								</tr>
								<tr>
									<td>Nama Distributor</td>
									<td>:</td>
									<td>{this.state.dataTable.nama_fardistributor}</td>
								</tr>
								<tr>
									<td className="border border-b-2">Jumlah</td>
									<td className="border border-b-2">:</td>
									<td className="border border-b-2">{this.state.dataTable.jml}</td>
								</tr>
							</tbody>
						</table>
						<table
							className="text-sm w-full mt-3"
							cellPadding={3}
							cellSpacing={0}
						>
							<tbody>
								<tr>
									<td colSpan={4} className="border border-black">Transaksi Gudang</td>
								</tr>
								<tr>
									<td align="right" className="border border-black">Tgl</td>
									<td className="border border-black">Distribusi</td>
									<td align="right" className="border border-black">Qty</td>
									<td align="center" className="border border-black">Action</td>
								</tr>
								{this.state.dataTable.transaksiGudang.map((row, i) => {
									total += row.jml;
									return (
										<tr key={i}>
											<td align="right" className="border border-black">{row.tgl}</td>
											<td className="border border-black">{row.nama_fardistribusi}</td>
											<td align="right" className="border border-black">{row.jml}</td>
											<td className="border border-black" align="center">
												<Tooltip title="Lihat Distribusi">
													<IconButton color="primary" style={{
														padding: 5, minHeight: 0, minWidth: 0 }} onClick={() => {
															const far_distribusi = row.id_fardistribusi === 1 ? { openCetakDistribusiDepo: true, } : { openCetakDistribusiRuangan: true };
															this.setState({ aktif: row, ...far_distribusi });
														}}
													><PageviewOutlinedIcon fontSize="small" /></IconButton>
												</Tooltip>
											</td>
										</tr>
									);
								})}
								<tr>
									<td align="right" colSpan={2} className="border border-black bg-red-200"><b>Total: {total + this.state.dataTable.stok_cutoff_gudang}</b></td>
									<td align="right" className="border border-black">{total}</td>
									<td className="border border-black">: {this.state.dataTable.stok_cutoff_gudang} <b>[CutOff]</b></td>
								</tr>
							</tbody>
						</table>
						
						<table
							className="text-sm w-full mt-3"
							cellPadding={3}
							cellSpacing={0}
						>
							<tbody>
								<tr>
									<td colSpan={3} className="border border-black">Transaksi Depo Apotek</td>
								</tr>
								<tr>
									<td align="right" className="border border-black">Tgl</td>
									<td align="right" className="border border-black">Qty</td>
									<td align="center" className="border border-black">Action</td>
								</tr>
								{this.state.dataTable.transaksiDepoApotek.map((row, i) => {
									total_depo_apotek += row.qty;
									return (
										<tr key={i}>
											<td align="right" className="border border-black">{row.tgl}</td>
											<td align="right" className="border border-black">{row.qty}</td>
											<td className="border border-black" align="center">
												<Tooltip title="Lihat Transaksi">
													<IconButton
														color="primary"
														style={{
															padding: 5, minHeight: 0, minWidth: 0
														}}
														onClick={() => {
															// const far_distribusi = row.id_fardistribusi === 1 ? { openCetakDistribusiDepo: true, } : { openCetakDistribusiRuangan: true };
															// this.setState({ aktif: row, ...far_distribusi });
														}}
													><PageviewOutlinedIcon fontSize="small" /></IconButton>
												</Tooltip>
											</td>
										</tr>
									);
								})}
								<tr>
									<td align="right" className="border border-black bg-red-200"><b>Total: {total_depo_apotek + this.state.dataTable.stok_cutoff_depo_apotek}</b></td>
									<td align="right" className="border border-black">{total_depo_apotek}</td>
									<td className="border border-black">: {this.state.dataTable.stok_cutoff_depo_apotek} <b>[CutOff]</b></td>
								</tr>
							</tbody>
						</table>

						<table
							className="text-sm w-full mt-3"
							cellPadding={3}
							cellSpacing={0}
						>
							<tbody>
								<tr>
									<td colSpan={3} className="border border-black">Transaksi Depo IGD</td>
								</tr>
								<tr>
									<td align="right" className="border border-black">Tgl</td>
									<td align="right" className="border border-black">Qty</td>
									<td align="center" className="border border-black">Action</td>
								</tr>
								{this.state.dataTable.transaksiDepoIgd.map((row, i) => {
									total_depo_igd += row.qty;
									return (
										<tr key={i}>
											<td align="right" className="border border-black">{row.tgl}</td>
											<td align="right" className="border border-black">{row.qty}</td>
											<td className="border border-black" align="center">
												<Tooltip title="Lihat Transaksi">
													<IconButton
														color="primary"
														style={{
															padding: 5, minHeight: 0, minWidth: 0
														}}
														onClick={() => {
															// const far_distribusi = row.id_fardistribusi === 1 ? { openCetakDistribusiDepo: true, } : { openCetakDistribusiRuangan: true };
															// this.setState({ aktif: row, ...far_distribusi });
														}}
													><PageviewOutlinedIcon fontSize="small" /></IconButton>
												</Tooltip>
											</td>
										</tr>
									);
								})}
								<tr>
									<td align="right" className="border border-black bg-red-200"><b>Total: {total_depo_igd + this.state.dataTable.stok_cutoff_depo_igd}</b></td>
									<td align="right" className="border border-black">{total_depo_igd}</td>
									<td className="border border-black">: {this.state.dataTable.stok_cutoff_depo_igd} <b>[CutOff]</b></td>
								</tr>
							</tbody>
						</table>
					
					</DialogContent>
				</Dialog>
				{
					this.state.openCetakDistribusiDepo ?
					<CetakDistribusiDepo
						close={() => {
							this.setState({ openCetakDistribusiDepo: false });
						}}
						id_fardistribusitransaksi={this.state.aktif.id_fardistribusitransaksi}
					/> : null
				}
				{
					this.state.openCetakDistribusiRuangan ?
					<CetakDistribusiRuangan
						close={() => {
							this.setState({ openCetakDistribusiRuangan: false });
						}}
						id_fardistribusitransaksi={this.state.aktif.id_fardistribusitransaksi}
					/> : null
				}
			</>
		)
	}

};

export default (HistoryStok);

import React from 'react';
import { Dialog, DialogContent, DialogActions } from '@material-ui/core';
import ReactToPrint from 'react-to-print';
import Button from 'components/inputs/Button.jsx';
import api from 'config/api.js';
import Loading from 'components/Loading.jsx';
import DataGrid from 'components/table/DataGrid.jsx';
import GridColumn from 'components/table/GridColumn.jsx';

class LogTransaksi extends React.Component {

  state = {
    transaksi: [],
		detail: [],
		loading: false,
  }
  
  componentDidMount() {
    this.setState({ loading: true }, () => {
      api.get(`/farmasiX/transaksi/penjualan/log/${this.props.data.id_fartransaksi}`)
      .then(result => {
				console.log(result.data)
				this.setState({
					transaksi: result.data.transaksi,
					detail: result.data.detail,
					loading: false
				});
      })
      .catch(error => {
        console.log(error.response);
				this.setState({
					loading: false
				});
      });
    });
	}
	
	cetakLabel = () => {
		this.setState({ openLabel: true });
	}
  
  render() {
    return (
			<>
				<Dialog open maxWidth="xl" fullWidth scroll="paper">
					<DialogContent ref={print => (this.componentRef = print)}>
						<div>
							<div>Log Data Transaksi</div>
							<DataGrid data={this.state.transaksi} loading={this.state.loading}>
								<GridColumn title="Tanggal" />
								<GridColumn title="Tgl. Checkin" field="tgl_checkin" />
							</DataGrid>
							<div className="mt-5">Log Detail Transaksi</div>
							<DataGrid data={this.state.detail} loading={this.state.loading}>
								<GridColumn title="Tanggal" />
								<GridColumn title="Tgl. Checkin" field="tgl_checkin" />
							</DataGrid>
							{
								this.state.loading ?
									<Loading />
								: null
							}
						</div>
					</DialogContent>
					<DialogActions>
						<ReactToPrint
							trigger={() => (
								<Button>Print</Button>
							)}
							content={() => this.componentRef}
						/>
						<Button onClick={this.props.close}>Close</Button>
					</DialogActions>
				</Dialog>

			</>
		)
	}

}

export default LogTransaksi;

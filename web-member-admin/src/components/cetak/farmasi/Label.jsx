import React from 'react';
import { Dialog, DialogContent, DialogActions } from '@material-ui/core';
import ReactToPrint from 'react-to-print';
import Button from 'components/inputs/Button.jsx';
import { tglIndo } from 'config/helpers.js';

const pageStyle = `
    @page {
        size: 70mm 50mm potrait;
        margin-left: 2mm;
        margin-top: 2mm;
        margin-right: 2mm;
        margin-bottom: 2mm;
    }
`;

class Label extends React.Component {

	state = {
		data: {
			id_farpermintaan: '',
			no_rekmedis: '',
			nama_pelpasien: '',
			tgl: '',
			nama_dokter: '',
			tgl_lahir: '',
		},
		datas: [],
	}
  
	componentDidMount() {
		let data = this.props.data.data.filter((row) => { return row.id_farjenis === 1 });
		this.setState({ data: this.props.data, datas: data });
	}
  
	render() {
		return (
			<Dialog open maxWidth="xl" fullWidth scroll="paper">
				<DialogContent ref={print => (this.componentRef = print)}>
					{
						this.state.datas.map((row, i) => (
							<div key={i} style={{ pageBreakAfter: 'always', fontSize: '12px' }}>
								<style>{pageStyle}</style>
								<table>
									<tbody>
										<tr className='pb-6'>
											<td>
												<img src={require('assets/imgs/logo.png')} className="w-auto h-6" alt="" />
											</td>
											<td></td>
											<td colSpan='3' style={{ fontSize: '12px' }}>
												RS PERTAMEDIKA UMMI ROSNATI<br />
												INSTALASI FARMASI (Telp) <span style={{ fontSize: '10px' }}>065135092 - 065135905</span><br />
												Jln Sekolah No 5 Gampong Ateuk Pahlawan<br />
												Banda Aceh 23241
											</td>
										</tr>
										<tr>
											<td>No Resep</td>
											<td>:</td>
											<td>{this.state.data.id_farpermintaan}</td>
											<td align='right' colSpan='2'>{tglIndo(this.state.data.tgl)}</td>
										</tr>
										<tr>
											<td>Nama Pasien</td>
											<td>:</td>
											<td>{this.state.data.nama_pelpasien}</td>
											<td align='right' colSpan={2}>MR.{this.state.data.no_rekmedis}</td>
										</tr>
										<tr>
											<td>Tgl Lahir</td>
											<td>:</td>
											<td colSpan='3'>{tglIndo(this.state.data.tgl_lahir)}</td>
										</tr>
										<tr className='spacer'>
											<td>Dokter</td>
											<td>:</td>
											<td colSpan='3'>{this.state.data.nama_dokter}</td>
										</tr>
										<tr className='spacer'>
											<td colSpan='5' align='center' style={{ fontSize: '16px' }}><b>{row.cara_pakai}</b></td>
										</tr>
										<tr>
											<td>Nama Obat</td>
											<td>:</td>
											<td colSpan='3'>{row.nama_farobat}</td>
										</tr>
										<tr>
											<td>Komposisi</td>
											<td>:</td>
											<td colSpan='3'>{row.komposisi}</td>
										</tr>
										<tr>
											<td>Expired</td>
											<td>:</td>
											<td colSpan='3'>{tglIndo(row.tgl_kadaluarsa)}</td>
										</tr>
										<tr>
											<td>BUD</td>
											<td>:</td>
											<td colSpan='3'>{row.bud}</td>
										</tr>
										<tr>
											<td>Jumlah</td>
											<td>:</td>
											<td colSpan='3'>{row.qty}</td>
										</tr>
									</tbody>
								</table>
							</div>
						))
					}
				</DialogContent>
				<DialogActions>
					<ReactToPrint
						trigger={() => (
							<Button>Print</Button>
						)}
						content={() => this.componentRef}
					/>
					<Button onClick={this.props.close}>Close</Button>
				</DialogActions>
			</Dialog>
		)
	}

}

export default Label;

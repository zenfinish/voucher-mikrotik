import React from 'react';
import ReactToPrint from 'react-to-print';
import api from 'config/api.js';
import HistoryStok from './HistoryStok.jsx';
import { separator, tglIndo } from 'config/helpers.js';
import { notification, Modal, Table, Tag, Input, Button, Space, Popover } from 'antd';
import SearchOutlined from '@ant-design/icons/lib/icons/SearchOutlined';

class StockOpname extends React.Component {

  state = {
    loading: false,
    data: [],
    aktif:{},
    openHistoryStok: false,
    openPrint: false,

    searchText: '',
    searchedColumn: '',
  }
  
  componentDidMount() {
    this.fetchData(this.props.id_farso);
  }

  fetchData = (id_farso) => {
    this.setState({ loading: true }, () => {
      api.get(`/farmasi/gudang/so/detail/${id_farso}`)
      .then(result => {
        this.setState({ data: result.data, loading: false });
      })
      .catch(error => {
        console.log(error.response);
        this.setState({ loading: false });
        notification.error({
          message: 'Error',
          description: JSON.stringify(error.response.data),
        });
      });
    });
  }

  getColumnSearchProps = dataIndex => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={node => {
            this.searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
          style={{ width: 188, marginBottom: 8, display: 'block' }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => {
              this.handleSearch(selectedKeys, confirm, dataIndex);
            }}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >Search</Button>
          <Button onClick={() => this.handleReset(clearFilters)} size="small" style={{ width: 90 }}>
            Reset
          </Button>
          <Button
            type="link"
            size="small"
            onClick={() => {
              confirm({ closeDropdown: false });
              this.setState({
                searchText: selectedKeys[0],
                searchedColumn: dataIndex,
              });
            }}
          >
            Filter
          </Button>
        </Space>
      </div>
    ),
    filterIcon: filtered => <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />,
    onFilter: (value, record) =>
      record[dataIndex]
        ? record[dataIndex].toString().toLowerCase().includes(value.toLowerCase())
        : '',
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => this.searchInput.select(), 100);
      }
    },
    render: text => 
      dataIndex === "id_farfakturdetlama" ?
        ( <Tag color="volcano">{text}</Tag> )
      : ( text ),
  });

  handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    this.setState({
      searchText: selectedKeys[0],
      searchedColumn: dataIndex,
    });
  };

  handleReset = clearFilters => {
    clearFilters();
    this.setState({ searchText: '' });
  };
  
  render() {
    // let total_rp = 0;
    return (
			<>
        <Modal
          visible={true}
          centered
          onCancel={this.props.close}
          title="Hasil Stock Opname"
          width={5000}
          footer={false}
        >
          {/* <DialogContent>
             */}
              {/* <table cellPadding={3} cellSpacing={0} border={1}
                className="w-full mb-2 text-sm overflow-auto whitespace-nowrap"
              >
                <tbody>
                  <tr>
                  </tr>
                  {this.state.data.map((row, i) => {
                    total_rp = total_rp + rp;
                    return (
                      <tr key={i}>
                      </tr>
                    );
                  })}
                  <tr>
                    <td className="border border-black" align="right" colSpan={20}>Total Rp</td>
                    <td className="border border-black" align="right"><b>{separator(total_rp)}</b></td>
                  </tr>
                </tbody>
              </table> */}
          <div ref={print => (this.componentRef = print)}>
            <Table
              columns={[
                {
                  title: 'No. Stok',
                  key: 'id_farfakturdetlama',
                  dataIndex: 'id_farfakturdetlama',
                  align: 'center',
                  render: (text, record, i) => <Tag color="volcano">{text}</Tag>,
                  fixed: 'left',
                  ...this.getColumnSearchProps('id_farfakturdetlama'),
                  width: 100,
                },
                {
                  title: 'Nama Obat',
                  key: 'nama_farobat',
                  dataIndex: 'nama_farobat',
                  fixed: 'left',
                  ...this.getColumnSearchProps('nama_farobat'),
                  render: (text, record, i) => (
                    <>
                      <Popover
                        content={(
                          <table>
                            <tbody>
                              <tr>
                                <td>ID Obat</td>
                                <td>:</td>
                                <td align="right">{record.id_farobat}</td>
                              </tr>
                              <tr>
                                <td>Harga Faktur</td>
                                <td>:</td>
                                <td align="right">{separator(record.har_faktur)}</td>
                              </tr>
                              <tr>
                                <td>Harga Satuan</td>
                                <td>:</td>
                                <td align="right">{separator(record.har_satuan)}</td>
                              </tr>
                              <tr>
                                <td>Diskon</td>
                                <td>:</td>
                                <td align="right">{record.diskon}</td>
                              </tr>
                              <tr>
                                <td>PPN</td>
                                <td>:</td>
                                <td align="right">{record.ppn}</td>
                              </tr>
                              <tr>
                                <td>Margin</td>
                                <td>:</td>
                                <td align="right">{record.margin}</td>
                              </tr>
                              <tr>
                                <td>Tgl. Kadaluarsa</td>
                                <td>:</td>
                                <td align="right">{tglIndo(record.tgl_kadaluarsa)}</td>
                              </tr>
                              <tr>
                                <td>Batch</td>
                                <td>:</td>
                                <td align="right">{record.batch}</td>
                              </tr>
                            </tbody>
                          </table>
                        )}
                        title={text}
                      >{text}</Popover>
                    </>
                  )
                },
                {
                  title: 'Stok Awal',
                  key: 'stok_awal',
                  dataIndex: 'stok_awal',
                  align: 'right',
                  width: 100,
                },
                {
                  title: 'Stok CutOff',
                  key: 'stok_cutoff',
                  dataIndex: 'stok_cutoff',
                  align: 'right',
                  render: (text, record, i) => <Tag color="geekblue" className="cursor-pointer" onClick={() => {
                    this.setState({
                      aktif: record,
                      openHistoryStok: true,
                    });
                  }}>{text}</Tag>,
                  width: 100,
                },
                {
                  title: 'Jml Pemakaian',
                  key: 'jmlPemakaian',
                  render: (text, record, i) => <>{record.stok_awal - record.stok_cutoff}</>,
                  align: 'right',
                  width: 100,
                },
                { title: 'Total Gudang', key: 'stok', dataIndex: 'stok', align: 'right' },
                { title: 'Total Depo Apotek', key: 'jml_apotek', dataIndex: 'jml_apotek', align: 'right' },
                { title: 'Total Depo IGD', key: 'jml_igd', dataIndex: 'jml_igd', align: 'right' },
                {
                  title: 'Jumlah',
                  key: 'jumlah',
                  align: 'right',
                  render: (text, record, i) => <>{separator(record.stok + record.jml_apotek + record.jml_igd)}</>,
                  width: 100,
                },
                {
                  title: 'Selisih',
                  key: 'selisih',
                  align: 'right',
                  render: (text, record, i) => <>{separator((record.stok + record.jml_apotek + record.jml_igd) - record.stok_cutoff)}</>,
                  width: 100,
                },
                { title: 'Rp', key: 'rp', render: (text, record, i) => <>{separator((record.stok + record.jml_apotek + record.jml_igd) * record.har_satuan)}</>, align: 'right' },
              ]}
              size="small"
              loading={this.state.loading}
              dataSource={this.state.data}
              bordered
              scroll={this.state.openPrint ? false : { x: 1500, y: 600 }}
              pagination={this.state.openPrint ? false : true}
              title={() => (
                <>
                  <ReactToPrint
                    trigger={() => (
                      <Button>Print</Button>
                    )}
                    content={() => this.componentRef}
                    onBeforeGetContent={() => {
                      this.setState({ openPrint: true });
                    }}
                    onAfterPrint={() => {
                      this.setState({ openPrint: false });
                    }}
                    key="print"
                  />
                </>
              )}
            />
          </div>
        </Modal>
        {
          this.state.openHistoryStok ?
            <HistoryStok
              data={{
                id_farfakturdet: this.state.aktif.id_farfakturdetlama,
                id_farobat: this.state.aktif.id_farobat,
                nama_farobat: this.state.aktif.nama_farobat,
              }}
              close={() => {
                this.setState({ openHistoryStok: false });
              }}
            />
          : null
        }
      </>
		)
	}

}

export default StockOpname;

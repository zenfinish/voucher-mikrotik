import React from 'react';
import { Dialog, DialogContent, DialogActions } from '@material-ui/core';
import ReactToPrint from 'react-to-print';
import Button from 'components/inputs/Button.jsx';
import api from 'config/api.js';
import Loading from 'components/Loading.jsx';
import KopSurat from 'components/KopSurat.jsx';
import { tglIndo, separator } from 'config/helpers.js';
import Label from './Label.jsx';
import LogTransaksi from './LogTransaksi.jsx';

class Nonresep extends React.Component {

  state = {
    data: {
      nama_fartransaksi: '',
			nama_farpelayanan: '',
			operator: '',
			tgl: '',
			total_sub: '',
			total_pelayanan: '',
			total_pembungkus: '',
			total: '',
      data: [],
    },
		loading: false,
		openLabel : false,
		openLogTransaksi : false,
  }
  
  componentDidMount() {
    this.setState({ loading: true }, () => {
      api.get(`/farmasi/transaksi/nonresep/${this.props.id_fartransaksi}`)
      .then(result => {
        this.setState({ data: result.data, loading: false });
      })
      .catch(error => {
        console.log(error.response);
      });
    });
	}
	
	cetakLabel = () => {
		this.setState({ openLabel: true });
	}
  
  render() {
    return (
			<>
			<Dialog open maxWidth="xl" fullWidth scroll="paper">
        <DialogContent ref={print => (this.componentRef = print)}>
          <div className="flex relative">
            <div className="w-full">
            	<KopSurat />
							<table className="text-sm mb-2 w-full">
								<tbody>
									<tr>
										<td><b>Nama</b></td>
										<td><b>:</b></td>
										<td>{this.state.data.nama_fartransaksi}</td>
									</tr>
									<tr>
										<td><b>No. Transaksi</b></td>
										<td><b>:</b></td>
										<td>
											<span
													className={`
														rounded
														cursor-pointer
														hover:bg-blue-900
														hover:text-white
													`}
													title="Lihat Log Transaksi"
													onClick={() => {
														this.setState({ openLogTransaksi: true });
													}}
												>
													{this.state.data.id_fartransaksi}
												</span>	
										</td>
										<td><b>Pelayanan</b></td>
										<td><b>:</b></td>
										<td>{this.state.data.nama_farpelayanan}</td>
									</tr>
									<tr>
										<td><b>Operator</b></td>
										<td><b>:</b></td>
										<td>{this.state.data.operator}</td>
										<td><b>Tanggal</b></td>
										<td><b>:</b></td>
										<td>{tglIndo(this.state.data.tgl)}</td>
									</tr>
								</tbody>
							</table>
							<table className="text-sm mb-2 w-full">
								<tbody>
									<tr>
										<td className="border border-black" align='center'><b>No</b></td>
										<td className="border border-black" align='center'><b>No. Stok</b></td>
										<td className="border border-black" align='center'><b>Nama Obat</b></td>
										<td className="border border-black" align='center'><b>Qty</b></td>
										<td className="border border-black" align='center'><b>Satuan</b></td>
										<td className="border border-black" align='right'><b>Jumlah</b></td>
									</tr>
									{
										this.state.data.data.map((row, i) => (
											<tr key={i}>
												<td className="border border-black" align='center'>{i+1}</td>
												<td className="border border-black" align='center'>{row.id_farfakturdet}</td>
												<td className="border border-black">{row.nama_farobat}</td>
												<td className="border border-black" align='center'>{row.qty}</td>
												<td className="border border-black" align='center'>{row.nama_farsatuan}</td>
												<td className="border border-black" align='right'>{separator(row.j_total)}</td>
											</tr>
										))
									}
								</tbody>
							</table>
							<table className="text-sm" align="right">
								<tbody>
									<tr>
										<td><b>Diskon / Sub Total</b></td>
										<td><b>:</b></td>
										<td align="right"> - | {separator(this.state.data.total_sub)}</td>
									</tr>
									<tr>
										<td><b>Pembungkus / Jasa Pelayanan</b></td>
										<td><b>:</b></td>
										<td align="right">{separator(this.state.data.total_pembungkus)} | {separator(this.state.data.total_pelayanan)}</td>
									</tr>
									<tr>
										<td><b>Total</b></td>
										<td><b>:</b></td>
										<td align="right"><b>Rp. {separator(this.state.data.total)}</b></td>
									</tr>
								</tbody>
							</table>
            </div>
            {
              this.state.loading ?
                <Loading />
              : null
            }
          </div>
        </DialogContent>
        <DialogActions>
          <Button onClick={this.cetakLabel}>Cetak Label</Button>
					<ReactToPrint
						trigger={() => (
							<Button>Print</Button>
						)}
						content={() => this.componentRef}
					/>
          <Button onClick={this.props.close}>Close</Button>
        </DialogActions>
			</Dialog>

				{
					this.state.openLabel ?
						<Label
							data={{
								...this.state.data,
								nama_pelpasien: this.state.data.nama_fartransaksi,
								no_rekmedis: '-',
								id_farpermintaan: '-',
								nama_dokter: '-',
							}}
							close={() => {
								this.setState({ openLabel: false });
							}}
						/>
					: null
				}
				{
					this.state.openLogTransaksi ?
						<LogTransaksi
							data={this.state.data}
							close={() => {
								this.setState({ openLogTransaksi: false });
							}}
						/>
					: null
				}

			</>
		)
	}

}

export default Nonresep;

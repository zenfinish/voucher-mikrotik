import React from 'react';
import KopSurat from 'components/KopSurat.jsx';
import { Dialog, DialogContent, DialogActions } from '@material-ui/core';
import ReactToPrint from 'react-to-print';
import Button from 'components/inputs/Button.jsx';
import api from 'config/api.js';
import { tglIndo } from 'config/helpers.js';

class Index extends React.Component {

  state = {
    data: {
      nama_pelpasien: '',
      no_rekmedis: '',
      tgl_lahir: '',
      umur: '',
      jkel: '',
      tipe: '',
      id_radtransaksi: '',
      nama_dokter: '',
      tgl: '',
      tgl_keluar: '',
      data: [],
    }
  }
  
  componentDidMount() {
    api.get(`/rad/${this.props.data.tipe === 1 ? 'jalan' : this.props.data.tipe === 2 ? 'inap': ''}/single/${this.props.data.id_radtransaksi}`)
		.then(result => {
      this.setState({ data: result.data });
		})
		.catch(error => {
			console.log(error.response);
		});
  }
  
  render() {
		return (
			<Dialog open maxWidth="xl" fullWidth scroll="paper">
        <DialogContent ref={print => (this.componentRef = print)}>
          <KopSurat />
          <table className="mb-2 text-sm w-full">
            <tbody>
              <tr>
                <td colSpan='6' align='center'><b>HASIL PEMERIKSAAN RADIOLOGI PER TRANSAKSI</b></td>
              </tr>
              <tr>
                <td>Nama Pasien</td>
                <td>:</td>
                <td>{this.state.data.nama_pelpasien}</td>
                <td>No. Rekmedis</td>
                <td>:</td>
                <td>{this.state.data.no_rekmedis}</td>
              </tr>
              <tr>
                <td>Tgl Lahir</td>
                <td>:</td>
                <td>{tglIndo(this.state.data.tgl_lahir)}</td>
                <td>Umur</td>
                <td>:</td>
                <td>{this.state.data.umur} Tahun</td>
              </tr>
              <tr>
                <td>Jenis Kelamin</td>
                <td>:</td>
                <td>{this.state.data.jkel === 'L' ? 'Laki - Laki' : this.state.data.jkel === 'P' ? 'Perempuan' : ''}</td>
                <td>Tipe Pelayanan</td>
                <td>:</td>
                <td>{this.state.data.tipe === 1 ? 'Rawat Jalan' : this.state.data.tipe === 2 ? 'Rawat Inap' : ''}</td>
              </tr>
              <tr>
                <td>No. Reg. Rad</td>
                <td>:</td>
                <td>{this.state.data.id_radtransaksi}</td>
                <td>Dokter Pengirim</td>
                <td>:</td>
                <td>{this.state.data.nama_dokter}</td>
              </tr>
              <tr>
                <td>Waktu Sample Datang</td>
                <td>:</td>
                <td>{tglIndo(this.state.data.tgl)}</td>
                <td>Waktu Hasil Keluar</td>
                <td>:</td>
                <td>{tglIndo(this.state.data.tgl_keluar)}</td>
              </tr>
            </tbody>
          </table>
          {
            this.state.data.data.map((row, i) => (
              <div key={i} className="border border-black w-full p-4 mb-2">
                <h3><u>Nama Tindakan : {row.nama_radtarif}</u></h3>
                <div className="text-sm">Hasil : <div dangerouslySetInnerHTML={{ __html: row.hasil }}></div></div>
              </div>
            ))
          }
          <table className="w-full text-sm">
            <tbody>
              <tr>
                <td>Pemeriksa<br /><br /><br /></td>
              </tr>
              <tr>
                <td>{this.state.data.nama_peluser}</td>
              </tr>
            </tbody>
          </table>
        </DialogContent>
        <DialogActions>
          <ReactToPrint
						trigger={() => (
							<Button>Print</Button>
						)}
						content={() => this.componentRef}
					/>
          <Button onClick={this.props.close}>Close</Button>
        </DialogActions>
      </Dialog>
		)
	}

}

export default Index;

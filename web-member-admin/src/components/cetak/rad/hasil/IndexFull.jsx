import React from 'react';
import KopSurat from 'components/KopSurat.jsx';
import { Dialog, DialogContent, DialogActions } from '@material-ui/core';
import ReactToPrint from 'react-to-print';
import Button from 'components/inputs/Button.jsx';
import api from 'config/api.js';
import { tglIndo } from 'config/helpers.js';

class IndexFull extends React.Component {

  state = {
    data: {
      nama_pelpasien: '',
      no_rekmedis: '',
      tgl_lahir: '',
      nama_pelcabar: '',
      jkel: '',
      tipe: '',
      data: [],
    }
  }
  
  componentDidMount() {
    api.get(`/rad/${this.props.data.tipe === 1 ? 'jalan' : this.props.data.tipe === 2 ? 'inap': ''}/multi/${this.props.data.tipe === 1 ? this.props.data.id_peljalan : this.props.data.tipe === 2 ? this.props.data.id_pelinap : ''}`)
		.then(result => {
      this.setState({ data: result.data });
		})
		.catch(error => {
			console.log(error.response);
		});
  }
  
  render() {
		return (
			<Dialog open maxWidth="xl" fullWidth scroll="paper">
        <DialogContent ref={print => (this.componentRef = print)}>
          <KopSurat />
          <table className="mb-2 text-sm w-full">
            <tbody>
              <tr>
                <td colSpan='6' align='center'><b>HASIL PEMERIKSAAN RADIOLOGI PER PELAYANAN</b></td>
              </tr>
              <tr>
                <td>Nama Pasien</td>
                <td>:</td>
                <td>{this.state.data.nama_pelpasien}</td>
                <td>No. Rekmedis</td>
                <td>:</td>
                <td>{this.state.data.no_rekmedis}</td>
              </tr>
              <tr>
                <td>Tgl Lahir</td>
                <td>:</td>
                <td>{tglIndo(this.state.data.tgl_lahir)}</td>
                <td>Umur</td>
                <td>:</td>
                <td>{this.state.data.umur} Tahun</td>
              </tr>
              <tr>
                <td>Jenis Kelamin</td>
                <td>:</td>
                <td>{this.state.data.jkel === 'L' ? 'Laki - Laki' : this.state.data.jkel === 'P' ? 'Perempuan' : ''}</td>
                <td>Tipe Pelayanan</td>
                <td>:</td>
                <td>{this.state.data.tipe === 1 ? 'Rawat Jalan' : this.state.data.tipe === 2 ? 'Rawat Inap' : ''}</td>
              </tr>
            </tbody>
          </table>
          {
            this.state.data.data.map((row, i) => (
              <div key={i}>
                {
                  row.data.map((row, j) => {
                    return (
                      <div key={j} className="border border-black w-full p-4">
                        <h3><u>Nama Tindakan : {row.nama_radtarif}</u></h3>
                        <div className="text-sm">Hasil : <div dangerouslySetInnerHTML={{ __html: row.hasil }}></div></div>
                      </div>
                    );
                  })
                }
              </div>
            ))
          }
        </DialogContent>
        <DialogActions>
          <ReactToPrint
						trigger={() => (
							<Button>Print</Button>
						)}
						content={() => this.componentRef}
					/>
          <Button onClick={this.props.close}>Close</Button>
        </DialogActions>
      </Dialog>
		)
	}

}

export default IndexFull;

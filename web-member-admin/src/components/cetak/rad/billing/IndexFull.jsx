import React from 'react';
import { Dialog, DialogContent, DialogActions } from '@material-ui/core';
import ReactToPrint from 'react-to-print';
import Button from 'components/inputs/Button.jsx';
import KopSurat from 'components/KopSurat.jsx';
import api from 'config/api.js';
import { tglIndo, separator } from 'config/helpers.js';

class IndexFull extends React.Component {

  state = {
    data: {
      nama_pelpasien: '',
      no_rekmedis: '',
      tgl_lahir: '',
      nama_pelcabar: '',
      jkel: '',
      tipe: '',
      data: [],
    }
  }
  
  componentDidMount() {
    api.get(`/rad/${this.props.data.tipe === 1 ? 'jalan' : this.props.data.tipe === 2 ? 'inap': ''}/multi/${this.props.data.tipe === 1 ? this.props.data.id_peljalan : this.props.data.tipe === 2 ? this.props.data.id_pelinap : ''}`)
		.then(result => {
			this.setState({ data: result.data });
		})
		.catch(error => {
			console.log(error.response);
		});
  }
  
  render() {
    let total = 0;
    return (
			<Dialog open maxWidth="xl" fullWidth scroll="paper">
        <DialogContent ref={print => (this.componentRef = print)}>
          <KopSurat />
          <h3 align="center" className="mb-2">KWITANSI RADIOLOGI PER PELAYANAN</h3>
          <table className="w-full text-sm mb-2">
            <tbody>
              <tr>
                <td><b>Nama Pasien</b></td>
                <td><b>:</b></td>
                <td>{this.state.data.nama_pelpasien}</td>
                <td><b>No. Rekmedis</b></td>
                <td><b>:</b></td>
                <td>{this.state.data.no_rekmedis}</td>
              </tr>
              <tr>
                <td><b>Tgl Lahir</b></td>
                <td><b>:</b></td>
                <td>{tglIndo(this.state.data.tgl_lahir)}</td>
                <td><b>Cara Bayar</b></td>
                <td><b>:</b></td>
                <td>{this.state.data.nama_pelcabar}</td>
              </tr>
              <tr>
                <td><b>Jenis Kelamin</b></td>
                <td><b>:</b></td>
                <td>{this.state.data.jkel === 'L' ? 'Laki - Laki' : this.state.data.jkel === 'P' ? 'Perempuan' : '-'}</td>
                <td><b>Tipe Pelayanan</b></td>
                <td><b>:</b></td>
                <td>{this.state.data.tipe === 1 ? 'Rawat Jalan' : this.state.data.tipe === 2 ? 'Rawat Inap': '-'}</td>
              </tr>
            </tbody>
          </table>
          {
            this.state.data.data.map((row, i) => (
              <div key={i}>
                <table className="text-sm mb-2 border border-black w-full">
                  <tbody>
                    <tr>
                      <td><b>Tanggal</b></td>
                      <td><b>:</b></td>
                      <td>{tglIndo(row.tgl)}</td>
                      <td><b>No. Transaksi</b></td>
                      <td><b>:</b></td>
                      <td>{row.id_radtransaksi}</td>
                    </tr>
                    <tr>
                      <td><b>Perinci</b></td>
                      <td><b>:</b></td>
                      <td>{row.nama_peluser}</td>
                      <td colSpan={3}></td>
                    </tr>
                  </tbody>
                </table>
                <table className="text-sm w-full mb-2">
                  <tbody>
                    <tr>
                      <td><b>No</b></td>
                      <td><b>Tindakan</b></td>
                      <td align="right"><b>Jumlah</b></td>
                    </tr>
                    {
                      row.data.map((row, i) => {
                        total += row.tarif;
                        return (
                          <tr key={i}>
                            <td>{i+1}</td>
                            <td>{row.nama_radtarif}</td>
                            <td align="right">{separator(row.tarif)}</td>
                          </tr>
                        );
                      })
                    }
                  </tbody>
                </table>
              </div>
            ))
          }
          <table className="text-sm" align="right">
            <tbody>
              <tr>
                <td><b>Total Tagihan</b></td>
                <td><b>:</b></td>
                <td align="right">{separator(total)}</td>
              </tr>
              <tr>
                <td><b>Dijamin</b></td>
                <td><b>:</b></td>
                <td align="right"> - </td>
              </tr>
              <tr>
                <td><b>Keringanan & Diskon</b></td>
                <td><b>:</b></td>
                <td align="right"> - </td>
              </tr>
              <tr>
                <td><b>Harus Bayar</b></td>
                <td><b>:</b></td>
                <td align="right"><b>Rp. {separator(total)}</b></td>
              </tr>
            </tbody>
          </table>
        </DialogContent>
        <DialogActions>
          <ReactToPrint
						trigger={() => (
							<Button>Print</Button>
						)}
						content={() => this.componentRef}
					/>
          <Button onClick={this.props.close}>Close</Button>
        </DialogActions>
			</Dialog>
		)
	}

}

export default IndexFull;

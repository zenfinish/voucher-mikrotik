import React from 'react';
import { Dialog, DialogContent, DialogActions } from '@material-ui/core';
import ReactToPrint from 'react-to-print';
import Button from 'components/inputs/Button.jsx';
import { tglIndo } from 'config/helpers.js';
import QRCode from 'qrcode.react';

class StikerGelang extends React.Component {

  render() {
    return (
			<Dialog open maxWidth="xl" fullWidth scroll="paper">
        <DialogContent ref={print => (this.componentRef = print)}>
          <div className="w-full"
            style={{
              fontSize: '14px',
              pageBreakBefore: 'always',
            }}
          >
            <div className="grid grid-cols-2">
              <div>
                <div><b>{this.props.data.nama_pelpasien}</b></div>
                <div>{this.props.data.no_rekmedis} / {this.props.data.nama_pelcabar}</div>
                <div>{this.props.data.jkel} / {tglIndo(this.props.data.tgl_lahir)} ({this.props.data.umur} th)</div>
                <div>{tglIndo(this.props.data.tgl_checkin)} / {this.props.data.nama_pelpoli}</div>
                <div>{this.props.data.nik} / {this.props.data.no_bpjs}</div>
              </div>
              <div className="text-right">
                <QRCode
                  value={{ id_pelinap: this.props.data.id_pelinap }}
                  style={{ width: 100, height: 'auto' }}
                />
              </div>
            </div>
          </div>
        </DialogContent>
        <DialogActions>
          <ReactToPrint
						trigger={() => (
							<Button>Print</Button>
						)}
						content={() => this.componentRef}
					/>
          <Button onClick={this.props.close}>Close</Button>
        </DialogActions>
			</Dialog>
		)
	}

}

export default StikerGelang;

import React from 'react';
import { notification, Modal } from 'antd';
import api from 'config/api.js';
import Button from 'components/inputs/Button.jsx';
import ReactToPrint from 'react-to-print';
import Form from "./Form";

class index extends React.Component {
   
   state = {
      loading: false,
      data: {
         noSep: "",
         tglSep: "",
         jnsPelayanan: "",
         kelasRawat: "",
         diagnosa: "",
         noRujukan: "",
         poli: "",
         poliEksekutif: "",
         catatan: "",
         penjamin: "",
         kdStatusKecelakaan: "",
         nmstatusKecelakaan: "",
         lokasiKejadian: {
            kdKab: "",
            kdKec: "",
            kdProp: "",
            ketKejadian: "",
            lokasi: "",
            tglKejadian: ""
         },
         dpjp: {
            kdDPJP: "",
            nmDPJP: ""
         },
         peserta: {
            asuransi: "",
            hakKelas: "",
            jnsPeserta: "",
            kelamin: "",
            nama: "",
            noKartu: "",
            noMr: "",
            tglLahir: ""
         },
         klsRawat: {
            klsRawatHak: "",
            klsRawatNaik: "",
            pembiayaan: "",
            penanggungJawab: ""
         },
         kontrol: {
            kdDokter: "",
            nmDokter: "",
            noSurat: ""
         },
         cob: "",
         katarak: "",
         // Data Ambil Di SIMRS //
         hp: "",
         faskesPerujuk: "",
     }
   }
   
   componentDidMount() {
      this.setState({ loading: true }, () => {
			api.post(`/bpjs/sep`, this.props.data)
			.then(result => {
            // console.log(result.data)
            if (result.data.jnsPelayanan === "Rawat Inap") {
               result.data.dpjp['kdDPJP'] = result.data.kontrol.kdDokter;
               result.data.dpjp['nmDPJP'] = result.data.kontrol.nmDokter;
            }
            this.setState({ data: { ...this.state.data, ...result.data }, loading: false });
			})
			.catch(error => {
            notification.error({
               message: 'Error',
               description: JSON.stringify(error.response.data),
            });
            this.props.close();
			});
		});
	}
   
   render() {
      return (
         <>
            <Modal
               centered
               width="100%"
               visible={true}
               onCancel={this.props.close}
               footer={[
                  <ReactToPrint
                     trigger={() => (
                        <Button color="primary">Print</Button>
                     )}
                     content={() => this.componentRef}
                     key="simpan"
                  />
               ]}
               title={`Cetak SEP BPJS`}
            >
               <Form
                    data={this.state.data}
                    loading={this.state.loading}
                    ref={print => (this.componentRef = print)}
               />
            </Modal>
         </>
      );
   }
}

export default (index);

import React from 'react';
import Loading from 'components/Loading.jsx';

class Form extends React.Component {
   
    render() {
      return (
         <>
            <div className="overflow-auto relative text-sm w-full">
                <table ref={el => (this.componentRef = el)} className="w-full">
                    <tbody>
                    <tr>
                        <td>
                            <img
                                src={require('assets/imgs/logo-bpjs.png')}
                                style={{width: 200, height: 'auto',}}
                                alt=""
                            />
                        </td>
                        <td>
                            <div>SURAT ELEGIBILITAS PESERTA</div>
                            <div>RS. PERTAMEDIKA UMMI ROSNATI</div>
                        </td>
                        <td>
                            <h3 style={{ fontSize: '1rem' }}>
                                {
                                this.props.data.informasi ?
                                    this.props.data.informasi.prolanisPRB ? `Pasien ${this.props.data.informasi.prolanisPRB}` : ''
                                : ''
                                }
                            </h3>
                        </td>
                    </tr>
                    <tr>
                        <td colSpan={3}>
                            <table className="w-full">
                                <tbody>
                                <tr>
                                    <td>No. SEP</td>
                                    <td>:</td>
                                    <td>{this.props.data.noSep}</td>
                                    <td colSpan={3}></td>
                                </tr>
                                <tr>
                                    <td>Tgl. SEP</td>
                                    <td>:</td>
                                    <td>{this.props.data.tglSep}</td>
                                    <td>Peserta</td>
                                    <td>:</td>
                                    <td>{this.props.data.peserta.jnsPeserta}</td>
                                </tr>
                                <tr>
                                    <td>No.Kartu</td>
                                    <td>:</td>
                                    <td>{this.props.data.peserta.noKartu} ( MR. {this.props.data.peserta.noMr} )</td>
                                    <td>COB</td>
                                    <td>:</td>
                                    <td>{this.props.data.cob === "0" ? "-" : this.props.data.cob}</td>
                                </tr>
                                <tr>
                                    <td>Nama Peserta</td>
                                    <td>:</td>
                                    <td>{this.props.data.peserta.nama} ({this.props.data.peserta.kelamin})</td>
                                    <td>Jns.Rawat</td>
                                    <td>:</td>
                                    <td>{this.props.data.jnsPelayanan}</td>
                                </tr>
                                <tr>
                                    <td>Tgl.Lahir</td>
                                    <td>:</td>
                                    <td>{this.props.data.peserta.tglLahir}</td>
                                    {
                                        this.props.dataInternal ?
                                            <>
                                                <td>Jns. Kunjungan</td>
                                                <td>:</td>
                                                <td>
                                                    {this.props.dataInternal ? "- Kunjungan Rujukan Internal" : ""}
                                                </td>
                                            </>
                                        : <td colSpan={3}></td>
                                    }
                                </tr>
                                <tr>
                                    <td>No.Telepon</td>
                                    <td>:</td>
                                    <td>
                                        {this.props.data.hp}
                                    </td>
                                    {
                                        this.props.dataInternal ?
                                            <>
                                                <td></td>
                                                <td>:</td>
                                                <td>
                                                    {this.props.dataInternal.flagprosedur === "0" ? "- Prosedur Tidak Berkelanjutan" : ""}
                                                </td>
                                            </>
                                        : <td colSpan={3}></td>
                                    }
                                </tr>
                                <tr>
                                    <td>Sub/Spesialis</td>
                                    <td>:</td>
                                    <td>{this.props.data.poli}</td>
                                    {
                                        this.props.dataInternal ?
                                            <>
                                                <td>Poli Perujuk</td>
                                                <td>:</td>
                                                <td>
                                                    {this.props.dataInternal.nmpoliasal}
                                                </td>
                                            </>
                                        : <td colSpan={3}></td>
                                    }
                                </tr>
                                <tr>
                                    <td>DPJP Yg Melayani</td>
                                    <td>:</td>
                                    <td>{this.props.data.dpjp.kdDPJP} - {this.props.data.dpjp.nmDPJP}</td>
                                    <td>Hak Kelas</td>
                                    <td>:</td>
                                    <td>{this.props.data.peserta.hakKelas}</td>
                                </tr>
                                <tr>
                                    <td>Faskes Perujuk</td>
                                    <td>:</td>
                                    <td>{this.props.data.provPerujuk ? `${this.props.data.provPerujuk.kode} - ${this.props.data.provPerujuk.nama}` : ''}</td>
                                    {
                                        this.props.data.jnsPelayanan === "Rawat Inap" ?
                                            <>
                                                <td>Kls.Rawat</td>
                                                <td>:</td>
                                                <td>{this.props.data.kelasRawat}</td>
                                            </>
                                        : <td colSpan={3}></td>
                                    }
                                </tr>
                                <tr>
                                    <td>Diagnosa Awal</td>
                                    <td>:</td>
                                    <td>{this.props.data.diagnosa}</td>
                                    <td>Penjamin</td>
                                    <td>:</td>
                                    <td>{this.props.data.penjamin}</td>
                                </tr>
                                <tr>
                                    <td>Catatan</td>
                                    <td>:</td>
                                    <td>{this.props.data.catatan}</td>
                                    <td colSpan={3}></td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colSpan={2} style={{ fontSize: '12px' }} className="text-gray-600">
                            *Saya Menyetujui BPJS Kesehatan menggunakan informasi medis pasien jika diperlukan.<br />
                            *SEP Bukan sebagai bukti penjaminan peserta. 
                        </td>
                        <td align="right">Pasien/Keluarga Pasien</td>
                    </tr>
                    <tr>
                        <td colSpan={2} style={{ fontSize: '12px' }}>
                            Cetakan : {new Date().toISOString().substr(0, 10)} {new Date().getHours()}{new Date().toISOString().substr(13, 6)}
                        </td>
                        <td align="right">____________________</td>
                    </tr>
                    </tbody>
                </table>
                <div className="font-bold text-2xl">
                    NOMOR ANTREAN: {this.props.data.nomorantrean}
                </div>
                {
                    this.props.loading ?
                    <Loading />
                    : null
                }
            </div>
         </>
      );
   }
}

export default (Form);

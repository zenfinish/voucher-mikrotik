import React from 'react';
import { notification, Modal } from 'antd';
import api from 'config/api.js';
import Button from 'components/inputs/Button.jsx';
import ReactToPrint from 'react-to-print';
import Form from "./Form";

class index extends React.Component {
   
   state = {
      loading: false,
      data: {
         noRujukan: "",
         noSep: "",
         noKartu: "",
         nama: "",
         kelasRawat: "",
         kelamin: "",
         tglLahir: "",
         tglSep: "",
         tglRujukan: "",
         tglRencanaKunjungan: "",
         ppkDirujuk: "",
         namaPpkDirujuk: "",
         jnsPelayanan: "",
         catatan: "",
         diagRujukan: "",
         namaDiagRujukan: "",
         tipeRujukan: "",
         namaTipeRujukan: "",
         poliRujukan: "",
         namaPoliRujukan: ""
      }
   }
   
   componentDidMount() {
      this.setState({ loading: true }, () => {
			api.get(`/bpjs/rujukan/keluar/${this.props.data.noRujukan}`)
			.then(result => {
            this.setState({ data: result.data, loading: false });
			})
			.catch(error => {
            notification.error({
               message: 'Error',
               description: JSON.stringify(error.response.data),
            });
            this.props.close();
			});
		});
	}
   
   render() {
      return (
         <>
            <Modal
               centered
               width="100%"
               visible={true}
               onCancel={this.props.close}
               footer={[
                  <ReactToPrint
                     trigger={() => (
                        <Button color="primary">Print</Button>
                     )}
                     content={() => this.componentRef}
                     key="simpan"
                  />
               ]}
               title={`Cetak Rujukan Keluar`}
            >
               <Form
                    data={this.state.data}
                    loading={this.state.loading}
                    ref={print => (this.componentRef = print)}
               />
            </Modal>
         </>
      );
   }
}

export default (index);

import React from 'react';
import Loading from 'components/Loading.jsx';
import { tglIndo } from 'config/helpers.js';

class Form extends React.Component {
   
    render() {
      return (
         <>
            {/* <style>{`
                    table td {
                        border: 1px solid black;
                    }
                `}</style> */}
            <div className="overflow-auto relative text-sm w-full">
                <table ref={el => (this.componentRef = el)} className="w-full">
                    <tbody>
                        <tr>
                            <td colSpan={4}>
                                <div className="flex justify-between">
                                    <div className="flex gap-2">
                                        <img
                                            src={require('assets/imgs/logo-bpjs.png')}
                                            style={{width: 200, height: 'auto',}}
                                            alt=""
                                        />
                                        <div>
                                            <div>SURAT RUJUKAN</div>
                                            <div>RS. PERTAMEDIKA UMMI ROSNATI</div>
                                        </div>
                                    </div>
                                    <div>
                                        <div>No. {this.props.data.noRujukan}</div>
                                        <div>Tgl. {tglIndo(this.props.data.tglRujukan)}</div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Kepada Yth</td>
                            <td colSpan={3}>: {this.props.data.namaPpkDirujuk}</td>
                        </tr>
                        <tr>
                            <td colSpan={3}>Mohon Pemeriksaan dan Penanganan Lebih Lanjut :</td>
                            <td rowSpan={5} valign="top" colSpan={1} align="center">
                                == {this.props.data.namaTipeRujukan} ==<br />
                                {this.props.data.jnsPelayanan === "1" ? "Rawat Inap" : this.props.data.jnsPelayanan === "2" ? "Rawat Jalan" : ""}
                            </td>
                        </tr>
                        <tr>
                            <td>No. Kartu</td>
                            <td>:</td>
                            <td>{this.props.data.noKartu}</td>
                        </tr>
                        <tr>
                            <td>Nama Peserta</td>
                            <td>:</td>
                            <td>{this.props.data.nama}</td>
                        </tr>
                        <tr>
                            <td>Tgl. Lahir</td>
                            <td>:</td>
                            <td>{tglIndo(this.props.data.tglLahir)}</td>
                        </tr>
                        <tr>
                            <td>Diagnosa</td>
                            <td>:</td>
                            <td>{this.props.data.namaDiagRujukan}</td>
                        </tr>
                        {/* <tr>
                            <td>Keterangan</td>
                            <td>:</td>
                            <td>{this.props.data.keterangan}</td>
                        </tr> */}
                        <tr>
                            <td colSpan={3}>Demikian atas bantuannya, diucapkan banyak terima kasih.</td>
                            <td>Mengetahui,</td>
                        </tr>
                        <tr>
                            <td colSpan={4} style={{ fontSize: '12px' }}>
                                * Rujukan ini Rujukan Parsial, tidak dapat digunakan untuk penerbitan SEP pada FKRTL penerima rujukan.<br />
                                * Tgl.Rencana Berkunjung {tglIndo(this.props.data.tglRencanaKunjungan)}
                            </td>
                        </tr>
                        <tr>
                            <td colSpan={3} style={{ fontSize: '11px' }}>
                                Tgl. Cetak : {new Date().toISOString().substr(0, 10)} {new Date().getHours()}{new Date().toISOString().substr(13, 6)}
                            </td>
                            <td>____________________</td>
                        </tr>
                    </tbody>
                </table>
                {
                    this.props.loading ?
                    <Loading />
                    : null
                }
            </div>
         </>
      );
   }
}

export default (Form);

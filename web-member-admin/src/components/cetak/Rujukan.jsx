import React from 'react';

class Rujukan extends React.Component {
   
   render() {
         return (
            <table>
               <tbody>
                  <tr>
                     <td rowSpan={2}>
                        <img
                           src={require('../config/logo-bpjs.png')}
                           style={{width: 200, height: 'auto',}}
                           alt=""
                        />
                     </td>
                     <td>SURAT RUJUKAN PESERTA</td>
                     <td>No. {Object.keys(this.props.dataRujukan).length === 0 ? '' : this.props.dataRujukan.no_rujukan}</td>
                  </tr>
                  <tr>
                     <td>RS. PERTAMEDIKA UMMI ROSNATI</td>
                     <td>Tgl. {Object.keys(this.props.dataRujukan).length === 0 ? '' : this.props.dataRujukan.tgl_rujukan}</td>
                  </tr>
                  <tr>
                     <td colSpan={3}>
                        <table style={{width: '100%'}}>
                           <tbody>
                              <tr>
                                 <td>Kepada Yth</td>
                                 <td>:</td>
                                 <td>{Object.keys(this.props.dataRujukan).length === 0 ? '' : this.props.dataRujukan.nama_fpk}</td>
                                 <td colSpan={3}></td>
                              </tr>
                              <tr>
                                 <td></td>
                                 <td></td>
                                 <td>{}</td>
                                 <td colSpan={3}></td>
                              </tr>
                              <tr>
                                 <td colSpan={3}>Mohon Pemeriksaan dan Penanganan Lebih Lanjut :</td>
                                 <td colSpan={3}></td>
                              </tr>
                              <tr>
                                 <td>No.Kartu</td>
                                 <td>:</td>
                                 <td>{Object.keys(this.props.dataRujukan).length === 0 ? '' : this.props.dataRujukan.no_kartu}</td>
                                 <td colSpan={3}>
                                    {Object.keys(this.props.dataRujukan).length === 0 ? '' : this.props.dataRujukan.nama_tipe_rujukan}
                                 </td>
                              </tr>
                              <tr>
                                 <td>Nama Peserta</td>
                                 <td>:</td>
                                 <td>{Object.keys(this.props.dataRujukan).length === 0 ? '' : this.props.dataRujukan.nama_pasien}</td>
                                 <td colSpan={3}></td>
                              </tr>
                              <tr>
                                 <td>Tgl.Lahir</td>
                                 <td>:</td>
                                 <td>{Object.keys(this.props.dataRujukan).length === 0 ? '' : this.props.dataRujukan.tgl_lahir}</td>
                                 <td colSpan={3}></td>
                              </tr>
                              <tr>
                                 <td>Diagnosa</td>
                                 <td>:</td>
                                 <td>{Object.keys(this.props.dataRujukan).length === 0 ? '' : this.props.dataRujukan.nama_diagnosis}</td>
                                 <td colSpan={3}></td>
                              </tr>
                              <tr>
                                 <td>Keterangan</td>
                                 <td>:</td>
                                 <td>{Object.keys(this.props.dataRujukan).length === 0 ? '' : this.props.dataRujukan.catatan}</td>
                                 <td colSpan={3}></td>
                              </tr>
                              <tr>
                                 <td colSpan={3}><br />Demikian atas bantuannya,diucapkan banyak terima kasih.</td>
                                 <td colSpan={3}></td>
                              </tr>
                           </tbody>
                        </table>
                     </td>
                  </tr>
                  <tr>
                     <td>
                        * Rujukan Berlaku Sampai Dengan {}<br />
                        * Tgl.Rencana Berkunjung {}
                     </td>
                     <td align="right" colSpan={2}>Mengetahui,</td>
                  </tr>
                  <tr>
                     <td>Tgl.Cetak.{}</td>
                     <td align="right" colSpan={2}>_____________</td>
                  </tr>
               </tbody>
            </table>
        );
    }
}

export default (Rujukan);

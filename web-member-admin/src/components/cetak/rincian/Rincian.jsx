import React from 'react';
import api from 'config/api.js';
import Loading from 'components/Loading.jsx';
import { separator } from 'config/helpers.js';
import BillingLab from 'components/cetak/lab/billing/IndexFull.jsx';
import BillingRad from 'components/cetak/rad/billing/IndexFull.jsx';
import BillingFarmasi from 'components/cetak/farmasi/Pelayanan.jsx';
import { Tag, notification } from 'antd';
import {
  CheckCircleOutlined,
  SyncOutlined,
} from '@ant-design/icons';

class Rincian extends React.Component {

	state = {
		loading: false,
    
		openBillingFarmasi: false,
		openBillingRad: false,
		openBillingLab: false,

		administrasi: {
		total: 0,
		data: [],
		},
		akomodasi: {
		total: 0,
		data: [],
		},
		prosedurNonBedah: {
		total: 0,
		data: [],
		},
		prosedurBedah: {
		total: 0,
		data: [],
		},
		konsultasi: {
		total: 0,
		data: [],
		},
		tenagaAhli: {
		total: 0,
		data: [],
		},
		keperawatan: {
		total: 0,
		data: [],
		},
		penunjang: {
		total: 0,
		data: [],
		},
		pelayananDarah: {
		total: 0,
		data: [],
		},
		rehabilitasi: {
		total: 0,
		data: [],
		},
		alkes: {
		total: 0,
		data: [],
		},
		bmhp: {
		total: 0,
		data: [],
		},
		sewaAlat: {
		total: 0,
		data: [],
		},
		lain: {
		total: 0,
		data: [],
		},
		obat: { total: 0 },
		laboratorium: { total: 0 },
		laboratorium_permintaan: { jml: 0 },
		radiologi: { total: 0 },
		radiologi_permintaan: { jml: 0 },

		totalSeluruh: 0,
	}
	
	componentDidMount() {
		this.fetchRincian();
	}

	fetchRincian = () => {
		this.setState({ loading: true }, () => {
				api.get(`/pasien/rincian/${this.props.data.tipe === 1 ? 'jalan' : this.props.data.tipe === 2 ? 'inap' : ''}/${this.props.data.tipe === 1 ? this.props.data.id_peljalan : this.props.data.tipe === 2 ? this.props.data.id_pelinap : ''}`)
				.then(result => {
          let totalSeluruh = 
						Number(result.data.administrasi.total) +
						Number(result.data.akomodasi.total) +
						Number(result.data.prosedurNonBedah.total) +
						Number(result.data.prosedurBedah.total) +
						Number(result.data.konsultasi.total) +
						Number(result.data.tenagaAhli.total) +
						Number(result.data.keperawatan.total) +
						Number(result.data.penunjang.total) +
						Number(result.data.pelayananDarah.total) +
						Number(result.data.rehabilitasi.total) +
						Number(result.data.alkes.total) +
						Number(result.data.bmhp.total) +
						Number(result.data.sewaAlat.total) +
						Number(result.data.lain.total) +
						Number(result.data.obat.total) +
						Number(result.data.laboratorium.total) +
						Number(result.data.radiologi.total)
					;
			this.setState({ ...result.data, totalSeluruh: totalSeluruh, loading: false });
			if (this.props.total) {
			this.props.total(totalSeluruh);
			}
			if (this.props.akomodasi) {
			this.props.akomodasi(result.data.akomodasi.data);
			}
			if (this.props.validasi) {
			this.props.validasi({
				laboratorium_permintaan: result.data.laboratorium_permintaan,
				radiologi_permintaan: result.data.radiologi_permintaan,
			});
			}
		})
				.catch(error => {
					console.log(error.response);
				});
			});
	}
  
  deleteAdministrasi = (data) => {
    this.setState({ loading: true }, () => {
			api.delete(`/rincian/${this.props.data.tipe === 1 ? 'jalan' : this.props.data.tipe === 2 ? 'inap' : ''}/administrasi/${this.props.data.tipe === 1 ? data.id_rinjalanadministrasi : this.props.data.tipe === 2 ? data.id_rininapadministrasi : ''}`)
			.then(result => {
        this.fetchRincian();
			})
			.catch(error => {
				console.log(error.response);
			});
		});
  }
  
  deleteAkomodasi = (data) => {
    this.setState({ loading: true }, () => {
			api.delete(`/rincian/inap/akomodasi/${data.id_rininapakomodasi}`)
			.then(result => {
        this.fetchRincian();
			})
			.catch(error => {
				console.log(error.response);
			});
		});
  }

  deleteProsedurNonBedah = (data) => {
    this.setState({ loading: true }, () => {
			api.delete(`/rincian/${this.props.data.tipe === 1 ? 'jalan' : this.props.data.tipe === 2 ? 'inap' : ''}/nonop/${this.props.data.tipe === 1 ? data.id_rinjalanno : this.props.data.tipe === 2 ? data.id_rininapno : ''}`)
			.then(result => {
        this.fetchRincian();
			})
			.catch(error => {
				console.log(error.response);
			});
		});
  }

  deleteKonsultasi = (data) => {
    this.setState({ loading: true }, () => {
			api.delete(`/rincian/${this.props.data.tipe === 1 ? 'jalan' : this.props.data.tipe === 2 ? 'inap' : ''}/konsultasi/${this.props.data.tipe === 1 ? data.id_rinjalankonsultasi : this.props.data.tipe === 2 ? data.id_rininapkonsultasi : ''}`)
			.then(result => {
        this.fetchRincian();
			})
			.catch(error => {
        // console.log(error.response.data);
				if (error.response.data === "Cannot delete or update a parent row: a foreign key constraint fails (`rspur3`.`far_permintaan_jalan`, CONSTRAINT `far_permintaan_jalan_RIN_JALAN_KONSULTASI` FOREIGN KEY (`id_rinjalankonsultasi`) REFERENCES `rin_jalan_konsultasi` (`id`))") {
          notification.error({
            message: "Tidak Bisa Dihapus, Obat Sudah Di Resepkan."
          });
        };
        this.setState({ loading: false })
			});
		});
  }

  deleteAhli = (data) => {
    this.setState({ loading: true }, () => {
			api.delete(`/rincian/${this.props.data.tipe === 1 ? 'jalan' : this.props.data.tipe === 2 ? 'inap' : ''}/ahli/${this.props.data.tipe === 1 ? data.id_rinjalanahli : this.props.data.tipe === 2 ? data.id_rininapahli : ''}`)
			.then(result => {
        this.fetchRincian();
			})
			.catch(error => {
				console.log(error.response);
			});
		});
  }

  deleteKeperawatan = (data) => {
    this.setState({ loading: true }, () => {
			api.delete(`/rincian/${this.props.data.tipe === 1 ? 'jalan' : this.props.data.tipe === 2 ? 'inap' : ''}/keperawatan/${this.props.data.tipe === 1 ? data.id_rinjalankeperawatan : this.props.data.tipe === 2 ? data.id_rininapkeperawatan : ''}`)
			.then(result => {
        this.fetchRincian();
			})
			.catch(error => {
				console.log(error.response);
			});
		});
  }

  deletePenunjang = (data) => {
    this.setState({ loading: true }, () => {
			api.delete(`/rincian/${this.props.data.tipe === 1 ? 'jalan' : this.props.data.tipe === 2 ? 'inap' : ''}/penunjang/${this.props.data.tipe === 1 ? data.id_rinjalanpenunjang : this.props.data.tipe === 2 ? data.id_rininappenunjang : ''}`)
			.then(result => {
        this.fetchRincian();
			})
			.catch(error => {
				console.log(error.response);
			});
		});
  }

  deleteDarah = (data) => {
    this.setState({ loading: true }, () => {
			api.delete(`/rincian/${this.props.data.tipe === 1 ? 'jalan' : this.props.data.tipe === 2 ? 'inap' : ''}/darah/${this.props.data.tipe === 1 ? data.id_rinjalandarah : this.props.data.tipe === 2 ? data.id_rininapdarah : ''}`)
			.then(result => {
        this.fetchRincian();
			})
			.catch(error => {
				console.log(error.response);
			});
		});
  }

  deleteRehabilitasi = (data) => {
    this.setState({ loading: true }, () => {
			api.delete(`/rincian/${this.props.data.tipe === 1 ? 'jalan' : this.props.data.tipe === 2 ? 'inap' : ''}/rehabilitasi/${this.props.data.tipe === 1 ? data.id_rinjalanrehabilitasi : this.props.data.tipe === 2 ? data.id_rininaprehabilitasi : ''}`)
			.then(result => {
        this.fetchRincian();
			})
			.catch(error => {
				console.log(error.response);
			});
		});
  }

  deleteAlkes = (data) => {
    this.setState({ loading: true }, () => {
			api.delete(`/rincian/${this.props.data.tipe === 1 ? 'jalan' : this.props.data.tipe === 2 ? 'inap' : ''}/alkes/${this.props.data.tipe === 1 ? data.id_rinjalanalkes : this.props.data.tipe === 2 ? data.id_rininapalkes : ''}`)
			.then(result => {
        this.fetchRincian();
			})
			.catch(error => {
				console.log(error.response);
			});
		});
  }

  deleteBmhp = (data) => {
    this.setState({ loading: true }, () => {
			api.delete(`/rincian/${this.props.data.tipe === 1 ? 'jalan' : this.props.data.tipe === 2 ? 'inap' : ''}/bmhp/${this.props.data.tipe === 1 ? data.id_rinjalanbmhp : this.props.data.tipe === 2 ? data.id_rininapbmhp : ''}`)
			.then(result => {
        this.fetchRincian();
			})
			.catch(error => {
				console.log(error.response);
			});
		});
  }

  deleteAlat = (data) => {
    this.setState({ loading: true }, () => {
			api.delete(`/rincian/${this.props.data.tipe === 1 ? 'jalan' : this.props.data.tipe === 2 ? 'inap' : ''}/alat/${this.props.data.tipe === 1 ? data.id_rinjalanalat : this.props.data.tipe === 2 ? data.id_rininapalat : ''}`)
			.then(result => {
        this.fetchRincian();
			})
			.catch(error => {
				console.log(error.response);
			});
		});
  }
	
	render() {
		return (
			<>
        <div className="relative">
          <div className="w-full">
            <table className="text-sm w-full" cellPadding={3}>
              <tbody>
                <tr>
                  <td colSpan={3} className="bg-gray-200 border border-black">Rincian Rawat {this.props.data.tipe === 1 ? 'Jalan' : this.props.data.tipe === 2 ? 'Inap' : ''}</td>
                </tr>
                <tr>
                  <td className="border border-black" style={{ width: '20%' }}>Jenis Pembayaran</td>
                  <td className="border border-black">Keterangan</td>
                  <td className="border border-black" align="right" style={{ width: '15%' }}>Jumlah</td>
                  {
                    this.props.checkout ?
                      <td align="center" style={{ width: '15%' }}>Status</td>
                    : null
                  }
                </tr>
                {
                  this.state.administrasi.data.length !== 0 ?
                    <tr key="Administrasi">
                      <td className="border border-black">Administrasi</td>
                      <td className="border border-black">
                        {
                          this.state.administrasi.data.map((row, i) => (
                            <span
                              key={i}
                              className="even:bg-gray-200 cursor-pointer hover:bg-red-200"
                              onClick={() => {
                                if (this.props.edit) {
                                  this.deleteAdministrasi(row);
                                }
                              }}
                            >{row.nama_taradministrasi}<br /></span>
                          ))
                        }
                      </td>
                      <td className="border border-black" align="right">
                        {
                          this.state.administrasi.data.map((row, i) => {
                            return (<span key={i}>{separator(row.tarif)}<br /></span>);
                          })
                        }
                      </td>
                    </tr>
                  : null
                }
                {
                  this.state.akomodasi.data.length !== 0 ?
                    <tr key="Akomodasi">
                      <td className="border border-black">Akomodasi</td>
                      <td className="border border-black">
                        {
                          this.state.akomodasi.data.map((row, i) => (
                            <div
                              key={i}
                              className="even:bg-gray-200 cursor-pointer hover:bg-red-200"
                              onClick={() => {
                                if (this.props.edit && this.props.aktifAkomodasi) {
                                  this.deleteAkomodasi(row);
                                }
                              }}
                            >
                              <div><b>Tgl. Checkin: {row.tgl}</b> | Tgl. Checkout: {row.tgl_checkout}</div>
                              <div>Nama Ruangan: {row.nama_pelruangan} - {row.nama_pelruangankamar} - {row.nama_pelruangankamarbed} | {row.total_hari} Hari</div>
                            </div>
                          ))
                        }
                      </td>
                      <td className="border border-black" align="right">
                        {
                          this.state.akomodasi.data.map((row, i) => {
                            return (
                              <div
                                key={i}
                                className="even:bg-gray-200 cursor-pointer hover:bg-red-200"
                              >{separator(row.total)}</div>
                            );
                          })
                        }
                      </td>
                    </tr>
                  : null
                }
                {
                  this.state.prosedurNonBedah.data.length !== 0 ?
                    <tr key="Prosedur Non Bedah">
                      <td className="border border-black">Prosedur Non Bedah</td>
                      <td className="border border-black">
                        {
                          this.state.prosedurNonBedah.data.map((row, i) => (
                            <div
                              key={i}
                              className="even:bg-gray-200 cursor-pointer hover:bg-red-200"
                              onClick={() => {
                                if (this.props.edit) {
                                  this.deleteProsedurNonBedah(row);
                                }
                              }}
                            >{row.nama_tarno}</div>
                          ))
                        }
                      </td>
                      <td className="border border-black" align="right">
                        {
                          this.state.prosedurNonBedah.data.map((row, i) => {
                            return (
                              <div
                                key={i}
                                className="even:bg-gray-200 cursor-pointer hover:bg-red-200"
                              >{separator(row.tarif)}</div>
                            );
                          })
                        }
                      </td>
                    </tr>
                  : null
                }
                {
                  <tr key="Prosedur Bedah">
                    <td className="border border-black">Prosedur Bedah</td>
                    <td className="border border-black">
                      {
                        this.state.prosedurBedah.data.map((row, i) => (
                          <table
                            key={i}
                            className="even:bg-gray-200 cursor-pointer hover:bg-red-200 w-full"
                          >
                            <tbody>
                              <tr>
                                <td>{i+1}.</td>
                                <td>{row.nama_taro}</td>
                              </tr>
                              {
                                row.nama_operator !== '' ?
                                  <tr>
                                    <td></td>
                                    <td>Operator: {row.nama_operator}</td>
                                  </tr>
                                : null
                              }
                              {
                                row.nama_asi_operator1 !== '' ?
                                  <tr>
                                    <td></td>
                                    <td>Asisten Operator 1: {row.nama_asi_operator1}</td>
                                  </tr>
                                : null
                              }
                              {
                                row.nama_asi_operator2 !== '' ?
                                  <tr>
                                    <td></td>
                                    <td>Asisten Operator 2: {row.nama_asi_operator2}</td>
                                  </tr>
                                : null
                              }
                              {
                                row.nama_anestesi !== '' ?
                                  <tr>
                                    <td></td>
                                    <td>Anestesi: {row.nama_anestesi}</td>
                                  </tr>
                                : null
                              }
                              {
                                row.nama_asi_anestesi !== '' ?
                                  <tr>
                                    <td></td>
                                    <td>Asisten Anestesi: {row.nama_asi_anestesi}</td>
                                  </tr>
                                : null
                              }
                            </tbody>
                          </table>
                        ))
                      }
                    </td>
                    <td className="border border-black" align="right">
                      {
                        this.state.prosedurBedah.data.map((row, i) => {
                          return (
                            <div
                              key={i}
                            >{separator(row.tarif)}</div>
                          );
                        })
                      }
                    </td>
                    {/* {
                      this.props.checkout ?
                        <td align="center">
                          <Tag icon={<CheckCircleOutlined />} color="success">
                            selesai
                          </Tag>
                          <Tag icon={<SyncOutlined spin />} color="error">
                            proses
                          </Tag>
                        </td>
                      : null
                    } */}
                  </tr>
                }
                {
                  this.state.konsultasi.data.length !== 0 ?
                    <tr key="Konsultasi">
                      <td className="border border-black">Konsultasi</td>
                      <td className="border border-black">
                        {
                          this.state.konsultasi.data.map((row, i) => (
                            <div
                              key={i}
                              className="even:bg-gray-200 cursor-pointer hover:bg-red-200"
                              onClick={() => {
                                if (this.props.edit) {
                                  this.deleteKonsultasi(row);
                                }
                              }}
                            >{row.nama_tarkonsultasi} [{row.nama_dokter}] {this.props.data.tipe === 1 ? `[Poli: ${row.nama_pelpoli}]` : ""}</div>
                          ))
                        }
                      </td>
                      <td className="border border-black" align="right">
                        {
                          this.state.konsultasi.data.map((row, i) => {
                            return (<div key={i}>{separator(row.tarif)}</div>);
                          })
                        }
                      </td>
                    </tr>
                  : null
                }
                {
                  this.state.tenagaAhli.data.length !== 0 ?
                    <tr key="Tenaga Ahli">
                      <td className="border border-black">Tenaga Ahli</td>
                      <td className="border border-black">
                        {
                          this.state.tenagaAhli.data.map((row, i) => (
                            <div
                              key={i}
                              className="even:bg-gray-200 cursor-pointer hover:bg-red-200"
                              onClick={() => {
                                if (this.props.edit) {
                                  this.deleteAhli(row);
                                }
                              }}
                            >{row.nama_tarahli}</div>
                          ))
                        }
                      </td>
                      <td className="border border-black" align="right">
                        {
                          this.state.tenagaAhli.data.map((row, i) => {
                            return (<div key={i}>{separator(row.tarif)}</div>);
                          })
                        }
                      </td>
                    </tr>
                  : null
                }
                {
                  this.state.keperawatan.data.length !== 0 ?
                    <tr key="Keperawatan">
                      <td className="border border-black">Keperawatan</td>
                      <td className="border border-black">
                        {
                          this.state.keperawatan.data.map((row, i) => (
                            <div
                              key={i}
                              className="even:bg-gray-200 cursor-pointer hover:bg-red-200"
                              onClick={() => {
                                if (this.props.edit) {
                                  this.deleteKeperawatan(row);
                                }
                              }}
                            >{row.nama_tarkeperawatan}</div>
                          ))
                        }
                      </td>
                      <td className="border border-black" align="right">
                        {
                          this.state.keperawatan.data.map((row, i) => {
                            return (<div key={i}>{separator(row.tarif)}</div>);
                          })
                        }
                      </td>
                    </tr>
                  : null
                }
                {
                  this.state.penunjang.data.length !== 0 ?
                    <tr key="Penunjang">
                      <td className="border border-black">Penunjang</td>
                      <td className="border border-black">
                        {
                          this.state.penunjang.data.map((row, i) => (
                            <div
                              key={i}
                              className="even:bg-gray-200 cursor-pointer hover:bg-red-200"
                              onClick={() => {
                                if (this.props.edit) {
                                  this.deletePenunjang(row);
                                }
                              }}
                            >{row.nama_tarpenunjang}</div>
                          ))
                        }
                      </td>
                      <td className="border border-black" align="right">
                        {
                          this.state.penunjang.data.map((row, i) => {
                            return (<div key={i}>{separator(row.tarif)}</div>);
                          })
                        }
                      </td>
                    </tr>
                  : null
                }
                {
                  this.state.pelayananDarah.data.length !== 0 ?
                    <tr key="Pelayanan Darah">
                      <td className="border border-black">Pelayanan Darah</td>
                      <td className="border border-black">
                        {
                          this.state.pelayananDarah.data.map((row, i) => (
                            <div
                              key={i}
                              className="even:bg-gray-200 cursor-pointer hover:bg-red-200"
                              onClick={() => {
                                if (this.props.edit) {
                                  this.deleteDarah(row);
                                }
                              }}
                            >{row.nama_tardarah}</div>
                          ))
                        }
                      </td>
                      <td className="border border-black" align="right">
                        {
                          this.state.pelayananDarah.data.map((row, i) => {
                            return (<div key={i}>{separator(row.tarif)}</div>);
                          })
                        }
                      </td>
                    </tr>
                  : null
                }
                {
                  this.state.rehabilitasi.data.length !== 0 ?
                    <tr key="Rehabilitasi">
                      <td className="border border-black">Rehabilitasi</td>
                      <td className="border border-black">
                        {
                          this.state.rehabilitasi.data.map((row, i) => (
                            <div
                              key={i}
                              className="even:bg-gray-200 cursor-pointer hover:bg-red-200"
                              onClick={() => {
                                if (this.props.edit) {
                                  this.deleteRehabilitasi(row);
                                }
                              }}
                            >{row.nama_tarrehabilitasi}</div>
                          ))
                        }
                      </td>
                      <td className="border border-black" align="right">
                        {
                          this.state.rehabilitasi.data.map((row, i) => {
                            return (<div key={i}>{separator(row.tarif)}</div>);
                          })
                        }
                      </td>
                    </tr>
                  : null
                }
                {
                  this.state.alkes.data.length !== 0 ?
                    <tr key="Alkes">
                      <td className="border border-black">Alkes</td>
                      <td className="border border-black">
                        {
                          this.state.alkes.data.map((row, i) => (
                            <div
                              key={i}
                              className="even:bg-gray-200 cursor-pointer hover:bg-red-200"
                              onClick={() => {
                                if (this.props.edit) {
                                  this.deleteAlkes(row);
                                }
                              }}
                            >{row.nama_taralkes}</div>
                          ))
                        }
                      </td>
                      <td className="border border-black" align="right">
                        {
                          this.state.alkes.data.map((row, i) => {
                            return (<div key={i}>{separator(row.tarif)}</div>);
                          })
                        }
                      </td>
                    </tr>
                  : null
                }
                {
                  this.state.bmhp.data.length !== 0 ?
                    <tr key="BMHP">
                      <td className="border border-black">BMHP</td>
                      <td className="border border-black">
                        {
                          this.state.bmhp.data.map((row, i) => (
                            <div
                              key={i}
                              className="even:bg-gray-200 cursor-pointer hover:bg-red-200"
                              onClick={() => {
                                if (this.props.edit) {
                                  this.deleteBmhp(row);
                                }
                              }}
                            >{row.nama_tarbmhp}</div>
                          ))
                        }
                      </td>
                      <td className="border border-black" align="right">
                        {
                          this.state.bmhp.data.map((row, i) => {
                            return (<div key={i}>{separator(row.tarif)}</div>);
                          })
                        }
                      </td>
                    </tr>
                  : null
                }
                {
                  this.state.sewaAlat.data.length !== 0 ?
                    <tr key="Sewa Alat">
                      <td className="border border-black">Sewa Alat</td>
                      <td className="border border-black">
                        {
                          this.state.sewaAlat.data.map((row, i) => (
                            <div
                              key={i}
                              className="even:bg-gray-200 cursor-pointer hover:bg-red-200"
                              onClick={() => {
                                if (this.props.edit) {
                                  this.deleteAlat(row);
                                }
                              }}
                            >{row.nama_taralat}</div>
                          ))
                        }
                      </td>
                      <td className="border border-black" align="right">
                        {
                          this.state.sewaAlat.data.map((row, i) => {
                            return (<div key={i}>{separator(row.tarif)}</div>);
                          })
                        }
                      </td>
                    </tr>
                  : null
                }
                <tr key="Obat / Farmasi">
                  <td className="border border-black">Obat / Farmasi</td>
                  <td className="border border-black"></td>
                  <td className="border border-black" align="right">
                    <div
                      className="cursor-pointer hover:bg-red-600 hover:text-white hover:p-5"
                      onClick={() => { this.setState({ openBillingFarmasi: true }) }}
                    >{separator(this.state.obat.total)}</div>
                  </td>
                </tr>
                <tr key="Laboratorium">
                  <td className="border border-black">Laboratorium</td>
                  <td className="border border-black"></td>
                  <td className="border border-black" align="right">
                    <div
                      className="cursor-pointer hover:bg-red-600 hover:text-white hover:p-5"
                      onClick={() => { this.setState({ openBillingLab: true }) }}
                    >{separator(this.state.laboratorium.total)}</div>
                  </td>
                  {
                    this.props.checkout ?
                      <td align="center">
                        {
                          this.state.laboratorium_permintaan.jml === 0 ?
                            <Tag icon={<CheckCircleOutlined />} color="success">
                              selesai
                            </Tag>
                          :
                            <Tag icon={<SyncOutlined spin />} color="error">
                              proses
                            </Tag>
                        }
                      </td>
                    : null
                  }
                </tr>
                <tr key="Radiologi">
                  <td className="border border-black">Radiologi</td>
                  <td className="border border-black" align="center"></td>
                  <td className="border border-black" align="right">
                    <div
                      className="cursor-pointer hover:bg-red-600 hover:text-white hover:p-5"
                      onClick={() => { this.setState({ openBillingRad: true }) }}
                    >{separator(this.state.radiologi.total)}</div>
                  </td>
                  {
                    this.props.checkout ?
                      <td align="center">
                        {
                          this.state.radiologi_permintaan.jml === 0 ?
                            <Tag icon={<CheckCircleOutlined />} color="success">
                              selesai
                            </Tag>
                          :
                            <Tag icon={<SyncOutlined spin />} color="error">
                              proses
                            </Tag>
                        }
                      </td>
                    : null
                  }
                </tr>
                <tr key="Lain - Lain">
                  <td className="border border-black">Lain - Lain</td>
                  <td className="border border-black">
                    {
                      this.state.lain.data.map((row, i) => (
                        <span key={i}>{row.keterangan}<br /></span>
                      ))
                    }
                  </td>
                  <td className="border border-black" align="right">
                    {
                      this.state.lain.data.map((row, i) => {
                        return (<span key={i}>{separator(row.jumlah)}<br /></span>);
                      })
                    }
                  </td>
                </tr>
                <tr key="Total Rawat">
                  <td className="border border-black" colSpan={2} align="right"><b>Total Rawat {this.props.data.tipe === 1 ? 'Jalan' : this.props.data.tipe === 2 ? 'Inap' : ''}</b></td>
                  <td className="border border-black" align="right">
                    <b>{separator(this.state.totalSeluruh)}</b>
                  </td>
                </tr>
                {
                  this.props.checkout ?
                    <tr><td></td></tr>
                  : null
                }
              </tbody>
            </table>
          </div>
          {
            this.state.loading ?
              <Loading />
            : null
          }
        </div>

        {
          this.state.openBillingLab ?
            <BillingLab
              data={this.props.data}
              close={() => { this.setState({ openBillingLab: false }); }}
            />
          : null
        }
        {
          this.state.openBillingRad ?
            <BillingRad
              data={this.props.data}
              close={() => { this.setState({ openBillingRad: false }); }}
            />
          : null
        }
        {
          this.state.openBillingFarmasi ?
            <BillingFarmasi
              close={() => { this.setState({ openBillingFarmasi: false }); }}
              id_farresep={this.props.data.tipe}
              id_pelinap={this.props.data.id_pelinap}
              id_peljalan={this.props.data.id_peljalan}
            />
          : null
        }

			</>
		)
	}

}

export default Rincian;

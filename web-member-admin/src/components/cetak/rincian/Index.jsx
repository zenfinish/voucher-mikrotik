import React from 'react';
import { notification, Modal } from 'antd';
import ReactToPrint from 'react-to-print';
import KopSurat from 'components/KopSurat.jsx';
import Button from 'components/inputs/Button.jsx';
import api from 'config/api.js';
import { tglIndo, separator } from 'config/helpers.js';
import Rincian from './Rincian.jsx';

class Index extends React.Component {

	state = {
		loading: false,
		
		total_jalan: 0,
		total_inap: 0,

		kasir_asuransi_disetujui: 0,

		kasir_klaim_kelas_daftar: 0,
		kasir_klaim_naik_kelas: 0,
		kasir_klaim_naik_kelas_vip: 0,

		kasir_diskon: 0,

		nama_checkoutuser: '',
	}

	componentDidMount() {
		// console.log("masuk=======", this.props.data)
		this.setState({ loading: true }, () => {
			api.get(`/pasien/${this.props.data.tipe === 1 ? 'jalan' : this.props.data.tipe === 2 ? 'inap' : ''}/${this.props.data.tipe === 1 ? this.props.data.id_peljalan : this.props.data.tipe === 2 ? this.props.data.id_pelinap : ''}`)
			.then(result => {
				this.setState({
					kasir_asuransi_disetujui: result.data.kasir_jml,
					kasir_klaim_kelas_daftar: result.data.kasir_klaim_kelas_daftar,
					kasir_klaim_naik_kelas: result.data.kasir_klaim_naik_kelas,
					kasir_klaim_naik_kelas_vip: result.data.kasir_klaim_naik_kelas_vip,
					kasir_diskon: result.data.kasir_diskon,
					nama_checkoutuser: result.data.nama_checkoutuser,
					loading: false,
				});
			})
			.catch(error => {
				console.log(error.response);
			});
		});
	}

	totalBayar = () => {
		if (this.props.data.id_pelcabar === 1) {
			return (this.state.total_jalan + this.state.total_inap) - this.state.kasir_diskon;
		} else if (this.props.data.id_pelcabar === 2) {
			if (this.state.kasir_klaim_naik_kelas_vip === 0) {
				return (this.state.kasir_klaim_naik_kelas - this.state.kasir_klaim_kelas_daftar) - this.state.kasir_diskon;
			} else {
				return ((this.state.kasir_klaim_naik_kelas * 75) / 100) - this.state.kasir_diskon;
			}
		} else {
			return ((this.state.total_jalan + this.state.total_inap) - this.state.kasir_asuransi_disetujui) - this.state.kasir_diskon;
		}
	}
	
	render() {
		return (
			<>
				<Modal
					centered
					width="100%"
					visible={true}
					onCancel={this.props.close}
					footer={[
						<div key="simpan">
							{
								this.props.kasir && this.props.data.tipe === 1 ?
									<Button onClick={() => {
										api.delete(`/pelayanan/jalan/${this.props.data.id_peljalan}`)
										.then(result => {
											this.props.closeRefresh();
										})
										.catch(error => {
											notification.error({
												message: 'Error',
												description: JSON.stringify(error.response.data),
										 });
											this.setState({ loading: false });
										});
									}}>Hapus Rawat Jalan</Button>
								: null
							}
							{
								this.props.kasir ?
									<Button onClick={() => {
										this.setState({ loading: true }, () => {
											api.post(`/keuanganX/kasir/${
												this.props.data.tipe === 1 ? 'jalan' :
												this.props.data.tipe === 2 ? 'inap' :
												''
											}/${
												this.props.data.id_pelcabar === 1 && this.props.data.tipeKasir === 'tunai' ? 'tunai' :
												this.props.data.id_pelcabar === 2 && this.props.data.tipeKasir === 'bpjscosharing' ? 'bpjscosharing' :
												this.props.data.id_pelcabar !== 1 && this.props.data.id_pelcabar !== 2 && this.props.data.tipeKasir === 'asuransi' ? 'asuransi' :
												''
											}`, {
												id_peljalan: this.props.data.id_peljalan,
												id_pelinap: this.props.data.id_pelinap,
												id_pelkelas: this.props.data.id_pelkelas,
												kasir_jml: 
													this.props.data.id_pelcabar !== 1 && this.props.data.id_pelcabar !== 2 && this.props.data.tipeKasir === 'asuransi' ? this.state.kasir_asuransi_disetujui :
													this.totalBayar(),
												kasir_jml_cosharing : this.totalBayar(),
												kasir_klaim_kelas_daftar: this.state.kasir_klaim_kelas_daftar,
												kasir_klaim_naik_kelas: this.state.kasir_klaim_naik_kelas,
												kasir_klaim_naik_kelas_vip: this.state.kasir_klaim_naik_kelas_vip,
												kasir_diskon: this.state.kasir_diskon,
												nama_pelpasien: this.props.data.nama_pelpasien,
												no_rekmedis: this.props.data.no_rekmedis,
											})
											.then(result => {
												this.props.closeRefresh();
											})
											.catch(error => {
												console.log('Error: ', error.response);
												notification.error({
													message: 'Error',
													description: JSON.stringify(error.response.data),
											 });
												this.setState({ loading: false });
											});
										});
									}} loading={this.state.loading}>Simpan Kasir</Button>
								: null
							}
							<ReactToPrint
								trigger={() => (
									<Button color="primary">Print</Button>
								)}
								content={() => this.componentRef}
							/>
							<Button onClick={this.props.close}>Close</Button>
						</div>
					]}
				>
					<div ref={print => (this.componentRef = print)}>
						<KopSurat />
						<h6>BLANGKO PERINCIAN RETRIBUSI {this.props.data.tipe === 1 ? 'RAWAT JALAN' : this.props.data.tipe === 2 ? 'RAWAT INAP' : ''}</h6>
						<div className="relative">
							<div className="w-full">
								<table className="text-sm mb-2 mt-2 w-full">
									<tbody>
										<tr>
											<td>Nama Pasien</td>
											<td>:</td>
											<td>{this.props.data.nama_pelpasien}</td>
											<td>No. Checkin</td>
											<td>:</td>
											<td>{this.props.data.tipe === 1 ? this.props.data.id_peljalan : this.props.data.tipe === 2 ? this.props.data.id_pelinap : ''}</td>
										</tr>
										<tr>
											<td>No. Rekmedis</td>
											<td>:</td>
											<td>{this.props.data.no_rekmedis}</td>
											<td>Tgl Checkin</td>
											<td>:</td>
											<td>{tglIndo(this.props.data.tgl_checkin)}</td>
										</tr>
										<tr>
											<td>Tgl Checkout</td>
											<td>:</td>
											<td>{tglIndo(this.props.data.tipe === 1 ? this.props.data.tgl_checkin : this.props.data.tipe === 2 ? this.props.data.tgl_checkout : '')}</td>
											<td>Cara Bayar</td>
											<td>:</td>
											<td>{this.props.data.nama_pelcabar}</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div className="mb-2">
							<Rincian
								data={{ tipe: 1, id_peljalan: this.props.data.id_peljalan }}
								total={(data) => {
									this.setState({ total_jalan: data });
								}}
							/>
						</div>
						{
							this.props.data.tipe === 2 ?
								<Rincian
									data={this.props.data}
									total={(data) => {
										this.setState({ total_inap: data });
									}}
								/>
							: null
						}
						{
							this.props.data.id_pelcabar === 2 && this.totalBayar() === 0 ?
								<div colSpan={2} className="mb-2" style={{ fontSize: '10px' }}>*Catatan : Pasien diatas telah dijamin sepenuhnya biaya pengobatan selama di {process.env.REACT_APP_NAMA_RS} oleh JKN (Jaminan Kesehatan Nasional) KIS</div>
							: null
						}
						<table className="text-sm" cellPadding={3}>
							<tbody>
								<tr>
									<td><b>TOTAL SELURUH RINCIAN</b></td>
									<td><b>Rp. {separator(this.state.total_jalan + this.state.total_inap)}</b></td>
								</tr>
								{
									this.props.data.id_pelcabar !== 1 && this.props.data.id_pelcabar !== 2 ?
										<>
											<tr>
												<td>Biaya Disetujui</td>
												<td>
													<input
														type="number"
														placeholder="0"
														onChange={(e) => { this.setState({ kasir_asuransi_disetujui: e.target.value }); }}
														value={this.state.kasir_asuransi_disetujui}
													/>
												</td>
											</tr>
										</>
									: null
								}
								{
									this.props.data.id_pelcabar === 2 && this.props.data.tipe === 2 ?
										<>
											<tr>
												<td>Jumlah Klaim BPJS Kelas Daftar</td>
												<td>
													<input
														type="number"
														placeholder="0"
														onChange={
															(e) => {
																this.setState({ kasir_klaim_kelas_daftar: e.target.value });
															}
														}
														className="border border-black"
														disabled={this.state.kasir_klaim_naik_kelas_vip === 1 ? true : false}
														value={this.state.kasir_klaim_kelas_daftar}
													/>
												</td>
											</tr>
											<tr>
												<td>Jumlah Klaim Bpjs Naik Kelas</td>
												<td>
													<input
														type="number"
														placeholder="0"
														onChange={
															(e) => {
																this.setState({ kasir_klaim_naik_kelas: e.target.value });
															}
														}
														className="border border-black mr-1"
														value={this.state.kasir_klaim_naik_kelas}
													/>
													<input
														type="checkbox"
														checked={this.state.kasir_klaim_naik_kelas_vip === 0 ? false : this.state.kasir_klaim_naik_kelas_vip === 1 ? true : false}
														onChange={
															() => {
																if (this.state.kasir_klaim_naik_kelas_vip === 0) {
																	this.setState({ kasir_klaim_naik_kelas_vip: 1 });
																} else {
																	this.setState({ kasir_klaim_naik_kelas_vip: 0 });
																}
															}
														}
													/> 75% Klaim Bpjs Naik Kelas
												</td>
											</tr>
										</>
									: null
								}
								<tr>
									<td>Diskon</td>
									<td>
										<input
											type="number"
											placeholder="0"
											onChange={(e) => { this.setState({ kasir_diskon: e.target.value }); }}
											value={this.state.kasir_diskon}
											className="border border-black mr-1"
										/>
									</td>
								</tr>
								<tr>
									<td><b>TOTAL BAYAR</b></td>
									<td><b>Rp. {separator(this.totalBayar())}</b></td>
								</tr>
							</tbody>
						</table>
						<div className="text-sm text-right">Petugas Checkout,<br />{this.state.nama_checkoutuser}</div>
					</div>
				</Modal>
			</>
		)
	}

}

export default Index;

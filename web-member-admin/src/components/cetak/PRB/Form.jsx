import React from 'react';
import Loading from 'components/Loading.jsx';
import { tglIndo } from 'config/helpers.js';

class Form extends React.Component {
   
    render() {
      return (
         <>
            {/* <style>{`
                    table td {
                        border: 1px solid black;
                    }
                `}</style> */}
            <div className="overflow-auto relative text-sm w-full">
                <table ref={el => (this.componentRef = el)} className="w-full">
                    <tbody>
                        <tr>
                            <td colSpan={4}>
                                <div className="flex justify-between">
                                    <div className="flex gap-2">
                                        <img
                                            src={require('assets/imgs/logo-bpjs.png')}
                                            style={{width: 200, height: 'auto',}}
                                            alt=""
                                        />
                                        <div>
                                            <div>SURAT RUJUK BALIK (PRB)</div>
                                            <div>RS. PERTAMEDIKA UMMI ROSNATI</div>
                                        </div>
                                    </div>
                                    <div>
                                        <div>No. SRB. {this.props.data.noSRB}</div>
                                        <div>Tanggal. {tglIndo(this.props.data.tglSRB)}</div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Kepada Yth</td>
                            <td colSpan={3}>:</td>
                        </tr>
                        <tr>
                            <td colSpan={3}></td>
                            <td>R/.</td>
                        </tr>
                        <tr>
                            <td colSpan={3}>Mohon Pemeriksaan dan Penanganan Lebih Lanjut :</td>
                            <td rowSpan={8} valign="top" colSpan={1}>
                                <table className="w-full">
                                    <tbody>
                                        {
                                            this.props.data.obat.obat.map((row, i) => (
                                                <tr key={i}>
                                                    <td>{i+1}</td>
                                                    <td>{row.signa1}x{row.signa2}</td>
                                                    <td>{row.nmObat}</td>
                                                    <td>{row.jmlObat}</td>
                                                </tr>
                                            ))
                                        }
                                        <tr><td></td></tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>No. Kartu</td>
                            <td>:</td>
                            <td>{this.props.data.peserta.noKartu}</td>
                        </tr>
                        <tr>
                            <td>Nama Peserta</td>
                            <td>:</td>
                            <td>{this.props.data.peserta.nama}</td>
                        </tr>
                        <tr>
                            <td>Tgl. Lahir</td>
                            <td>:</td>
                            <td>{tglIndo(this.props.data.peserta.tglLahir)}</td>
                        </tr>
                        <tr>
                            <td>Diagnosa</td>
                            <td>:</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Program PRB</td>
                            <td>:</td>
                            <td>{this.props.data.programPRB.nama}</td>
                        </tr>
                        <tr>
                            <td>Keterangan</td>
                            <td>:</td>
                            <td>{this.props.data.keterangan}</td>
                        </tr>
                        <tr>
                            <td colSpan={3}>Saran Pengelolaan lanjutan di FKTP :</td>
                        </tr>
                        <tr>
                            <td colSpan={3}>{this.props.data.saran}</td>
                        </tr>
                        <tr>
                            <td colSpan={3}>Demikian atas bantuannya, diucapkan banyak terima kasih.</td>
                            <td>Mengetahui,</td>
                        </tr>
                        <tr>
                            <td colSpan={3} style={{ fontSize: '12px' }}>
                                Tgl. Cetak : {new Date().toISOString().substr(0, 10)} {new Date().getHours()}{new Date().toISOString().substr(13, 6)}
                            </td>
                            <td>____________________</td>
                        </tr>
                    </tbody>
                </table>
                {
                    this.props.loading ?
                    <Loading />
                    : null
                }
            </div>
         </>
      );
   }
}

export default (Form);

import React from 'react';
import { notification, Modal } from 'antd';
import api from 'config/api.js';
import Button from 'components/inputs/Button.jsx';
import ReactToPrint from 'react-to-print';
import Form from "./Form";

class index extends React.Component {
   
   state = {
      loading: false,
      data: {
         DPJP: {
            kode: "",
            nama: ""
         },
         noSEP: "",
         noSRB: "",
         obat: {
            obat: [],
         },
         peserta: {
            alamat: "",
            asalFaskes: {
               kode: "",
               nama: ""
            },
            email: "",
            kelamin: "",
            nama: "",
            noKartu: "",
            noTelepon: "",
            tglLahir: ""
         },
         programPRB: {
            kode: "",
            nama: ""
         },
         keterangan: "",
         saran: "",
         tglSRB: "",
      }
   }
   
   componentDidMount() {
      this.setState({ loading: true }, () => {
			api.get(`/bpjs/prb/data/nomor`, {
            headers: {
               noSEP: this.props.data.noSEP,
               noSRB: this.props.data.noSRB,
            },
         })
			.then(result => {
            this.setState({ data: result.data, loading: false });
			})
			.catch(error => {
            notification.error({
               message: 'Error',
               description: JSON.stringify(error.response.data),
            });
            this.props.close();
			});
		});
	}
   
   render() {
      return (
         <>
            <Modal
               centered
               width="100%"
               visible={true}
               onCancel={this.props.close}
               footer={[
                  <ReactToPrint
                     trigger={() => (
                        <Button color="primary">Print</Button>
                     )}
                     content={() => this.componentRef}
                     key="simpan"
                  />
               ]}
               title={`Cetak PRB`}
            >
               <Form
                    data={this.state.data}
                    loading={this.state.loading}
                    ref={print => (this.componentRef = print)}
               />
            </Modal>
         </>
      );
   }
}

export default (index);

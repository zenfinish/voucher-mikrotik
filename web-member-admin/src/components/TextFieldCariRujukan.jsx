import React from 'react';
import TextField from '@material-ui/core/TextField';

class TextFieldCariRujukan extends React.Component {

   render() {
      return (
         <TextField
            label="Ketik Nomor Rujukan"
            name="cari_rujukan"
            type="text" variant="outlined"
            value={this.props.value}
            fullWidth
            onChange={this.props.onChange}
            InputLabelProps={{ shrink: true }}
            autoComplete="off"
            style={{height: '40px'}}
         />
      );
   }

}

export default TextFieldCariRujukan;

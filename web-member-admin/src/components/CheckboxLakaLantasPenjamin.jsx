import React from 'react';
import { FormGroup, FormControlLabel, Checkbox } from '@material-ui/core';

class SelectAgama extends React.Component {

   render() {
      return (
         <FormGroup row>
            <FormControlLabel
               control={
                  <Checkbox
                     onChange={this.props.onChange}
                     value="1"
                     color="primary"
							name="laka_lantas_penjamin"
							disabled={this.props.disabled}
                  />
               }
               label="Jasa raharja"
            />
            <FormControlLabel
               control={
                  <Checkbox
                     onChange={this.props.onChange}
                     value="2"
                     color="primary"
							name="laka_lantas_penjamin"
							disabled={this.props.disabled}
                  />
               }
               label="BPJS Ketenagakerjaan"
            />
            <FormControlLabel
               control={
                  <Checkbox
                     onChange={this.props.onChange}
                     value="3"
                     color="primary"
							name="laka_lantas_penjamin"
							disabled={this.props.disabled}
                  />
               }
               label="TASPEN PT"
            />
            <FormControlLabel
               control={
                  <Checkbox
                     onChange={this.props.onChange}
                     value="4"
                     color="primary"
							name="laka_lantas_penjamin"
							disabled={this.props.disabled}
                  />
               }
               label="ASABRI PT"
            />
         </FormGroup>
      );
   }

}

export default SelectAgama;

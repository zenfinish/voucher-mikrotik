import { atom } from 'recoil';

export default atom({
	key: 'userState',
	default: {
		id: null,
		username: null,
		name: null,
		whatsapp: null,
	},
});

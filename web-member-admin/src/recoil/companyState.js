import { atom } from 'recoil';

export default atom({
	key: 'companyState',
	default: {
		id: null,
		name: null,
        tgl_jt: null,
        tgl_invoice: null,
	},
});

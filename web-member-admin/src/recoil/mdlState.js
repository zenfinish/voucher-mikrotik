import { atom } from 'recoil';

export default atom({
	key: 'mdlState',
	default: [],
});

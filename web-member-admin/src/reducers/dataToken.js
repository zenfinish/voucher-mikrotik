const defaultState = {
	dataToken: {},
	loading: false,
	error: false,
};

export default function(state = defaultState, action) {
	switch (action.type) {
		case 'CEK_TOKEN_LOADING':
			return {
				...state,
				loading: true,
			};
		case 'CEK_TOKEN_SUCCESS':
			return {
				...state,
				loading: false,
				dataToken: action.payload,
			};
		case 'CEK_TOKEN_ERROR':
			return {
				...state,
				loading: false,
				error: true,
				dataToken: {},
			};
		default: 
			return state;
	}
};
import React from "react";

import AppleFilled from '@ant-design/icons/lib/icons/AppleFilled';
import SnippetsFilled from '@ant-design/icons/lib/icons/SnippetsFilled';
import HeartFilled from '@ant-design/icons/lib/icons/HeartFilled';
import TrademarkCircleFilled from '@ant-design/icons/lib/icons/TrademarkCircleFilled';
import InsuranceFilled from '@ant-design/icons/lib/icons/InsuranceFilled';
import FunnelPlotFilled from '@ant-design/icons/lib/icons/FunnelPlotFilled';
import FileDoneOutlined from '@ant-design/icons/lib/icons/FileDoneOutlined';
import TeamOutlined from '@ant-design/icons/lib/icons/TeamOutlined';
import BarChartOutlined from '@ant-design/icons/lib/icons/BarChartOutlined';
import CalendarOutlined from '@ant-design/icons/lib/icons/CalendarOutlined';
import ZoomOutOutlined from '@ant-design/icons/lib/icons/ZoomOutOutlined';
import FileExcelOutlined from '@ant-design/icons/lib/icons/FileExcelOutlined';
import MailOutlined from '@ant-design/icons/lib/icons/MailOutlined';
import KeyOutlined from '@ant-design/icons/lib/icons/KeyOutlined';
import SettingOutlined from '@ant-design/icons/lib/icons/SettingOutlined';

const icon = (name) => {
  if (name === 'SnippetsFilled') {
    return <SnippetsFilled className="text-xl" />
  } else if (name === 'HeartFilled') {
    return <HeartFilled className="text-xl" />
  } else if (name === 'TrademarkCircleFilled') {
    return <TrademarkCircleFilled className="text-xl" />
  } else if (name === 'InsuranceFilled') {
    return <InsuranceFilled className="text-xl" />
  } else if (name === 'FunnelPlotFilled') {
    return <FunnelPlotFilled className="text-xl" />
  } else if (name === 'FileDoneOutlined') {
    return <FileDoneOutlined className="text-xl" />
  } else if (name === 'AppleFilled') {
    return <AppleFilled className="text-xl" />
  } else if (name === 'TeamOutlined') {
    return <TeamOutlined className="text-xl" />
  } else if (name === 'BarChartOutlined') {
    return <BarChartOutlined className="text-xl" />
  } else if (name === 'CalendarOutlined') {
    return <CalendarOutlined className="text-xl" />
  } else if (name === 'ZoomOutOutlined') {
    return <ZoomOutOutlined className="text-xl" />
  } else if (name === 'FileExcelOutlined') {
    return <FileExcelOutlined className="text-xl" />
  } else if (name === 'MailOutlined') {
    return <MailOutlined />
  } else if (name === 'KeyOutlined') {
    return <KeyOutlined />
  } else if (name === 'SettingOutlined') {
    return <SettingOutlined />
  } else {
    return <AppleFilled className="text-xl" />
  }
}

export default icon;
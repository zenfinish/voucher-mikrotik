import React from 'react'
import NotFoundAtom from '../../components/atoms/NotFound'

const NotFound = () => {
  return (
    <NotFoundAtom />
  )
}

export default NotFound

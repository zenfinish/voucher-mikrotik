import React from 'react';
import { Tabs, Spin } from 'antd';
import api from '../../../../../config/api.js';
import Children from './Children';

class index extends React.Component {

	state = {
		data: [{
			key: 'loading',
			label: 'Loading...',
			children: <Spin />,
		}],
        aktif: {},

        openAdd: false,
        openEdit: false,
        openDelete: false,

		loading: false,
	}

	componentDidMount() {
		this.refreshTable();
	}

	refreshTable = () => {
        this.setState({ loading: true }, () => {
			api.get(`/mikrotik/rest-api/user-manager/user/group`)
			.then(result => {
                const data = result.data.map(row => {
                    return {
                        key: row['.id'],
                        label: row.name,
                        children: <Children group={row.name} />,
                    };
                });
                this.setState({ data, loading: false });
			})
			.catch(error => {
				this.setState({ loading: false });
			});
		});
	}
	
	render() {
		const { data } = this.state;
		
		return (
            <>
                <Tabs
					type='line'
					destroyInactiveTabPane
					items={data}
				/>
            </>
		);
	}

};

export default index;

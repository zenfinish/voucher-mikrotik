import React from 'react';
import { Table, Button } from 'antd';
import {
    EditOutlined, DeleteOutline,
} from '@material-ui/icons';
import api from '../../../../../config/api.js';

class index extends React.Component {

	state = {
		data: [],
		loading: false,
	}

	componentDidMount() {
		this.refreshTable();
	}

	refreshTable = () => {
		this.setState({ loading: true }, () => {
			api.get(`/mikrotik/rest-api/user-manager/limitation`)
			.then(result => {
                this.setState({ data: result.data, loading: false });
			})
			.catch(error => {
				console.log(error.response);
				this.setState({ loading: false });
			});
		});
	}
	
	render() {
		const { data, loading } = this.state;
		
		return (
            <>
                <Button className='mb-1' onClick={() => this.setState({ openAdd: true })}>Add</Button>
                <Table
                    dataSource={data}
                    bordered
                    size='small'
                    loading={loading}
                    rowKey={row => row['.id']}
                    columns={[
                        {
                            title: 'Action',
                            render: row => (
                                <>
                                    <EditOutlined
                                        fontSize="small"
                                        onClick={() => this.setState({ aktif: row, openEdit: true })}
                                        className='cursor-pointer text-blue-500'
                                    />
                                    <DeleteOutline
                                        fontSize="small"
                                        onClick={() => this.setState({ aktif: row, openDelete: true })}
                                        className='cursor-pointer text-red-500'
                                    />
                                </>
                            ),
                        },
                        {
                            title: '.id',
                            dataIndex: '.id',
                        },
                        {
                            title: 'download-limit',
                            dataIndex: 'download-limit',
                        },
                        {
                            title: 'name',
                            dataIndex: 'name',
                        },
                        {
                            title: 'rate-limit-burst-rx',
                            dataIndex: 'rate-limit-burst-rx',
                        },
                        {
                            title: 'rate-limit-burst-threshold-rx',
                            dataIndex: 'rate-limit-burst-threshold-rx',
                        },
                        {
                            title: 'rate-limit-burst-threshold-tx',
                            dataIndex: 'rate-limit-burst-threshold-tx',
                        },
                        {
                            title: 'rate-limit-burst-time-rx',
                            dataIndex: 'rate-limit-burst-time-rx',
                        },
                        {
                            title: 'rate-limit-burst-time-tx',
                            dataIndex: 'rate-limit-burst-time-tx',
                        },
                        {
                            title: 'rate-limit-burst-tx',
                            dataIndex: 'rate-limit-burst-tx',
                        },
                        {
                            title: 'rate-limit-min-rx',
                            dataIndex: 'rate-limit-min-rx',
                        },
                        {
                            title: 'rate-limit-min-tx',
                            dataIndex: 'rate-limit-min-tx',
                        },
                        {
                            title: 'rate-limit-priority',
                            dataIndex: 'rate-limit-priority',
                        },
                        {
                            title: 'rate-limit-rx',
                            dataIndex: 'rate-limit-rx',
                        },
                        {
                            title: 'rate-limit-tx',
                            dataIndex: 'rate-limit-tx',
                        },
                        {
                            title: 'reset-counters-interval',
                            dataIndex: 'reset-counters-interval',
                        },
                        {
                            title: 'reset-counters-start-time',
                            dataIndex: 'reset-counters-start-time',
                        },
                        {
                            title: 'transfer-limit',
                            dataIndex: 'transfer-limit',
                        },
                        {
                            title: 'upload-limit',
                            dataIndex: 'upload-limit',
                        },
                        {
                            title: 'uptime-limit',
                            dataIndex: 'uptime-limit',
                        },
                    ]}
                />
            </>
		);
	}

};

export default index;

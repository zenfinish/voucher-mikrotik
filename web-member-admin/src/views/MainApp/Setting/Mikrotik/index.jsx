import React from 'react';
import { Tabs, Tag, } from 'antd';
import ProfileLimitations from './ProfileLimitations';
import Limitations from './Limitations';
import Profile from './Profile';
import Users from './Users';
import UserGroups from './UserGroups';

class index extends React.Component {

	render() {
		return (
			<>
				<div className='pb-2'>
					<Tag style={{ fontSize: '1.25rem', lineHeight: '1.75rem' }} color='yellow'>User Manager</Tag>
				</div>
				<Tabs
					type='card'
					destroyInactiveTabPane
					items={[
						{
							key: '1',
							label: 'Profiles',
							children: <Profile />,
						},
						{
							key: '2',
							label: 'Limitations',
							children: <Limitations />,
						},
						{
							key: '3',
							label: 'Profile Limitations',
							children: <ProfileLimitations />,
						},
						{
							key: '4',
							label: 'Users',
							children: <Users />,
						},
						{
							key: '5',
							label: 'User Groups',
							children: <UserGroups />,
						},
					]}
				/>
			</>
		);
	}

};

export default index;

import React from 'react';
import { Table } from 'antd';
import api from '../../../../../config/api.js';

class index extends React.Component {

	state = {
		data: [],
        aktif: {},

        openAdd: false,
        openEdit: false,
        openDelete: false,

		loading: false,
	}

	componentDidMount() {
		this.refreshTable();
	}

	refreshTable = () => {
		this.setState({ loading: true }, () => {
			api.get(`/mikrotik/rest-api/user-manager/user/group`)
			.then(result => {
                this.setState({ data: result.data, loading: false });
			})
			.catch(error => {
				console.log(error.response);
				this.setState({ loading: false });
			});
		});
	}
	
	render() {
		const { data, loading } = this.state;
		
		return (
            <>
                <Table
                    dataSource={data}
                    bordered
                    size='small'
                    loading={loading}
                    rowKey={row => row['.id']}
                    columns={[
                        {
                            title: '.id',
                            dataIndex: '.id',
                        },
                        {
                            title: 'name',
                            dataIndex: 'name',
                        },
                        {
                            title: 'name-for-users',
                            dataIndex: 'name-for-users',
                        },
                        {
                            title: 'override-shared-users',
                            dataIndex: 'override-shared-users',
                        },
                        {
                            title: 'price',
                            dataIndex: 'price',
                        },
                        {
                            title: 'starts-when',
                            dataIndex: 'starts-when',
                        },
                        {
                            title: 'validity',
                            dataIndex: 'validity',
                        },
                    ]}
                />
            </>
		);
	}

};

export default index;

import React from 'react';
import { Table, Button } from 'antd';
import {
    EditOutlined, DeleteOutline,
} from '@material-ui/icons';
import api from '../../../../../config/api.js';

class index extends React.Component {

	state = {
		data: [],
		loading: false,
	}

	componentDidMount() {
		this.refreshTable();
	}

	refreshTable = () => {
		this.setState({ loading: true }, () => {
			api.get(`/mikrotik/rest-api/user-manager/profile-limitation`)
			.then(result => {
				this.setState({ data: result.data, loading: false });
			})
			.catch(error => {
				console.log(error.response);
				this.setState({ loading: false });
			});
		});
	}
	
	render() {
		const { data, loading } = this.state;
		
		return (
            <>
                <Button className='mb-1' onClick={() => this.setState({ openAdd: true })}>Add</Button>
                <Table
                    dataSource={data}
                    bordered
                    size='small'
                    loading={loading}
                    rowKey={row => row['.id']}
                    columns={[
                        {
                            title: 'Action',
                            render: row => (
                                <>
                                    <EditOutlined
                                        fontSize="small"
                                        onClick={() => this.setState({ aktif: row, openEdit: true })}
                                        className='cursor-pointer text-blue-500'
                                    />
                                    <DeleteOutline
                                        fontSize="small"
                                        onClick={() => this.setState({ aktif: row, openDelete: true })}
                                        className='cursor-pointer text-red-500'
                                    />
                                </>
                            ),
                        },
                        {
                            title: '.id',
                            dataIndex: '.id',
                        },
                        {
                            title: 'from-time',
                            dataIndex: 'from-time',
                        },
                        {
                            title: 'limitation',
                            dataIndex: 'limitation',
                        },
                        {
                            title: 'profile',
                            dataIndex: 'profile',
                        },
                        {
                            title: 'till-time',
                            dataIndex: 'till-time',
                        },
                        {
                            title: 'weekdays',
                            dataIndex: 'weekdays',
                        },
                    ]}
                />
            </>
		);
	}

};

export default index;

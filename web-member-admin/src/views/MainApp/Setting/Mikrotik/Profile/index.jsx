import React from 'react';
import { Table, Button } from 'antd';
import {
    EditOutlined, DeleteOutline,
} from '@material-ui/icons';
import api from '../../../../../config/api.js';
import Form from './Form.jsx';

class index extends React.Component {

	state = {
		data: [],
        aktif: {},

        openAdd: false,
        openEdit: false,
        openDelete: false,

		loading: false,
	}

	componentDidMount() {
		this.refreshTable();
	}

	refreshTable = () => {
		this.setState({ loading: true }, () => {
			api.get(`/mikrotik/rest-api/user-manager/profile`)
			.then(result => {
                this.setState({ data: result.data, loading: false });
			})
			.catch(error => {
				console.log(error.response);
				this.setState({ loading: false });
			});
		});
	}
	
	render() {
		const { data, loading, openEdit, openAdd, openDelete, aktif } = this.state;
		
		return (
            <>
                <Button className='mb-1' onClick={() => this.setState({ openAdd: true })}>Add</Button>
                <Table
                    dataSource={data}
                    bordered
                    size='small'
                    loading={loading}
                    rowKey={row => row['.id']}
                    columns={[
                        {
                            title: 'Action',
                            render: row => (
                                <>
                                    <EditOutlined
                                        fontSize="small"
                                        onClick={() => this.setState({ aktif: row, openEdit: true })}
                                        className='cursor-pointer text-blue-500'
                                    />
                                    <DeleteOutline
                                        fontSize="small"
                                        onClick={() => this.setState({ aktif: row, openDelete: true })}
                                        className='cursor-pointer text-red-500'
                                    />
                                </>
                            ),
                        },
                        {
                            title: '.id',
                            dataIndex: '.id',
                        },
                        {
                            title: 'name',
                            dataIndex: 'name',
                        },
                        {
                            title: 'name-for-users',
                            dataIndex: 'name-for-users',
                        },
                        {
                            title: 'override-shared-users',
                            dataIndex: 'override-shared-users',
                        },
                        {
                            title: 'price',
                            dataIndex: 'price',
                        },
                        {
                            title: 'starts-when',
                            dataIndex: 'starts-when',
                        },
                        {
                            title: 'validity',
                            dataIndex: 'validity',
                        },
                    ]}
                />

                {
                    openAdd ?
                        <Form
                            close={() => this.setState({ openAdd: false })}
                            closeRefresh={() => this.setState({ openAdd: false }, () => this.refreshTable())}
                        />
                    : null
                }
                {
                    openEdit ?
                        <Form
                            close={() => this.setState({ openEdit: false })}
                            closeRefresh={() => this.setState({ openEdit: false }, () => this.refreshTable())}
                            data={aktif}
                            edit
                        />
                    : null
                }
                {
                    openDelete ?
                        <Form
                            close={() => this.setState({ openDelete: false })}
                            closeRefresh={() => this.setState({ openDelete: false }, () => this.refreshTable())}
                            data={aktif}
                            hapus
                        />
                    : null
                }
            </>
		);
	}

};

export default index;

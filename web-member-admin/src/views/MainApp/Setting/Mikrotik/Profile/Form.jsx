import React from 'react';
import { Modal, Input, Button, notification } from 'antd';
import api from '../../../../../config/api.js';

class Edit extends React.Component {

	state = {
        id: null,
        name: null,
        name_for_users: null,
        price: null,

		loading: false,
	}

    componentDidMount() {
        const { edit, hapus } = this.props;

        if (edit || hapus) {
            this.setState(this.props.data);
        };
    }

    simpan = () => {
        const { price } = this.state;

        this.setState({ loading: true }, () => {
			api.post(`/mikrotik/rest-api/user-manager/profile`, {
                price,
            })
			.then(result => {
                this.props.closeRefresh();
			})
			.catch(error => {
				// console.log(error.response);
				this.setState({ loading: false });
                notification.error({ message: error.response.data });
			});
		});
    }

    update = () => {
        const { price } = this.state;

        this.setState({ loading: true }, () => {
			api.patch(`/mikrotik/rest-api/user-manager/profile/${this.state.id}`, {
                price,
            })
			.then(result => {
                this.props.closeRefresh();
			})
			.catch(error => {
				// console.log(error.response);
				this.setState({ loading: false });
                notification.error({ message: error.response.data });
			});
		});
    }

    hapus = () => {
        this.setState({ loading: true }, () => {
			api.delete(`/mikrotik/rest-api/user-manager/profile/${this.state.id}`)
			.then(result => {
                this.props.closeRefresh();
			})
			.catch(error => {
				// console.log(error.response);
				this.setState({ loading: false });
                notification.error({ message: error.response.data });
			});
		});
    }

	render() {
		const { edit, hapus } = this.props;
        const { price, name, validity } = this.state;
        
        return (
			<>
				<Modal
					centered
					width="100%"
					visible={true}
					onCancel={this.props.close}
					footer={[
						<React.Fragment key="1">
                            <Button
                                onClick={edit ? this.update : hapus ? this.hapus : this.simpan}
                            >
                                {edit ? 'Update' : hapus ? 'Hapus' : 'Simpan'}
                            </Button>
                        </React.Fragment>,
					]}
					title={`${edit ? 'Edit' : hapus ? 'Delete' : 'Add'}: User Manager - Profiles`}
				>
                    <div>
                        <div>
                            <div>.id</div>
                            {this.state.id}
                        </div>
                        <div>
                            <div>name</div>
                            {name}
                        </div>
                        <div>name-for-users: {this.state.name_for_users}</div>
                        <div>override-shared-users: {this.state['override-shared-users']}</div>
                        <div>starts-when: {this.state['starts-when']}</div>
                        <div>validity: {validity}</div>
                        <div>
                            <div>Price</div>
                            <Input
                                className="mb-2 w-full"
                                value={price}
                                type='number'
                                onChange={(e) => this.setState({ price: e.target.value })}
                            />
                        </div>
                    </div>
				</Modal>

			</>
		);
	}

};

export default Edit;

import React from 'react';
import { Select, Button, notification } from 'antd';
import { companyState } from "../../../../recoil";
import { useRecoilState } from "recoil";
import { api } from "../../../../helpers";

const Index = () => {
	const [ company, setCompany ] = useRecoilState(companyState);

	return <Pembayaran
		companyState={company}
		setCompany={setCompany}
	/>;
};

class Pembayaran extends React.Component {

	state = {
		loading: false,
	}

	componentDidMount() {
		this.setState(this.props.company);
	}

	update = () => {
		const { companyState } = this.props;

		api.patch(`/company`, {
			tgl_jt: companyState.tgl_jt,
			tgl_invoice: companyState.tgl_invoice,
		})
		.then(result => {
			notification.success({ message: 'Data Berhasil Di Update' });
		})
		.catch(error => {
			notification.error({ message: error.response });
		});
	}

	render() {
		const { loading } = this.state;
		const { companyState, setCompany } = this.props;

		const selOp = [];
		for (let i = 1; i <= 28; i++) {
			selOp.push(`${i}`.padStart(2, '0'));
		};

		return (
			<div className='grid gap-1'>
				<div>
					<div>Tanggal Cetak Invoice</div>
					<Select
						className='w-24'
						value={companyState.tgl_invoice}
						onChange={(value) => setCompany({ ...companyState, tgl_invoice: value })}
					>
						{
							selOp.map((row, i) => (
								<Select.Option value={row} key={i}>{row}</Select.Option>
							))
						}
					</Select>
				</div>
				<div>
					<div>Tanggal Jatuh Tempo</div>
					<Select
						className='w-24'
						value={companyState.tgl_jt}
						onChange={(value) => setCompany({ ...companyState, tgl_jt: value })}
					>
						{
							selOp.map((row, i) => (
								<Select.Option value={row} key={i}>{row}</Select.Option>
							))
						}
					</Select>
				</div>
				<div>
					<Button onClick={this.update} disabled={loading}>Simpan</Button>
				</div>
			</div>
		);
	}

};

export default Index;

import React from 'react';
import { Tabs } from 'antd';
import Member from './Member';
import Voucher from './Voucher';

class index extends React.Component {

	render() {
		return (
			<>
				<Tabs
					type='card'
					destroyInactiveTabPane
				>
					<Tabs.TabPane tab="Member" key="1">
						<Member />
					</Tabs.TabPane>
					<Tabs.TabPane tab="Voucher" key="2">
						<Voucher />
					</Tabs.TabPane>
				</Tabs>
			</>
		);
	}

};

export default index;

import React from 'react';
import { Table, Button } from 'antd';
import {
    DeleteForeverOutlined as DeleteForeverOutlinedIcon,
} from '@material-ui/icons';
import api from '../../../../../config/api.js';

class index extends React.Component {

	state = {
		data: [],
		loading: false,
	}

	componentDidMount() {
		this.refreshTable();
	}

	refreshTable = () => {
		this.setState({ loading: true }, () => {
			api.get(`/profile?limit=all&jenis=1`)
			.then(result => {
                this.setState({ data: result.data, loading: false });
			})
			.catch(error => {
				console.log(error.response);
				this.setState({ loading: false });
			});
		});
	}
	
	render() {
		const { data, loading } = this.state;
		
		return (
            <>
                <div className='mb-1'>
                    <Button>Tambah Profil Voucher</Button>
                </div>
                <Table
                    dataSource={data}
                    bordered
                    size='small'
                    loading={loading}
                    rowKey={row => row.id}
                    columns={[
                        {
                            title: 'name',
                            dataIndex: 'name',
                        },
                        {
                            title: 'price',
                            dataIndex: 'price',
                        },
                        {
                            title: 'kode',
                            dataIndex: 'kode',
                        },
                        {
                            title: 'Action',
                            render: row => (
                                <DeleteForeverOutlinedIcon
                                    fontSize="small"
                                    // onClick={}
                                    className='cursor-pointer text-red-500'
                                />
                            ),
                        },
                    ]}
                />
            </>
		);
	}

};

export default index;

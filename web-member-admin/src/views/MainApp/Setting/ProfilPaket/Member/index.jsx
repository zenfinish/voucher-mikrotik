import React from 'react';
import { Table, Button } from 'antd';
import {
    DeleteForeverOutlined as DeleteForeverOutlinedIcon,
} from '@material-ui/icons';
import api from '../../../../../config/api.js';
import Add from './Add.jsx';

class index extends React.Component {

	state = {
		data: [],
		loading: false,
        openAdd: false,
	}

	componentDidMount() {
		this.refreshTable();
	}

	refreshTable = () => {
		this.setState({ loading: true }, () => {
			api.get(`/profile?limit=all&jenis=2`)
			.then(result => {
                this.setState({ data: result.data, loading: false });
			})
			.catch(error => {
				// console.log(error.response);
				this.setState({ loading: false });
			});
		});
	}

    hapus = (profile_id) => {
		this.setState({ loading: true }, () => {
			api.delete(`/profile/${profile_id}`)
			.then(result => {
                this.refreshTable();
			})
			.catch(error => {
				// console.log(error.response);
				this.setState({ loading: false });
			});
		});
	}
	
	render() {
		const { data, loading, openAdd } = this.state;
		
		return (
            <>
                <div className='mb-1'>
                    <Button
                        onClick={() => this.setState({ openAdd: true })}
                    >Tambah Profil Member</Button>
                </div>
                <Table
                    dataSource={data}
                    bordered
                    size='small'
                    loading={loading}
                    rowKey={row => row.id}
                    columns={[
                        {
                            title: 'name',
                            dataIndex: 'name',
                        },
                        {
                            title: 'price',
                            dataIndex: 'price',
                        },
                        {
                            title: 'kode',
                            dataIndex: 'kode',
                        },
                        {
                            title: 'Action',
                            render: row => (
                                <DeleteForeverOutlinedIcon
                                    fontSize="small"
                                    onClick={() => this.hapus(row.id)}
                                    className='cursor-pointer text-red-500'
                                />
                            ),
                        },
                    ]}
                />
                {
                    openAdd ?
                    <Add
                        close={() => this.setState({ openAdd: false })}
                        closeRefresh={() => this.setState({ openAdd: false }, () => this.refreshTable())}
                    />
                    : null
                }
            </>
		);
	}

};

export default index;

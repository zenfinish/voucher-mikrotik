import React from 'react';
import { Modal, Input, Button, Select, notification } from 'antd';
import api from '../../../../../config/api.js';

class Add extends React.Component {

	state = {
        name: null,
        price: null,
        kode: null,

        dataProfil: [],

		loading: false,
	}

    componentDidMount() {
        this.fetchProfil();
    }

    fetchProfil = () => {
        this.setState({ loading: true }, () => {
			api.get(`/mikrotik/rest-api/user-manager/profile`)
			.then(result => {
                // console.log(result.data)
                this.setState({ dataProfil: result.data, loading: false });
			})
			.catch(error => {
				// console.log(error.response);
				this.setState({ loading: false });
			});
		});
    }

    simpan = () => {
        const { closeRefresh } = this.props;
        const { name, kode, price } = this.state;

        this.setState({ loading: true }, () => {
			api.post(`/profile`, {
                name, kode, price, jenis: 2,
            })
			.then(result => {
                closeRefresh();
			})
			.catch(error => {
				// console.log(error.response);
				this.setState({ loading: false });
                notification.error({
                    message: error.response.data
                });
			});
		});
    }
	
	render() {
		const { price, kode, loading, dataProfil } = this.state;
        
        return (
			<>
				<Modal
					centered
					width="100%"
					visible={true}
					onCancel={this.props.close}
					footer={[
						<React.Fragment key="1">
                            <Button onClick={this.simpan}>Simpan</Button>
                        </React.Fragment>,
					]}
					title={`Tambah Profil Member`}
				>
                    <div>
                        <div>
                            <label>Pilih Profil Mikrotik</label>
                            <Select
                                loading={loading}
                                className="mb-2 w-full"
                                onChange={(value, { data }) => {
                                    this.setState({
                                        name: data['name-for-users'],
                                        price: data.price,
                                        kode: data.name,
                                    });
                                }}
                            >
                                {
                                    dataProfil.map((row, i) => (
                                        <Select.Option
                                            key={i}
                                            value={row['.id']}
                                            data={row}
                                        >{row['name-for-users']}</Select.Option>
                                    ))
                                }
                            </Select>
                        </div>
                        <div> {/* kode */}
                            <label>Kode</label>
                            <Input
                                className="mb-2 w-full"
                                value={kode}
                                readOnly
                            />
                        </div>
                        <div> {/* price */}
                            <label>Harga</label>
                            <Input
                                className="mb-2 w-full"
                                value={price}
                                type="number"
                                readOnly
                            />
                        </div>
                    </div>
				</Modal>

			</>
		);
	}

};

export default Add;

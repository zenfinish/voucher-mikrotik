import React from 'react';
import { Table, Button, notification, Popconfirm } from 'antd';
import api from 'config/api.js';
import Tambah from "./Tambah";

class index extends React.Component {

	state = {
		datas: [],
		loading: false,
		openTambah: false,
		openEdit: false,
		aktif: {},
	}

	componentDidMount() {
		this.refreshTable();
	}

	refreshTable = () => {
		this.setState({ loading: true }, () => {
			api.get(`/nas`)
			.then(result => {
				this.setState({ datas: result.data, loading: false });
			})
			.catch(error => {
				notification.error({ message: error.response.data });
				this.setState({ loading: false });
			});
		});
	}

	hapus = (nas_id) => {
		this.setState({ loading: true }, () => {
			api.delete(`/nas/${nas_id}`)
			.then(result => {
				this.refreshTable();
			})
			.catch(error => {
				notification.error({ message: error.response.data });
				this.setState({ loading: false });
			});
		});
	}
	
	render() {
		const { datas, openTambah, openEdit, aktif } = this.state;

		return (
			<>
				<div>
					<Button onClick={() => this.setState({ openTambah: true })}>Tambah</Button>
				</div>
				<Table
					size='small'
					dataSource={datas}
					rowKey={row => row.nas_id}
					columns={[
						{
							title: "Url",
							dataIndex: 'url',
						},
						{
							title: "Username",
							dataIndex: 'username',
						},
						{
							title: "Password",
							dataIndex: 'password',
						},
						{
							title: "Action",
							render: row => <>
								<Button onClick={() => this.setState({ aktif: row, openEdit: true })}>Edit</Button>
								<Popconfirm
									title="Delete"
									description="Are you sure to delete?"
									onConfirm={() => this.hapus(row.nas_id)}
									okText="Yes"
									cancelText="No"
								>
									<Button>Delete</Button>
								</Popconfirm>
							</>
						},
					]}
				/>
				{
					!openTambah ? null :
					<Tambah
						close={() => this.setState({ openTambah: false })}
						closeRefresh={() => this.setState({ openTambah: false }, () => this.refreshTable())}
						mode="tambah"
					/>
				}
				{
					!openEdit ? null :
					<Tambah
						close={() => this.setState({ openEdit: false })}
						closeRefresh={() => this.setState({ openEdit: false }, () => this.refreshTable())}
						mode="edit"
						data={aktif}
					/>
				}
			</>
		);
	}

};

export default index;

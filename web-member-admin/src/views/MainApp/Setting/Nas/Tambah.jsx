import React from 'react';
import { Modal, Input, notification, Button } from 'antd';
import api from 'config/api.js';

class Tambah extends React.Component {

	state = {
		url: null,
        username: null,
        password: null,

		loading: false,
	}

	componentDidMount() {
		const { mode } = this.props;

		if (mode === "edit") {
			this.fetchData();
		};
	}

	onDataChange = (data) => {
        this.setState({ ...data });
    }

	fetchData = () => {
		const { data } = this.props;

		this.setState({ loading: true }, () => {
			api.get(`/nas/${data.nas_id}`)
			.then(result => {
				this.setState({ ...result.data });
			})
			.catch(error => {
				notification.error({ message: error.response.data });
                this.setState({ loading: false });
			});
		});
	}

	simpan = () => {
		this.setState({ loading: true }, () => {
			api.post(`/nas`, this.state)
			.then(result => {
				this.props.closeRefresh();
			})
			.catch(error => {
				notification.error({ message: error.response.data });
                this.setState({ loading: false });
			});
		});
	}

    ubah = () => {
		const { data } = this.props;

        this.setState({ loading: true }, () => {
			api.put(`/nas/${data.nas_id}`, this.state)
			.then(result => {
				this.props.closeRefresh();
			})
			.catch(error => {
				notification.error({ message: error.response.data });
                this.setState({ loading: false });
			});
		});
	}
	
	render() {
		const { url, username, password } = this.state;
		const { close, mode } = this.props;
        
        return (
			<>
				<Modal
					centered
					width="100%"
					open={true}
					onCancel={close}
					footer={[
						<div key="simpan">
                            {
                                mode === "tambah" ?
                                <Button onClick={this.simpan}>Simpan</Button>
                                :
                                mode === "edit" ?
                                <Button onClick={this.ubah}>Update</Button>
                                : null
                            }
                        </div>,
					]}
					title={mode === "tambah" ? `Tambah` : mode === "edit" ? `Edit` : null}
				>
					<div>URL : <Input onChange={e => this.setState({ url: e.target.value })} value={url} /></div>
                    <div>Username : <Input onChange={e => this.setState({ username: e.target.value })} value={username} /></div>
                    <div>Password : <Input onChange={e => this.setState({ password: e.target.value })} value={password} /></div>
				</Modal>
			</>
		);
	}

};

export default Tambah;

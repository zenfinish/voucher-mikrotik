import React from 'react';
import { Select, Button, Input, Card } from 'antd';
import api from '../../../../config/api.js';
import { separator } from 'config/helpers.js';
import _ from "lodash";
import moment from 'moment';

import 'chart.js/auto';
import { Bar } from 'react-chartjs-2';
import ChartDataLabels from 'chartjs-plugin-datalabels';

import DetailGrapik from './DetailGrapik.jsx';
import DetailTabel from './DetailTabel.jsx';

class index extends React.Component {
	
	state = {
		selectedDate: moment(new Date()),
		selectedDate2: moment(new Date()),
		propinsi_id: '',
		user_id: '',
		profile_id: '',
		
		dataReport: [],
		dataGrapik: [],
		userList: [],
		voucherList: [],
		
		loading: false,

		status: [
			{ value: null, text: "-Semua-" },
			{ value: "1", text: "Salah" },
		],

		aktif: null,

		openDetailGrapik: false,
		openDetailTabel: false,
	};

	componentDidMount() {
		api.get(`/users`)
		.then(result => {
			this.setState({
				userList: result.data,
			});
		})
		.catch(error => {
			console.log(error.response)
		});

		api.get(`/profile`)
		.then(result => {
			this.setState({
				voucherList: result.data,
			});
		})
		.catch(error => {
			console.log(error.response)
		});

		api.get(`/transactions/voucher/all`)
		.then(result => {
			this.setState({
				dataChart3: result.data,
			});
		})
		.catch(error => {
			console.log(error.response)
		});
	}

	handleChange = (e) => {
		this.setState({
			[e.target.name]: e.target.value,
		});
	};

	cetak = () => {
		const { selectedDate, selectedDate2, propinsi_id, user_id, profile_id } = this.state;

		this.setState({
			loading: true
		}, () => {
			api.post(`/transactions`, {
				date1: selectedDate,
				date2: selectedDate2,
				propinsi_id: propinsi_id,
				user_id: user_id,
				profile_id,
			}, {
				headers: { token: localStorage.getItem('token') }
			})
			.then(result => {
				const data = _.orderBy(result.data, 'tgl', 'desc');
				const datas = _.groupBy(result.data, 'tahun');
				const dataGrapik = Object.keys(datas).map(tahun => ({
					tahun,
					datas: datas[tahun],
				}));

				this.setState({
					dataReport: data,
					dataGrapik,
					loading: false,
				});
			})
			.catch(error => {
				this.error(error.response.data);
				this.setState({
					loading: false,
				});
			});
		});
	}

	footerData = () => {
		let total = 0;
		let total_salah = 0;
		let data = this.state.dataReport;
		for (let i = 0; i < data.length; i++) {
			if (data[i].salah === 1) {
				total_salah += data[i].price;
			}
			total += data[i].price;
		};
		return [
			{ user_name: "Total Seluruh", price_format: separator(total, 0) },
			{ user_name: "Total Salah", price_format: separator(total_salah, 0) },
			{ user_name: "Total Bersih", price_format: separator(total - total_salah, 0) },
		];
	}

	error = (data) => {
		this.messager.alert({
			title: "Alert",
			msg: data
		});
	}

	generateRandomColor = () => {
        const r = Math.floor(Math.random() * 256);
        const g = Math.floor(Math.random() * 256);
        const b = Math.floor(Math.random() * 256);

        return `rgba(${r},${g},${b},0.5)`;
    };

	render() {
		const { selectedDate, selectedDate2, propinsi_id, user_id, profile_id, dataReport, dataGrapik, openDetailGrapik, aktif, openDetailTabel } = this.state;
		const maxValue = Math.max(...dataGrapik.map(row => row.datas.length));

		return (
			<div className='overflow-auto' style={{ height: 'calc(100vh - 80px)' }}>
				<div className='flex gap-1 mb-1'>
					<Input
						type='date'
						placeholder="Dari Tanggal"
						value={selectedDate}
						onChange={e => this.setState({ selectedDate: e.target.value })}
					/>
					<Input
						type='date'
						placeholder="Sampai Tanggal"
						value={selectedDate2}
						onChange={e => this.setState({ selectedDate2: e.target.value })}
					/>
					<Select
						placeholder="Propinsi"
						onChange={propinsi_id => this.setState({ propinsi_id })}
						value={propinsi_id}
						popupMatchSelectWidth={false}
					>
						<Select.Option value="">Semua Propinsi</Select.Option>
						<Select.Option value="1">Lampung</Select.Option>
						<Select.Option value="2">Aceh</Select.Option>
						<Select.Option value="3">Midtrans</Select.Option>
					</Select>
					<Select
						placeholder="Penjual"
						onChange={user_id => this.setState({ user_id })}
						value={user_id}
						popupMatchSelectWidth={false}
					>
						<Select.Option value="">Semua Penjual</Select.Option>
						{this.state.userList.map((data, index) => (
							<Select.Option value={data.id} key={index}>{data.name}</Select.Option>
						))}
					</Select>
					<Select
						placeholder="Voucher"
						onChange={profile_id => this.setState({ profile_id })}
						value={profile_id}
						popupMatchSelectWidth={false}
					>
						<Select.Option value="">Semua Voucher</Select.Option>
						{this.state.voucherList.map((data, index) => (
							<Select.Option value={data.id} key={index}>{data.name}</Select.Option>
						))}
					</Select>
					<Button
						type="primary"
						onClick={this.cetak}
					>Tampilkan</Button>
					<Button
						type="primary"
						danger
						onClick={() => this.setState({ openDetailTabel: true })}
					>Data Tabel</Button>
				</div>
				<Card
					size='small'
				>
					<div>
						<div>Total Qty : {dataReport.length}</div>
						<div>Total Pendapatan : {separator(dataReport.reduce((sum, data) => sum + data.price, 0))}</div>
					</div>
				</Card>
				<Card
					size='small'
				>
					{
						dataGrapik.length > 0 ?
							<Bar
								data={{
									labels: dataGrapik.map(row => row.tahun),
									datasets: [{
										label: "Total Qty",
										data: dataGrapik.map(row => row.datas.length),
										borderWidth: 1,
										backgroundColor: (dataGrapik.map(row => row.datas.length)).map(() => this.generateRandomColor()),
									}],
								}}
								options={{
									onClick: (_, e) => {
										if (e.length === 1) {
											const index = e[0].index;
											const dataGrapik2 = dataGrapik[index];
											
											this.setState({ aktif: dataGrapik2, openDetailGrapik: true });
										};
									},
									responsive: true,
									plugins: {
										datalabels: {
											display: true,
											color: 'black',
											font: {
												weight: 'bold',
											},
											align: 'top',
											anchor: 'end',
											formatter: (value, e) => {
												const index = e.dataIndex;
												const dataGrapik2 = dataGrapik[index];
												const total = dataGrapik2.datas.reduce((sum, data) => sum + data.price, 0);

												return `Total Qty : ${dataGrapik2.datas.length}\nTotal Pendapatan : ${separator(total)}`;
											},
										},
										legend: {
											display: false,
										},
										tooltip: {
											callbacks: {
												label: e => {
													const index = e.dataIndex;
													const dataGrapik2 = dataGrapik[index];
													const total = dataGrapik2.datas.reduce((sum, data) => sum + data.price, 0);

													return `Total Qty : ${dataGrapik2.datas.length}\nTotal Pendapatan : ${separator(total)}`;
												},
											},
										},
									},
									scales: {
										y: {
											max: maxValue + (maxValue > 4000 ? 1000 : 100),
										},
									},
								}}
								plugins={[ChartDataLabels]}
								height={100}
							/>
						: null
					}
				</Card>
				{
					openDetailGrapik ?
						<DetailGrapik
							data={aktif}
							close={() => this.setState({ openDetailGrapik: false })}
						/>
					: null
				}
				{
					openDetailTabel ?
						<DetailTabel
							dataReport={dataReport}
							close={() => this.setState({ openDetailTabel: false })}
						/>
					: null
				}
			</div>
		);
	}

}

export default index;

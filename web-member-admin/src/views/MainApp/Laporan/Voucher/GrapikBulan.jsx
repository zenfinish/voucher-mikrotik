import React from 'react';
import { Modal } from 'antd';
import { separator } from 'config/helpers.js';
import _ from "lodash";

import 'chart.js/auto';
import { Line } from 'react-chartjs-2';
import ChartDataLabels from 'chartjs-plugin-datalabels';

class GrapikBulan extends React.Component {
	
	state = {
        dataGrapik: [],
    }

    componentDidMount() {
        const { data } = this.props;

        const datas = _.groupBy(data.datas, 'tanggal');
        const dataGrapik = Object.keys(datas).map(tanggal => ({
            tanggal,
            datas: datas[tanggal],
        }));

        this.setState({ dataGrapik });
    }

    generateRandomColor = () => {
        const r = Math.floor(Math.random() * 256);
        const g = Math.floor(Math.random() * 256);
        const b = Math.floor(Math.random() * 256);

        return `rgba(${r},${g},${b},0.5)`;
    };
    
    render() {
        const { dataGrapik } = this.state;
        const { data } = this.props;
        // const maxValue = Math.max(...dataGrapik.map(row => row.datas.length));

        return (
			<>
				<Modal
					open={true}
					onCancel={this.props.close}
					footer={false}
					title={`Bulan ${data.bulan}`}
                    centered
                    width={`80%`}
				>
                    <Line
                        plugins={[ChartDataLabels]}
                        // height={100}
                        data={{
                            labels: dataGrapik.map(row => row.tanggal),
                            datasets: [{
                                label: "Total Qty",
                                data: dataGrapik.map(row => row.datas.length),
                                borderWidth: 1,
                                backgroundColor: (dataGrapik.map(row => row.datas.length)).map(() => this.generateRandomColor()),
                            }],
                        }}
                        options={{
                            // onClick: (_, e) => {
                            //     if (e.length === 1) {
                            //         const index = e[0].index;
                            //         const dataGrapik2 = dataGrapik[index];
                                    
                            //         this.setState({ aktif: dataGrapik2, openDetailGrapik: true });
                            //     };
                            // },
                            responsive: true,
                            plugins: {
                                datalabels: {
                                    display: true,
                                    color: 'black',
                                    font: {
                                        weight: 'bold',
                                    },
                                    align: 'top',
                                    anchor: 'end',
                                    formatter: (value, e) => {
                                        const index = e.dataIndex;
                                        const dataGrapik2 = dataGrapik[index];
                                        const total = dataGrapik2.datas.reduce((sum, data) => sum + data.price, 0);

                                        return `${separator(total)}`;
                                    },
                                },
                                legend: {
                                    display: false,
                                },
                                tooltip: {
                                    callbacks: {
                                        label: e => {
                                            const index = e.dataIndex;
                                            const dataGrapik2 = dataGrapik[index];
                                            const total = dataGrapik2.datas.reduce((sum, data) => sum + data.price, 0);

                                            return `Total Qty : ${dataGrapik2.datas.length}\nTotal Pendapatan : ${separator(total)}`;
                                        },
                                    },
                                },
                            },
                            // scales: {
                            //     y: {
                            //         max: maxValue + (maxValue > 4000 ? 1000 : 100),
                            //     },
                            // },
                        }}
                    />
				</Modal>
			</>
		);
	}

};

export default GrapikBulan;

import React from 'react';
import { Drawer, Table, Input, Space, Button } from 'antd';
import { SearchOutlined } from '@ant-design/icons';

class DetailTabel extends React.Component {
    
    getColumnSearchProps = dataIndex => ({
		filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
		<div style={{ padding: 8 }}>
			<Input
			ref={node => {
				this.searchInput = node;
			}}
			placeholder={`Search ${dataIndex}`}
			value={selectedKeys[0]}
			onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
			onPressEnter={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
			style={{ width: 188, marginBottom: 8, display: 'block' }}
			/>
			<Space>
			<Button
				type="primary"
				onClick={() => {
				this.handleSearch(selectedKeys, confirm, dataIndex);
				}}
				icon={<SearchOutlined />}
				size="small"
				style={{ width: 90 }}
			>Search</Button>
			<Button onClick={() => this.handleReset(clearFilters)} size="small" style={{ width: 90 }}>
				Reset
			</Button>
			<Button
				type="link"
				size="small"
				onClick={() => {
				confirm({ closeDropdown: false });
				this.setState({
					searchText: selectedKeys[0],
					searchedColumn: dataIndex,
				});
				}}
			>
				Filter
			</Button>
			</Space>
		</div>
		),
		filterIcon: filtered => <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />,
		onFilter: (value, record) =>
		record[dataIndex]
			? record[dataIndex].toString().toLowerCase().includes(value.toLowerCase())
			: '',
		onFilterDropdownOpenChange: visible => {
		if (visible) {
			setTimeout(() => this.searchInput.select(), 100);
		}
		},
		render: text => 
		( text ),
	});

	handleSearch = (selectedKeys, confirm, dataIndex) => {
		confirm();
		this.setState({
            searchText: selectedKeys[0],
            searchedColumn: dataIndex,
		});
	};

	handleReset = clearFilters => {
		clearFilters();
		this.setState({ searchText: '' });
	};
    
    render() {
        const { dataReport } = this.props;

        return (
			<>
				<Drawer
                    width="100%"
					open={true}
					onClose={this.props.close}
					footer={false}
					title={false}
				>
                    <Table
                        dataSource={dataReport}
                        size='small'
                        bordered
                        rowKey={row => row.transaction_id}
                        scroll={{ y: 'calc(100vh - 220px)' }}
                        columns={[
                            {
                                title: 'Tanggal',
                                dataIndex: 'tgl',
                            },
                            {
                                title: 'Username',
                                dataIndex: 'username',
                                ...this.getColumnSearchProps('username'),
                            },
                            {
                                title: 'Voucher',
                                dataIndex: 'voucher',
                            },
                            {
                                title: 'No. Hp',
                                dataIndex: 'hp',
                            },
                            {
                                title: 'Penjual',
                                render: (row) => row.user_name ? row.user_name : 'Midtrans',
                            },
                            {
                                title: 'Harga',
                                dataIndex: 'price_format',
                            },
                        ]}
                    />
				</Drawer>
			</>
		);
	}

};

export default DetailTabel;

import React from 'react';
import { Drawer } from 'antd';
import { separator } from 'config/helpers.js';
import _ from "lodash";

import 'chart.js/auto';
import { Bar } from 'react-chartjs-2';
import ChartDataLabels from 'chartjs-plugin-datalabels';

import GrapikBulan from "./GrapikBulan";

class DetailGrapik extends React.Component {
	
	state = {
        dataGrapik: [],

        aktif: null,

        openGrapikBulan: false,
    }

    componentDidMount() {
        const { data } = this.props;

        const datas = _.groupBy(data.datas, 'bulan');
        const dataGrapik = Object.keys(datas).map(bulan => ({
            bulan,
            datas: datas[bulan],
        }));

        this.setState({ dataGrapik });
    }

    generateRandomColor = () => {
        const r = Math.floor(Math.random() * 256);
        const g = Math.floor(Math.random() * 256);
        const b = Math.floor(Math.random() * 256);

        return `rgba(${r},${g},${b},0.5)`;
    };
    
    render() {
        const { dataGrapik, openGrapikBulan, aktif } = this.state;
        const { data } = this.props;
        const maxValue = Math.max(...dataGrapik.map(row => row.datas.length));

        return (
			<>
				<Drawer
                    width="100%"
					open={true}
					onClose={this.props.close}
					footer={false}
					title={`Tahun ${data.tahun}`}
				>
                    <Bar
                        plugins={[ChartDataLabels]}
                        height={100}
                        data={{
                            labels: dataGrapik.map(row => row.bulan),
                            datasets: [{
                                label: "Total Qty",
                                data: dataGrapik.map(row => row.datas.length),
                                borderWidth: 1,
                                backgroundColor: (dataGrapik.map(row => row.datas.length)).map(() => this.generateRandomColor()),
                            }],
                        }}
                        options={{
                            onClick: (_, e) => {
                                if (e.length === 1) {
                                    const index = e[0].index;
                                    const dataGrapik2 = dataGrapik[index];
                                    
                                    this.setState({ aktif: dataGrapik2, openGrapikBulan: true });
                                };
                            },
                            responsive: true,
                            plugins: {
                                datalabels: {
                                    display: true,
                                    color: 'black',
                                    font: {
                                        weight: 'bold',
                                    },
                                    align: 'top',
                                    anchor: 'end',
                                    formatter: (value, e) => {
                                        const index = e.dataIndex;
                                        const dataGrapik2 = dataGrapik[index];
                                        const total = dataGrapik2.datas.reduce((sum, data) => sum + data.price, 0);

                                        return `Total Qty : ${dataGrapik2.datas.length}\nTotal Pendapatan : ${separator(total)}`;
                                    },
                                },
                                legend: {
                                    display: false,
                                },
                                tooltip: {
                                    callbacks: {
                                        label: e => {
                                            const index = e.dataIndex;
                                            const dataGrapik2 = dataGrapik[index];
                                            const total = dataGrapik2.datas.reduce((sum, data) => sum + data.price, 0);

                                            return `Total Qty : ${dataGrapik2.datas.length}\nTotal Pendapatan : ${separator(total)}`;
                                        },
                                    },
                                },
                            },
                            scales: {
                                y: {
                                    max: maxValue + (maxValue > 4000 ? 1000 : 100),
                                },
                            },
                        }}
                    />
				</Drawer>
                {
					openGrapikBulan ?
						<GrapikBulan
                            data={aktif}
                            close={() => this.setState({ openGrapikBulan: false })}
						/>
					: null
				}
			</>
		);
	}

};

export default DetailGrapik;

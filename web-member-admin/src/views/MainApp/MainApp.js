import React, { useEffect } from 'react'
import { connect } from 'react-redux';
import { MainAppContent, MainAppHeader, MainAppSideBar } from '../../components/organisms'
import { MainAppRoutes } from '../../config';
import { mdlState, userState, companyState } from "../../recoil";
import { useSetRecoilState } from "recoil";
import { api } from "../../helpers";

const MainApp = ({ dispatch }) => {
	const setMdl = useSetRecoilState(mdlState);
	const setUser = useSetRecoilState(userState);
	const setCompany = useSetRecoilState(companyState);

	useEffect(() => {
		window.addEventListener('storage', e => {
			if (e.key === "token" && e.newValue && !e.oldValue) {
				window.location.assign(`${process.env.REACT_APP_BASE_URL}`);
			};
		});

		api.get(`/users/check-token`)
		.then(result => {
			setMdl(result.mdl);
			setUser(result.user);
			setCompany(result.company);
		})
		.catch(error => {
			window.location.assign(`${process.env.REACT_APP_BASE_URL}`);
		});
	}, [setUser, setMdl, setCompany]);

	return <Index
		dispatch={dispatch}
	/>;
};

class Index extends React.Component {

	render() {
		return (
			<>
				<MainAppHeader />
				<MainAppContent>
					<MainAppRoutes />
				</MainAppContent>
				<MainAppSideBar />
			</>
		)
	}

};

export default connect()(MainApp);

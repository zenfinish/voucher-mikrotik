import React from 'react';
import { Button } from 'antd';
import TextField from '../../../../../components/inputs/TextField.jsx';
import Loading from '../../../../../components/Loading.jsx';
import Form from './Form/index.jsx';
import api from 'config/api.js';
import List from './List';
import Pasien from './Pasien';

class index extends React.Component {

	state = {
		data: [],
		
		loading: false,

		openForm: false,
		openList: false,
	}

	componentDidMount() {
		this.refreshTable();
	}

	refreshTable = () => {
		this.setState({ loading: true }, () => {
			api.get(`/member/calon`)
			.then(result => {
				this.setState({ data: result.data, loading: false });
			})
			.catch(error => {
				// console.log(error.response);
				this.setState({ loading: false });
			});
		});
	}
	
	render() {
		const { openForm, openList, data } = this.state;
		
		return (
			<>
				<div className="relative">
					
					<div className="grid grid-cols-3 gap-4 mb-2">
						<div className='flex gap-1'>
							<Button
								onClick={() => this.setState({ openForm: true })}
								type="primary"
							>Tambah Calon Pelanggan</Button>
						</div>
						<div className="text-center text-sm text-gray-700">

						</div>
						<div className="text-right">
							<TextField
								placeholder="Cari..."
								onEnter={(value) => {
									this.setState({ loading: true, loadingSearch: true }, () => {
										api.get(`/member/search?limit=100`, {
											headers: { search: value, }
										})
										.then(result => {
											this.setState({ data: result.data, loading: false, loadingSearch: true });
										})
										.catch(error => {
											console.log(error.response);
											this.setState({ loading: false, loadingSearch: true });
										});
									});
								}}
								loading={this.loadingSearch}
								className="w-full"
								title="Berdasarkan Nama / No. Handphone"
							/>
						</div>
					</div>
					<div className="grid grid-cols-4 gap-3 overflow-auto" style={{ height: "calc(100vh - 100px)" }}>
						{
							data.map((row, i) => (
								<Pasien
									key={i}
									row={row}
									refreshTable={this.refreshTable}
								/>
							))
						}
					</div>

					{ this.state.loading ? <Loading /> : null }

				</div>
				{
					openForm ?
						<Form
							edit={false}
							close={() => this.setState({ openForm: false })}
							closeRefresh={() => this.setState({ openForm: false }, () => this.refreshTable())}
						/>
					: null
				}
				{
					openList ?
						<List
							close={() => this.setState({ openList: false })}
						/>
					: null
				}
			</>
		);
	}

};

export default index;

import React from 'react';
import {
    Button,
	Drawer,
	notification,
    Skeleton,
} from 'antd';
import api from 'config/api.js';
import PaketLayanan from '../Form/Content/PaketLayanan';
import Dokumen from '../Form/Content/Dokumen';

class AddLangganan extends React.Component {

	state = {
		dataLayanan: {
            id_paket: null,
            nama_paket: null,
			tgl_langganan: null,
			odp: null,
			koordinat: null,
			tipe_modem: null,
			sn_modem: null,
			tarif: null,
        },
		dataDokumen: {
			modem: null,
			rumah: null,
			redaman: null,
			ktp: null,
		},

        loading: false,
	}

	simpan = () => {
		this.setState({ loading: true }, () => {
			api.post(`/member/transaksi`, this.state)
			.then(result => {
				
				this.setState({
					page: result.data.page,
					limit: result.data.limit,
					datas: result.data.datas,

					loading: false,
				});
			})
			.catch(error => {
				this.setState({ loading: false });
				notification.error({ message: error.response.data });
			});
		});
	}

    onDataChange = data => {
		this.setState({ ...data });
    }
	
	render() {
		const { dataLayanan, dataDokumen, loading } = this.state;
        
        return (
			<>
				<Drawer
					width="90%"
					open={true}
					onClose={this.props.close}
                    title={<div className='flex justify-between'>
                        <div>Tambah Langganan</div>
                        <div>
                            {
                                loading ? <Skeleton active /> :
                                <Button onClick={this.simpan} size='small' type='primary'>Simpan</Button>
                            }
                        </div>
                    </div>}
                    footer={false}
				>
                    <div className="grid grid-cols-2 gap-4">
                        <div>
                            <div className="border border-green-300">
                                <div className="w-full border-none bg-green-100 text-green-600 p-1">Paket Layanan</div>
                                <div className="grid grid-cols-2 gap-2 content-start overflow-scroll p-3" style={{ height: "calc(100vh - 200px)" }}>
                                    <PaketLayanan
                                        data={dataLayanan}
                                        onDataChange={(data) => this.onDataChange({ dataLayanan: { ...dataLayanan, ...data } })}
                                    />
                                </div>
                            </div>
                        </div>
                        <div>
                            <div className="border border-yellow-300">
                                <div className="w-full border-none bg-yellow-100 text-yellow-600 p-1">Upload Dokumen</div>
                                <div className="grid grid-cols-2 gap-2 content-start overflow-scroll p-3" style={{ height: "calc(100vh - 200px)" }}>
                                    <Dokumen
                                        data={dataDokumen}
                                        onDataChange={dataDokumen2 => {
                                            this.onDataChange({
                                                dataDokumen: {
                                                    ...dataDokumen,
                                                    ...dataDokumen2,
                                                },
                                            });
                                        }}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
				</Drawer>
			</>
		);
	}

};

export default AddLangganan;

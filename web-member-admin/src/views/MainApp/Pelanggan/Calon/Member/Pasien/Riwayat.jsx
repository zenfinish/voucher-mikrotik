import React from 'react';
import {
	Modal,
	notification,
	Table,
	Tag,
	Popconfirm,
	Button,
} from 'antd';
import api from 'config/api.js';
import { tglIndo, separator } from 'config/helpers.js';
import Detail from '../List/Detail';
import AddLangganan from "./AddLangganan";

class Riwayat extends React.Component {

	state = {
		datas: [],
		page: null,
		limit: null,

		aktif: null,

		loading: false,
		openDetail: false,
		openAddLangganan: false,
	}

	componentDidMount() {
		this.fetchData();
	}

	fetchData = () => {
		const { data } = this.props;

		this.setState({ loading: true }, () => {
			api.get(`/member/transaksi?id_member=${data.id}&page=1&limit=10`)
			.then(result => {
				
				this.setState({
					page: result.data.page,
					limit: result.data.limit,
					datas: result.data.datas,

					loading: false,
				});
			})
			.catch(error => {
				this.setState({ loading: false });
				notification.error({ message: error.response.data });
			});
		});
	}
	
	render() {
		const { datas, loading, openDetail, aktif, openAddLangganan } = this.state;
		const { data } = this.props;
		
		return (
			<>
				<Modal
					centered
					width="100%"
					open={true}
					onCancel={this.props.close}
					footer={[
						<React.Fragment key="footer">
						</React.Fragment>
					]}
					title={<div className='flex gap-2'>
						<div>{data.nama}</div>
						<div>
							<Button size='small' onClick={() => this.setState({ openAddLangganan: true })}>Tambah Langganan</Button>
						</div>
					</div>}
				>
					<Table
                        dataSource={datas}
                        size='small'
                        bordered
                        rowKey={row => row.id_membertransaksi}
						loading={loading}
						// onRow={record => ({
						// 	onClick: () => this.setState({ aktif: record, openDetail: true }),
						// 	style: { cursor: "pointer" },
						// })}
                        columns={[
                            {
                                title: 'No',
								align: "center",
                                render: (text, row, i) => i+1,
                            },
							{
                                title: 'ID',
								align: "center",
                                render: row => `${row.nomor}${row.tgl_langganan2}`,
								onCell: record => ({
									onClick: () => this.setState({ aktif: record, openDetail: true }),
									style: { cursor: "pointer" },
								}),
                            },
							{
                                title: 'Layanan',
								align: "center",
                                dataIndex: 'nama_paket',
                            },
							{
                                title: 'ODP',
                                dataIndex: 'odp',
                            },
							{
                                title: 'Aktivasi',
								align: "center",
                                dataIndex: 'tgl_langganan',
								render: text => tglIndo(text),
                            },
							{
                                title: 'Tarif',
								align: "right",
                                dataIndex: 'tarif',
								render: text => separator(text, 0)
                            },
							{
                                title: 'Status',
								align: "center",
                                dataIndex: 'id_mikrotik',
								render: text => <Popconfirm
									title={text ? 'Nonaktifkan' : 'Aktifkan'}
									// onConfirm={confirm}
									// onCancel={cancel}
									okText="Ya"
									cancelText="Tidak"
								>
									<Tag
										color={text ? 'green' : 'red'}
										className='cursor-pointer'
									>{text ? 'Aktif' : 'Isolir'}</Tag>
								</Popconfirm>
                            },
							{
                                title: 'Action',
								render: row => <div className='w-full flex gap-1'>
									<Button type='primary' size='small'>Ubah Layanan</Button>
									<Button type='primary' size='small'>Hitory Pembayaran</Button>
									<Button type='primary' size='small'>Ubah Dokumen</Button>
								</div>
                            },
                        ]}
                    />
				</Modal>
				{
					!openDetail ? null :
					<Detail
						data={aktif}
						close={() => this.setState({ openDetail: false })}
					/>
				}
				{
					!openAddLangganan ? null :
					<AddLangganan
						data={aktif}
						close={() => this.setState({ openAddLangganan: false })}
					/>
				}
			</>
		);
	}

};

export default Riwayat;

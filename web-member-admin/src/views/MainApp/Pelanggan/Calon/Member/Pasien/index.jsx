import React from 'react';
import {
    Button,
} from 'antd';
import Card from '../../../../../../components/Card.jsx';
import {
	WhatsApp,
	Home,
} from '@material-ui/icons';
import Riwayat from './Riwayat.jsx';
import UpdateData from './UpdateData.jsx';

class index extends React.Component {
	
    state = {
        aktif: null,

        openRiwayat: false,
        openUpdateData: false,
    }
    
    render() {
		const { openRiwayat, openUpdateData, aktif } = this.state;
		const { row, refreshTable } = this.props;
		
		return (
			<>
                <Card className="flex flex-col" style={{ flex: '1 0 21%', height: '17rem' }}>
                    <div className="flex items-center mb-4">
                        <div className="block h-12 w-12">
                            <img
                                src={
                                    row.jkel === 'L' ? require('assets/imgs/man.svg') :
                                    row.jkel === 'P' ? require('assets/imgs/woman.png') :
                                    require('assets/imgs/logo.svg')
                                }
                                alt=""
                                className="h-full w-full object-cover rounded"
                            />
                        </div>
                        <div className="ml-2">
                            <div className="leading-tight text-sm font-bold">{row.nama.toUpperCase()}</div>
                        </div>
                    </div>

                    <div className="h-32 mb-2 overflow-auto grid grid-cols-1">
                        <div className="text-sm text-gray-700">
                            <div className="flex gap-2">
                                <WhatsApp />
                                <span>{row.hp}</span>
                            </div>
                            <div className="flex gap-2">
                                <Home />
                                <span>{row.alamat}</span>
                            </div>
                        </div>
                    </div>

                    <div className="grid grid-cols-2 gap-2">
                        <Button
                            onClick={() => { this.setState({ aktif: row, openRiwayat: true }) }}
                            size='small'
                            type='primary'
                        >Convert to Customer</Button>
                        <Button
                            onClick={() => { this.setState({ aktif: row, openRiwayat: true }) }}
                            size='small'
                            type='primary'
                            danger
                        >Hapus</Button>
                    </div>
                </Card>
                {
                    !openRiwayat ? null :
                    <Riwayat
                        close={() => this.setState({ openRiwayat: false })}
                        data={aktif}
                    />
                }
                {
                    !openUpdateData ? null :
                    <UpdateData
                        close={() => this.setState({ openUpdateData: false })}
                        closeRefresh={() => this.setState({ openUpdateData: false }, refreshTable)}
                        data={aktif}
                    />
                }
            </>
		);
	}

};

export default index;

import React from 'react';
import {
    Button,
	Drawer,
	notification,
    Skeleton,
} from 'antd';
import api from 'config/api.js';
import DataPribadi from '../Form/Content/DataPribadi';

class UpdateData extends React.Component {

	state = {
		dataPribadi: {
			nama: null,
			hp: null,
			alamat: null,
		},

        loading: false,
	}

    componentDidMount() {
        const { nama, hp, alamat } = this.props.data;

        this.setState({ dataPribadi: { nama, hp, alamat } });
    }

	simpan = () => {
		const { data, closeRefresh } = this.props;
		const { dataPribadi } = this.state;

        this.setState({ loading: true }, () => {
			api.patch(`/member/${data.id}`, {
                hp: dataPribadi.hp,
                alamat: dataPribadi.alamat,
            })
			.then(result => {
				
				closeRefresh();
			})
			.catch(error => {
				this.setState({ loading: false });
				notification.error({ message: error.response.data });
			});
		});
	}

    onDataChange = data => {
		this.setState({ ...data });
    }
	
	render() {
		const { dataPribadi, loading } = this.state;
        
        return (
			<>
				<Drawer
					width="90%"
					open={true}
					onClose={this.props.close}
                    title={<div className='flex justify-between'>
                        <div>Update Biodata</div>
                        <div>
                            {
                                loading ? <Skeleton active /> :
                                <Button onClick={this.simpan} size='small' type='primary'>Update</Button>
                            }
                        </div>
                    </div>}
                    footer={false}
				>
                    <div className="grid grid-cols-2 gap-4">
                        <div>
                            <div className="border border-green-300">
                                <div className="w-full border-none bg-green-100 text-green-600 p-1">Data Pribadi</div>
                                <div className="grid grid-cols-2 gap-2 content-start overflow-scroll p-3" style={{ height: "calc(100vh - 200px)" }}>
                                    <DataPribadi
                                        data={dataPribadi}
                                        onDataChange={(data) => this.onDataChange({ dataPribadi: { ...dataPribadi, ...data } })}
                                        edit
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
				</Drawer>
			</>
		);
	}

};

export default UpdateData;

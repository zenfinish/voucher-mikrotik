import React from 'react';
import { Tag, Skeleton } from 'antd';
import api from 'config/api.js';

class StatusModem extends React.Component {
	
	state = {
		isAktif: false,
		loading: false,
	}
	
	componentDidMount() {
		this.refreshTable();
	}

	refreshTable = () => {
		const { id } = this.props;

		this.setState({ loading: true }, () => {
			api.get(`/mikrotik/user-manager/session?user=${id}`)
			.then(result => {
				const datas = result.data;
				const isAktif = datas.filter(row => row.active === 'true');
				this.setState({ isAktif: isAktif.length === 1 ? true : false, loading: false });
			})
			.catch(error => {
				console.log(error.response);
			});
		});
	}
	
	render() {
		const { isAktif, loading } = this.state;
		
		return (
			<>
				{
					loading ? <Skeleton active /> :
					<Tag color={isAktif? 'green' : 'red'}>{isAktif ? 'Online' : 'Offline'}</Tag>
				}
			</>
		);
	}

};

export default StatusModem;

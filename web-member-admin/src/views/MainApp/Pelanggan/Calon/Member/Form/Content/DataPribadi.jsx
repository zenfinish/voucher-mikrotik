import React from 'react';
import { Input } from 'antd';

class DataPribadi extends React.Component {

	componentDidMount() {
        this.setState(this.props.data);
	}

    render() {
		const { edit } = this.props;

        return (
			<>
                <div className="col-span-2">
                    <label>Nama</label>
                    <Input
                        className="mb-2 w-full"
                        value={this.props.data.nama}
                        onChange={(e) => { this.props.onDataChange({ nama: e.target.value }); }}
                        disabled={edit ? true : false}
                    />
                </div>
                <div className="col-span-2">
                    <label>No. Handphone</label>
                    <Input
                        className="w-full"
                        value={this.props.data.hp}
                        onChange={(e) => { this.props.onDataChange({ hp: e.target.value }); }}
                    />
                </div>
                <div className="col-span-2">
                    <label>Alamat</label>
                    <Input.TextArea
                        className="w-full"
                        onChange={(e) => { this.props.onDataChange({ alamat: e.target.value }); }}
                        value={this.props.data.alamat}
                    />
                </div>
			</>
		);
	}

};

export default DataPribadi;

import React from 'react';
import PaketLayanan from './PaketLayanan';
import DataPribadi from './DataPribadi';
import Dokumen from './Dokumen';

class index extends React.Component {

	render() {
		const { data, onDataChange } = this.props;
		
		return (
			<>
				<div className="grid grid-cols-3 gap-4">
					<div>
						<div className="border border-blue-300">
							<div className="w-full border-none bg-blue-100 text-blue-600 p-1">Data Pribadi</div>
							<div className="grid grid-cols-2 gap-2 content-start overflow-scroll p-3" style={{ height: "calc(100vh - 200px)" }}>
								<DataPribadi
									data={this.props.data.dataPribadi}
									onDataChange={(data) => onDataChange({ dataPribadi: { ...this.props.data.dataPribadi, ...data } })}
									edit={this.props.edit}
								/>
							</div>
						</div>
					</div>
					<div>
						<div className="border border-green-300">
							<div className="w-full border-none bg-green-100 text-green-600 p-1">Paket Layanan</div>
							<div className="grid grid-cols-2 gap-2 content-start overflow-scroll p-3" style={{ height: "calc(100vh - 200px)" }}>
								<PaketLayanan
									data={data.dataLayanan}
									onDataChange={(data) => onDataChange({ dataLayanan: { ...this.props.data.dataLayanan, ...data } })}
									edit={this.props.edit}
								/>
							</div>
						</div>
					</div>
					<div>
						<div className="border border-yellow-300">
							<div className="w-full border-none bg-yellow-100 text-yellow-600 p-1">Upload Dokumen</div>
							<div className="grid grid-cols-2 gap-2 content-start overflow-scroll p-3" style={{ height: "calc(100vh - 200px)" }}>
								<Dokumen
									data={data.dataDokumen}
									onDataChange={dataDokumen => {
										onDataChange({
											dataDokumen: {
												...data.dataDokumen,
												...dataDokumen,
											},
										});
									}}
									edit={this.props.edit}
								/>
							</div>
						</div>
					</div>
				</div>
			</>
		);
	}

};

export default index;

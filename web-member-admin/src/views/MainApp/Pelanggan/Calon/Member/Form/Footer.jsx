import React from 'react';
import { Button } from 'antd';

class Footer extends React.Component {

	render() {
		return (
			<>
				<Button onClick={this.props.simpan} type="primary" loading={this.props.loading}>{this.props.edit ? "Update" : "Simpan"}</Button>
				<Button onClick={this.props.close} type="primary" danger>Close</Button>
			</>
		);
	}

};

export default Footer;

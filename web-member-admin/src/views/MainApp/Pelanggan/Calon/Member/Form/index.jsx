import React from 'react';
import {
	Modal,
	notification,
	Skeleton,
} from 'antd';
import Content from './Content';
import Footer from './Footer';
import api from 'config/api.js';

class index extends React.Component {

	state = {
		dataPribadi: {
			nama: null,
			hp: null,
			alamat: null,
		},
		dataLayanan: {
            id_paket: null,
            nama_paket: null,
			tgl_langganan: null,
			odp: null,
			koordinat: null,
			tipe_modem: null,
			sn_modem: null,
			tarif: null,
        },
		dataDokumen: {
			modem: null,
			rumah: null,
			redaman: null,
			ktp: null,
		},

		loading: false,
	}

	onDataChange = data => {
		this.setState({ ...data });
    }

	simpan = () => {
		const { dataPribadi, dataLayanan, dataDokumen } = this.state;
		const formData = new FormData();

        formData.append("dataPribadi", JSON.stringify(dataPribadi));
        formData.append("dataLayanan", JSON.stringify(dataLayanan));
        formData.append("modem", dataDokumen.modem);
        formData.append("rumah", dataDokumen.rumah);
        formData.append("redaman", dataDokumen.redaman);
        formData.append("ktp", dataDokumen.ktp);

		this.setState({ loading: true }, () => {
			api.post(`/member`, formData, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                },
            })
			.then(result => {
				this.props.closeRefresh();
			})
			.catch(error => {
				this.setState({ loading: false });
				notification.error({ message: error.response.data });
			});
		});
	}
	
	render() {
		const { loading } = this.state;
		
		return (
			<>
				<Modal
					centered
					width="100%"
					open={true}
					onCancel={this.props.close}
					footer={[
						<React.Fragment key="footer">
							{
								loading ? <Skeleton active /> :
								<Footer
									edit={this.props.edit}
									simpan={this.simpan}
									close={this.props.close}
								/>
							}
						</React.Fragment>
					]}
					title={`${this.props.edit ? "Edit" : "Daftar"}`}
				>
					<Content
						onDataChange={this.onDataChange}
						edit={this.props.edit}
						data={this.state}
					/>
				</Modal>

			</>
		);
	}

};

export default index;

import React from 'react';
import { Table, Drawer, Tag, Select, Button } from 'antd';
import api from 'config/api.js';
import { tglIndo, separator } from 'config/helpers.js';
import Detail from './Detail';
import Pembayaran from './Pembayaran';
import StatusLangganan from '../StatusLangganan';
import StatusModem from '../StatusModem';

class index extends React.Component {
	
	state = {
		datas: [],
		page: 1,
		limit: 10,
		bulan: new Date().getMonth() + 1,
		tahun: new Date().getFullYear(),

		aktif: null,

		loading: false,
		openDetail: false,
		openPembayaran: false,
	}
	
	componentDidMount() {
		this.refreshTable();
	}

	refreshTable = () => {
		const { page, limit, bulan, tahun } = this.state;
		
		this.setState({ datas: [], loading: true }, () => {
			api.get(`/member/transaksi/detail?page=${page}&limit=${limit}&bulan=${bulan}&tahun=${tahun}`)
			.then(result => {
				this.setState({ ...result.data, loading: false });
			})
			.catch(error => {
				// console.log(error.response);
				this.setState({ loading: false });
			});
		});
	}
	
	render() {
		const { datas, openDetail, openPembayaran, aktif, bulan, tahun } = this.state;
		
		return (
			<>
				<Drawer
					width="90%"
					open={true}
					onClose={this.props.close}
					footer={false}
					title="Data Transaksi"
				>
                    <Table
                        dataSource={datas}
                        size='small'
                        bordered
                        rowKey={row => row.id_membertransaksidetail}
						title={() => <div className='w-full text-right flex gap-1'>
							<Select
								value={bulan}
								popupMatchSelectWidth={false}
								onChange={bulan => this.setState({ bulan })}
							>
								<Select.Option value={1}>Januari</Select.Option>
								<Select.Option value={2}>Februari</Select.Option>
								<Select.Option value={3}>Maret</Select.Option>
								<Select.Option value={4}>April</Select.Option>
								<Select.Option value={5}>Mei</Select.Option>
								<Select.Option value={6}>Juni</Select.Option>
								<Select.Option value={7}>Juli</Select.Option>
								<Select.Option value={8}>Agustus</Select.Option>
								<Select.Option value={9}>September</Select.Option>
								<Select.Option value={10}>Oktober</Select.Option>
								<Select.Option value={11}>November</Select.Option>
								<Select.Option value={12}>Desember</Select.Option>
							</Select>
							<Select
								value={tahun}
								popupMatchSelectWidth={false}
								onChange={tahun => this.setState({ tahun })}
							>
								<Select.Option value={2024}>2024</Select.Option>
								<Select.Option value={2025}>2025</Select.Option>
								<Select.Option value={2026}>2026</Select.Option>
							</Select>
							<Button type='primary' onClick={this.refreshTable}>Cari</Button>
						</div>}
						// onRow={record => ({
						// 	onClick: () => this.setState({ aktif: record, openDetail: true }),
						// 	style: { cursor: "pointer" },
						// })}
                        columns={[
                            {
                                title: 'No',
								align: "center",
                                render: (text, row, i) => i+1,
                            },
							{
                                title: 'ID',
								align: "center",
								onCell: record => ({
									onClick: () => this.setState({ aktif: record, openDetail: true }),
									style: { cursor: "pointer" },
								}),
                                render: row => `${row.nomor}${row.tgl_langganan2}`,
                            },
							{
                                title: 'Nama',
                                dataIndex: 'nama_member',
                            },
							{
                                title: 'Layanan',
								align: "center",
                                dataIndex: 'nama_paket',
                            },
							{
                                title: 'ODP',
                                dataIndex: 'odp',
                            },
							{
                                title: 'Aktivasi',
								align: "center",
                                dataIndex: 'tgl_aktivasi',
								render: text => tglIndo(text),
                            },
							{
                                title: 'Tarif',
								align: "right",
                                dataIndex: 'tarif',
								render: text => separator(text, 0)
                            },
							{
                                title: 'Pembayaran',
								align: "center",
								render: row => <Tag
									color={row.id_users ? 'green' : 'red'}
									className='cursor-pointer'
									onClick={row.id_users ? undefined : () => this.setState({ aktif: row, openPembayaran: true })}
								>{row.id_users ? 'Sudah' : 'Belum'}</Tag>
                            },
							{
                                title: 'Status Langganan',
								align: "center",
								render: row => <StatusLangganan id_mikrotik={row.id_mikrotik} />
                            },
							{
                                title: 'Status Modem',
								align: "center",
								render: row => <StatusModem id={`${row.nama_member.split(' ').join('.')}.${row.nomor}${row.tgl_langganan2}`} />
                            },
                        ]}
                    />
				</Drawer>
				{
					!openDetail ? null :
					<Detail
						data={aktif}
						close={() => this.setState({ openDetail: false })}
					/>
				}
				{
					!openPembayaran ? null :
					<Pembayaran
						data={aktif}
						close={() => this.setState({ openPembayaran: false })}
						closeRefresh={() => this.setState({ openPembayaran: false }, () => this.refreshTable())}
					/>
				}
			</>
		);
	}

};

export default index;

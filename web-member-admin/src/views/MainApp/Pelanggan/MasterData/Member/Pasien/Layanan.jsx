import React from 'react';
import { Drawer, notification, Skeleton, Button, Input, Select } from 'antd';
import api from 'config/api.js';
import { tglIndo } from 'config/helpers.js';

class Layanan extends React.Component {
	
	state = {
		nama_member: null,
        hp: null,
        kode: null,
        tgl_langganan: null,

        id_paket: null,
        nama_paket: null,
        tarif: null,
        koordinat: null,
        alamat_modem: null,
        odp: null,
        tipe_modem: null,
        sn_modem: null,

        dataPaket: [],

		loading: false,
	}
	
	componentDidMount() {
		this.refreshTable();
        this.getPaket();
	}

	refreshTable = () => {
		const { data } = this.props;

        this.setState({ loading: true }, () => {
			api.get(`/member/transaksi/${data.id_membertransaksi}`)
			.then(result => {
				
                this.setState({
                    ...result.data,
                    loading: false,
                });
			})
			.catch(error => {
				notification.error({ message: error.response.data });
				this.setState({ loading: false });
			});
		});
	}

    update = () => {
		const { data, closeRefresh } = this.props;
        const {
            id_paket,
            nama_paket,
            tarif,
            koordinat,
            alamat_modem,
            odp,
            tipe_modem,
            sn_modem,
        } = this.state;

        this.setState({ loading: true }, () => {
			api.patch(`/member/transaksi/${data.id_membertransaksi}`, {
                id_paket,
                nama_paket,
                tarif,
                koordinat,
                alamat_modem,
                odp,
                tipe_modem,
                sn_modem,

                id_mikrotik: data.id_mikrotik,
                kode: data.kode,
            })
			.then(result => {
				
                closeRefresh();
			})
			.catch(error => {
				notification.error({ message: error.response.data });
				this.setState({ loading: false });
			});
		});
	}

    getPaket = () => {
		this.setState({ loading: true }, () => {
			api.get(`/mikrotik/rest-api/user-manager/profile`)
			.then(result => {

                this.setState({ dataPaket: result.data, loading: false });
			})
			.catch(error => {
				// console.log(error.response);
				this.setState({ loading: false });
			});
		});
	}
	
	render() {
		const {
            nama_member,
            hp,
            kode,

            id_paket,
            tarif,
            koordinat,
            alamat_modem,
            odp,
            tgl_langganan,
            tipe_modem,
            sn_modem,

            dataPaket,

            loading,
        } = this.state;
        const { data } = this.props;
		
		return (
			<>
				<Drawer
					centered
                    width="60%"
					open={true}
					onClose={this.props.close}
					footer={false}
					title={<div className='flex justify-between'>
                        <div>Nama: {nama_member} - Nomor HP/WA: {hp}</div>
                        <div>
                            <Button size='small' type='primary' danger onClick={this.update}>Update</Button>
                        </div>
                    </div>}
				>
                    <div className='overflow-auto' style={{ height: "calc(100vh - 100px)" }}>
                        <div>
                            {
                                loading ? <Skeleton active /> :
                                <table className='w-full' cellPadding={5}>
                                    <tbody>
                                        <tr>
                                            <td>ID</td>
                                            <td>{data.id_membertransaksi}</td>
                                        </tr>
                                        <tr>
                                            <td>Nomor Pelanggan</td>
                                            <td>{kode}</td>
                                        </tr>
                                        <tr>
                                            <td>Tanggal Aktivasi</td>
                                            <td>{tglIndo(tgl_langganan)}</td>
                                        </tr>
                                        <tr>
                                            <td>Jenis Layanan</td>
                                            <td>
                                                <Select
                                                    loading={loading}
                                                    className='w-full'
                                                    value={id_paket}
                                                    onChange={(id_paket, option) => {
                                                        this.setState({
                                                            id_paket,
                                                            nama_paket: option.children,
                                                        });
                                                    }}
                                                >
                                                    {
                                                        dataPaket.map((row, i) => (
                                                            <Select.Option
                                                                key={i}
                                                                value={row['.id']}
                                                            >{row['name-for-users']}</Select.Option>
                                                        ))
                                                    }
                                                </Select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Tarif</td>
                                            <td>
                                                <Input
                                                    className='w-full'
                                                    type='number'
                                                    value={tarif}
                                                    onChange={e => this.setState({ tarif: e.target.value })}
                                                />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Koordinat</td>
                                            <td>
                                                <Input
                                                    className='w-full'
                                                    value={koordinat}
                                                    onChange={e => this.setState({ koordinat: e.target.value })}
                                                />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Alamat Modem</td>
                                            <td>
                                                <Input.TextArea
                                                    className='w-full'
                                                    value={alamat_modem}
                                                    onChange={e => this.setState({ alamat_modem: e.target.value })}
                                                />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Nomor ODP</td>
                                            <td>
                                                <Input
                                                    className='w-full'
                                                    value={odp}
                                                    onChange={e => this.setState({ odp: e.target.value })}
                                                />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Tipe Modem</td>
                                            <td>
                                                <Input
                                                    className='w-full'
                                                    value={tipe_modem}
                                                    onChange={e => this.setState({ tipe_modem: e.target.value })}
                                                />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>S/N Modem</td>
                                            <td>
                                                <Input
                                                    className='w-full'
                                                    value={sn_modem}
                                                    onChange={e => this.setState({ sn_modem: e.target.value })}
                                                />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            }
                        </div>
                    </div>
				</Drawer>
			</>
		);
	}

};

export default Layanan;

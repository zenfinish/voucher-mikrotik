import React from 'react';
import {
	Modal,
	notification,
	Table,
	Tag,
	Popconfirm,
	Button,
} from 'antd';
import api from 'config/api.js';
import { tglIndo, separator } from 'config/helpers.js';
import Detail from './Layanan';
import AddLangganan from "./AddLangganan";
import UpdateDokumen from "./UpdateDokumen";
import History from "./History";
import StatusLangganan from '../StatusLangganan';
import StatusModem from '../StatusModem';

class Riwayat extends React.Component {

	state = {
		datas: [],
		page: null,
		limit: null,

		aktif: null,

		loading: false,
		openDetail: false,
		openAddLangganan: false,
		openUpdateDokumen: false,
		openHistory: false,
	}

	componentDidMount() {
		this.fetchData();
	}

	fetchData = () => {
		const { data } = this.props;

		this.setState({ loading: true }, () => {
			api.get(`/member/transaksi?id_member=${data.id}&page=1&limit=10`)
			.then(result => {
				
				this.setState({
					page: result.data.page,
					limit: result.data.limit,
					datas: result.data.datas,

					loading: false,
				});
			})
			.catch(error => {
				this.setState({ loading: false });
				notification.error({ message: error.response.data });
			});
		});
	}

	confirm = (tipe, row) => {
		this.setState({ loading: true }, () => {
			api.post(`/member/transaksi/${tipe === "Nonaktifkan" ? 'nonaktif' : tipe === "Aktifkan" ? 'aktif' : null}`, {
				id_membertransaksi: row.id_membertransaksi,
				id_mikrotik: row.id_mikrotik,
			})
			.then(result => {
				
				this.fetchData();
			})
			.catch(error => {
				this.setState({ loading: false });
				notification.error({ message: error.response.data });
			});
		});
	}
	
	render() {
		const { datas, loading, openDetail, aktif, openAddLangganan, openUpdateDokumen, openHistory } = this.state;
		const { data } = this.props;
		
		return (
			<>
				<Modal
					centered
					width="100%"
					open={true}
					onCancel={this.props.close}
					footer={[
						<React.Fragment key="footer">
						</React.Fragment>
					]}
					title={<div className='flex gap-2'>
						<div>{data.nama}</div>
						<div>
							<Button size='small' onClick={() => this.setState({ openAddLangganan: true })}>Tambah Langganan</Button>
						</div>
					</div>}
				>
					<Table
                        dataSource={datas}
                        size='small'
                        bordered
                        rowKey={row => row.id_membertransaksi}
						loading={loading}
						// onRow={record => ({
						// 	onClick: () => this.setState({ aktif: record, openDetail: true }),
						// 	style: { cursor: "pointer" },
						// })}
                        columns={[
                            {
                                title: 'No',
								align: "center",
                                render: (text, row, i) => i+1,
                            },
							{
                                title: 'ID',
								align: "center",
								dataIndex: 'id_membertransaksi',
                                // render: row => `${row.nomor}${row.tgl_langganan2}`,
								// onCell: record => ({
								// 	onClick: () => this.setState({ aktif: record, openDetail: true }),
								// 	style: { cursor: "pointer" },
								// }),
                            },
							{
                                title: 'No Pelanggan',
								align: "center",
                                dataIndex: 'kode',
                            },
							{
                                title: 'Password',
								align: "center",
                                dataIndex: 'password',
                            },
							{
                                title: 'Layanan',
								align: "center",
                                dataIndex: 'nama_paket',
                            },
							{
                                title: 'Aktivasi',
								align: "center",
                                dataIndex: 'tgl_langganan',
								render: text => tglIndo(text),
                            },
							{
                                title: 'Tarif',
								align: "right",
                                dataIndex: 'tarif',
								render: text => separator(text, 0)
                            },
							{
                                title: 'Status Pembayaran',
								align: "center",
								render: row => <Popconfirm
									title={!row.status ? 'Nonaktifkan' : 'Aktifkan'}
									onConfirm={() => this.confirm(!row.status ? 'Nonaktifkan' : 'Aktifkan', row)}
									okText="Ya"
									cancelText="Tidak"
									disabled={row.status === "1" ? true : false}
								>
									<Tag
										color={!row.status ? 'green' : 'red'}
										className='cursor-pointer'
									>{row.status === "1" ? 'Isolir' : row.status === "2" ? 'Menunggu Pembayaran' : 'Aktif'}</Tag>
								</Popconfirm>
                            },
							{
                                title: 'Status Langganan',
								align: "center",
								render: row => <StatusLangganan id_mikrotik={row.id_mikrotik} />
                            },
							{
                                title: 'Status Modem',
								align: "center",
								render: row => <StatusModem id={row.kode} />
                            },
							{
                                title: 'Action',
								render: row => <div className='w-full flex flex-col gap-1'>
									<Button
										type='primary'
										size='small'
										onClick={() => this.setState({ aktif: row, openDetail: true })}
									>Layanan</Button>
									<Button
										type='primary'
										size='small'
										onClick={() => this.setState({ aktif: row, openHistory: true })}
									>History Pembayaran</Button>
									<Button
										type='primary'
										size='small'
										onClick={() => this.setState({ aktif: row, openUpdateDokumen: true })}
									>Dokumen</Button>
									<Button
										type='primary'
										danger
										size='small'
										onClick={() => this.setState({ aktif: row, openUpdateDokumen: true })}
									>Isolir</Button>
								</div>
                            },
                        ]}
                    />
				</Modal>
				{
					!openDetail ? null :
					<Detail
						data={aktif}
						close={() => this.setState({ openDetail: false })}
						closeRefresh={() => this.setState({ openDetail: false }, () => this.fetchData())}
					/>
				}
				{
					!openAddLangganan ? null :
					<AddLangganan
						id_member={data.id}
						close={() => this.setState({ openAddLangganan: false })}
						closeRefresh={() => this.setState({ openAddLangganan: false }, this.fetchData)}
					/>
				}
				{
					!openUpdateDokumen ? null :
					<UpdateDokumen
						data={aktif}
						close={() => this.setState({ openUpdateDokumen: false })}
					/>
				}
				{
					!openHistory ? null :
					<History
						data={aktif}
						close={() => this.setState({ openHistory: false })}
					/>
				}
			</>
		);
	}

};

export default Riwayat;

import React from 'react';
import {
	Drawer,
	notification,
	Table,
	// Button,
} from 'antd';
import api from 'config/api.js';
import { tglIndo, separator } from 'config/helpers.js';
import AddPembayaran from './AddPembayaran';
import _ from "lodash";

class index extends React.Component {

	state = {
		datas: [],
		page: null,
		limit: null,

		aktif: null,

		loading: false,
		openAddPembayaran: false,
	}

	componentDidMount() {
		this.fetchData();
	}

	fetchData = () => {
		const { data } = this.props;

		this.setState({ loading: true }, () => {
			api.get(`/member/transaksi/detail?id_membertransaksi=${data.id_membertransaksi}&page=1&limit=10`)
			.then(result => {
				const datas = _.orderBy(result.data.datas, ['tahun', 'bulan'], ['desc', 'desc']);

				this.setState({
					page: result.data.page,
					limit: result.data.limit,
					datas,

					loading: false,
				});
			})
			.catch(error => {
				this.setState({ loading: false });
				notification.error({ message: error.response.data });
			});
		});
	}
	
	render() {
		const { datas, loading, openAddPembayaran } = this.state;
		const { data } = this.props;
		
		return (
			<>
				<Drawer
					centered
					width="90%"
					open={true}
					onClose={this.props.close}
					footer={[
						<React.Fragment key="footer">
						</React.Fragment>
					]}
					title={<div className='flex gap-2'>
						<div>{data.nama_member} - {data.kode}</div>
						{/* <div>
							<Button size='small' onClick={() => this.setState({ openAddPembayaran: true })}>Tambah Pembayaran</Button>
						</div> */}
					</div>}
				>
					<Table
                        dataSource={datas}
                        size='small'
                        bordered
                        rowKey={row => row.id_membertransaksidetail}
						loading={loading}
						// onRow={record => ({
						// 	onClick: () => this.setState({ aktif: record, openDetail: true }),
						// 	style: { cursor: "pointer" },
						// })}
                        columns={[
                            {
                                title: 'No',
								align: "center",
                                render: (text, row, i) => i+1,
                            },
							{
                                title: 'Layanan',
								align: "center",
                                dataIndex: 'nama_paket',
                            },
							{
                                title: 'Bulan',
								align: "center",
								render: row => `${row.bulan} - ${row.tahun}`,
                            },
							{
                                title: 'Aktivasi',
								align: "center",
                                dataIndex: 'tgl_aktivasi',
								render: text => tglIndo(text),
                            },
							// {
                            //     title: 'Selesai',
							// 	align: "center",
                            //     dataIndex: 'tgl_selesai',
							// 	render: text => tglIndo(text),
                            // },
							// {
                            //     title: 'Tarif Awal',
							// 	align: "right",
                            //     dataIndex: 'tarif_awal',
							// 	render: text => separator(text, 0)
                            // },
							{
                                title: 'Tarif',
								align: "right",
                                dataIndex: 'tarif',
								render: text => separator(text, 0)
                            },
							{
                                title: 'Tgl Bayar',
								align: "right",
                                dataIndex: 'tgl_bayar',
								render: text => tglIndo(text),
                            },
                        ]}
                    />
				</Drawer>
				{
					openAddPembayaran ?
						<AddPembayaran
							id_membertransaksi={data.id_membertransaksi}
							close={() => this.setState({ openAddPembayaran: false })}
							closeRefresh={() => this.setState({ openAddPembayaran: false }, () => this.fetchData())}
						/>
					: null
				}
			</>
		);
	}

};

export default index;

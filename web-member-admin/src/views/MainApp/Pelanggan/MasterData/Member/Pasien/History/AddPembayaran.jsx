import React from 'react';
import {
    Button,
	Drawer,
	notification,
    Skeleton,
    Select,
    Input,
} from 'antd';
import api from 'config/api.js';

class AddPembayaran extends React.Component {

	state = {
		id_paket: null,
        nama_paket: null,
        tgl_aktivasi: null,
        tgl_selesai: null,
        bulan: null,
        tahun: 2014,
        tarif_awal: null,
        tarif: null,
        tgl_bayar: null,

        dataPaket: [],

        loading: false,
	}

    componentDidMount() {
        this.getPaket();
    }

	getPaket = () => {
		this.setState({ loading: true }, () => {
			api.get(`/mikrotik/rest-api/user-manager/profile`)
			.then(result => {

                this.setState({ dataPaket: result.data, loading: false });
			})
			.catch(error => {
				// console.log(error.response);
				this.setState({ loading: false });
			});
		});
	}

	simpan = () => {
		const { id_membertransaksi, closeRefresh } = this.props;

        this.setState({ loading: true }, () => {
			api.post(`/member/transaksi/detail`, {
                id_membertransaksi,
                ...this.state,
            })
			.then(result => {
				closeRefresh();
			})
			.catch(error => {
				console.log(error.response)
                this.setState({ loading: false });
				notification.error({ message: error.response.data });
			});
		});
	}

    onDataChange = data => {
		this.setState({ ...data });
    }
	
	render() {
		const {
            loading,

            dataPaket,

            id_paket,
            tgl_aktivasi,
            tgl_selesai,
            bulan,
            tahun,
            tarif_awal,
            tarif,
            tgl_bayar,
        } = this.state;
        
        return (
			<>
				<Drawer
					width="60%"
					open={true}
					onClose={this.props.close}
                    title={<div className='flex justify-between'>
                        <div>Tambah Pembayaran</div>
                        <div>
                            {
                                loading ? <Skeleton active /> :
                                <Button onClick={this.simpan} size='small' type='primary'>Simpan</Button>
                            }
                        </div>
                    </div>}
                    footer={false}
				>
                    <div>
                        <div>Paket</div>
                        <div>
                            <Select
                                loading={loading}
                                className='w-full'
                                value={id_paket}
                                onChange={(id_paket, option) => {
                                    this.setState({
                                        id_paket,
                                        nama_paket: option.children,
                                    });
                                }}
                            >
                                {
                                    dataPaket.map((row, i) => (
                                        <Select.Option
                                            key={i}
                                            value={row['.id']}
                                        >{row['name-for-users']}</Select.Option>
                                    ))
                                }
                            </Select>
                        </div>
                    </div>
                    <div>
                        <div>Tgl Aktivasi</div>
                        <div>
                            <Input
                                className='w-full'
                                value={tgl_aktivasi}
                                type='date'
                                onChange={e => this.setState({ tgl_aktivasi: e.target.value })}
                            />
                        </div>
                    </div>
                    <div>
                        <div>Tgl Selesai</div>
                        <div>
                            <Input
                                className='w-full'
                                value={tgl_selesai}
                                type='date'
                                onChange={e => this.setState({ tgl_selesai: e.target.value })}
                            />
                        </div>
                    </div>
                    <div>
                        <div>Bulan</div>
                        <div>
                            <Select
                                loading={loading}
                                className='w-full'
                                value={bulan}
                                onChange={bulan => this.setState({ bulan })}
                            >
                                <Select.Option value="1">1</Select.Option>
                                <Select.Option value="2">2</Select.Option>
                                <Select.Option value="3">3</Select.Option>
                                <Select.Option value="4">4</Select.Option>
                                <Select.Option value="5">5</Select.Option>
                                <Select.Option value="6">6</Select.Option>
                                <Select.Option value="7">7</Select.Option>
                                <Select.Option value="8">8</Select.Option>
                                <Select.Option value="9">9</Select.Option>
                                <Select.Option value="10">10</Select.Option>
                                <Select.Option value="11">11</Select.Option>
                                <Select.Option value="12">12</Select.Option>
                            </Select>
                        </div>
                    </div>
                    <div>
                        <div>Tahun</div>
                        <div>{tahun}</div>
                    </div>
                    <div>
                        <div>Tarif Awal</div>
                        <div>
                            <Input
                                className='w-full'
                                value={tarif_awal}
                                type='number'
                                onChange={e => this.setState({ tarif_awal: e.target.value })}
                            />
                        </div>
                    </div>
                    <div>
                        <div>Tarif</div>
                        <div>
                            <Input
                                className='w-full'
                                value={tarif}
                                type='number'
                                onChange={e => this.setState({ tarif: e.target.value })}
                            />
                        </div>
                    </div>
                    <div>
                        <div>Tgl Bayar</div>
                        <div>
                            <Input
                                className='w-full'
                                value={tgl_bayar}
                                type='date'
                                onChange={e => this.setState({ tgl_bayar: e.target.value })}
                            />
                        </div>
                    </div>
				</Drawer>
			</>
		);
	}

};

export default AddPembayaran;

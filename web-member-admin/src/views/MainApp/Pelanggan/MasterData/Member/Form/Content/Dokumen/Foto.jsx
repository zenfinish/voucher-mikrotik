import React from 'react';
import { Upload, Modal } from 'antd';
import { UploadOutlined } from '@ant-design/icons';

class Foto extends React.Component {

	state = {
        fileList: [],
        previewImage: null,
        previewVisible: false,
    }

    handleChange = ({ fileList }) => {
        const { onDataChange } = this.props;

        this.setState({ fileList }, () => {
            if (fileList.length > 0) {
                onDataChange(fileList[0].originFileObj);
            } else {
                onDataChange(null);
            };
        });
    }

    handlePreview = async (file) => {
        if (!file.url && !file.preview) {
            file.preview = URL.createObjectURL(file.originFileObj);
        };
        this.setState({
            previewImage: file.url || file.preview,
            previewVisible: true,
        });
    }

    render() {
		const { fileList, previewImage, previewVisible } = this.state;

        return (
			<>
                <Upload
                    accept="image/*"
                    listType="picture-card"
                    fileList={fileList}
                    onChange={this.handleChange}
                    onPreview={this.handlePreview}
                >
                    {
                        fileList.length > 0 ? null :
                        <div>
                            <UploadOutlined />
                            <div style={{ marginTop: 8 }}>Upload</div>
                        </div>
                    }
                </Upload>
                <Modal
                    open={previewVisible}
                    footer={null}
                    onCancel={() => this.setState({ previewVisible: false })}
                    width={400}
                >
                    <img
                        alt="preview"
                        style={{ width: '100%', height: 'auto', objectFit: 'contain' }} // Responsive image
                        src={previewImage}
                    />
                </Modal>
			</>
		);
	}

};

export default Foto;

import React from 'react';
import { Select, Input, InputNumber } from 'antd';
import api from 'config/api.js';

class PaketLayanan extends React.Component {

	state = {
        dataPaket: [],

        loading: false,
    }

    componentDidMount() {
        this.getPaket();
    }

	getPaket = () => {
		this.setState({ loading: true }, () => {
			api.get(`/mikrotik/rest-api/user-manager/profile`)
			.then(result => {

                this.setState({ dataPaket: result.data, loading: false });
			})
			.catch(error => {
				// console.log(error.response);
				this.setState({ loading: false });
			});
		});
	}
    
    render() {
		const { dataPaket, loading } = this.state;

        return (
			<>
                <div className="col-span-2">
                    <div>Tanggal Aktivasi</div>
                    <Input
                        className='w-full'
                        value={this.props.data.tgl_langganan}
                        type='date'
                        onChange={(e) => { this.props.onDataChange({ tgl_langganan: e.target.value }); }}
                    />
                </div>
                <div className="col-span-2">
                    <div>Nomor Antrean</div>
                    <Input
                        className='w-full'
                        value={this.props.data.nomor}
                        type='number'
                        onChange={(e) => { this.props.onDataChange({ nomor: e.target.value }); }}
                    />
                </div>
                <div className="col-span-2">
                    <div>Pilih Paket</div>
                    <Select
                        loading={loading}
                        className='w-full'
                        value={this.props.data.id_paket}
                        onChange={(id_paket, option) => {
                            this.props.onDataChange({
                                id_paket,
                                nama_paket: option.children,
                            });
                        }}
                    >
                        {
                            dataPaket.map((row, i) => (
                                <Select.Option
                                    key={i}
                                    value={row['.id']}
                                >{row['name-for-users']}</Select.Option>
                            ))
                        }
                    </Select>
                </div>
                <div className="col-span-2">
                    <div>No. ODP</div>
                    <Input
                        className='w-full'
                        value={this.props.data.odp}
                        onChange={(e) => { this.props.onDataChange({ odp: e.target.value }); }}
                    />
                </div>
                <div className="col-span-2">
                    <div>Koordinat</div>
                    <Input
                        className='w-full'
                        value={this.props.data.koordinat}
                        onChange={(e) => { this.props.onDataChange({ koordinat: e.target.value }); }}
                    />
                </div>
                <div className="col-span-2">
                    <div>Tipe Modem</div>
                    <Input
                        className='w-full'
                        value={this.props.data.tipe_modem}
                        onChange={(e) => { this.props.onDataChange({ tipe_modem: e.target.value }); }}
                    />
                </div>
                <div className="col-span-2">
                    <div>S/N Modem</div>
                    <Input
                        className='w-full'
                        value={this.props.data.sn_modem}
                        onChange={(e) => { this.props.onDataChange({ sn_modem: e.target.value }); }}
                    />
                </div>
                <div className="col-span-2">
                    <div>Tarif</div>
                    <InputNumber
                        className='w-full'
                        value={this.props.data.tarif}
                        formatter={(value) => `Rp ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                        parser={(value) => value?.replace(/Rp\s?|(,*)/g, '')}
                        onChange={tarif => this.props.onDataChange({ tarif })}
                    />
                </div>
			</>
		);
	}

};

export default PaketLayanan;

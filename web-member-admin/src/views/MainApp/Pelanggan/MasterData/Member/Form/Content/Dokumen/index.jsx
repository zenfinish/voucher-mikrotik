import React from 'react';
import Foto from './Foto';

class index extends React.Component {
    
    componentDidMount() {
        this.setState(this.props.data);
	}

    render() {
        const { data, onDataChange } = this.props;
        
        return (
			<>
                <div>
                    <label>Foto Modem</label>
                    <Foto
                        data={data.modem}
                        onDataChange={modem => {
                            onDataChange({
                                ...data,
                                modem,
                            });
                        }}
                    />
                </div>
                <div>
                    <label>Foto Rumah Tampak Depan</label>
                    <Foto
                        data={data.rumah}
                        onDataChange={rumah => {
                            onDataChange({
                                ...data,
                                rumah,
                            });
                        }}
                    />
                </div>
                <div>
                    <label>Foto Hasil Redaman</label>
                    <Foto
                        data={data.redaman}
                        onDataChange={redaman => {
                            onDataChange({
                                ...data,
                                redaman,
                            });
                        }}
                    />
                </div>
                <div>
                    <label>Foto KTP</label>
                    <Foto
                        data={data.ktp}
                        onDataChange={ktp => {
                            onDataChange({
                                ...data,
                                ktp,
                            });
                        }}
                    />
                </div>
			</>
		);
	}

};

export default index;

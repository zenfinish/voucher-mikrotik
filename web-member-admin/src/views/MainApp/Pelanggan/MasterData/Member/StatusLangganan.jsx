import React from 'react';
import { Tag, Skeleton } from 'antd';
import api from 'config/api.js';

class StatusModem extends React.Component {
	
	state = {
		isAktif: false,
		loading: false,
	}
	
	componentDidMount() {
		this.refreshTable();
	}

	refreshTable = () => {
		const { id_mikrotik } = this.props;

		this.setState({ loading: true }, () => {
			api.get(`/mikrotik/user-manager/user?.id=${id_mikrotik}`)
			.then(result => {
				const datas = result.data;

				if (datas.length === 1) {
					const isAktif = datas[0].disabled === "true" ? false : true;
					this.setState({ isAktif, loading: false });
				} else {
					this.setState({ loading: false });
				};
			})
			.catch(error => {
				console.log(error.response);
			});
		});
	}
	
	render() {
		const { isAktif, loading } = this.state;
		
		return (
			<>
				{
					loading ? <Skeleton active /> :
					<Tag color={isAktif? 'green' : 'red'}>{isAktif ? 'Aktif' : 'Nonaktif'}</Tag>
				}
			</>
		);
	}

};

export default StatusModem;

import React from 'react';
import { Modal, notification, Skeleton, Select, Button, Popconfirm } from 'antd';
import api from 'config/api.js';

class Pembayaran extends React.Component {
	
	state = {
        metode: "",
        
        loading: false,
    }
	
	simpan = () => {
		const { metode } = this.state;
		const { data, closeRefresh } = this.props;

        this.setState({ loading: true }, () => {
			api.post(`/member/transaksi/detail/bayar`, {
                id_membertransaksidetail: data.id_membertransaksidetail,
                id_mikrotik: data.id_mikrotik,
                metode,
            })
			.then(result => {
				
                closeRefresh();
			})
			.catch(error => {
				notification.error({ message: error.response.data });
				this.setState({ loading: false });
			});
		});
	}

	render() {
		const { loading, metode } = this.state;
        const { data } = this.props;
		
		return (
			<>
				<Modal
					centered
                    width="80%"
					open={true}
					onCancel={this.props.close}
					footer={[
                        <React.Fragment key="bayar">
                            {
                                loading ? <Skeleton active /> :
                                <Popconfirm
                                    title="Lakukan Pembayaran?"
                                    onConfirm={this.simpan}
                                    okText="Ya"
                                    cancelText="Tidak"
                                >
                                    <Button type='primary' danger>Bayar</Button>
                                </Popconfirm>
                            }
                        </React.Fragment>
                    ]}
					title={`Pembayaran`}
				>
                    <div>Nama : {data.nama_member}</div>
                    <div>ID : {`${data.nomor}${data.tgl_langganan2}`}</div>
                    <div>
                        <Select
                            value={metode}
                            popupMatchSelectWidth={false}
                            onChange={metode => this.setState({ metode })}
                        >
                            <Select.Option value="">- Pilih Metode Pembayaran -</Select.Option>
                            <Select.Option value="1">Transfer Manual</Select.Option>
                            <Select.Option value="2">Bayar Cash</Select.Option>
                        </Select>
                    </div>
				</Modal>
			</>
		);
	}

};

export default Pembayaran;

import React from 'react';
import { Table, Drawer, Tag, Select, Button, Space, Input, Card } from 'antd';
import { SearchOutlined } from '@ant-design/icons';
import api from 'config/api.js';
import { tglIndo, separator } from 'config/helpers.js';
import Detail from './Detail';
import Pembayaran from './Pembayaran';
import StatusLangganan from '../StatusLangganan';
import StatusModem from '../StatusModem';
import _ from "lodash";
import { CSVLink } from "react-csv";

class index extends React.Component {
	
	state = {
		datas: [],
		datasTemp: [],
		page: 1,
		limit: 1000,
		bulan: new Date().getMonth() + 1,
		tahun: new Date().getFullYear(),

		aktif: null,

		loading: false,
		openDetail: false,
		openPembayaran: false,

		pagination: {
			current: 1,
			pageSize: 10,
		},

		dataGroup: [],
	}
	
	componentDidMount() {
		this.refreshTable();
	}
	
	refreshTable = () => {
		const { page, limit, bulan, tahun } = this.state;
		
		this.setState({ datas: [], dataGroup: [], loading: true }, () => {
			api.get(`/member/transaksi/detail?page=${page}&limit=${limit}&bulan=${bulan}&tahun=${tahun}`)
			.then(result => {
				
				const datas = _.groupBy(result.data.datas, 'id_paket');
				const dataGroup = Object.keys(datas).map(id_paket => ({
					id_paket,
					nama_paket: datas[id_paket][0].nama_paket,
					datas: datas[id_paket],
				}));

				this.setState({
					...result.data,
					datasTemp: result.data.datas,
					dataGroup,
					loading: false,
				});
			})
			.catch(error => {
				// console.log(error.response);
				this.setState({ loading: false });
			});
		});
	}

	getColumnSearchProps = dataIndex => ({
		filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
		<div style={{ padding: 8 }}>
			<Input
			ref={node => {
				this.searchInput = node;
			}}
			placeholder={`Search ${dataIndex}`}
			value={selectedKeys[0]}
			onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
			onPressEnter={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
			style={{ width: 188, marginBottom: 8, display: 'block' }}
			/>
			<Space>
			<Button
				type="primary"
				onClick={() => {
				this.handleSearch(selectedKeys, confirm, dataIndex);
				}}
				icon={<SearchOutlined />}
				size="small"
				style={{ width: 90 }}
			>Search</Button>
			<Button onClick={() => this.handleReset(clearFilters)} size="small" style={{ width: 90 }}>
				Reset
			</Button>
			<Button
				type="link"
				size="small"
				onClick={() => {
				confirm({ closeDropdown: false });
				this.setState({
					searchText: selectedKeys[0],
					searchedColumn: dataIndex,
				});
				}}
			>
				Filter
			</Button>
			</Space>
		</div>
		),
		filterIcon: filtered => <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />,
		onFilter: (value, record) =>
		record[dataIndex]
			? record[dataIndex].toString().toLowerCase().includes(value.toLowerCase())
			: '',
		onFilterDropdownOpenChange: visible => {
		if (visible) {
			setTimeout(() => this.searchInput.select(), 100);
		}
		},
		render: text => 
		( text ),
	});

	handleSearch = (selectedKeys, confirm, dataIndex) => {
		confirm();
		this.setState({
		searchText: selectedKeys[0],
		searchedColumn: dataIndex,
		});
	};

	handleReset = clearFilters => {
		clearFilters();
		this.setState({ searchText: '' });
	};
	
	render() {
		const { datas, datasTemp, openDetail, openPembayaran, aktif, bulan, tahun, pagination, dataGroup } = this.state;
		
		return (
			<>
				<Drawer
					width="90%"
					open={true}
					onClose={this.props.close}
					footer={false}
					title={<div className='flex justify-between'>
						<div>Data Transaksi</div>
						<div className='flex gap-1'>
							<Select
								value={bulan}
								popupMatchSelectWidth={false}
								size='small'
								onChange={bulan => this.setState({ bulan })}
							>
								<Select.Option value={1}>Januari</Select.Option>
								<Select.Option value={2}>Februari</Select.Option>
								<Select.Option value={3}>Maret</Select.Option>
								<Select.Option value={4}>April</Select.Option>
								<Select.Option value={5}>Mei</Select.Option>
								<Select.Option value={6}>Juni</Select.Option>
								<Select.Option value={7}>Juli</Select.Option>
								<Select.Option value={8}>Agustus</Select.Option>
								<Select.Option value={9}>September</Select.Option>
								<Select.Option value={10}>Oktober</Select.Option>
								<Select.Option value={11}>November</Select.Option>
								<Select.Option value={12}>Desember</Select.Option>
							</Select>
							<Select
								value={tahun}
								popupMatchSelectWidth={false}
								size='small'
								onChange={tahun => this.setState({ tahun })}
							>
								<Select.Option value={2024}>2024</Select.Option>
								<Select.Option value={2025}>2025</Select.Option>
								<Select.Option value={2026}>2026</Select.Option>
							</Select>
							<Button type='primary' size='small' onClick={this.refreshTable}>Cari</Button>
						</div>
					</div>}
				>
					<Card
						size='small'
						title="Semua Data"
						style={{ backgroundColor: "cyan" }}
					>
						<table>
							<tbody>
								<tr>
									<td>Member Lama</td>
									<td>:</td>
									<td>
										<Tag
											onClick={() => {
												const datas = this.state.datas;
												const datas2 = datas.filter(row => row.is_baru !== "1");
												this.setState({ datasTemp: datas2 });
											}}
											className='cursor-pointer'
										>{datas.filter(row => row.is_baru !== "1").length}</Tag>
									</td>
									<td>=</td>
									<td align='right'>
										{
											separator(
												datas.filter(row => row.is_baru !== "1")
													.reduce((sum, data) => sum + data.tarif, 0)
											)
										}
									</td>
									<td>Sudah Bayar</td>
									<td>:</td>
									<td>
										<Tag
											onClick={() => {
												const datas = this.state.datas;
												const datas2 = datas.filter(row => row.id_users);
												this.setState({ datasTemp: datas2 });
											}}
											className='cursor-pointer'
										>{datas.filter(row => row.id_users).length}</Tag>
									</td>
								</tr>
								<tr>
									<td>Aktivasi Baru</td>
									<td>:</td>
									<td>
										<Tag
											onClick={() => {
												const datas = this.state.datas;
												const datas2 = datas.filter(row => row.is_baru === "1");
												this.setState({ datasTemp: datas2 });
											}}
											className='cursor-pointer'
										>{datas.filter(row => row.is_baru === "1").length}</Tag>
									</td>
									<td>=</td>
									<td align='right'>
										{
											separator(
												datas.filter(row => row.is_baru === "1")
													.reduce((sum, data) => sum + data.tarif, 0)
											)
										}
									</td>
									<td>Belum Bayar</td>
									<td>:</td>
									<td>
										<Tag
											onClick={() => {
												const datas = this.state.datas;
												const datas2 = datas.filter(row => !row.id_users);
												this.setState({ datasTemp: datas2 });
											}}
											className='cursor-pointer'
										>{datas.filter(row => !row.id_users).length}</Tag>
									</td>
								</tr>
								<tr>
									<td>Total</td>
									<td>:</td>
									<td>{datas.length}</td>
									<td>=</td>
									<td align='right'>
										{
											separator(
												datas.reduce((sum, data) => sum + data.tarif, 0)
											)
										}
									</td>
								</tr>
							</tbody>
						</table>
					</Card>
                    <div className='grid grid-cols-4 gap-3 mt-3'>
						{
							dataGroup.map((row, i) => (
								<Card
									size='small'
									key={i}
									title={row.nama_paket}
									className='cursor-pointer'
									onClick={() => this.setState({ datasTemp: row.datas })}
								>
									<table className='w-full'>
										<tbody>
											<tr>
												<td>Member Lama</td>
												<td>:</td>
												<td>{row.datas.filter(row => row.is_baru !== "1").length}</td>
												<td>=</td>
												<td align='right'>
													{
														separator(
															row.datas.filter(row => row.is_baru !== "1")
																.reduce((sum, data) => sum + data.tarif, 0)
														)
													}
												</td>
											</tr>
											<tr>
												<td>Aktivasi Baru</td>
												<td>:</td>
												<td>{row.datas.filter(row => row.is_baru === "1").length}</td>
												<td>=</td>
												<td align='right'>
													{
														separator(
															row.datas.filter(row => row.is_baru === "1")
																.reduce((sum, data) => sum + data.tarif, 0)
														)
													}
												</td>
											</tr>
											<tr>
												<td>Total</td>
												<td>:</td>
												<td>{row.datas.length}</td>
												<td>=</td>
												<td align='right'>
													{
														separator(
															row.datas.reduce((sum, data) => sum + data.tarif, 0)
														)
													}
												</td>
											</tr>
										</tbody>
									</table>
								</Card>
							))
						}
					</div>
					<Table
                        dataSource={datasTemp}
                        size='small'
                        bordered
                        rowKey={row => row.id_membertransaksidetail}
						// scroll={{
						// 	y: "calc(100vh - 200px)",
						// }}
						onChange={pagination => this.setState({ pagination })}
						pagination={{
							position: ["topRight"],
						}}
						// onRow={record => ({
						// 	onClick: () => this.setState({ aktif: record, openDetail: true }),
						// 	style: { cursor: "pointer" },
						// })}
						title={() => <div>
							<CSVLink
								filename={`Data-Transaksi.csv`}
								data={datasTemp.map((row) => {
									return {
										'ID': row.kode,
										'Nama': row.nama_member,
										'Layanan': row.nama_paket,
										'Aktivasi': tglIndo(row.tgl_aktivasi),
										'Tarif': separator(row.tarif, 0),
									};
								})}
							>Download CSV</CSVLink>
						</div>}
                        columns={[
                            {
                                title: 'No',
								align: "center",
								width: 50,
                                render: (_, __, i) => (pagination.current - 1) * pagination.pageSize + (i+1),
                            },
							{
                                title: 'ID',
								align: "center",
								// onCell: record => ({
								// 	onClick: () => this.setState({ aktif: record, openDetail: true }),
								// 	style: { cursor: "pointer" },
								// }),
                                render: row => row.kode,
                            },
							{
                                title: 'Nama',
                                dataIndex: 'nama_member',
								...this.getColumnSearchProps('nama_member'),
                            },
							{
                                title: 'Layanan',
								align: "center",
                                dataIndex: 'nama_paket',
                            },
							{
                                title: 'Aktivasi',
								align: "center",
                                dataIndex: 'tgl_aktivasi',
								render: text => tglIndo(text),
                            },
							{
                                title: 'Tarif',
								align: "right",
                                dataIndex: 'tarif',
								width: 100,
								render: text => separator(text, 0)
                            },
							{
                                title: 'Pembayaran',
								align: "center",
								render: row => <Tag
									color={row.id_users ? 'green' : 'red'}
									className='cursor-pointer'
									onClick={row.id_users ? undefined : () => this.setState({ aktif: row, openPembayaran: true })}
								>{row.id_users ? 'Sudah' : 'Belum'}</Tag>
                            },
							{
                                title: 'Status Langganan',
								align: "center",
								render: row => <StatusLangganan id_mikrotik={row.id_mikrotik} />
                            },
							{
                                title: 'Status Modem',
								align: "center",
								render: row => <StatusModem id={row.kode} />
                            },
                        ]}
                    />
				</Drawer>
				{
					!openDetail ? null :
					<Detail
						data={aktif}
						close={() => this.setState({ openDetail: false })}
					/>
				}
				{
					!openPembayaran ? null :
					<Pembayaran
						data={aktif}
						close={() => this.setState({ openPembayaran: false })}
						closeRefresh={() => this.setState({ openPembayaran: false }, () => this.refreshTable())}
					/>
				}
			</>
		);
	}

};

export default index;

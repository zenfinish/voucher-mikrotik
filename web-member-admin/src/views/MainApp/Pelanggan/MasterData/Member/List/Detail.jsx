import React from 'react';
import { Modal, notification, Skeleton } from 'antd';
import api from 'config/api.js';
import { tglIndo, separator } from 'config/helpers.js';

class Detail extends React.Component {
	
	state = {
		nama_member: null,
        hp: null,
        nama_paket: null,
        nomor: null,
        tgl_aktivasi2: null,
        tarif: null,
        koordinat: null,
        alamat: null,
        odp: null,
        tgl_aktivasi: null,
        tipe_modem: null,
        sn_modem: null,

        modem: null,
        rumah: null,
        redaman: null,
        ktp: null,

		loading: false,
		loadingModem: false,
		loadingRumah: false,
		loadingRedaman: false,
		loadingKtp: false,
	}
	
	componentDidMount() {
		this.refreshTable();
        this.fetchModem();
        this.fetchRumah();
        this.fetchRedaman();
        this.fetchKtp();
	}

	refreshTable = () => {
		const { data } = this.props;

        this.setState({ loading: true }, () => {
			api.get(`/member/transaksi/${data.id_membertransaksi}`)
			.then(result => {
				
                this.setState({
                    ...result.data,
                    loading: false,
                });
			})
			.catch(error => {
				notification.error({ message: error.response.data });
				this.setState({ loading: false });
			});
		});
	}

    fetchModem = () => {
        const { data } = this.props;

        this.setState({ loadingModem: true }, () => {
			api.get(`/member/transaksi/dokumen/modem/${data.id_membertransaksi}`, {
                responseType: 'blob',
            })
            .then(result => {
                
                const imageUrl = URL.createObjectURL(result.data);
                this.setState({
                    modem: imageUrl,
                    loadingModem: false,
                });
            })
            .catch(error => {
                notification.error({ message: error.response.data });
                this.setState({ loadingModem: false });
            });
		});
    }

    fetchRumah = () => {
        const { data } = this.props;

        this.setState({ loadingRumah: true }, () => {
			api.get(`/member/transaksi/dokumen/rumah/${data.id_membertransaksi}`, {
                responseType: 'blob',
            })
            .then(result => {
                
                const imageUrl = URL.createObjectURL(result.data);
                this.setState({
                    rumah: imageUrl,
                    loadingRumah: false,
                });
            })
            .catch(error => {
                notification.error({ message: error.response.data });
                this.setState({ loadingRumah: false });
            });
		});
    }

    fetchRedaman = () => {
        const { data } = this.props;

        this.setState({ loadingRedaman: true }, () => {
			api.get(`/member/transaksi/dokumen/redaman/${data.id_membertransaksi}`, {
                responseType: 'blob',
            })
            .then(result => {
                
                const imageUrl = URL.createObjectURL(result.data);
                this.setState({
                    redaman: imageUrl,
                    loadingRedaman: false,
                });
            })
            .catch(error => {
                notification.error({ message: error.response.data });
                this.setState({ loadingRedaman: false });
            });
		});
    }

    fetchKtp = () => {
        const { data } = this.props;

        this.setState({ loadingKtp: true }, () => {
			api.get(`/member/transaksi/dokumen/ktp/${data.id_membertransaksi}`, {
                responseType: 'blob',
            })
            .then(result => {
                
                const imageUrl = URL.createObjectURL(result.data);
                this.setState({
                    ktp: imageUrl,
                    loadingKtp: false,
                });
            })
            .catch(error => {
                notification.error({ message: error.response.data });
                this.setState({ loadingKtp: false });
            });
		});
    }
	
	render() {
		const {
            nama_member,
            hp,
            nama_paket,
            nomor,
            tgl_aktivasi2,
            tarif,
            koordinat,
            alamat,
            odp,
            tgl_aktivasi,
            tipe_modem,
            sn_modem,

            modem,
            rumah,
            redaman,
            ktp,

            loadingModem,
            loadingRumah,
            loadingRedaman,
            loadingKtp,

            loading,
        } = this.state;
		
		return (
			<>
				<Modal
					centered
                    width="100%"
					open={true}
					onCancel={this.props.close}
					footer={false}
					title={`Nama: ${nama_member} - Nomor HP/WA: ${hp}`}
				>
                    <div className='overflow-auto' style={{ height: "calc(100vh - 100px)" }}>
                        <div>
                            {
                                loading ? <Skeleton active /> :
                                <table className='w-full'>
                                    <tbody>
                                        <tr>
                                            <td>Jenis Layanan</td>
                                            <td>{nama_paket}</td>
                                        </tr>
                                        <tr>
                                            <td>Nomor Pelanggan</td>
                                            <td>{`${nomor}${tgl_aktivasi2}`}</td>
                                        </tr>
                                        <tr>
                                            <td>Tarif</td>
                                            <td>{separator(tarif, 0)}</td>
                                        </tr>
                                        <tr>
                                            <td>Koordinat</td>
                                            <td>{koordinat}</td>
                                        </tr>
                                        <tr>
                                            <td>Alamat</td>
                                            <td>{alamat}</td>
                                        </tr>
                                        <tr>
                                            <td>Nomor ODP</td>
                                            <td>{odp}</td>
                                        </tr>
                                        <tr>
                                            <td>Tanggal Aktifasi</td>
                                            <td>{tglIndo(tgl_aktivasi)}</td>
                                        </tr>
                                        <tr>
                                            <td>Tipe Modem</td>
                                            <td>{tipe_modem}</td>
                                        </tr>
                                        <tr>
                                            <td>S/N Modem</td>
                                            <td>{sn_modem}</td>
                                        </tr>
                                        <tr>
                                            <td>Foto KTP</td>
                                            <td>
                                                {
                                                    loadingKtp ? <Skeleton active /> :
                                                    <img src={ktp} alt="Uploaded" style={{ maxWidth: '100%', maxHeight: '400px' }} />
                                                }
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Foto Rumah Tampak Depan</td>
                                            <td>
                                                {
                                                    loadingRumah ? <Skeleton active /> :
                                                    <img src={rumah} alt="Uploaded" style={{ maxWidth: '100%', maxHeight: '400px' }} />
                                                }
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Foto Modem</td>
                                            <td>
                                                {
                                                    loadingModem ? <Skeleton active /> :
                                                    <img src={modem} alt="Uploaded" style={{ maxWidth: '100%', maxHeight: '400px' }} />
                                                }
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Foto Hasil Redaman</td>
                                            <td>
                                                {
                                                    loadingRedaman ? <Skeleton active /> :
                                                    <img src={redaman} alt="Uploaded" style={{ maxWidth: '100%', maxHeight: '400px' }} />
                                                }
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            }
                        </div>
                    </div>
				</Modal>
			</>
		);
	}

};

export default Detail;

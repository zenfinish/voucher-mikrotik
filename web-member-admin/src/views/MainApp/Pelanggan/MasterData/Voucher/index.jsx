import React from 'react';
import { Table } from 'antd';
import api from 'config/api.js';

class index extends React.Component {

	state = {
		data: [],
		loading: false,
	}

	componentDidMount() {
		this.refreshTable();
	}

	refreshTable = () => {
		this.setState({ loading: true }, () => {
			api.get(`/transactions/peruser`)
			.then(result => {
				console.log(result.data)
				this.setState({ data: result.data, loading: false });
			})
			.catch(error => {
				this.setState({ loading: false });
			});
			// setTimeout(() => {
			// 	this.setState({ data: [
			// 		{
			// 			nama_pelanggan: 'Agus Sumarno',
			// 			id_pelanggan: '',
			// 			no_pelanggan: '1658672328066',
			// 			hp: '081287653389',
			// 			jkel: 'L',
			// 			nik: '1871132104500006',
			// 			alamat: 'Jl. Eboni No. 20 Kemiling Bandar Lampung',
			// 		},
			// 		{
			// 			nama_pelanggan: 'Siti Maisaroh',
			// 			id_pelanggan: '',
			// 			no_pelanggan: '1658679466093',
			// 			hp: '081287653389',
			// 			jkel: 'P',
			// 			nik: '1871132104500007',
			// 			alamat: 'Jl. Eboni No. 20 Kemiling Bandar Lampung',
			// 		},
			// 	], loading: false });
			// }, 3000);
		});
	}
	
	render() {
		const { data } = this.state;

		return (
			<>
				<Table
					size='small'
					dataSource={data}
					columns={[
						{
							title: "Hp",
							dataIndex: 'hp',
						},
						{
							title: "Tanggal",
							dataIndex: 'tgl',
						},
					]}
				/>
			</>
		);
	}

};

export default index;

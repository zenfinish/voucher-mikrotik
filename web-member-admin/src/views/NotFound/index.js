import React from 'react'
import NotFound from '../../components/atoms/NotFound'

const index = () => {
  return (
    <NotFound />
  )
}

export default index

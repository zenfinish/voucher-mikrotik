import React from 'react';
import { notification, Form, Input, Button } from 'antd';
import { Redirect } from 'react-router-dom'
import { icon } from "../../utils";
import { api } from '../../helpers';
import backgroundImg from '../../assets/imgs/background.jpg';
import logoImg from '../../assets/imgs/logo.png';

class Login extends React.Component {

	state = {
		redirect: false,
		loading: false,
	}

	// componentDidMount() {
	// 	this.setState({ loading: true }, () => {
	// 		api.get(`/pelayanan/user/cektoken`, {
	// 			headers: { token: localStorage.getItem('token') }
	// 		})
	// 		.then(result => {
	// 			// console.log(result)
	// 			this.setState({ redirect: true });
	// 		})
	// 		.catch(error => {
	// 			this.setState({ loading: false });
	// 		});
	// 	});
	// }

	login = (values) => {
		this.setState({ loading: true }, () => {
			api.post(`/users/login`, {
				username: values.username,
				password: values.password,
			})
			.then(result => {
				// api.defaults.headers['token'] = result.token;
				localStorage.setItem('token', result);
				this.setState({ redirect: true });
			})
			.catch(error => {
				notification.error({
					message: 'Error',
					description: JSON.stringify(error),
				});
				this.setState({ loading: false })
			});
			// setTimeout(() => {
			// 	this.setState({ redirect: true });
			// }, 3000);
		});
	}
	
	render() {
		return (
			<>
				<div className="p-0 w-full h-screen lg:p-3">
					<div
						className="grid grid-cols-2 h-full flex items-center justify-center rounded bg-cover bg-no-repeat bg-center p-10"
						style={{ backgroundImage: `url(${backgroundImg})` }}
					>
						<div className="h-full flex flex-col justify-between">
							<div
								className="bg-white rounded-full w-24 h-24 block bg-contain bg-no-repeat bg-center p-2"
								style={{ backgroundImage: `url(${logoImg})` }}
							></div>
							<div className="text-white">
								<div className="text-5xl"><b>WMA</b></div>
								<div>Web Member Admin</div>
							</div>
							<div className="text-gray-600">
								<div>Verd IT Solution</div>
								<div>Telp. 08xx xxxx xxxx</div>
							</div>
						</div>
						<div className="flex items-center h-full justify-end">
							<div className="p-5 bg-white rounded-lg w-64">
								<h1 className="mb-3 text-xl">Silahkan Login</h1>
								<Form
									onFinish={this.login}
									initialValues={{
										username: '',
										password: '',
									}}
								>
									<Form.Item
										name="username"
										rules={[
											{
												required: true,
												message: "Username Kosong!",
											},
										]}
									><Input placeholder="Username" prefix={icon("TeamOutlined")} /></Form.Item>
									<Form.Item
										name="password"
										rules={[
											{
												required: true,
												message: 'Password Kosong!',
											},
										]}
									><Input.Password placeholder="Password" prefix={icon("KeyOutlined")} /></Form.Item>
									<Form.Item>
										<Button
											className="mb-3 w-full text-white h-10 p-2 rounded bg-blue-600"
											type="text"
											htmlType="submit"
											loading={this.state.loading}
										>Sign in</Button>
									</Form.Item>
								</Form>
								<p className="text-sm text-gray-600">
									&copy; 2019 Verd IT Solution
								</p>
							</div>
						</div>
					</div>
				</div>

				{
					this.state.redirect ?
						<Redirect to={`/main`} />
					: null
				}

			</>
		);
	}

};

export default (Login);

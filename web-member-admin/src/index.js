import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import 'styles/index.css';
import 'styles/tailwind.generate.css';
import 'antd/dist/reset.css';

ReactDOM.render(
		<App />,
	document.getElementById('root')
);

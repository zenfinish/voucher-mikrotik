import React from 'react';
import { Routes, store } from './config';
import { Provider } from 'react-redux';
import { RecoilRoot } from 'recoil';

function App() {
	return (
		<RecoilRoot>
			<Provider store={store}>
				<Routes />
			</Provider>
		</RecoilRoot>
	);

}
  
export default (App);

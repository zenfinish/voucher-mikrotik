import React from "react";
import { HashRouter as Router, Switch, Route } from "react-router-dom";
import { Login, MainApp, NotFound } from "../../views";

const Routes = () => {
	return (
		<Router>
			<Switch>
				<Route exact path="/"><Login /></Route>
				<Route path="/main"><MainApp /></Route>
				<Route><NotFound /></Route>
			</Switch>
		</Router>
	)
};

export default Routes;
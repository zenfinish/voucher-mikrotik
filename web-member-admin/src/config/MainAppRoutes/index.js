import React from "react";
import { Switch, Route, useRouteMatch } from "react-router-dom";
import {
	Home,
	NotFound,
} from "../../views/MainApp";
import Pelanggan from "./Pelanggan";
import Setting from "./Setting";
import Laporan from "./Laporan";

const MainAppRoutes = () => {
	const { path } = useRouteMatch();
	return (
		<Switch>
			<Route exact path={path}><Home /></Route>
			<Route path={`${path}/pelanggan`}><Pelanggan /></Route>
			<Route path={`${path}/setting`}><Setting /></Route>
			<Route path={`${path}/laporan`}><Laporan /></Route>
			<Route><NotFound /></Route>
		</Switch>
	)
};

export default MainAppRoutes;
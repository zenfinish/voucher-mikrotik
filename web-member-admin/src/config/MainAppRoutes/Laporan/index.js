import React from "react";
import { Route, useRouteMatch } from "react-router-dom";
import {
	Voucher,
} from "../../../views/MainApp/Laporan";

const Index = () => {
	const { path } = useRouteMatch();
	return (
		<>
			<Route path={`${path}/voucher`}><Voucher /></Route>
		</>
	)
};

export default Index;
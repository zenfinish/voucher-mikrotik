import React from "react";
import { Route, useRouteMatch } from "react-router-dom";
import {
	MasterData,
	Calon,
} from "../../../views/MainApp/Pelanggan";

const Administrasi = () => {
	const { path } = useRouteMatch();
	return (
		<>
			<Route path={`${path}/master-data`}><MasterData /></Route>
			<Route path={`${path}/calon`}><Calon /></Route>
		</>
	)
};

export default Administrasi;
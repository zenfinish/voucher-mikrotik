import React from "react";
import { Route, useRouteMatch } from "react-router-dom";
import {
	Pembayaran,
	ProfilPaket,
	Mikrotik,
	Nas,
} from "../../../views/MainApp/Setting";

const Index = () => {
	const { path } = useRouteMatch();
	return (
		<>
			<Route path={`${path}/pembayaran`}><Pembayaran /></Route>
			<Route path={`${path}/profil-paket`}><ProfilPaket /></Route>
			<Route path={`${path}/mikrotik`}><Mikrotik /></Route>
			<Route path={`${path}/nas`}><Nas /></Route>
		</>
	)
};

export default Index;
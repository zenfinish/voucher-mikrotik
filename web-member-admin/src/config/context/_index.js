import React, { createContext } from 'react';

const GlobalContext = createContext({ wkwkkw: "wkwkwk" });

class AppContext extends React.Component {

  render() {
    return (
      <GlobalContext.Provider value={{ wkwkkw: "wkwkwk" }}>
        {this.props.children}
      </GlobalContext.Provider>
    );
  }

}

// function AppContext() {
//   const UserContext = createContext();

//   const UserProvider = (props) => {
//     const [user, setUser] = useState();
//     const changeLang = e => setLang(e.target.value);
//     const langState = { lang, changeLang }; 
//     return (
//       <UserContext.Provider value={langState}>
//         {props.children}
//       </UserContext.Provider>
//     );
//   }
// }


export default AppContext;
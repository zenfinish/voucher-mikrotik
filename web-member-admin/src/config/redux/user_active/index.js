const initialState = {
  id_peluser: null,
  nama_peluser: null,
  dokter: null,
  bagian: null,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case 'USER_ACTIVE_SET_ALL':
      return {
        ...state,
        ...action.payload
      };
    default:
      return state;
  };
};

export default reducer;
import { api } from "../../../helpers";

export const USER_ACTIVE_CHECK_TOKEN = () => (dispatch) => {
	api.get(`/pelayanan/user/cektoken`, {
		headers: { token: localStorage.getItem('token') }
	})
	.then(result => {
		dispatch({
			type: 'USER_ACTIVE_SET_ALL',
			payload: result,
		});
		dispatch({ type: 'MENUS_SET_MAIN_MENUS', payload: result.menus, });
	})
	.catch(error => {
		// console.log(error)
		window.location.assign(`${process.env.REACT_APP_PUBLIC_URL}`);
	});
};



import { combineReducers } from "redux";

import user_active from "./user_active";
import menus from "./menus";

const reducer = combineReducers({ user_active, menus });

export default reducer;
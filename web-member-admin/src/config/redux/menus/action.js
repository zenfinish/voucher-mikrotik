export const MENUS_SET_MAIN_MENUS = (data) => (dispatch) => {
  dispatch({ type: 'MENUS_SET_MAIN_MENUS', payload: data, });
};

export const MENUS_SET_SUB_MENUS = (data) => (dispatch) => {
  dispatch({ type: 'MENUS_SET_SUB_MENUS', payload: data, });
};

export const MENUS_SET_HEADER_MENUS = (data) => (dispatch) => {
  dispatch({ type: 'MENUS_SET_HEADER_MENUS', payload: data, });
};
const initialState = {
  mainMenus: [],
  subMenus: {
    nama: '',
    menus: [],
  },
  headerMenus: [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case 'MENUS_SET_MAIN_MENUS':
      return {
        ...state,
        mainMenus: action.payload,
      };
    case 'MENUS_SET_SUB_MENUS':
      return {
        ...state,
        subMenus: action.payload
      };
    case 'MENUS_SET_HEADER_MENUS':
      return {
        ...state,
        headerMenus: action.payload
      };
    default:
      return state;
  };
};

export default reducer;
import { createStore, combineReducers, compose, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

import dataToken from '../reducers/dataToken.js';

// createStore hanya bisa satu reducer, makanya kita pakai combineReducers supaya bisa lebih dari satu reducer.
const reducers = combineReducers({
   dataToken,
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(reducers, /* preloadedState, */ composeEnhancers(applyMiddleware(thunk)));

export default store;

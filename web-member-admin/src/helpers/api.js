import axios from 'axios';
import packageJson from '../../package.json';

const version = packageJson.version;
const api = axios.create({
	baseURL: process.env.REACT_APP_SERVER_URL,
});

api.interceptors.request.use(
	(config) => {
		config.headers['version'] = `${version}`;
		const token = localStorage.getItem('token');
		if (token) {
			config.headers['token'] = `${token}`;
		};
		return config;
	},
	(error) => {
		return Promise.reject(error);
	},
);

api.interceptors.response.use(
	(result) => {
		return result.data;
	},
	(error) => {
		if (error.response) {
			return Promise.reject(error.response.data);
		} else {
			return Promise.reject(error.message);
		};
	},
);

export default api;

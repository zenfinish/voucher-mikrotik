import api from '../config/api.js';

export default function() {
   return (dispatch) => {
      dispatch({ type: 'CEK_TOKEN_LOADING' });
      api.get(`/user/check-token`, {
         headers: {
            token: localStorage.getItem('token')
         }
      })
      .then(result => {
         dispatch({ type: 'CEK_TOKEN_SUCCESS', payload: result.data });
      })
      .catch(error => {
         dispatch({ type: 'CEK_TOKEN_ERROR' });
      });
   };
};
const express = require('express');
const cors = require('cors');
const http = require('http');
const randomstring = require("randomstring");
const { create } = require('@open-wa/wa-automate');

require('dotenv').config();
const mysql = require('./helpers/mysql.js');
const api = require('./helpers/connect-mikrotik.js');

const mikrotikRoute = require('./routes/mikrotik.js');
const userRoute = require('./routes/user.js');
const transactionRoute = require('./routes/transaction.js');
const profileRoute = require('./routes/profile.js');
const limitationRoute = require('./routes/limitation.js');
const roleRoute = require('./routes/role.js');
const membersRoute = require('./routes/members.js');
const pemberitahuanRoute = require('./routes/pemberitahuan.js');

const app = express();
const httpServer = http.createServer(app); 

const port = process.env.PORT || 3002;

app.use(cors());
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

app.use('/public', express.static('public'));

app.use('/users', userRoute);
app.use('/mikrotik', mikrotikRoute);
app.use('/transactions', transactionRoute);
app.use('/profile', profileRoute);
app.use('/limitation', limitationRoute);
app.use('/role', roleRoute);
app.use('/members', membersRoute);
app.use('/pemberitahuan', pemberitahuanRoute);

app.use('*', function(req, res) {
    res.status(404).json(`Link Tidak Ditemukan`);
});

const cekPengirim = (nomor) => {
    return new Promise((resolve, reject) => {
        mysql.query(`
            SELECT * FROM user WHERE whatsapp = '${nomor}'
        `, function(error, result) {
            if (error) {
                reject(error);
            } else {
                if (result.length > 0) {
                    resolve(result[0]);
                } else {
                    reject("Nomor Tidak Berlaku");
                };
            };
        });
    });
};

const toolUserManagerUserAdd = (profile) => {
    return new Promise((resolve, reject) => {
        api.connect()
		.then(function(conn) {
			const chan = conn.openChannel();
			let username1 = randomstring.generate({
				length: 2,
				charset: 'alphabetic',
				capitalization: 'uppercase'
			});
			let username2 = randomstring.generate({
				length: 2,
				charset: 'numeric',
				capitalization: 'uppercase'
			});
			let username = `${username1}${username2}`;
			const password = randomstring.generate({
				length: 5,
				charset: 'numeric',
				capitalization: 'uppercase'
			});
            chan.write('/tool/user-manager/user/add', {
                'username':`${username}`,
                'password':`${password}`,
                'customer': 'admin'
            })
            .then(function(result3) {
                
                setTimeout(() => {}, 2000);
                chan.write('/tool/user-manager/user/create-and-activate-profile', {
                    'numbers':`"${username}"`,
                    'profile':`${profile}`,
                    'customer': 'admin'
                })
                .then(function(result2) {
                    
                    result2.cmd.args['username'] = result3.cmd.args.username;
                    result2.cmd.args['password'] = result3.cmd.args.password;
                    resolve(result2);
                })
                .catch(function(error2) {
                    reject(error2.data.message);
                });
            })
            .catch(function(error) {
                reject(error);
            });
		})
        .catch(function(error) {
            reject(error);
        });
    });
};

const textSent = (data) => {
    return `${data.voucher} Anda Adalah:
User. ${data.username}
Password. ${data.password}
Bila ada kendala hubungi nomor. 085384980303`;
};

const cariProfil = (kode) => {
    return new Promise((resolve, reject) => {
        mysql.query(`
            SELECT * FROM profile WHERE kode = '${kode}'
        `, function(error, result) {
            if (error) {
                reject(error);
            } else {
                if (result.length > 0) {
                    resolve(result[0]);
                } else {
                    reject("Kode Tidak Ditemukan");
                };
            };
        });
    });
};

create({
    sessionId: "VOUCHER_MIKROTIK",
    authTimeout: 60, //wait only 60 seconds to get a connection with the host account device
    blockCrashLogs: true,
    disableSpins: true,
    headless: true,
    hostNotificationLang: 'PT_BR',
    logConsole: false,
    // popup: true,
    qrTimeout: 0, //0 means it will wait forever for you to scan the qr code,
    useStealth: true,
})
    .then((client) => {
        app.use(client.middleware());
        httpServer.listen(port, function() {
            console.log(`Running on port ${port}`)
        });
        client.onMessage(message => {
            const body = (message.body).split(" ");
            const sandi = body[0];
            const kode = body[1];
            const hp = body[2];
            if (sandi.toUpperCase() !== "V") return client.sendText(message.from, '👋 GAGAL!');
            
            const nomor = message.from.replace("62", "0").replace("@c.us", "");
            cekPengirim(nomor)
            .then((result) => {
                const user_id = result.id;
                const role_id = result.role_id;

                cariProfil(kode)
                .then((result) => {

                    const profil = result.name_for_users;
                    const price = result.price;
                    toolUserManagerUserAdd(profil)
                    .then((result) => {
                        const username = result.cmd.args.username;
                        const password = result.cmd.args.password;

                        mysql.beginTransaction((error) => {
                            if (error) return client.sendText(message.from, `👋 ${error.sqlMessage}`);;
                            
                            mysql.query(`
                                INSERT INTO transaction(username, password, voucher, price, role_id, user_id, hp)
                                VALUES('${username}', '${password}', '${profil}', '${price}', '${role_id}', '${user_id}', '${hp}')
                            `, (error, result) => {
                                if (error) {
                                    mysql.rollback(() => {
                                        client.sendText(message.from, `👋 ${error.sqlMessage}`);
                                    });
                                } else {
                                    
                                    mysql.query(`
                                        SELECT
                                            DATE_FORMAT(transaction.tgl, '%d %M %Y, %k:%i:%s') AS tgl,
                                            transaction.username,
                                            transaction.password,
                                            transaction.voucher,
                                            user.name AS user_name
                                        FROM transaction
                                        LEFT JOIN user ON transaction.user_id = user.id
                                        WHERE transaction.id = '${result.insertId}'
                                    `, (error, result) => {
                                        if (error) {
                                            mysql.rollback(() => {
                                                client.sendText(message.from, `👋 ${error.sqlMessage}`);
                                            });
                                        } else {
                                            const voucher = result[0].voucher;
                                            
                                            client.sendText(`62${hp.substring(1)}@c.us`, textSent({ username, password, voucher }))
                                            .then((result) => {
                                                if (result) {
                                                    mysql.commit(() => {
                                                        client.sendText(message.from, `BERHASIL...`);
                                                    });
                                                } else {
                                                    mysql.rollback(() => {
                                                        client.sendText(message.from, `👋 GAGAL Kirim Ke: ${hp}`);
                                                    });
                                                };
                                            })
                                            .catch((error) => {
                                                // console.log("guguk====", error)
                                                mysql.rollback(() => {
                                                    client.sendText(message.from, `👋 ${error}`);
                                                });
                                            });
                                        };
                                    });
                                };
                            });
                        });
                    })
                    .catch((error) => {
                        client.sendText(message.from, `👋 ${error}`);
                    });
                })
                .catch((error) => {
                    client.sendText(message.from, `👋 ${error}`);
                });
            })
            .catch((error) => {
                client.sendText(message.from, `👋 ${error}`);
            });
        });
    });

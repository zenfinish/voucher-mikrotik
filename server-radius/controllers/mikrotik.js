// https://www.npmjs.com/package/randomstring
const randomstring = require("randomstring");

const api = require('../helpers/connect-mikrotik.js');
const Transaction = require('../models/transaction.js');
// const Member = require('../models/member.js');
const mysql = require('../helpers/mysql.js');
const axios = require('axios').default;

class Mikrotik {

	static toolUserManagerUserAdd(req, res) {
		api.connect()
		.then(function(conn) {
			const chan = conn.openChannel();
			let username1 = randomstring.generate({
				length: 2,
				charset: 'alphabetic',
				capitalization: 'uppercase'
			});
			let username2 = randomstring.generate({
				length: 2,
				charset: 'numeric',
				capitalization: 'uppercase'
			});
			let username = `${username1}${username2}`;
			const password = randomstring.generate({
				length: 5,
				charset: 'numeric',
				capitalization: 'uppercase'
			});
				chan.write('/tool/user-manager/user/add', {
					'username':`${username}`,
					'password':`${password}`,
					'customer': 'admin'
				})
				.then(function(result3) {
						setTimeout(() => {
						}, 2000);
						chan.write('/tool/user-manager/user/create-and-activate-profile', {
							'numbers':`"${username}"`,
							'profile':`${req.body.profile}`,
							'customer': 'admin'
						})
						.then(function(result2) {
							
							Transaction.create({
								user_id: req.user.id,
								role_id: req.user.role_id,
								username: result3.cmd.args.username,
								password: result3.cmd.args.password,
								voucher: req.body.voucher,
								price: req.body.price,
								hp: req.body.hp,
							})
							.then(function(result) {
								
								Transaction.findById(result.insertId)
								.then(function(result2) {
									
// 									axios.post(`http://wa.telebot.my.id/wabot/api/send-message.php`, {
// 										"api_key": "68172c4edaca837ea3c2ff948a57ccc28c9db3a8",
// 										"sender": "6285384980303",
// 										"number": `"${req.body.hp}"`,
// 										"message": `${result2.voucher} Anda Adalah:
// User. ${result3.cmd.args.username}
// Password. ${result3.cmd.args.password}
// Bila ada kendala hubungi nomor. 085384980303`
									axios.post(`http://${process.env.IP_WABOT}/sendText`, {
										args: [
											`62${req.body.hp.substring(1)}@c.us`,    
											`${result2.voucher} Anda Adalah:
User. ${result3.cmd.args.username}
Password. ${result3.cmd.args.password}
Bila ada kendala hubungi nomor. 085384980303`   
										]
									})
									// .then((result) => {
									// 	console.log("masuk=======", result)
									// })
									// .catch((error) => {
									// 	console.log("keluar=======", error)
									// });
									res.status(200).json(result2);
								})
								.catch(function(error) {
									console.log(error)
									res.status(500).json(error.message);
								});
							})
							.catch(function(error) {
								console.log(error)
								res.status(500).json(error.message);
							})
						})
						.catch(function(error2) {
							console.log(error2)
							res.status(500).json(error2.data[0].value);
						});
				})
				.catch(function(error) {
					res.status(500).json(error);
				});
		})
	}

	static toolUserManagerUserAdd2(req, res) {
		api.connect()
			.then(function(conn) {
				const chan = conn.openChannel();
				chan.write('/tool/user-manager/user/add', {
					'username':`${req.body.username}`,
					'password':`${req.body.password}`,
					'customer': 'admin'
				})
				.then(function(result3) {
					chan.write('/tool/user-manager/user/create-and-activate-profile', {
						'numbers':`"${req.body.username}"`,
						'profile':`${req.body.profileName}`,
						'customer': 'admin'
					})
					.then(function(result2) {
						Member.create({
							username: req.body.username,
							password: req.body.password,
							email: req.body.email,
							hp: req.body.hp,
							userId: req.user._id,
							profiles: {
								profileId: req.body.profileId,
							},
						})
						.then(function(result) {
							let data = {
								username: result3.cmd.args.username,
								password: result3.cmd.args.password,
							};
							res.status(201).json(data);
						})
						.catch(function(error) {
							res.status(500).json(error.message);
						})
					})
					.catch(function(error) {
						res.status(500).json(error.data[0].value)
					});
				})
				.catch(function(error) {
					res.status(500).json(error.data[0].value);
				});
			})
	}

	static toolUserManagerProfilePrint(req, res) {
		api.connect()
			.then(function(conn) {
					const chan = conn.openChannel();
					chan.write('/tool/user-manager/profile/print')
						.then(function(result) {
					res.status(200).json(result.data);
						})
						.catch(function(error) {
					res.status(500).json(error);
						});
			})
	}

	static toolUserManagerProfileLimitationAdd(req, res) {
		api.connect()
		.then(function(conn) {
				const chan = conn.openChannel();
				chan.write('/tool/user-manager/profile/limitation/add', {
					name: `${req.body.name}`,
					'rate-limit-rx': `${req.body.rateLimitRx * 10485760}`,
					'rate-limit-tx': `${req.body.rateLimitTx * 10485760}`,
					owner: 'admin',
					'address-list': 'unknown',
				})
				.then(function(result) {
					res.status(201).json(result.data);
				})
				.catch(function(error) {
					res.status(500).json(error.data[0].value);
				});
		})
	}

	static toolUserManagerProfileAdd(req, res) {
		api.connect()
			.then(function(conn) {
					const chan = conn.openChannel();
			chan.write('/tool/user-manager/profile/add', {
						name: `${req.body.name}`,
				'name-for-users': `${req.body.nameUsers}`,
				validity: req.body.validity,
				price: req.body.price,
						owner: 'admin'
					})
						.then(function(result) {
					chan.write('/tool/user-manager/profile/profile-limitation/add', {
						'profile': req.body.name,
						'limitation': req.body.limitName,
					})
						.then(function(result2) {
							res.status(201).json('success');
						})
						.catch(function(error) {
							res.status(500).json(error.data[0].value);
						});
						})
						.catch(function(error) {
							res.status(500).json(error.data[0].value);
						});
			})
	}

	static getAllProfile(req, res) {
		api.connect()
			.then(function(conn) {
					const chan = conn.openChannel();
					chan.write('/tool/user-manager/profile/print')
						.then(function(result) {
					let result2 = [];
					result.data.map(result3 => {
									let data = {};
						data['profileId'] = result3[0].value;
									data['name'] = result3[1].value;
									data['price'] = result3[6].value;
						result2.push(data);
					})
					res.status(200).json(result2);
						})
						.catch(function(error) {
					res.status(500).json(error);
						});
			})
	}

	static getProfile(req, res) {
		api.connect()
			.then(function(conn) {
					const chan = conn.openChannel();
					chan.write('/tool/user-manager/profile/get', { number: req.params.profileId })
						.then(function(result) {
					
					let data = result.data[0].split(';');
					let hasil = [];
					for(let i = 0; i < data.length; i++) {
						hasil.push(data[i].split('='));
					};
					
					let hasil2 = {};
					hasil2['profileId'] = hasil[0][1];
					hasil2['name'] = hasil[1][1];
					hasil2['nameForUsers'] = hasil[2][1];
					hasil2['owner'] = hasil[4][1];
					hasil2['price'] = hasil[5][1];
					hasil2['validity'] = hasil[7][1];
					hasil2['limitation'] = {};
					hasil2.price = hasil2.price.slice(0, hasil2.price.length - 2);
					
					chan.write('/tool/user-manager/profile/profile-limitation/print')
						.then(function(result2) {
							let limitName = '';
							for (let i = 0; i < result2.data.length; i++) {
								if (result2.data[i][1].value === hasil2.name) {
									limitName = result2.data[i][2].value;
									hasil2.limitation.name = limitName;
									// hasil2.profileLimitationId = result2.data[i][0].value;
								}
							}
							chan.write('/tool/user-manager/profile/limitation/print')
								.then(function(result3) {
									for (let i = 0; i < result3.data.length; i++) {
										if (result3.data[i][1].value === limitName) {
											hasil2.limitation.limitId = result3.data[i][0].value;
										}
													}
									res.status(200).json(hasil2);
								})
								.catch(function(error) {
									res.status(500).json(error.data[0].value);
								});
						})
						.catch(function(error) {
							res.status(500).json(error.data[0].value);
						});
						})
						.catch(function(error) {
							res.status(500).json(error.data[0].value);
						});
			});
	}

	static updateProfile(req, res) {
		api.connect()
			.then(function(conn) {
				const chan = conn.openChannel();
				chan.write('/tool/user-manager/profile/set', {
					numbers: req.body.numbers,
					name: req.body.name,
							'name-for-users': req.body.nameUsers,
					owner: 'admin',
					validity: req.body.validity,
					price: req.body.price,
						})
							.then(function(result) {
						chan.write('/tool/user-manager/profile/profile-limitation/remove', {
							'numbers': req.body.profileLimitationId
						})
							.then(function(result2) {
								chan.write('/tool/user-manager/profile/profile-limitation/add', {
									'profile': req.body.name,
									'limitation': req.body.limitName,
								})
									.then(function(result3) {
										res.status(200).json('success');
									})
									.catch(function(error) {
										res.status(500).json(error.data[0].value);
									});
							})
							.catch(function(error) {
								console.log(error)
								res.status(500).json(error.data[0].value);
							});
							})
							.catch(function(error) {
						res.status(500).json(error.data[0].value);
							});
			});
	}

	static deleteProfile(req, res) {
		api.connect()
			.then(function(conn) {
				const chan = conn.openChannel();
				chan.write('/tool/user-manager/profile/remove', {
					numbers: req.params.profileId,
						})
							.then(function(result) {
						res.status(200).json('success');
							})
							.catch(function(error) {
						res.status(500).json(error.data[0].value);
							});
			});
	}

	static getLimit(req, res) {
		api.connect()
			.then(function(conn) {
					const chan = conn.openChannel();
					chan.write('/tool/user-manager/profile/limitation/print')
						.then(function(result) {
					let result2 = {};
					for (let i = 0; i < result.data.length; i++) {
						if (result.data[i][0].value === req.params.limitId) {
							result2.limitId = result.data[i][0].value;
							result2.name = result.data[i][1].value;
							result2.rateLimitRx = result.data[i][7].value / 10485760;
							result2.rateLimitTx = result.data[i][8].value / 10485760;
						}
					}
					res.status(200).json(result2);
						})
						.catch(function(error) {
					console.log(error)
					res.status(500).json(error.data[0].value);
						});
			});
	}

	static updateLimit(req, res) {
		api.connect()
		.then(function(conn) {
			const chan = conn.openChannel();
			chan.write('/tool/user-manager/profile/limitation/set', {
				numbers: req.body.numbers,
				name: req.body.name,
				'rate-limit-rx': `${req.body.rateLimitRx * 10485760}`,
				'rate-limit-tx': `${req.body.rateLimitTx * 10485760}`,
			})
			.then(function(result) {
				res.status(200).json('success');
			})
			.catch(function(error) {
				console.log(error)
				res.status(500).json(error.data[0].value);
			});
		});
	}

	// static tambahProfile(req, res) {
	// 	Member.findByIdAndUpdate(
	// 		{ _id: req.body.memberId, },
	// 		{ $push: { profiles: { profileId: req.body.profileId } } }
	// 	)
	// 	.then(function(result) {
	// 		let subProfileId = result.profiles[result.profiles.length - 1]._id;
	// 		api.connect()
	// 		.then(function(conn) {
	// 			const chan = conn.openChannel();
	// 			chan.write('/tool/user-manager/user/create-and-activate-profile', {
	// 				'profile':`${req.body.profileName}`,
	// 				'customer': 'admin',
	// 				'numbers': `${req.body.username}`,
	// 			})
	// 			.then(function(result) {
	// 				res.status(200).json('Success');
	// 			})
	// 			.catch(function(error) {
	// 				Member.findOneAndUpdate(
	// 					{ _id: req.body.memberId, },
	// 					{ $pull: { profiles: { profileId: subProfileId } } }
	// 				)
	// 				.then(function(result) {
	// 					res.status(500).json(error.data[0].value);
	// 				});
	// 			});
	// 		})
	// 		.catch(function(error) {
	// 			Member.findOneAndUpdate(
	// 				{ _id: req.body.memberId, },
	// 				{ $pull: { profiles: { profileId: subProfileId } } }
	// 			)
	// 			.then(function(result) {
	// 				res.status(500).json(error.data[0].value);
	// 			});
	// 		});
	// 	})
	// 	.catch(function(error) {
	// 		res.status(500).json(error.message);
	// 	});
	// }

	static postKick(req, res) {
		const user = req.body.user;
		api.connect()
            .then(function(conn) {
				const chan = conn.openChannel();

				chan.write('/ip/hotspot/active/print')
                    .then(function(result) {
						const data = result.data;
						const data2 = data.filter((row) => { return row[2].value === user });

						chan.write('/ip/hotspot/active/remove', {
							numbers: data2[0][0].value,
						})
							.then(function(result) {
								res.status(201).json(result);
							})
							.catch(function(error) {
								res.status(500).json(error.data[0].value);
							});
                    })
                    .catch(function(error) {
						res.status(500).json(error.data[0].value);
                    });
            })
	}
};

module.exports = Mikrotik;
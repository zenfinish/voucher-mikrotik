const Limitation = require('../models/limitation.js');
const api = require('../helpers/connect-mikrotik.js');

class LimitationController {

	static post(req, res) {
		Limitation.create({ ...req.body })
		.then(function(result) {
			let limitation_id = result.id;
			api.connect()
			.then(function(conn) {
				const chan = conn.openChannel();
				chan.write('/tool/user-manager/profile/limitation/add', {
					name: `${req.body.name}`,
					'download-limit': req.body.download * 1048576,
					'upload-limit': req.body.upload * 1048576,
					'transfer-limit': req.body.transfer * 1048576,
					// 'transfer-limit': req.body.transfer,
					'uptime-limit': `${req.body.masa_aktif}`,
					owner: 'admin',
					'address-list': 'unknown',
				})
				.then(function(result) {
					res.status(200).json('Sukses');
				})
				.catch(function(error) {
					Limitation.findByIdAndDelete(limitation_id)
					.then(function(result) {
						res.status(500).json(error.data[0].value);
					});
				});
			})
			.catch(function(error) {
				Limitation.findByIdAndDelete(limitation_id)
				.then(function(result) {
					res.status(500).json('Koneksi Mikrotik Error');
				});
			});
		})
		.catch(function(error) {
			res.status(500).json(error.message);
		});
	}

	static get(req, res) {
		Limitation.find()
		.then(function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error.message);
		});
	}

};

module.exports = LimitationController;
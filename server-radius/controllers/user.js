const User = require('../models/user.js');
const token = require('../helpers/jsonwebtoken.js');
const { hashingPassword, comparePassword } = require('../helpers/bcrypt.js');
const { cekUser, getModule } = require('../helpers');

class UserController {

	static register(req, res) {
		if (req.body.role) {
			User.create({
				name: req.body.fullname,
				username: req.body.username,
				password: hashingPassword('123456'),
				role_id: req.body.role,
			})
			.then(function(result) {
				res.status(201).json(result);
			})
			.catch(function(error) {
				res.status(500).json(error);
			})
		} else {
			res.status(500).json("Role masih kosong");
		}
	}

	static login(req, res) {
		cekUser(req.body.username)
		.then((result) => {
			
			let compare = comparePassword(req.body.password, result.password);
			if (compare) {
				getModule(result.id)
				.then((result2) => {

					result['modules'] = result2;
					delete result.password;
					token.createToken({ ...result }, function(error, token) {
						if (error) {
							res.status(500).json(error);
						} else {
							res.status(200).json(token);
						}
					});
				})
				.catch((error) => {
					res.status(500).json(error);
				});
			} else {
				res.status(500).json('Password Salah');
			};
		})
		.catch((error) => {
			res.status(500).json("User Tidak Ditemukan");
		});
	}

	static checkToken(req, res) {
		token.verifyToken(req.headers.token, function(error, decoded) {
			if (error) {
				res.status(500).json(error);
			} else {
				res.status(200).json(decoded);
			}
		});
	}

	static getBarcode(req, res) {
		token.createToken({ time: new Date().toLocaleString() }, function(error, token) {
			if (error) {
				res.status(500).json(error);
			} else {
				res.status(200).json(token);
			}
		});
	}

	static getAll(req, res) {
		User.find()
		.then(function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error);
		});
	}

	static updateUser(req, res) {
		User.findByIdAndUpdate({
			name: req.body.fullname,
			username: req.body.username,
			password: hashingPassword(req.body.password),
		})
		.then(function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error);
		});
	}

	static updatePassword(req, res) {
		User.findOne(req.user.username)
		.then(function(result) {
			if (result) {
				let compare = comparePassword(req.body.password_lama, result.password);
				if (compare) {
					User.updatePassword({
						username: req.user.username,
						password: hashingPassword(req.body.password_baru),
					})
					.then(function(result) {
						res.status(201).json(result);
					})
					.catch(function(error) {
						res.status(500).json(error);
					});
				} else {
					res.status(500).json('Password Lama Salah');
				}
			} else {
				res.status(500).json('Username Tidak Ditemukan');
			}
		})
		.catch(function(error) {
			console.log(error)
			res.status(500).json(error);
		});
	}

};

module.exports = UserController;
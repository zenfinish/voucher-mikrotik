const Transaction = require('../models/transaction.js');
const MemberTransactions = require('../models/member_transactions');
const mysql = require('../helpers/mysql.js');

class TransactionController {

	static getTransaction(req, res) {
		Transaction.find(req.body)
		.then(function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error.message);
		})
	}

	static getPerUser(req, res) {
		Transaction.getPerUser(req.user.id)
		.then(function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error.message);
		});
	}

	static getPerId(req, res) {
		Transaction.findById(req.params.transaction_id)
		.then(function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error.message);
		});
	}

	static getTotal(req, res) {
		Transaction.getTotal()
		.then(function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error.message);
		})
	}

	static getHarian(req, res) {
		Transaction.getHarian()
		.then(function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error.message);
		})
	}

	static getBulanan(req, res) {
		Transaction.getBulanan()
		.then(function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error.message);
		})
	}

	static getTahunan(req, res) {
		Transaction.getTahunan()
		.then(function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error.message);
		})
	}

	static getMember(req, res) {
		MemberTransactions.findByBulanAndTahun(mysql, req.query)
		.then(function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error.message);
		})
	}

	static getVoucherAll(req, res) {
		Transaction.getAll(mysql, req.query)
		.then(function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error.message);
		})
	}

};

module.exports = TransactionController;
const randomstring = require("randomstring");
const api = require('../helpers/connect-mikrotik.js');
const Member = require('../models/member.js');
const MemberTransactions = require('../models/member_transactions.js');
const mysql = require('../helpers/mysql.js');
const fs = require("fs");

const updateDisabled = (data) => {
	return new Promise((resolve, reject) => {
		mysql.query(`
			UPDATE members SET
				disabled = ${data.disabled === 'true' ? '1' : `NULL`}
			WHERE filter_id = '${data['.id']}';
		`, function(error, result) {
			if (error) {
				reject(error.sqlMessage);
			} else {
				resolve('berhasil=====')
			}
		});
	});
}

const getFilterId = (data) => {
	return new Promise((resolve, reject) => {
		api.connect()
		.then(async function(conn) {
			const chan = conn.openChannel();
			await chan.write('/ip/firewall/filter/get', {
				'.id': `${data}`
			})
			.then(function(result) {
				const data = result.data[0];
				const datas = data.split(';');
				let datass = {};
				for (let i = 0; i < datas.length; i++) {
					let guguk = datas[i].split('=');
					datass[guguk[0]] = guguk[1];
				}
				resolve(datass);
			})
			.catch(function(error) {
				reject(error);
			});
		})
		.catch(function(error) {
			reject(error);
		});
	});
}

class MembersController {

	static get(req, res) {
		Member.find(mysql)
		.then(function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error.message);
		})
	}

	static getProfiles(req, res) {
		mysql.query(`
			SELECT
				a.id AS member_profile_id,
				a.name AS member_profile_name,
				a.rate_limit,
				a.price
			FROM member_profiles a
		`, function(error, result) {
			if (error) {
				res.status(500).json(error.sqlMessage);
			} else {
				res.status(200).json(result);
			}
		})
	}

	static getFilterFilterId(req, res) {
		api.connect()
		.then(function(conn) {
			const chan = conn.openChannel();
			
			chan.write('/ip/firewall/filter/get', {
				'.id': `${req.params.filter_id}`
			})
			.then(function(result) {
				const data = result.data[0];
				const datas = data.split(';');
				let datass = {};
				for (let i = 0; i < datas.length; i++) {
					let guguk = datas[i].split('=');
					datass[guguk[0]] = guguk[1];
				}
				res.status(200).json(datass);
			})
			.catch(function(error) {
				res.status(500).json(error);
			});
		})
		.catch(function(error) {
			res.status(500).json(error);
		});
	}

	static getTransactionsTotal(req, res) {
		MemberTransactions.findTotal(mysql)
		.then(function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error);
		});
	}

	static getTransactionsMemberId(req, res) {
		MemberTransactions.findByMemberId(mysql, req.params)
		.then(function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error);
		});
	}

	static deleteTransactionsMemberTransactionId(req, res) {
		mysql.beginTransaction(function(error) {
			if (error) res.status(500).json(error);

			MemberTransactions.removeByMemberTransactionId(mysql, req.params)
			.then(function(result) {

				fs.unlink(`../public/${req.headers.img}`, (error) => {
					if (error) {
						mysql.rollback();
						res.status(500).json(error);
					} else {
						mysql.commit(function(error) {
							if (error) {
								mysql.rollback();
								res.status(500).json(error);
							} else {
								res.status(200).json(result);
							}
						});
					}
				});
				
			})
			.catch(function(error) {
				mysql.rollback();
				res.status(500).json(error);
			});

		});
	}

	static putFilterEnableFilterId(req, res) {
		api.connect()
		.then(function(conn) {
			const chan = conn.openChannel();
			
			chan.write('/ip/firewall/filter/enable', {
				'.id': `${req.params.filter_id}`
			})
			.then(function(result) {
				updateDisabled({ disabled: null, ['.id']: req.params.filter_id })
				.then(function(result) {
					res.status(201).json(result);
				})
				.catch(function(error) {
					res.status(500).json(error);
				});
			})
			.catch(function(error) {
				res.status(500).json(error);
			});
		})
		.catch(function(error) {
			res.status(500).json(error);
		});
	}

	static putFilterDisableFilterId(req, res) {
		api.connect()
		.then(function(conn) {
			const chan = conn.openChannel();
			
			chan.write('/ip/firewall/filter/disable', {
				'.id': `${req.params.filter_id}`
			})
			.then(function(result) {
				
				updateDisabled({ disabled: 'true', ['.id']: req.params.filter_id })
				.then(function(result) {
					res.status(201).json(result);
				})
				.catch(function(error) {
					res.status(500).json(error);
				});
			})
			.catch(function(error) {
				res.status(500).json(error);
			});
		})
		.catch(function(error) {
			res.status(500).json(error);
		});
	}

	static patchStatusFilter(req, res) {
		mysql.query(`
			SELECT * FROM members
		`, function(error, result) {
			if (error) {
				res.status(500).json(error.sqlMessage);
			} else {
				for (let i = 0; i < result.length; i++) {
					getFilterId(result[i].filter_id)
					.then(async function(result2) {
						
						await updateDisabled(result2)
						.catch(function(error) {
							return res.status(500).json(error);
						});
					})
					.catch(function(error) {
						return res.status(500).json(error);
					});
				}
				mysql.query(`
					SELECT * FROM members
				`, function(error, result) {
					if (error) {
						res.status(500).json(error.sqlMessage);
					} else {
						res.status(200).json(result);
					}
				});
			}
		});
	}

	static post(req, res) {
		const { hp, name, src_mac_address, member_profile_name } = req.body;
		
		api.connect()
		.then(function(conn) {
			
			const chan = conn.openChannel();
			const password = randomstring.generate({
				length: 6,
				charset: 'numeric',
				capitalization: 'uppercase'
			});
			req.body['password'] = password;
			chan.write('/ppp/secret/add', {
				'name':`${hp}`,
				'password':`${password}`,
				'profile': `${member_profile_name}`,
				'service': 'pppoe',
				'comment': `${name}`,
			})
			.then(function(result3) {
				req.body['id'] = result3.data[0];

				chan.write('/ip/firewall/filter/add', {
					'action': 'drop',
					'chain': 'forward',
					'disabled': 'yes',
					'comment': `${name}`,
					'src-mac-address': `${src_mac_address}`,
				})
				.then(function(result4) {
					req.body['filter_id'] = result4.data[0];
					
					Member.create(mysql, req.body)
					.then(function(result) {
						res.status(201).json(result);
					})
					.catch(function(error) {
						chan.write('/ppp/secret/remove', {
							numbers: `${req.body.id}`
						})
						.then(function(result) {
							res.status(500).json(error);
						});
					});
				})
				.catch(function(error) {
					chan.write('/ppp/secret/remove', {
						numbers: `${req.body.id}`
					})
					.then(function(result) {
						res.status(500).json(error);
					});
				});
			})
			.catch(function(error) {
				res.status(500).json(error);
			});
		});
	}

	static postBayar(req, res) {
		MemberTransactions.create(mysql, req.body)
		.then(function(result) {
			res.status(201).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error);
		});
	}

	static postProfile(req, res) {
		const { member_profile_name, rate_limit, price } = req.body;
		
		api.connect()
		.then(function(conn) {
			
			const chan = conn.openChannel();
			chan.write('/ppp/profile/add', {
				'local-address': `${process.env.LOCAL_ADDRESS}`,
				'only-one': 'no',
				'remote-address': 'pool-pppoe',
				'use-encryption': 'yes',

				'name':`${member_profile_name}`,
				'rate-limit':`${rate_limit}`,
			})
			.then(function(result3) {
				const member_profile_id = result3.data[0];

				mysql.query(`
					INSERT INTO member_profiles (id, name, rate_limit, price)
					VALUES ('${member_profile_id}', '${member_profile_name}', '${rate_limit}', '${price}')
				`, function(error, result) {
					if (error) {
						chan.write('/ppp/profile/remove', {
							'.id': `${member_profile_id}`
						})
						.then(function(result) {
							res.status(500).json(error);
						})
						// .catch(function(error) {
						// 	console.log('error==========', error);
						// });
					} else {
						res.status(201).json({ ...req.body, member_profile_id: member_profile_id });
					}
				});
			})
			.catch(function(error) {
				res.status(500).json(error);
			});
		});
	}

	static deleteMemberId(req, res) {
		const { member_id } = req.params;
		
		api.connect()
		.then(function(conn) {
			
			const chan = conn.openChannel();
			const password = randomstring.generate({
				length: 6,
				charset: 'numeric',
				capitalization: 'uppercase'
			});
			// chan.write('/ip/firewall/filter/getall')
			chan.write('/ppp/secret/remove', {
				numbers: `${member_id}`
			})
			.then(function(result3) {
				// console.log('masuk======', JSON.stringify(result3))
				Member.remove(mysql, { member_id: member_id })
				.then(function(result) {
					res.status(201).json(result);
				})
				.catch(function(error) {
					res.status(500).json(error);
				});
			})
			.catch(function(error) {
				res.status(500).json(error);
			});
		});
	}

};

module.exports = MembersController;
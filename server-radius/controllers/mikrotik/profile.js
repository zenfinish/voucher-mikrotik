const api = require('../../helpers/connect-mikrotik.js');

class MikrotikProfile {

    static toolUserManagerProfileEdit(req, res) {
		api.connect()
            .then(function(conn) {
				const chan = conn.openChannel();
                chan.write('/tool/user-manager/profile/set', {
					numbers: req.body.numbers,
					name: req.body.name,
                    'name-for-users': req.body.nameUsers,
					owner: 'admin',
					validity: req.body.validity,
					price: req.body.price,
                })
                    .then(function(result) {
						res.status(200).json(result.data);
                    })
                    .catch(function(error) {
						res.status(500).json(error.data[0].value);
                    });
            })
	}
	
    static toolUserManagerProfileLimitationPrint(req, res) {
		api.connect()
            .then(function(conn) {
				const chan = conn.openChannel();
                chan.write('/tool/user-manager/profile/limitation/print')
                    .then(function(result) {
						let result2 = [];
						for (let i = 0; i < result.data.length; i++) {
							let tmp = {};
							tmp.limitId = result.data[i][0].value;
							tmp.name = result.data[i][1].value;
							tmp.rateLimitRx = result.data[i][7].value;
							tmp.rateLimitTx = result.data[i][8].value;
							result2.push(tmp);
						}
						res.status(200).json(result2);
                    })
                    .catch(function(error) {
						res.status(500).json(error.data[0].value);
                    });
            })
	}
	
    static toolUserManagerProfileProfileLimitationPrint(req, res) {
		api.connect()
            .then(function(conn) {
				const chan = conn.openChannel();
                chan.write('/tool/user-manager/profile/profile-limitation/print')
                    .then(function(result) {
						let result2 = [];
						for (let i = 0; i < result.data.length; i++) {
							if (result.data[i][1].value === req.params.nameProfile) {
								result2.push(result.data[i]);
							}
						}
						res.status(200).json(result2);
                    })
                    .catch(function(error) {
						res.status(500).json(error.data[0].value);
                    });
            })
	}

};

module.exports = MikrotikProfile;

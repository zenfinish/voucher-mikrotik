const Profile = require('../models/profile.js');
const api = require('../helpers/connect-mikrotik.js');

class ProfileController {

	static post(req, res) {
		Profile.create({
			name: req.body.name,
			name_for_users: req.body.name_for_users,
			validity: req.body.validity,
			price: req.body.price,
			type: req.body.type,
			limitation_id: req.body.limitation_id
		})
		.then(function(result) {
			let profile_id = result.insertId;
			api.connect()
			.then(function(conn) {
				const chan = conn.openChannel();
				chan.write('/tool/user-manager/profile/add', {
					name: `${req.body.name}`,
					'name-for-users': `${req.body.name_for_users}`,
					validity: req.body.validity,
					price: req.body.price,
					owner: 'admin',
					'starts-at': 'logon',
				})
				.then(function(result) {
					chan.write('/tool/user-manager/profile/profile-limitation/add', {
						'profile': req.body.name,
						'limitation': req.body.limitation_name,
					})
					.then(function(result2) {
						res.status(200).json('Sukses');
					})
					.catch(function(error) {
						Profile.findByIdAndDelete(profile_id)
						.then(function(result) {
							res.status(500).json(error.data[0].value);
						});
					});
				})
				.catch(function(error) {
					Profile.findByIdAndDelete(profile_id)
					.then(function(result) {
						res.status(500).json(error.data[0].value);
					});
				});
			})
			.catch(function(error) {
				Profile.findByIdAndDelete(profile_id)
				.then(function(result) {
					res.status(500).json('Koneksi Mikrotik Error');
				});
			});
		})
		.catch(function(error) {
			res.status(500).json(error.message);
		});
	}

	static get(req, res) {
		Profile.get()
		.then(function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error.message);
		});
	}

	static getReguler(req, res) {
		Profile.getReguler()
		.then(function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error.message);
		});
	}

	static getMember(req, res) {
		Profile.getMember()
		.then(function(result) {
			res.status(200).json(result);
		})
		.catch(function(error) {
			res.status(500).json(error.message);
		});
	}

};

module.exports = ProfileController;
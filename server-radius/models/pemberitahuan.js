const mysql = require('../helpers/mysql.js');

class Pemberitahuan {

	static remove() {
		return new Promise((resolve, reject) => {
			mysql.query(`
				TRUNCATE TABLE pemberitahuan
			`, function(error, result) {
				if (error) {
					reject(error);
				} else {
					resolve(result);
				}
			});
		});
	}

	static create(data) {
		return new Promise((resolve, reject) => {
			mysql.query(`
				INSERT INTO pemberitahuan(name)
				VALUES('${data.name}')
			`, function(error, result) {
				if (error) {
					reject(error);
				} else {
					resolve(result);
				}
			});
		});
	}

	static get() {
		return new Promise((resolve, reject) => {
			mysql.query(`
				SELECT * FROM pemberitahuan
			`, function(error, result) {
				if (error) {
					reject(error);
				} else {
					resolve(result[0]);
				}
			});
		});
	}
	
}

module.exports = Pemberitahuan;

const mysql = require('../helpers/mysql.js');

class Profile {

	static create(data) {
		return new Promise((resolve, reject) => {
			mysql.query(`
				INSERT INTO profile(name, name_for_users, validity, price, type, limitation_id)
				VALUES('${data.name}', '${data.name_for_users}', '${data.validity}', '${data.price}', '${data.type}', '${data.limitation_id}')
			`, function(error, result) {
				if (error) {
					reject(error);
				} else {
					resolve(result);
				}
			});
		});
	}

	static findByIdAndDelete(profile_id) {
		return new Promise((resolve, reject) => {
			mysql.query(`
				DELETE FROM profile WHERE id = '${profile_id}'
			`, function(error, result) {
				if (error) {
					reject(error);
				} else {
					resolve(result);
				}
			});
		});
	}

	static get() {
		return new Promise((resolve, reject) => {
			mysql.query(`
				SELECT * FROM profile ORDER BY tgl DESC
			`, function(error, result) {
				if (error) {
					reject(error);
				} else {
					resolve(result);
				}
			});
		});
	}

	static getReguler() {
		return new Promise((resolve, reject) => {
			mysql.query(`
				SELECT * FROM profile WHERE type = '1' && hapus != '1' ORDER BY tgl DESC
			`, function(error, result) {
				if (error) {
					reject(error);
				} else {
					resolve(result);
				}
			});
		});
	}

	static getMember() {
		return new Promise((resolve, reject) => {
			mysql.query(`
				SELECT * FROM profile WHERE type = '2' ORDER BY tgl DESC
			`, function(error, result) {
				if (error) {
					reject(error);
				} else {
					resolve(result);
				}
			});
		});
	}
	
}

module.exports = Profile;


// const mongoose = require('mongoose');

// const Schema = mongoose.Schema;

// const profileSchema = new Schema({
// 	name: {
// 		type: String,
// 		required: [true, 'Name Profile Kosong'],
// 	},
// 	nameForUsers: {
// 		type: String,
// 		required: [true, 'Name For Users Kosong'],
// 	},
// 	validity: {
// 		type: String,
// 		required: [true, 'Validity Kosong'],
// 	},
// 	price: {
// 		type: Number,
// 		required: [true, 'Price Kosong'],
// 	},
// 	type: {
// 		type: Number,
// 		required: [true, 'Type Kosong'],
// 	},
// 	limitationId: {
// 		type: Schema.Types.ObjectId,
// 		ref: 'Limitation',
// 		required: [true, 'Limitation ID Kosong'],
// 	},
// }, { timestamps: true });

// const Profile = mongoose.model('Profile', profileSchema);

// module.exports = Profile;
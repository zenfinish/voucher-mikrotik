const mysql = require('../helpers/mysql.js');

class User {

	static findOne(username) {
		return new Promise((resolve, reject) => {
			mysql.query(`
				SELECT
					*, id AS user_id
				FROM user
				WHERE username = '${username}'
			`, function(error, result) {
				if (error) {
					reject(error);
				} else {
					resolve(result[0]);
				}
			});
		});
	}

	static find() {
		return new Promise((resolve, reject) => {
			mysql.query(`
				SELECT
					user.id,
					user.username,
					user.name,
					role.nama AS role_name
				FROM user
				LEFT JOIN role ON user.role_id = role.id
			`, function(error, result) {
				if (error) {
					reject(error);
				} else {
					resolve(result);
				}
			});
		});
	}

	static create(data) {
		return new Promise((resolve, reject) => {
			mysql.query(`
				INSERT INTO user(username, password, name, role_id, hapus)
				VALUES('${data.username}', '${data.password}', '${data.name}', '${data.role_id}', '0')
			`, function(error, result) {
				if (error) {
					reject(error);
				} else {
					resolve(result);
				}
			});
		});
	}

	static findByIdAndUpdate(data) {
		return new Promise((resolve, reject) => {
			mysql.query(`
				UPDATE user SET
					name = '${data.name}',
					password = '${data.password}'
				WHERE username = '${data.username}'
			`, function(error, result) {
				if (error) {
					reject(error);
				} else {
					resolve(result);
				}
			});
		});
	}

	static updatePassword(data) {
		return new Promise((resolve, reject) => {
			mysql.query(`
				UPDATE user SET
					password = '${data.password}'
				WHERE username = '${data.username}'
			`, function(error, result) {
				if (error) {
					reject(error);
				} else {
					resolve(result);
				}
			});
		});
	}
	
}

module.exports = User;

const mysql = require('../helpers/mysql.js');

class Transaction {

	static find(data) {
		return new Promise((resolve, reject) => {
			let batas = '';
			let batas2 = '';
			if (data.propinsi_id !== '') batas = `&& transaction.propinsi_id = ${data.propinsi_id}`;
			if (data.user_id !== '') batas2 = `&& transaction.user_id = ${data.user_id}`;
			const query = `
				SELECT
					DATE_FORMAT(transaction.tgl, '%d %M %Y %H:%i:%s') AS tgl,
					transaction.voucher,
					transaction.username,
					transaction.hp,
					FORMAT(transaction.price, 0) AS price_format,
					transaction.price,
					transaction.salah,
					propinsi.nama AS propinsi_nama,
					user.name AS user_name,
					(a.download / 1048576) AS download,
					(a.upload / 1048576) AS upload,
					b.value AS expiration
				FROM transaction
				LEFT JOIN propinsi ON transaction.propinsi_id = propinsi.id
				LEFT JOIN user ON transaction.user_id = user.id
				LEFT JOIN (
					SELECT
						a.username,
						SUM(acctinputoctets) AS download,
						SUM(acctoutputoctets) AS upload
					FROM radacct a
					GROUP BY a.username
				) a ON transaction.username = a.username
				LEFT JOIN (
					SELECT
						a.username,
						a.value
					FROM radcheck a
					WHERE a.attribute = 'Expiration'
					GROUP BY a.username
				) b ON transaction.username = b.username
				WHERE (transaction.tgl BETWEEN '${data.date1}' AND '${data.date2} 23:59:59') ${batas} ${batas2}
			`;
			// console.log(query)
			mysql.query(query, function(error, result) {
				if (error) {
					reject(error);
				} else {
					resolve(result);
				}
			});
		});
	}

	static getPerUser(user_id) {
		return new Promise((resolve, reject) => {
			mysql.query(`
				SELECT
					TIME(transaction.tgl) AS jam,
					transaction.voucher,
					FORMAT(transaction.price, 0) AS price_format,
					transaction.price
				FROM transaction
				WHERE transaction.user_id = '${user_id}' && transaction.tgl >= CURDATE()
				ORDER BY transaction.tgl DESC
			`, function(error, result) {
				if (error) {
					reject(error);
				} else {
					resolve(result);
				}
			});
		});
	}

	static findById(transaction_id) {
		return new Promise((resolve, reject) => {
			mysql.query(`
				SELECT
					DATE_FORMAT(transaction.tgl, '%d %M %Y, %k:%i:%s') AS tgl,
					transaction.username,
					transaction.password,
					transaction.voucher,
					user.name AS user_name
				FROM transaction
				LEFT JOIN user ON transaction.user_id = user.id
				WHERE transaction.id = '${transaction_id}'
			`, function(error, result) {
				if (error) {
					reject(error);
				} else {
					resolve(result[0]);
				}
			});
		});
	}

	static create(data) {
		return new Promise((resolve, reject) => {
			mysql.query(`
				INSERT INTO transaction(username, password, voucher, price, propinsi_id, user_id, hp)
				VALUES('${data.username}', '${data.password}', '${data.voucher}', '${data.price}', '${data.propinsi_id}', '${data.user_id}', '${data.hp}')
			`, function(error, result) {
				if (error) {
					reject(error);
				} else {
					resolve(result);
				}
			});
		});
	}

	static getTotal() {
		return new Promise((resolve, reject) => {
			mysql.query(`
				SELECT SUM(price) AS total FROM transaction
				WHERE transaction.salah IS NULL
			`, function(error, result) {
				if (error) {
					reject(error);
				} else {
					resolve(result[0]);
				}
			});
		});
	}

	static getHarian() {
		return new Promise((resolve, reject) => {
			mysql.query(`
				SELECT
					CONCAT(DAY(transaction.tgl), " : ", hariIndo(DAYNAME(transaction.tgl))) AS tanggal,
					SUM(transaction.price) AS total
				FROM transaction
				WHERE
					(DATE(transaction.tgl) BETWEEN NOW() - INTERVAL 20 DAY AND NOW()) &&
					transaction.salah IS NULL
				GROUP BY DAY(transaction.tgl)
				ORDER BY DATE(tgl) ASC LIMIT 20
			`, function(error, result) {
				if (error) {
					reject(error);
				} else {
					resolve(result);
				}
			});
		});
	}

	static getBulanan() {
		return new Promise((resolve, reject) => {
			mysql.query(`
				SELECT
					MONTHNAME(transaction.tgl) AS bulan,
					SUM(transaction.price) AS total
				FROM transaction
				WHERE
					(YEAR(transaction.tgl) = YEAR(NOW())) &&
					transaction.salah IS NULL
				GROUP BY MONTH(transaction.tgl)
			`, function(error, result) {
				if (error) {
					reject(error);
				} else {
					resolve(result);
				}
			});
		});
	}

	static getTahunan() {
		return new Promise((resolve, reject) => {
			mysql.query(`
				SELECT
					YEAR(transaction.tgl) AS tahun,
					SUM(transaction.price) AS total
				FROM transaction
				WHERE transaction.salah IS NULL
				GROUP BY tahun LIMIT 5
			`, function(error, result) {
				if (error) {
					reject(error);
				} else {
					resolve(result);
				}
			});
		});
	}

	static getAll() {
		return new Promise((resolve, reject) => {
			mysql.query(`
				SELECT
					SUM(a.price) AS total,
					a.hp
				FROM transaction a
				WHERE a.salah IS NULL && a.hp != ""
				GROUP BY a.hp LIMIT 5
			`, function(error, result) {
				if (error) {
					reject(error);
				} else {
					resolve(result);
				}
			});
		});
	}
	
}

module.exports = Transaction;

class Member {

	static find(connection) {
		return new Promise((resolve, reject) => {
			connection.query(`
				SELECT
					a.id,
					a.email,
					a.name,
					a.password,
					a.hp,
					a.created_at,
					a.address,
					a.src_mac_address,
					a.filter_id,
					a.disabled,
					DATE_FORMAT(a.bulan_daftar, '%Y-%m-%d') AS bulan_daftar,
					DATE_FORMAT(a.terakhir_bayar, '%Y-%m-%d') AS terakhir_bayar
				FROM members a
			`, function(error, result) {
				if (error) {
					reject(error);
				} else {
					resolve(result);
				}
			});
		});
	}

	static create(connection, data) {
		return new Promise((resolve, reject) => {
			connection.query(`
				INSERT INTO members (id, email, hp, name, password, address, src_mac_address, filter_id, member_profile_id, disabled)
				VALUES ('${data.id}', '${data.email}', '${data.hp}', '${data.name}', '${data.password}', '${data.address}', '${data.src_mac_address}', '${data.filter_id}', '${data.member_profile_id}', '1')
			`, function(error, result) {
				if (error) {
					reject(error.sqlMessage);
				} else {
					resolve(result);
				}
			});
		});
	}

	static remove(connection, data) {
		return new Promise((resolve, reject) => {
			connection.query(`
				DELETE FROM members WHERE id = '${data.member_id}'
			`, function(error, result) {
				if (error) {
					reject(error.sqlMessage);
				} else {
					resolve(result);
				}
			});
		});
	}
	
}

module.exports = Member;

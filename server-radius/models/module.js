const mysql = require('../helpers/mysql.js');

class Module {

	static findByUserId(user_id) {
		return new Promise((resolve, reject) => {
			mysql.query(`
				SELECT
					b.name AS module_name,
          b.id AS module_id,
          b.link
				FROM module_hak a
        JOIN module b ON a.module_id = b.id
				WHERE a.user_id = '${user_id}'
			`, function(error, result) {
				if (error) {
					reject(error);
				} else {
					resolve(result);
				}
			});
		});
	}
	
}

module.exports = Module;

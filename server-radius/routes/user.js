const express = require('express');

const UserController = require('../controllers/user.js');
const { isUser } = require('../middlewares/is-user.js');

const router = express.Router();

router.post('/register', isUser, UserController.register);
router.post('/login', UserController.login);
router.get('/check-token', UserController.checkToken);

router.get('/barcode', UserController.getBarcode);

router.put('/password', isUser, UserController.updatePassword);
router.get('/', UserController.getAll);
router.put('/:userId', isUser, UserController.updateUser);

module.exports = router;
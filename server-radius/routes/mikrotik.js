const express = require('express');

const mikrotikController = require('../controllers/mikrotik.js');
const MikrotikProfileController = require('../controllers/mikrotik/profile.js');
const { isUser } = require('../middlewares/is-user.js');

const router = express.Router();

router.post('/tool-user-manager-user-add', isUser, mikrotikController.toolUserManagerUserAdd);
router.post('/tool-user-manager-user-add2', isUser, mikrotikController.toolUserManagerUserAdd2);
router.get('/tool-user-manager-profile-print', mikrotikController.toolUserManagerProfilePrint);
router.post('/tool-user-manager-profile-limitation-add', isUser, mikrotikController.toolUserManagerProfileLimitationAdd);
router.post('/tool-user-manager-profile-add', isUser, mikrotikController.toolUserManagerProfileAdd);

router.get('/get-profile', mikrotikController.getAllProfile);
router.get('/get-profile/:profileId', mikrotikController.getProfile);
router.put('/update-profile', isUser, mikrotikController.updateProfile);
router.delete('/delete-profile/:profileId', isUser, mikrotikController.deleteProfile);

router.get('/get-limit/:limitId', mikrotikController.getLimit);
router.put('/update-limit', isUser, mikrotikController.updateLimit);

router.get('/tool-user-manager-profile-limitation-print', MikrotikProfileController.toolUserManagerProfileLimitationPrint);
router.get('/tool-user-manager-profile-profile-limitation-print/:nameProfile', MikrotikProfileController.toolUserManagerProfileProfileLimitationPrint);

router.post('/kick', isUser, mikrotikController.postKick);

// router.post('/tambah-profile', isUser, mikrotikController.tambahProfile);

module.exports = router;
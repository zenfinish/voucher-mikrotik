const express = require('express');
const MembersController = require('../controllers/members');
const { isUser } = require('../middlewares/is-user.js');
const multer = require('../middlewares/multer.js');
const router = express.Router();

router.get('/', isUser, MembersController.get);
router.post('/', isUser, MembersController.post);
router.delete('/:member_id', isUser, MembersController.deleteMemberId);

router.get('/profiles', isUser, MembersController.getProfiles);
router.post('/profile', isUser, MembersController.postProfile);

router.get('/transactions/total', isUser, MembersController.getTransactionsTotal);
router.get('/transactions/:member_id', isUser, MembersController.getTransactionsMemberId);
router.delete('/transactions/:member_transaction_id', isUser, MembersController.deleteTransactionsMemberTransactionId);

router.get('/filter/:filter_id', isUser, MembersController.getFilterFilterId);
router.put('/filter/enable/:filter_id', isUser, MembersController.putFilterEnableFilterId);
router.put('/filter/disable/:filter_id', isUser, MembersController.putFilterDisableFilterId);

router.patch('/status/filter', isUser, MembersController.patchStatusFilter);

router.post('/bayar',
  isUser,
  multer.single('file'),
  (req, res, next) => {
    // const url = req.protocol + '://' + req.get('host');
    // req.body['img'] = url + '/public/' + req.file.filename;
    req.body['img'] = req.file.filename;
    next()
  },
  MembersController.postBayar);
  
module.exports = router;

const express = require('express');

const PemberitahuanController = require('../controllers/pemberitahuan.js');
const { isUser } = require('../middlewares/is-user.js');

const router = express.Router();

router.post('/', isUser, PemberitahuanController.post);
router.get('/', PemberitahuanController.get);

module.exports = router;
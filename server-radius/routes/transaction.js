const express = require('express');

const Transaction = require('../controllers/transaction.js');
const { isUser } = require('../middlewares/is-user.js');

const router = express.Router();

router.post('/', isUser, Transaction.getTransaction);
router.get('/peruser', isUser, Transaction.getPerUser);
router.get('/perid/:transaction_id', isUser, Transaction.getPerId);
router.get('/total', isUser, Transaction.getTotal);
router.get('/harian', isUser, Transaction.getHarian);
router.get('/bulanan', isUser, Transaction.getBulanan);
router.get('/tahunan', isUser, Transaction.getTahunan);
router.get('/member', isUser, Transaction.getMember);
router.get('/voucher/all', isUser, Transaction.getVoucherAll);

module.exports = router;
const express = require('express');
const randomstring = require("randomstring");
const axios = require('axios');

const mysql = require('../helpers/mysql.js');
const { createVoucher } = require("../helpers");

const router = express.Router();

const cekPengirim = (nomor) => {
    const nomor2 = nomor.replace("62", "0").replace("@s.whatsapp.net", "");
    // console.log("nomor=======", nomor2)
    return new Promise((resolve, reject) => {
        mysql.query(`
            SELECT * FROM user WHERE whatsapp = '${nomor2}'
        `, function(error, result) {
            if (error) {
                reject(error);
            } else {
                if (result.length > 0) {
                    resolve(result[0]);
                } else {
                    reject("Nomor Tidak Berlaku");
                };
            };
        });
    });
};

const cariProfil = (kode) => {
    return new Promise((resolve, reject) => {
        mysql.query(`
            SELECT * FROM profile WHERE kode = '${kode.toUpperCase()}' && hapus IS NULL
        `, function(error, result) {
            if (error) {
                reject(error);
            } else {
                if (result.length > 0) {
                    resolve(result[0]);
                } else {
                    reject("Kode Tidak Ditemukan");
                };
            };
        });
    });
};

const sendMessage = (receiver, text) => {
    return new Promise((resolve, reject) => {
        axios.post(`${process.env.WHATSAPP_URL}/chats/send/`, {
            receiver,
            message: {
                text,
            },
            token: process.env.WHATSAPP_TOKEN,
        })
        .then(result => {
            resolve(result);
        })
        .catch(error => {
            // console.log("sendMessage=========", error.response.data)
            reject(error);
        });
    });
};

const voucher = (body, sender) => {
    const kode = body[1].toUpperCase();

    cekPengirim(sender)
    .then((result) => {
        const user_id = result.id;
        const propinsi_id = result.propinsi_id;
    
        cariProfil(kode)
        .then((result) => {
            const profile_id = result.id;
            const profil = result.name;
            const price = result.price;

            mysql.beginTransaction((error) => {
                if (error) {
                    sendMessage(sender, { text: '👋 GAGAL!' });
                } else {

                    const username1 = randomstring.generate({
                        length: 3,
                        charset: 'alphabetic',
                        capitalization: 'uppercase'
                    });
                    const username2 = randomstring.generate({
                        length: 3,
                        charset: 'numeric',
                        capitalization: 'uppercase'
                    });
                    const username = `${username1}${username2}`;
                    const password = 'verd';
                    mysql.query(`
                        INSERT INTO transactions(profile_id, username, password, voucher, price, propinsi_id, user_id, hp)
                        VALUES('${profile_id}', '${username}', '${password}', '${profil}', '${price}', '${propinsi_id}', '${user_id}', '${body[2]}')
                    `, (error, result) => {
                        if (error) {
                            mysql.rollback(() => {
                                // console.log("masuk======", error)
                                sendMessage(sender, '👋 GAGAL!');
                            });
                        } else {
    
                            createVoucher(kode, username, password)
                            .then(result => {
                                const { username, password } = result;
    
                                sendMessage(`62${body[2].substring(1)}@s.whatsapp.net`, textSent({ username, password, voucher: profil }))
                                .then((result) => {
                                    
                                    sendMessage(sender, 'BERHASIL...').then(result => {
                                        mysql.commit();
                                    }).catch(error => {
                                        mysql.rollback(() => {
                                            sendMessage(sender, `👋 ${error}`);
                                        });
                                    });
                                })
                                .catch((error) => {
                                    // console.log("keluar====", error)
                                    mysql.rollback(() => {
                                        sendMessage(sender, `👋 ${error}`);
                                    });
                                });
                            })
                            .catch(error => {
                                // console.log("createVoucher==========", error)
                                mysql.rollback(() => {
                                    sendMessage(sender, `👋 ${error.detail}`);
                                });
                            });
                        };
                    });
                };
            });
        })
        .catch((error) => {
            // console.log("cariProfil==========", error)
            sendMessage(sender, `👋 ${error}`);
        });
    })
    .catch((error) => {
        // console.log("cekPengirim==========", error)
        sendMessage(sender, `👋 ${error}`);
    });
};

const textSent = (data) => {
    return `Terimakasih telah membeli Elpeoo Wifi,
${data.voucher} Anda Adalah:
User. ${data.username}
Password. ${data.password}
Bila ada kendala hubungi nomor. 085384980303
Voucer ini hanya bisa dipakai untuk 1 perangkat
balas OK pesan ini untuk aktivasi kode vouchernya`;
};

// const report = (conn, body, sender) => {
//     const tglDari = body[1];
//     const tglSampai = body[2];
//     let query;
//     if (tglDari.toUpperCase() === 'DAY') {
//         query = `
//             SELECT SUM(a.price) AS total
//             FROM transaction a
//             WHERE a.salah IS NULL && a.tgl >= NOW() - INTERVAL 1 DAY
//         `;
//     } else if (tglDari.toUpperCase() === 'MONTH') {
//         query = `
//             SELECT SUM(a.price) AS total
//             FROM transaction a
//             WHERE a.salah IS NULL && a.tgl >= NOW() - INTERVAL 1 MONTH
//         `;
//     } else if (tglDari.toUpperCase() === 'WEEK') {
//         query = `
//             SELECT SUM(a.price) AS total
//             FROM transaction a
//             WHERE a.salah IS NULL && a.tgl >= NOW() - INTERVAL 1 WEEK
//         `;
//     } else {
//         query = `
//             SELECT SUM(price) AS total
//             FROM transaction a
//             WHERE a.salah IS NULL && (a.tgl BETWEEN '${tglDari}' AND '${tglSampai} 23:59:59')
//         `;
//     };
    
//     mysql.query(query, (error, result) => {
//         if (error) {
//             conn.sendMessage(sender, { text: `👋 ${error}` });
//         } else {
//             conn.sendMessage(sender, { text: `Jumlah: Rp. ${Number(result[0].total).toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}` });
//         };
//     });
// };

// const kuota = (conn, body, sender) => {
//     const username = body[1];
//     const query = `
//         SELECT
//             a.username,
//             (SUM(a.acctinputoctets) / 1048576) AS download,
//             (SUM(a.acctoutputoctets) / 1048576) AS upload,

//             b.value AS expiration
//         FROM radacct a
//         JOIN (
//             SELECT
//                 a.username,
//                 a.value
//             FROM radcheck a
//             WHERE a.attribute = 'Expiration' && a.username = '${username}'
//         ) b ON a.username = b.username
//         WHERE a.username = '${username}'
//         GROUP BY a.username
//     `;
//     mysql.query(query, (error, result) => {
//         if (error) {
//             conn.sendMessage(sender, { text: `👋 Error Di Server` });
//         } else {
//             if (result.length > 0) {
//                 conn.sendMessage(sender, { text: `Username: ${result[0].username}
// Download: ${Math.round(Number(result[0].download))}
// Upload: ${Math.round(Number(result[0].upload))}
// Total: ${Number((result[0].download + result[0].upload) / 1000).toFixed(2)} GB
// Expiration: ${result[0].expiration}` });
//             } else {
//                 conn.sendMessage(sender, { text: `👋 Data Tidak Ditemukan.` });
//             };
//         };
//     });
// };

// const broadCast = (conn) => {
//     mysql.query(`
//         SELECT
//             a.hp
//         FROM vouchers a
//         WHERE a.broadcast IS NULL
//         ORDER BY a.tgl DESC
//     `, (error, result) => {
//         if (error) {
//             console.log(error)
//         } else {
//             let counter = 0;
//             const eksekusi = setInterval(() => {
//                 // console.log("masuk=======", result[counter])

//                 mysql.beginTransaction((error) => {
//                     if (error) return console.log("keluar===", error.sqlMessage);

//                     mysql.query(`
//                         UPDATE vouchers SET broadcast = 1 WHERE hp = '${result[counter].hp}'
//                     `, (error, result2) => {
//                         if (error) return console.log("keluar2===", error.sqlMessage);

//                         conn.sendMessage(`62${result[counter].hp.substring(1)}@s.whatsapp.net`, {
//                             text: `Halo pelanggan setia verdhotspot...
// Oktober ceria promo ceria !
// Promo diperpanjang hingga 31 Oktober 2022
// Dapatkan voucher unlimited hanya 50 ribu !!!
// Untuk info lebih lanjut 08117230699`
//                         })
//                         .then((result3) => {
//                             mysql.commit();
//                         })
//                         .catch((error) => {
//                             mysql.rollback();
//                         });
//                     });
//                 });

//                 counter++;
//                 if (counter === result.length) clearInterval(eksekusi);
//             }, 20000); // 20 Detik
//         };
//     });
// };

router.post('/masuk', (req, res) => {
    const { sender, message } = req.body;

    const body = (message).split(" ");
    const sandi = body[0];

    if (sandi.toUpperCase() === "V") {
        voucher(body, sender);
    }
    // else if (sandi.toUpperCase() === "REPORT") {
    //     report(body, sender);
    // }
    // else if (sandi.toUpperCase() === "KUOTA") {
    //     kuota(body, sender);
    // }
    // else if (sandi.toUpperCase() === "BROADCAST") {
    //     broadCast();
    // }
    else {
        sendMessage(sender,'👋 Bila ada kendala hubungi nomor. 085384980303 atau 08117230699!!!');
    };

});

module.exports = router;

const express = require('express');

const RoleController = require('../controllers/role.js');
const { isUser } = require('../middlewares/is-user.js');

const router = express.Router();

router.get('/', isUser, RoleController.get);

module.exports = router;
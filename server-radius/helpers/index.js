const cekUser = require("./cekUser");
const getModule = require("./getModule");
const createVoucher = require("./createVoucher");

module.exports = {
  cekUser, getModule, createVoucher,
};
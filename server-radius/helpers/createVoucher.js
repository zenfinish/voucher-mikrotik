const ApiMikrotik = require("./ApiMikrotik");

const createVoucher = (kode, username, password) => {
    return new Promise((resolve, reject) => {
        ApiMikrotik.put(`user-manager/user`, {
            "disabled": "false",
            "group": "default",
            "name": username,
            "otp-secret": "",
            "password": password,
            "shared-users": "1"
        })
        .then(result => {
            
            ApiMikrotik.put(`user-manager/user-profile`, {
                "profile": kode,
                "user": username
            })
            .then(result => {
                // {
                //     ".id": "*3",
                //     "end-time": "dec/28/2022 17:34:20",
                //     "profile": "5",
                //     "state": "running-active",
                //     "user": "test"
                // }
                resolve({ username, password, endtime: result['end-time'], })
            })
            .catch(error => {
                reject(error.response.data)
            });
        })
        .catch(error => {
            reject(error.response.data)
        });
    });
};

module.exports = createVoucher;

const User = require("../models/user");

const cekUser = (username) => {
  return new Promise((resolve, reject) => {
    User.findOne(username)
    .then(function(result) {
      result.username === username ? resolve(result) : reject(false);
    })
    .catch(function(error) {
      reject(false);
    });
  });
};

module.exports = cekUser;


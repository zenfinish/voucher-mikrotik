const express = require('express');
const cors = require('cors');
const http = require('http');

require('dotenv').config();

const mikrotikRoute = require('./routes/mikrotik.js');
const userRoute = require('./routes/user.js');
const transactionRoute = require('./routes/transaction.js');
const profileRoute = require('./routes/profile.js');
const limitationRoute = require('./routes/limitation.js');
const roleRoute = require('./routes/role.js');
const membersRoute = require('./routes/members.js');
const pemberitahuanRoute = require('./routes/pemberitahuan.js');
const whatsappRoute = require('./routes/whatsapp.js');

const app = express();
const httpServer = http.createServer(app); 

const port = process.env.PORT || 3002;

app.use(cors());
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

app.use('/public', express.static('public'));

app.use('/users', userRoute);
app.use('/mikrotik', mikrotikRoute);
app.use('/transactions', transactionRoute);
app.use('/profile', profileRoute);
app.use('/limitation', limitationRoute);
app.use('/role', roleRoute);
app.use('/members', membersRoute);
app.use('/pemberitahuan', pemberitahuanRoute);
app.use('/whatsapp', whatsappRoute);

app.use('*', function(req, res) {
    res.status(404).json(`Link Tidak Ditemukan`);
});

httpServer.listen(port, function() {
    console.log(`Running on port ${port}`)
});

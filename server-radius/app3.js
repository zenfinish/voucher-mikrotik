const express = require('express');
const cors = require('cors');
const http = require('http');
const randomstring = require("randomstring");
const {
    WAConnection,
    MessageType,
    ReconnectMode,
} = require("@adiwajshing/baileys");
const fs = require("fs");

require('dotenv').config();
const mysql = require('./helpers/mysql.js');

const mikrotikRoute = require('./routes/mikrotik.js');
const userRoute = require('./routes/user.js');
const transactionRoute = require('./routes/transaction.js');
const profileRoute = require('./routes/profile.js');
const limitationRoute = require('./routes/limitation.js');
const roleRoute = require('./routes/role.js');
const membersRoute = require('./routes/members.js');
const pemberitahuanRoute = require('./routes/pemberitahuan.js');

const app = express();
const httpServer = http.createServer(app); 

const port = process.env.PORT || 3002;

app.use(cors());
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

app.use('/public', express.static('public'));

app.use('/users', userRoute);
app.use('/mikrotik', mikrotikRoute);
app.use('/transactions', transactionRoute);
app.use('/profile', profileRoute);
app.use('/limitation', limitationRoute);
app.use('/role', roleRoute);
app.use('/members', membersRoute);
app.use('/pemberitahuan', pemberitahuanRoute);

app.use('*', function(req, res) {
    res.status(404).json(`Link Tidak Ditemukan`);
});

httpServer.listen(port, function() {
    console.log(`Running on port ${port}`)
});

const cekPengirim = (nomor) => {
    return new Promise((resolve, reject) => {
        mysql.query(`
            SELECT * FROM user WHERE whatsapp = '${nomor}'
        `, function(error, result) {
            if (error) {
                reject(error);
            } else {
                if (result.length > 0) {
                    resolve(result[0]);
                } else {
                    reject("Nomor Tidak Berlaku");
                };
            };
        });
    });
};

const textSent = (data) => {
    return `${data.voucher} Anda Adalah:
User. ${data.username}
Password. ${data.password}
Bila ada kendala hubungi nomor. 085384980303`;
};

const cariProfil = (kode) => {
    return new Promise((resolve, reject) => {
        mysql.query(`
            SELECT * FROM profile WHERE kode = '${kode}'
        `, function(error, result) {
            if (error) {
                reject(error);
            } else {
                if (result.length > 0) {
                    resolve(result[0]);
                } else {
                    reject("Kode Tidak Ditemukan");
                };
            };
        });
    });
};

const SESSIONS_FILE = './whatsapp-sessions.json';

const createSessionsFileIfNotExists = function () {
    if (!fs.existsSync(SESSIONS_FILE)) {
        try {
            fs.writeFileSync(SESSIONS_FILE, JSON.stringify([]));
            console.log('Sessions file created successfully.');
        } catch (err) {
            console.log('Failed to create sessions file: ', err);
        };
    };
};

createSessionsFileIfNotExists();

const setSessionsFile = function (sessions) {
    fs.writeFile(SESSIONS_FILE, JSON.stringify(sessions), function (err) {
        if (err) {
            console.log(err);
        };
    });
};

const getSessionsFile = function () {
    return JSON.parse(fs.readFileSync(SESSIONS_FILE));
};

const report = (conn, body, sender) => {
    const tglDari = body[1];
    const tglSampai = body[2];
    let query;
    if (tglDari.toUpperCase() === 'DAY') {
        query = `
            SELECT SUM(a.price) AS total
            FROM transaction a
            WHERE a.salah IS NULL && a.tgl >= NOW() - INTERVAL 1 DAY
        `;
    } else if (tglDari.toUpperCase() === 'MONTH') {
        query = `
            SELECT SUM(a.price) AS total
            FROM transaction a
            WHERE a.salah IS NULL && a.tgl >= NOW() - INTERVAL 1 MONTH
        `;
    } else if (tglDari.toUpperCase() === 'WEEK') {
        query = `
            SELECT SUM(a.price) AS total
            FROM transaction a
            WHERE a.salah IS NULL && a.tgl >= NOW() - INTERVAL 1 WEEK
        `;
    } else {
        query = `
            SELECT SUM(price) AS total
            FROM transaction a
            WHERE a.salah IS NULL && (a.tgl BETWEEN '${tglDari}' AND '${tglSampai} 23:59:59')
        `;
    };
    
    mysql.query(query, (error, result) => {
        if (error) {
            conn.sendMessage(sender, `👋 ${error}`, MessageType.text);
        } else {
            conn.sendMessage(sender, `Jumlah: Rp. ${Number(result[0].total).toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}`, MessageType.text);
        };
    });
};

const voucher = (conn, body, sender) => {
    const kode = body[1];
    const hp = body[2];
    const nomor = '0' + sender.replace("@s.whatsapp.net", "").substring(2);

    conn.isOnWhatsApp('62' + hp.substring(1))
    .then((result) => {
        if (!result) return conn.sendMessage(sender, `👋 GAGAL Kirim Ke: ${hp}`, MessageType.text);

        cekPengirim(nomor)
        .then((result) => {
            const user_id = result.id;
            const propinsi_id = result.propinsi_id;
        
            cariProfil(kode)
            .then((result) => {
                const profil = result.name;
                const price = result.price;
                const sql_date = result.sql_date;

                mysql.beginTransaction((error) => {
                    if (error) return conn.sendMessage(sender, '👋 GAGAL!', MessageType.text);
                    const groupname = result.groupname;
        
                    let username1 = randomstring.generate({
                        length: 2,
                        charset: 'alphabetic',
                        capitalization: 'uppercase'
                    });
                    let username2 = randomstring.generate({
                        length: 2,
                        charset: 'numeric',
                        capitalization: 'uppercase'
                    });
                    let username = `${username1}${username2}`;
                    const password = randomstring.generate({
                        length: 5,
                        charset: 'numeric',
                        capitalization: 'uppercase'
                    });
                    mysql.query(`
                        INSERT INTO transaction(username, password, voucher, price, propinsi_id, user_id, hp)
                        VALUES('${username}', '${password}', '${profil}', '${price}', '${propinsi_id}', '${user_id}', '${hp}')
                    `, (error, result) => {
                        if (error) {
                            mysql.rollback(() => {
                                conn.sendMessage(sender, '👋 GAGAL!', MessageType.text);
                            });
                        } else {
                            const transaction_id = result.insertId;
        
                            mysql.query(`
                                SELECT
                                    DATE_FORMAT(transaction.tgl + ${sql_date}, '%d %b %Y %T') AS tgl,
                                    transaction.username,
                                    transaction.password,
                                    transaction.voucher,
                                    user.name AS user_name
                                FROM transaction
                                LEFT JOIN user ON transaction.user_id = user.id
                                WHERE transaction.id = '${transaction_id}'
                            `, (error, result) => {
                                if (error) {
                                    mysql.rollback(() => {
                                        conn.sendMessage(sender, '👋 GAGAL!', MessageType.text);
                                    });
                                } else {
                                    const voucher = result[0].voucher;
                                    const tgl = result[0].tgl;
                                    
                                    let sql = "INSERT INTO radcheck(username, attribute, op, value, transaction_id) VALUES ?";
                                    let values = [
                                        [username, 'Cleartext-Password', ':=', password, transaction_id],
                                        [username, 'Expiration', ':=', tgl, transaction_id],
                                    ];
                                    mysql.query(sql, [values], (error, result) => {
                                        if (error) {
                                            mysql.rollback(() => {
                                                conn.sendMessage(sender, '👋 GAGAL!', MessageType.text);
                                            });
                                        } else {
        
                                            let sql = "INSERT INTO radusergroup(username, groupname, transaction_id) VALUES ?";
                                            let values = [
                                                [username, groupname, transaction_id],
                                            ];;
                                            mysql.query(sql, [values], (error, result) => {
                                                if (error) {
                                                    mysql.rollback(() => {
                                                        conn.sendMessage(sender, '👋 GAGAL!', MessageType.text);
                                                    });
                                                } else {
                                                    
                                                    conn.sendMessage(`62${hp.substring(1)}@s.whatsapp.net`, textSent({ username, password, voucher }), MessageType.text)
                                                    .then((result) => {
                                                        mysql.commit(() => {
                                                            conn.sendMessage(sender, 'BERHASIL...', MessageType.text);
                                                        });
                                                    })
                                                    .catch((error) => {
                                                        // console.log("keluar====", error)
                                                        mysql.rollback(() => {
                                                            conn.sendMessage(sender, `👋 ${error}`, MessageType.text);
                                                        });
                                                    });
                                                };
                                            });
                                        };
                                    });
                                };
                            });
                        };
                    });
                });
            })
            .catch((error) => {
                conn.sendMessage(sender, `👋 ${error}`, MessageType.text);
            });
        })
        .catch((error) => {
            conn.sendMessage(sender, `👋 ${error}`, MessageType.text);
        });
    })
    .catch((error) => {
        conn.sendMessage(sender, `👋 GAGAL Kirim Ke: ${hp}`, MessageType.text);
    });
};

const createSession = function () {
    const conn =  new WAConnection();

    conn.version = [3, 3234, 9];
    conn.setMaxListeners(0)
    conn.autoReconnect = ReconnectMode.onConnectionLost;
    conn.browserDescription = ['MPWA', 'Chrome'];
  
    const SESSION_FILE_PATH = `./whatsapp-session.json`;
    let sessionCfg;
    if (fs.existsSync(SESSION_FILE_PATH)) {
        sessionCfg = require(SESSION_FILE_PATH);
        conn.loadAuthInfo(`./whatsapp-session.json`)
        if(conn.state == 'open') {
            return conn;
        } else if ( conn.state == 'connecting') {
            return;
        };
    };

    conn.on('qr', (qr) => {
        // console.log('QR RECEIVED', qr);
    });

    conn.on('open', (result) => {
        const session = conn.base64EncodedAuthInfo()
        fs.writeFile(SESSION_FILE_PATH, JSON.stringify(session), function (err) {
            if (err) {
                console.error(err);
            } else {
                console.log('berhasil buat');
            };
        });
        // console.log("session ===> ", session);
        const savedSessions = getSessionsFile();
        const sessionIndex = savedSessions.findIndex(sess => sess.id == session.clientID);
        // console.log(sessionIndex)
        if (sessionIndex == -1) {
            savedSessions.push({
                id: session.clientID,
                ready: true,
            });
        } else {
            savedSessions[sessionIndex].ready = true;
        };
        setSessionsFile(savedSessions);
    });

    conn.on('close', ({ reason }) => {
        // if (reason == 'invalid_session') {
        // const nomors =  phoneNumberFormatter(conn.user.jid);
        // const nomor = nomors.replace(/\D/g, '');
        // console.log(nomor)
        //     if (fs.existsSync(`./whatsapp-session-${nomor}.json`)) {
        //         fs.unlinkSync(`./whatsapp-session-${nomor}.json`);
            
        //         io.emit('close', { id: nomor, text: 'Connection Lost..' });
        //         const savedSessions = getSessionsFile();
        //         const sessionIndex = savedSessions.findIndex(sess => sess.id == nomor);
        //         savedSessions[sessionIndex].ready = false;
        //         //setSessionsFile(savedSessions);
        //         setSessionsFile(savedSessions);          
        //     };
        // };
    });

    conn.on('initial-data-received', async () => {
        console.log('initialize ====>');
    });

    conn.on('chat-update', async (chat) => {
        if (chat.messages) {
            const m = chat.messages.all()[0];
            const text = m.message.conversation ? m.message.conversation : m.message.extendedTextMessage ? m.message.extendedTextMessage.text : null;
            const sender = m.key.remoteJid;
            
            if(chat.count && sender !== 'status@broadcast') {
    
                const body = (text).split(" ");
                const sandi = body[0];
    
                if (sandi.toUpperCase() === "V") {
                    voucher(conn, body, sender);
                } else if (sandi.toUpperCase() === "REPORT") {
                    report(conn, body, sender);
                } else {
                    conn.sendMessage(sender, '👋 Bila ada kendala hubungi nomor. 085384980303 atau 08117230699!', MessageType.text);
                };
            };
        };
    });

    conn.connect();
};

createSession();

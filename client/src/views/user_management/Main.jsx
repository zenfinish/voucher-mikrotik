import React from 'react';
import FormAddUser from './FormAddUser';
import FormEditUser from './FormEditUser';
import Hapus from './Hapus';
// import getUser from '../../actions/getUser.js';
import api from '../../config/api.js';

class Main extends React.Component {
	
	state = {
		openFormAddUser: false,
		openFormEditUser: false,
		openHapus: false,
		userAktif: null,
		variant: '',
		message: '',
		openAlert: false,
		data: [],
	}

	componentDidMount() {
		api.get(`/users`)
		.then(result => {
			this.setState({
				data: result.data
			});
		})
		.catch(error => {
			console.log(error.response)
		});
	}
	
	closeFormAddUser = () => {
		this.props.getUser();
		this.setState({ openFormAddUser: false });
	};

	closeFormEditUser = () => {
		this.props.getUser();
		this.setState({ openFormEditUser: false, userAktif: null });
	};

	openFormAddUser = () => {
		this.setState({ openFormAddUser: true });
	};

	openFormEditUser = (data) => {
		this.setState({ userAktif: data, openFormEditUser: true,  });
	};

	changeUserAktif = (e) => {
		this.setState({
			userAktif: {
				...this.state.userAktif,
				[e.target.name]: e.target.value,
			}
		});
	}

	updateUser = (e) => {
		e.preventDefault();
		api.put(`/users/${this.state.userAktif._id}`, {
			fullname: this.state.userAktif.fullname,
			username: this.state.userAktif.username,
			role: this.state.userAktif.role,
		}, {
			headers: {
					token: localStorage.getItem('token')
			}
		})
		.then(result => {
			this.setState({
				variant: 'success',
				message: 'Update berhasil',
				openAlert: true,
			}, () => {
				this.closeFormEditUser();
			});
		})
		.catch(error => {
			this.setState({
				variant: 'error',
				message: error.response.data,
				openAlert: true,
			});
		});
	}

	render() {
		const { classes } = this.props;
		return (
			<>
				{/* <Fab color="primary" aria-label="Add" size="small" className={classes.fab} onClick={this.openFormAddUser}><AddIcon /></Fab>
				<Paper className={classes.root}>
					<Table className={classes.table}>
						<TableHead>
							<TableRow>
								<CustomTableCell>Full Name</CustomTableCell>
								<CustomTableCell align="right">Username</CustomTableCell>
								<CustomTableCell align="right">Role</CustomTableCell>
								<CustomTableCell align="right">Action</CustomTableCell>
							</TableRow>
						</TableHead>
						<TableBody>
							{this.state.data.map(row => (
								<TableRow className={classes.row} key={row.id}>
									<CustomTableCell component="th" scope="row">
										{row.name}
									</CustomTableCell>
									<CustomTableCell align="right">{row.username}</CustomTableCell>
									<CustomTableCell align="right">{row.role_name}</CustomTableCell>
									<CustomTableCell align="right">
										<IconButton color="primary" className={classes.button} aria-label="Delete" onClick={this.openFormEditUser.bind(this, row)}>
											<BorderColorIcon />
										</IconButton>
										<IconButton color="secondary" className={classes.button} aria-label="Delete">
											<DeleteIcon />
										</IconButton>
									</CustomTableCell>
								</TableRow>
							))}
						</TableBody>
					</Table>
				</Paper>
				<FormAddUser 
					openFormAddUser={this.state.openFormAddUser}
					closeFormAddUser={this.closeFormAddUser}
				/>
				<FormEditUser 
					openFormEditUser={this.state.openFormEditUser}
					closeFormEditUser={this.closeFormEditUser}
					dataUser={this.state.userAktif}
					changeUserAktif={this.changeUserAktif}
					updateUser={this.updateUser}
				/>
				{
					this.state.openHapus ?
						<Hapus 
							closeFormEditUser={this.closeFormEditUser}
							dataUser={this.state.userAktif}
							changeUserAktif={this.changeUserAktif}
							updateUser={this.updateUser}
						/>
					: null
				} */}
			</>
		);
	}
	
}
export default ((Main));
import React from 'react';

class FormEditUser extends React.Component {
	
	handleClose = () => {
		this.props.closeFormEditUser();
	};
	
	render() {
		const { classes } = this.props;
		return (
			<>
				{/* <Dialog
					open={this.props.openFormEditUser}
					aria-labelledby="form-dialog-title"
					onClose={this.handleClose}
				>
					<DialogTitle id="form-dialog-title">Edit User</DialogTitle>
					<DialogContent>
						<form className={classes.form} onSubmit={this.props.updateUser}>
							<FormControl margin="normal" required fullWidth>
								<InputLabel htmlFor="fullname">Full Name</InputLabel>
								<Input
									id="fullname"
									name="fullname"
									value={this.props.dataUser ? this.props.dataUser.name : ''}
									autoComplete="fullname"
									autoFocus
									onChange={this.props.changeUserAktif}
								/>
							</FormControl>
							<FormControl margin="normal" required fullWidth>
								<InputLabel htmlFor="username">Username</InputLabel>
								<Input
									id="username"
									name="username"
									value={this.props.dataUser ? this.props.dataUser.username : ''}
									autoComplete="username"
									onChange={this.props.changeUserAktif}
								/>
							</FormControl>
							<FormControl required className={classes.formControl} margin="normal" fullWidth>
								<InputLabel htmlFor="role">Role</InputLabel>
								<Select
									onChange={this.props.changeUserAktif}
									name="role"
									className={classes.selectEmpty}
									value={this.props.dataUser ? this.props.dataUser.role_id : ''}
									id="role"
								>
									<MenuItem value="1">Admin</MenuItem>
									<MenuItem value="2">Report</MenuItem>
									<MenuItem value="3">Kabara Cafe</MenuItem>
									<MenuItem value="4">Kabara Shop</MenuItem>
								</Select>
							</FormControl>
							<Button
								type="submit"
								variant="contained"
								color="primary"
								className={classes.submit}
								style={{marginRight: 5}}
								fullWidth
							>Update</Button>
						</form>
					</DialogContent>
				</Dialog> */}
			</>
		);
	}
}

export default (FormEditUser);
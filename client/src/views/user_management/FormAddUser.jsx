import React from 'react';
import api from '../../config/api.js';
import Alert from '../../components/Alert.jsx';

class FormAddUser extends React.Component {
	
	state = {
		roles: [],
		
		fullname: '',
		username: '',
		role: '',
		openAlert: false,
		variant: '',
		message: '',
	}

	componentDidMount() {
		api.get(`/role`, {
			headers: { token: localStorage.getItem('token') }
		})
		.then(result => {
			this.setState({
				roles: result.data
			});
		})
		.catch(error => {
			console.log(error.response)
		});
	}
	
	handleClose = () => {
		this.props.closeFormAddUser();
	};

	closeAlert = () => {
		this.setState({
			openAlert: false,
		});
	}

	register = (e) => {
		e.preventDefault();
		api.post(`/users/register`, {
			fullname: this.state.fullname,
			username: this.state.username,
			role: this.state.role,
		}, {
			headers: {
				token: localStorage.getItem('token')
			}
		})
		.then(result => {
			this.setState({
				variant: 'success',
				message: 'Register berhasil, silahkan login',
				openAlert: true,
			}, () => {
				this.handleClose();
			});
		})
		.catch(error => {
			// console.log("masuk======", error.response)
			this.setState({
				variant: 'error',
				message: error.response.data,
				openAlert: true,
			});
		});
	}
		
	render() {
		const { classes } = this.props;
		return (
			<>
				{/* <Dialog
					open={this.props.openFormAddUser}
					aria-labelledby="form-dialog-title"
					onClose={this.handleClose}
				>
					<DialogTitle id="form-dialog-title">Register User</DialogTitle>
					<DialogContent>
						<form className={classes.form} onSubmit={this.register}>
							<FormControl margin="normal" required fullWidth>
								<InputLabel htmlFor="fullname">Full Name</InputLabel>
								<Input id="fullname" name="fullname" autoComplete="fullname" autoFocus onChange={(e) => {this.setState({fullname: e.target.value})}} />
							</FormControl>
							<FormControl margin="normal" required fullWidth>
								<InputLabel htmlFor="username">Username</InputLabel>
								<Input id="username" name="username" autoComplete="username" onChange={(e) => {this.setState({username: e.target.value})}} />
							</FormControl>
							<FormControl required className={classes.formControl} margin="normal" fullWidth>
								<InputLabel htmlFor="role">Role</InputLabel>
								<Select
									onChange={(e) => {this.setState({ role: e.target.value })}}
									name="role"
									className={classes.selectEmpty}
									value={this.state.role}
									id="role"
								>
									{
										this.state.roles.map((result, index) => (
											<MenuItem key={index} value={result.id}>{result.nama}</MenuItem>
										))
									}
								</Select>
							</FormControl>
							<Button
								type="submit"
								variant="contained"
								color="primary"
								className={classes.submit}
								style={{marginRight: 5}}
								fullWidth
							>Register</Button>
						</form>
					</DialogContent>
				</Dialog>
				{
					this.state.openAlert ?
						<Alert
							open={true}
							closeAlert={() => {this.setState({ openAlert: false })}}
							variant={this.state.variant}
							message={this.state.message}
						/>
					: null
				} */}
			</>
		);
	}
}

export default (FormAddUser);
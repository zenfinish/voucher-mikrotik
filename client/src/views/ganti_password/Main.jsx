import React from 'react';
import api from '../../config/api.js';
import Alert from '../../components/Alert.jsx';

class Main extends React.Component {
    
	state = {
		password_lama: '',
		password_baru: '',
		ulang_password_baru: '',

		openAlert: false,
		variant: 'error',
		message: '',
	}

	handleChange = (e) => {
		this.setState({
			[e.target.name]: e.target.value,
		});
	};

	simpan = (e) => {
		e.preventDefault();
		if (this.state.password_baru === '') {
			this.setState({
				variant: 'error',
				message: 'Password Baru Kosong !',
				openAlert: true,
			});
		} else if (this.state.password_baru !== this.state.ulang_password_baru) {
			this.setState({
				variant: 'error',
				message: 'Password Baru Dan Ulang Tidak Sama !',
				openAlert: true,
			});
		} else {
			api.put(`/users/password/`, {
				password_lama: this.state.password_lama,
				password_baru: this.state.password_baru,
			})
			.then(result => {
				this.setState({
					variant: 'success',
					message: 'Update Password Berhasil',
					openAlert: true,
				}, () => {
					localStorage.removeItem('token');
					this.props.history.push('/');
				});
			})
			.catch(error => {
				this.setState({
					variant: 'error',
					message: error.response.data,
					openAlert: true,
				});
			});
		}
	}
	
	render() {
		return (
			<>
				{/* <h2>Ganti Password</h2>
				<form onSubmit={this.simpan}>
					<FormControl margin="normal" style={{ width: '100%' }}>
						<InputLabel>Masukkan Password Lama</InputLabel>
						<Input
							id="password_lama"
							name="password_lama"
							autoComplete="password_lama"
							autoFocus
							onChange={this.handleChange}
							type="password"
						/>
					</FormControl>
					<FormControl margin="normal" style={{ width: '100%' }}>
						<InputLabel>Masukkan Password Baru</InputLabel>
						<Input
							name="password_baru"
							id="password_baru"
							autoComplete="password_baru"
							onChange={this.handleChange}
							type="password"
						/>
					</FormControl>
					<FormControl margin="normal" style={{ width: '100%' }}>
						<InputLabel>Ulang Password Baru</InputLabel>
						<Input
							name="ulang_password_baru"
							id="ulang_password_baru"
							autoComplete="ulang_password_baru"
							onChange={this.handleChange}
							type="password"
						/>
					</FormControl><br/><br/>
					<Button
						type="submit"
						variant="contained"
						color="primary"
						fullWidth
					>SIMPAN</Button>
				</form>
				<Alert
               open={this.state.openAlert}
               closeAlert={() => {this.setState({ openAlert: false })}}
               variant={this.state.variant}
               message={this.state.message}
            /> */}
			</>
		);
	}

};

export default (Main);

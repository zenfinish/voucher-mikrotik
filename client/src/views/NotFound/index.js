import React from 'react'
import NotFound from '../../components/NotFound/Text'

const index = () => {
  return (
    <NotFound />
  )
}

export default index

import React, { useEffect } from 'react';
import { DashboardRoutes } from '../config';
import api from '../config/api';
import { Layout, Menu } from 'antd';
import { FaAlignJustify } from "react-icons/fa";
import { userState } from "../recoil/user";
import { useRecoilState } from "recoil";
import { useNavigate } from 'react-router-dom';

const { Header, Content, Sider, Footer } = Layout;

const Dashboard = () => {
	const [user, setUser] = useRecoilState(userState);

	const navigate = useNavigate();

	useEffect(() => {
		api.get(`/users/check-token`)
		.then(result => {
			const { id, username, name, modules } = result.data;
			setUser({ id, username, name, modules });
			// console.log(modules)
		})
		.catch(error => {
			// console.log(error)
			window.location.assign(`${process.env.REACT_APP_PUBLIC_URL}`);
		});
	}, []);

	return <DashboardClass user={user} navigate={navigate} />;
};


class DashboardClass extends React.Component {
	
	render() {
		const { user, navigate } = this.props;
		return (
			<>
				<Layout>
					<Sider
						collapsible
						trigger={null}
						style={{
							overflow: 'auto',
							height: '100vh',
							position: 'fixed',
							left: 0,
							top: 0,
							bottom: 0,
						}}
					>
						<div className="bg-white h-8 m-2 text-center">PT. LPO</div>
						<Menu
							theme="dark"
							mode="inline"
							defaultSelectedKeys={['1']}
							items={user.modules.map(row => { return {
								key: row.module_id,
								label: row.module_name,
								// icon: <UserOutlined />,
								onClick: () => {
									navigate(`/dashboard/${row.link}`);
								},
							} })}
						/>
					</Sider>
					<Layout
						style={{
							marginLeft: 200,
						}}
					>
						<Header
							className="bg-slate-100"
							style={{
								padding: 0,
								position: 'fixed',
								zIndex: 1,
								width: '100%',
								// right: 0,
							}}
						>
							<div className="border border-black">
								<div className="border border-black">
									{React.createElement(FaAlignJustify, {
										className: 'trigger',
										// onClick: () => setCollapsed(!collapsed),
									})}
								</div>
								<div className="border border-black">
									{user.username}
								</div>
							</div>
						</Header>
						<Content
							className="bg-white"
							style={{
								padding: 20,
								marginTop: 64,
								marginBottom: 20,
							}}
						>
							<DashboardRoutes />
						</Content>
						<Footer
							className="bg-slate-100"
							style={{
								textAlign: 'center',
								padding: 0,
								position: 'fixed',
								zIndex: 1,
								bottom: 0,
								width: '100%',
							}}
						>
							PT. Lintas Persada Optika ©2014 Created by Verd Team
						</Footer>
					</Layout>
				</Layout>
			</>
		);
	}
	
}

export default (Dashboard);

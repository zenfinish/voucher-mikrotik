import React from 'react';
import api from '../config/api';
import { separator, tglSql } from '../config/helpers';
import { Doughnut } from 'react-chartjs-2';
import _ from "lodash";
import { Card, DatePicker, Select, Button, Table } from 'antd';

class Home extends React.Component {
	
	state = {
		selectedDate: null,
		selectedDate2: null,
		propinsi_id: '',
		user_id: '',
		dataReport: [],
		loading: false,
		userList: [],

		status: [
			{ value: null, text: "-Semua-" },
			{ value: "1", text: "Salah" },
		],

		dataChart1: [],
		dataChart2: [],
		dataChart3: [],
	};

	componentDidMount() {
		api.get(`/users`)
		.then(result => {
			this.setState({
				userList: result.data,
			});
		})
		.catch(error => {
			console.log(error.response)
		});

		api.get(`/transactions/voucher/all`)
		.then(result => {
			this.setState({
				dataChart3: result.data,
			});
		})
		.catch(error => {
			console.log(error.response)
		});
	}

	handleDateChange = date => {
		this.setState({ selectedDate: date });
	};

	handleDateChange2 = date => {
		this.setState({ selectedDate2: date });
	};

	handleChange = (e) => {
		this.setState({
			[e.target.name]: e.target.value,
		});
	};

	cetak = () => {
		this.setState({
			loading: true
		}, () => {
			api.post(`/transactions`, {
				date1: tglSql(this.state.selectedDate),
				date2: tglSql(this.state.selectedDate2),
				propinsi_id: this.state.propinsi_id,
				user_id: this.state.user_id,
			}, {
				headers: { token: localStorage.getItem('token') }
			})
			.then(result => {
				this.setState({
					dataReport: result.data,
					loading: false,
				});
				this.masukDataChart1(result.data);
				this.masukDataChart2(result.data);
			})
			.catch(error => {
				this.error(error.response.data);
				this.setState({
					loading: false,
				});
			});
		});
	}

	footerData = () => {
		let total = 0;
		let total_salah = 0;
		let data = this.state.dataReport;
		for (let i = 0; i < data.length; i++) {
			if (data[i].salah === 1) {
				total_salah += data[i].price;
			}
			total += data[i].price;
		};
		return [
		  { user_name: "Total Seluruh", price_format: separator(total, 0) },
			{ user_name: "Total Salah", price_format: separator(total_salah, 0) },
			{ user_name: "Total Bersih", price_format: separator(total - total_salah, 0) },
		];
	}

	error = (data) => {
		this.messager.alert({
			title: "Alert",
			msg: data
		});
	}

	masukDataChart1(guguk) {
		const datas = guguk.filter((row) => row.salah !== 1);
		const dataChart1 = _.orderBy(
			_(datas)
				.groupBy('hp')
				.map(function(items, hp) {
					return {
						hp: hp,
						totalBeli: items.length,
						totalUang: _.sumBy(items, function(o) { return o.price; }),
						data: items,
					};
				})
				.value()
		, ['totalBeli'], ['desc']).slice(0, 5);
		this.setState({ dataChart1 });
	}

	masukDataChart2(guguk) {
		const datas = guguk.filter((row) => row.salah !== 1);
		const dataChart2 = _(datas)
			.groupBy('voucher')
			.map(function(items, voucher) {
				return {
					voucher: voucher,
					totalBeli: items.length,
					totalUang: _.sumBy(items, function(o) { return o.price; }),
					data: items,
				};
			})
			.value();
		this.setState({ dataChart2 });
	}

	getRandomColor() {
		var letters = '0123456789ABCDEF'.split('');
		var color = '#';
		for (var i = 0; i < 6; i++ ) {
			color += letters[Math.floor(Math.random() * 16)];
		}
		return color;
			}

	render() {
		let total = 0;
		this.state.dataReport.map(row => {
			return total += row.price;
		});
		return (
			<div className="grid grid-cols-1 gap-2">
				<div>
					<DatePicker
						placeholder="Dari Tanggal"
						value={this.state.selectedDate}
						onChange={this.handleDateChange}
					/>
					<DatePicker
						placeholder="Sampai Tanggal"
						value={this.state.selectedDate2}
						onChange={this.handleDateChange2}
					/>
					<Select
						onChange={this.handleChange}
					>
						<Select.Option value={null}>Semua</Select.Option>
						<Select.Option value={1}>Lampung</Select.Option>
						<Select.Option value={2}>Aceh</Select.Option>
					</Select>
					<Select
						onChange={this.handleChange}
					>
						<Select.Option value={null}>Semua</Select.Option>
						{this.state.userList.map((data, i) => (
							<Select.Option value={data.id} key={i}>{data.name}</Select.Option>
						))}
					</Select>
					<Button
						onClick={this.cetak}
					>Tampilkan</Button>
				</div>
				<Table
					size="small"
					bordered
					columns={[
						{
							title: 'Tanggal',
							dataIndex: 'tanggal',
						},
						{
							title: 'Username',
							dataIndex: 'username',
						},
						{
							title: 'Voucher',
							dataIndex: 'voucher',
						},
						{
							title: 'No. Handphone',
							dataIndex: 'hp',
						},
						{
							title: 'Penjual',
							dataIndex: 'penjual',
						},
						{
							title: 'Harga',
							dataIndex: 'price',
						},
					]}
					dataSource={this.state.dataReport}
				/>
				<div className="grid grid-cols-2 gap-2">
					<Card title={`5 Penjualan Terbanyak Berdasarkan Berapa Kali Beli Tgl. {tglSql(this.state.selectedDate)} s/d {tglSql(this.state.selectedDate2)}`}>
						<Doughnut
							data={{
								labels: this.state.dataChart1.map((row) => { return `${row.hp} ${row.totalBeli}`; }),
								datasets: [{
									data: this.state.dataChart1.map((obj) => { return obj.totalBeli; }),
									backgroundColor: [
										'rgba(255, 99, 132, 0.2)',
										'rgba(54, 162, 235, 0.2)',
										'rgba(255, 206, 86, 0.2)',
										'rgba(75, 192, 192, 0.2)',
										'rgba(153, 102, 255, 0.2)',
										'rgba(255, 159, 64, 0.2)',
									],
									borderWidth: 1,
								}],
							}}
							options={{
								legend: {
									display: true,
									position: "right"
								}
							}}
						/>
					</Card>
					<Card title={`Total Penjualan Voucher Berdasarkan Nilai Voucher Tgl. {tglSql(this.state.selectedDate)} s/d {tglSql(this.state.selectedDate2)}`}>
						<Doughnut
							data={{
								labels: this.state.dataChart2.map((row) => { return `${row.voucher} ${row.totalBeli}`; }),
								datasets: [{
									data: this.state.dataChart2.map((obj) => { return obj.totalBeli; }),
									backgroundColor: this.state.dataChart2.map(() => { return this.getRandomColor(); }),
									borderWidth: 1,
								}],
							}}
							options={{
								legend: {
									display: true,
									position: "right"
								}
							}}
						/>
					</Card>
				</div>
				<Card title="5 Penjualan Terbanyak Berdasarkan Jumlah Uang Seluruh Database">
					<Doughnut
						data={{
							labels: this.state.dataChart3.map((row) => { return `${row.hp} ${separator(row.total, 0)}`; }),
							datasets: [{
								data: this.state.dataChart3.map((obj) => { return obj.total; }),
								backgroundColor: this.state.dataChart3.map(() => { return this.getRandomColor(); }),
								borderWidth: 1,
							}],
						}}
						options={{
							legend: {
								display: true,
								position: "right"
							}
						}}
					/>
				</Card>
			</div>
		);
	}

}

export default ((Home));
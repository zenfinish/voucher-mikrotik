import React from 'react';
import api from '../../config/api.js';
// import FormAddProfile from './FormAddProfile';
// import FormAddLimitation from './FormAddLimitation';

function TabContainer(props) {
	return (
		<>
		{/* <Typography component="div" style={{ padding: 8 * 3 }}>
			{props.children}
		</Typography> */}
		</>
	);
}

class Main extends React.Component {
	
state = {
	dataProfile: [],
	dataLimitation: [],
	
	openFormAddProfile: false,
	openFormAddLimitation: false,

	value: 0,
};

componentDidMount() {
	this.fetchProfile();
	this.fetchLimitation();
}

handleChange = (event, value) => {
	this.setState({ value });
};

fetchProfile = () => {
	api.get(`/profile`, { headers: { token: localStorage.getItem('token') } })
	.then(result => {
		this.setState({
			dataProfile: result.data
		});
	})
	.catch(error => {
		console.log(error.response)
	});
}

fetchLimitation = () => {
	api.get(`/limitation`, { headers: { token: localStorage.getItem('token') } })
	.then(result => {
		this.setState({
			dataLimitation: result.data
		});
	})
	.catch(error => {
		console.log(error.response)
	});
}

suksesAddProfile = () => {
	this.fetchProfile();
}

suksesAddLimitation = () => {
	this.fetchLimitation();
}

error = (data) => {
	this.messager.alert({
		title: "Alert",
		msg: data
	});
}

render() {
	const { value } = this.state;
	return (
		<>
			{/* <AppBar position="static">
				<Tabs value={value} onChange={this.handleChange} variant="fullWidth">
					<Tab label="Profile" />
					<Tab label="Limitation" />
				</Tabs>
			</AppBar>
			{
				value === 0 &&
					<>
					<TabContainer>
						<LinkButton
							iconCls="icon-add"
							onClick={() => {this.setState({ openFormAddProfile: true })}}
						>Tambah Profile</LinkButton>
						<DataGrid data={this.state.dataProfile} style={{height:250}}>
							<GridColumn field="name" title="Name"></GridColumn>
							<GridColumn field="name_for_users" title="Name For Users"></GridColumn>
							<GridColumn field="validity" title="Validity"></GridColumn>
							<GridColumn field="price" title="Price"></GridColumn>
							<GridColumn field="type" title="Type"></GridColumn>
						</DataGrid>
						<FormAddProfile 
							openFormAddProfile={this.state.openFormAddProfile}
							closeFormAddProfile={() => {this.setState({ openFormAddProfile: false })}}
							dataLimitation={this.state.dataLimitation}
							success={this.suksesAddProfile}
							error={this.error}
						/>
					</TabContainer>
					</>
			}
			{
				value === 1 &&
					<TabContainer>
						<LinkButton
							iconCls="icon-add"
							onClick={() => {this.setState({ openFormAddLimitation: true })}}
						>Tambah Limitation</LinkButton>
						<DataGrid data={this.state.dataLimitation} style={{height:250}}>
							<GridColumn field="name" title="Nama"></GridColumn>
							<GridColumn field="download" title="Download (MB)"></GridColumn>
							<GridColumn field="upload" title="Upload (MB)"></GridColumn>
							<GridColumn field="transfer" title="Transfer (MB)"></GridColumn>
							<GridColumn field="masa_aktif" title="Masa Aktif"></GridColumn>
						</DataGrid>
						<FormAddLimitation
							openFormAddLimitation={this.state.openFormAddLimitation}
							closeFormAddLimitation={() => {this.setState({ openFormAddLimitation: false })}}
							success={this.suksesAddLimitation}
							error={this.error}
						/>
					</TabContainer>
			}
			<Messager ref={ref => this.messager = ref}></Messager> */}
		</>
	);
}
}

export default (Main);
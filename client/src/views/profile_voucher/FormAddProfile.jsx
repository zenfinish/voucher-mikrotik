import React from 'react';
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';
import { Button, FormControl, Dialog, DialogContent, MenuItem, TextField } from '@material-ui/core';

import api from '../../config/api.js';

const styles = theme => ({
	main: {
		width: 'auto',
		display: 'block', // Fix IE 11 issue.
		marginLeft: theme.spacing.unit * 3,
		marginRight: theme.spacing.unit * 3,
		[theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
			width: 400,
			marginLeft: 'auto',
			marginRight: 'auto',
		},
	},
	paper: {
		marginTop: theme.spacing.unit * 8,
		display: 'flex',    
		flexDirection: 'column',
		alignItems: 'center',
		padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
	},
	avatar: {
		margin: theme.spacing.unit,
		backgroundColor: theme.palette.secondary.main,
	},
	form: {
		width: '100%', // Fix IE 11 issue.
		marginTop: theme.spacing.unit,
	},
	submit: {
		marginTop: theme.spacing.unit * 3,
	},
});

class FormAddProfile extends React.Component {
	
	state = {
		name: '',
		name_for_users: '',
		owner: '',
		validity: '',
		price: '',
		type: '',
		limitation_id: '',
		limitation_name: '',
	}
	
handleClose = () => {
	this.setState({
		name: '',
		name_for_users: '',
		owner: '',
		validity: '',
		price: '',
		type: '',
		limitation_id: '',
		limitation_name: '',
	}, () => {
		this.props.closeFormAddProfile();
	});
};

handleChange = (e) => {
	this.setState({
		[e.target.name]: e.target.value,
	});
};

saveProfile = (e) => {
	e.preventDefault();
	api.post(`/profile`, {
		name: this.state.name,
		name_for_users: this.state.name_for_users,
		validity: this.state.validity,
		price: this.state.price,
		type: this.state.type,
		limitation_id: this.state.limitation_id,
		limitation_name: this.state.limitation_name,
	}, {
		headers: { token: localStorage.getItem('token') }
	})
	.then(result => {
		this.props.success();
		this.props.closeFormAddProfile();
	})
	.catch(error => {
		// this.props.error(error.response.data);
		console.log(error.response)
	});
}
		
	render() {
		const { classes } = this.props;
		return (
			<Dialog
				open={this.props.openFormAddProfile}
				aria-labelledby="form-dialog-title"
				onClose={this.handleClose}
			>
				<DialogContent>
					<form className={classes.form} onSubmit={this.saveProfile}>
						<FormControl margin="normal" required fullWidth>
							<TextField
								label="Name"
								className={classes.textField}
								variant="outlined"
								onChange={this.handleChange}
								name="name"
								value={this.state.name}
							/>
						</FormControl>
						<FormControl margin="normal" required fullWidth>
							<TextField
								label="Name For Users"
								className={classes.textField}
								variant="outlined"
								onChange={this.handleChange}
								name="name_for_users"
								value={this.state.name_for_users}
							/>
						</FormControl>
						<FormControl margin="normal" required fullWidth>
							<TextField
								label="Validity"
								className={classes.textField}
								variant="outlined"
								onChange={this.handleChange}
								name="validity"
								select
								value={this.state.validity}
							>
								<MenuItem value="3d">3d</MenuItem>
								<MenuItem value="7d">7d</MenuItem>
								<MenuItem value="1w">1w</MenuItem>
								<MenuItem value="4w2d">4w2d</MenuItem>
							</TextField>
						</FormControl>
						<FormControl margin="normal" required fullWidth>
							<TextField
								label="Type"
								className={classes.textField}
								variant="outlined"
								onChange={this.handleChange}
								name="type"
								select
								value={this.state.type}
							>
								<MenuItem value="1">Reguler</MenuItem>
								<MenuItem value="2">Member</MenuItem>
							</TextField>
						</FormControl>
						<FormControl margin="normal" required fullWidth>
							<TextField
								label="Price"
								className={classes.textField}
								variant="outlined"
								type="number"
								onChange={this.handleChange}
								name="price"
								value={this.state.price}
							/>
						</FormControl>
						<FormControl required className={classes.formControl} margin="normal" fullWidth>
							<TextField
								select
								name="limitation_name"
								value={JSON.stringify({
									limitation_name: this.state.limitation_name,
									limitation_id: this.state.limitation_id,
								})}
								label="Limitation"
								helperText="Please select your limitation"
								variant="outlined"
								onChange={
									(e) => {
										this.setState({
											limitation_name: JSON.parse(e.target.value).limitation_name,
											limitation_id: JSON.parse(e.target.value).limitation_id,
										});
									}
								}
							>
								{
									this.props.dataLimitation.map(result => (
										<MenuItem
											key={result.id}
											value={JSON.stringify({
												limitation_name: result.name,
												limitation_id: result.id,
											})}
										>{result.name}</MenuItem>
									))
								}
							</TextField>
						</FormControl>
						<Button
							type="submit"
							variant="contained"
							color="primary"
							className={classes.submit}
							style={{marginRight: 5}}
						>Save</Button>
						<Button
							variant="contained"
							color="secondary"
							className={classes.submit}
							style={{marginRight: 5}}
							onClick={() => {this.props.closeFormAddProfile()}}
						>Close</Button>
					</form>
				</DialogContent>
			</Dialog>
		);
	}
}

FormAddProfile.propTypes = {
	classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(FormAddProfile);
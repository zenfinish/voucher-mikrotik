import React from 'react';
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';
import { Button, FormControl, Dialog, DialogContent, TextField } from '@material-ui/core';

import api from '../../config/api.js';

const styles = theme => ({
	main: {
		width: 'auto',
		display: 'block', // Fix IE 11 issue.
		marginLeft: theme.spacing.unit * 3,
		marginRight: theme.spacing.unit * 3,
		[theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
			width: 400,
			marginLeft: 'auto',
			marginRight: 'auto',
		},
	},
	paper: {
		marginTop: theme.spacing.unit * 8,
		display: 'flex',    
		flexDirection: 'column',
		alignItems: 'center',
		padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
	},
	avatar: {
		margin: theme.spacing.unit,
		backgroundColor: theme.palette.secondary.main,
	},
	form: {
		width: '100%', // Fix IE 11 issue.
		marginTop: theme.spacing.unit,
	},
	submit: {
		marginTop: theme.spacing.unit * 3,
	},
});

class FormAddLimitation extends React.Component {
	
	state = {
		name: '',
		download: '',
		upload: '',
		transfer: '',
		masa_aktif: '',
	}
	
	handleChange = (e) => {
		this.setState({
			[e.target.name]: e.target.value,
		});
	};

saveData = (e) => {
	e.preventDefault();
	api.post(`/limitation`, { ...this.state }, {
		headers: { token: localStorage.getItem('token') }
	})
	.then(result => {
		this.props.success();
		this.props.closeFormAddLimitation();
	})
	.catch(error => {
		this.props.error(error.response.data);
	});
}
		
	render() {
		const { classes, openFormAddLimitation, closeFormAddLimitation } = this.props;
		return (
			<Dialog
				open={openFormAddLimitation}
				aria-labelledby="form-dialog-title"
				onClose={closeFormAddLimitation.bind(this)}
			>
				<DialogContent>
					<form className={classes.form} onSubmit={this.saveData}>
						<FormControl margin="normal" required fullWidth>
							<TextField
								label="Name"
								className={classes.textField}
								variant="outlined"
								name="name"
								onChange={this.handleChange}
							/>
						</FormControl>
						<FormControl margin="normal" required fullWidth>
							<TextField
								label="Download (MB)"
								className={classes.textField}
								variant="outlined"
								name="download"
								onChange={this.handleChange}
								type="number"
							/>
						</FormControl>
						<FormControl margin="normal" required fullWidth>
							<TextField
								label="Upload (MB)"
								className={classes.textField}
								variant="outlined"
								name="upload"
								onChange={this.handleChange}
								type="number"
							/>
						</FormControl>
						<FormControl margin="normal" required fullWidth>
							<TextField
								label="Transfer (MB)"
								className={classes.textField}
								variant="outlined"
								name="transfer"
								type="number"
								onChange={this.handleChange}
							/>
						</FormControl>
						<FormControl margin="normal" required fullWidth>
							<TextField
								label="Masa Aktif"
								className={classes.textField}
								variant="outlined"
								name="masa_aktif"
								onChange={this.handleChange}
							/>
						</FormControl>
						<Button
							type="submit"
							variant="contained"
							color="primary"
							className={classes.submit}
							style={{marginRight: 5}}
						>Save</Button>
						<Button
							variant="contained"
							color="secondary"
							className={classes.submit}
							onClick={() => {this.props.closeFormAddLimitation()}}
						>Close</Button>
					</form>
				</DialogContent>
			</Dialog>
		);
	}
}

FormAddLimitation.propTypes = {
	classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(FormAddLimitation);
import React from 'react';
import api from '../../config/api.js';

class FormAddProfile extends React.Component {
	
	state = {
		member_profile_name: '',
		rate_limit: '',
		price: '',

		profiles: [],

		loading: false,
	}

	componentDidMount() {
		this.getProfile();
	}

	getProfile = () => {
		this.setState({ loading: true }, () => {
			api.get(`/members/profiles`,
				{ headers: { token: localStorage.getItem('token') } }
			)
			.then(result => {
				this.setState({
					profiles: result.data,
					loading: false
				});
			})
			.catch(error => {
				console.log(error.response);
				this.setState({ loading: false });
			});
		});
	}
	
	handleClose = () => {
		this.props.closeFormAddProfile();
	};

	handleChange = (e) => {
		this.setState({
			[e.target.name]: e.target.value,
		});
	};

	saveProfile = (e) => {
		e.preventDefault();
		this.setState({ loading: true }, () => {
			api.post(`/members/profile`, this.state, { headers: { token: localStorage.getItem('token') } })
			.then(result => {
				this.getProfile();
			})
			.catch(error => {
				console.log(error.response)
				this.setState({ loading: false });
			});
		});
	}
		
	render() {
		const { classes } = this.props;
		return (
			<>
				{/* <Dialog
					open={true}
					aria-labelledby="form-dialog-title"
					onClose={this.handleClose}
					fullWidth
					maxWidth="xl"
				>
					<DialogContent>
						<form className={classes.form} onSubmit={this.saveProfile}>
							<FormControl margin="normal" required fullWidth>
								<TextField
									label="Name"
									className={classes.textField}
									variant="outlined"
									onChange={this.handleChange}
									name="member_profile_name"
									value={this.state.name}
								/>
							</FormControl>
							<FormControl margin="normal" required fullWidth>
								<TextField
									label="Rate Limit"
									className={classes.textField}
									variant="outlined"
									onChange={this.handleChange}
									name="rate_limit"
									value={this.state.rate_limit}
								/>
							</FormControl>
							<FormControl margin="normal" required fullWidth>
								<TextField
									label="Price"
									className={classes.textField}
									variant="outlined"
									onChange={this.handleChange}
									name="price"
									value={this.state.price}
									type="number"
								/>
							</FormControl>
							<DataGrid
								style={{ height: 230 }}
								data={this.state.profiles}
							>
								<GridColumn field="member_profile_name" title="Name"></GridColumn>
								<GridColumn field="member_profile_id" title="ID"></GridColumn>
								<GridColumn field="rate_limit" title="Rate Limit"></GridColumn>
								<GridColumn field="price" title="Price"></GridColumn>
							</DataGrid>
							<Button
								type="submit"
								variant="contained"
								color="primary"
								className={classes.submit}
								style={{marginRight: 5}}
								fullWidth
								disabled={this.state.loading}
							>Save</Button>
						</form>
					</DialogContent>
				</Dialog> */}
			</>
		);
	}
}

export default (FormAddProfile);

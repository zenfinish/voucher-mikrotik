import React from 'react';
import api from '../../config/api.js';
import { tglIndo } from "../../config/helpers";
import FormAddMember from './FormAddMember.jsx';
import FormAddProfile from './FormAddProfile.jsx';
import DeleteMember from './DeleteMember.jsx';
import StatusFilter from './StatusFilter.jsx';

class Main extends React.Component {

	state = {
		dataProfile: [],

		dataTable: [],
		loading: false,
		total: 0,
    pageNumber: 1,
		pageSize: 20,
		
		openFormAddMember: false,
		openFormAddProfile: false,
		openDeleteMember: false,
		openStatusFilter: false,
		dataAktif: {},
	}
	
	componentDidMount() {
		this.loadPage(this.state.pageNumber, this.state.pageSize);
		
		setInterval(() => {
			this.getStatusFilter();
		}, 900000) // 15 Menit;
	}

	handlePageChange(event) {
		this.loadPage(event.pageNumber, event.pageSize)
	}

	loadPage() {
		this.setState({ loading: true });
		api.get(`/members`,
			{ headers: { token: localStorage.getItem('token') } }
		)
		.then(result => {
			this.setState({
				dataTable: result.data,
				loading: false
			});
		})
		.catch(error => {
			console.log(error.response);
		});
	}

	error = (data) => {
		this.messager.alert({
			title: "Alert",
			msg: JSON.stringify(data)
		});
	}

	getStatusFilter = () => {
		this.setState({ loading: true }, () => {
			api.patch(`/members/status/filter`,
				{ headers: { token: localStorage.getItem('token') } }
			)
			.then(result => {
				this.setState({
					dataTable: result.data,
					loading: false
				});
			})
			.catch(error => {
				console.log(error.response);
			});
		});
	}
	
	render() {
		return (
			<>
				{/* <LinkButton
					iconCls="icon-add"
					onClick={() => {this.setState({ openFormAddMember: true })}}
				>Add Member</LinkButton>
				<LinkButton
					iconCls="icon-add"
					onClick={() => {this.setState({ openFormAddProfile: true })}}
				>Add Profile</LinkButton>
				<LinkButton
					iconCls="icon-add"
					onClick={this.getStatusFilter}
					disabled={this.state.loading}
				>Refresh Status Internet</LinkButton>
				<DataGrid
					style={{ height: "80%" }}
					data={this.state.dataTable}
					loading={this.state.loading}
				>
					<GridColumnGroup frozen align="left" width={390}>
            <GridHeaderRow>
							<GridColumn
								field="created_at"
								title="Tanggal"
								render={({ value }) => (
									<>{tglIndo(value)}</>
								)}
								align="right"
								width={140}
							></GridColumn>
							<GridColumn field="name" title="Nama" width={250}></GridColumn>
						</GridHeaderRow>
					</GridColumnGroup>
					<GridColumnGroup>
            <GridHeaderRow>
							<GridColumn field="id" title="ID" width={50}></GridColumn>
							<GridColumn field="email" title="Email" width={120}></GridColumn>
							<GridColumn field="hp" title="No. Hp" width={120}></GridColumn>
							<GridColumn field="password" title="Password" width={100}></GridColumn>
							<GridColumn field="address" title="Alamat" width={300}></GridColumn>
							<GridColumn field="src_mac_address" title="Mac Address" width={150}></GridColumn>
						</GridHeaderRow>
					</GridColumnGroup>
					<GridColumnGroup frozen align="right">
            <GridHeaderRow>
							<GridColumn
								title="Action"
								render={({ row }) => (
									<>
										<LinkButton
											iconCls="icon-edit"
											onClick={() => {this.setState({ dataAktif: row, openDeleteMember: true })}}
										>Hapus</LinkButton>
										<LinkButton
											onClick={() => {this.setState({ dataAktif: row, openStatusFilter: true })}}
											style={{ color: row.disabled === 1 ? "red" : "green" }}
										>{row.disabled === 1 ? "Disabled" : "Aktif"}</LinkButton>
									</>
								)}
								align="center"
							></GridColumn>
						</GridHeaderRow>
					</GridColumnGroup>
				</DataGrid>
				{
					this.state.openFormAddMember ?
						<FormAddMember 
							closeFormAddMember={() => {this.setState({ openFormAddMember: false });}}
							success={() => {this.loadPage()}}
							error={this.error}
						/>
					: null
				}
				{
					this.state.openFormAddProfile ?
						<FormAddProfile 
							closeFormAddProfile={() => {this.setState({ openFormAddProfile: false });}}
							success={() => {this.loadPage()}}
							error={this.error}
						/>
					: null
				}
				{
					this.state.openDeleteMember ?
						<DeleteMember 
							closeDeleteMember={() => {this.setState({ openDeleteMember: false });}}
							data={this.state.dataAktif}
							success={() => {this.loadPage()}}
							error={this.error}
						/>
					: null
				}
				{
					this.state.openStatusFilter ?
						<StatusFilter 
							closeStatusFilter={() => {this.setState({ openStatusFilter: false });}}
							data={this.state.dataAktif}
							success={() => {this.loadPage()}}
							error={this.error}
						/>
					: null
				}
				<Messager ref={ref => this.messager = ref}></Messager> */}
			</>
		);
	}

};

export default Main;

import React from 'react';

import api from '../../config/api.js';

class DeleteMember extends React.Component {
	
	state = {
		name: '',
		email: '',
		hp: '',
		address: '',

		loading: false,
	}
	
	handleClose = () => {
		this.props.closeDeleteMember();
	};

	hapus = () => {
		this.setState({ loading: true }, () => {
			api.delete(`/members/${this.props.data.id}`, { headers: { token: localStorage.getItem('token') } })
			.then(result => {
				this.props.success();
				this.handleClose();
			})
			.catch(error => {
				console.log(error.response)
				this.props.error(error.response.data);
				this.setState({ loading: false });
			});
		});
	}
		
	render() {
		return (
			<>
				{/* <Dialog
					open={true}
					aria-labelledby="form-dialog-title"
					onClose={this.handleClose}
				>
					<DialogContent>
						<div>Yakin Hapus ?</div>
						<div>Nama : {this.props.data.name}</div>
						<div>Hp : {this.props.data.hp}</div>
					</DialogContent>
					<DialogActions>
						<Button
							variant="contained"
							color="primary"
							disabled={this.state.loading}
							onClick={this.hapus}
						>Hapus</Button>
					</DialogActions>
				</Dialog> */}
			</>
		);
	}
}

export default (DeleteMember);
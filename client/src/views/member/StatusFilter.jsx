import React from 'react';
import api from '../../config/api.js';

class StatusFilter extends React.Component {
	
	state = {
		loading: false,
	}
	
	handleClose = () => {
		this.props.closeStatusFilter();
	};

	aktif = () => {
		this.setState({ loading: true }, () => {
			api.put(`/members/filter/enable/${this.props.data.filter_id}`, { headers: { token: localStorage.getItem('token') } })
			.then(result => {
				this.props.success();
				this.handleClose();
			})
			.catch(error => {
				console.log(error.response)
				this.props.error(error.response.data);
				this.setState({ loading: false });
			});
		});
	}

	nonaktif = () => {
		this.setState({ loading: true }, () => {
			api.put(`/members/filter/disable/${this.props.data.filter_id}`, { headers: { token: localStorage.getItem('token') } })
			.then(result => {
				this.props.success();
				this.handleClose();
			})
			.catch(error => {
				console.log(error.response)
				this.props.error(error.response.data);
				this.setState({ loading: false });
			});
		});
	}
		
	render() {
		return (
			<>
				{/* <Dialog
					open={true}
					aria-labelledby="form-dialog-title"
					onClose={this.handleClose}
				>
					<DialogContent>
						<div><u>Status Filter</u></div>
						<div>Nama : {this.props.data.name}</div>
						<div>Hp : {this.props.data.hp}</div>
						<div>Mac Address : {this.props.data.src_mac_address}</div>
						<div>Disabled : {this.props.data.disabled === 1 ? 'True' : 'False'}</div>
					</DialogContent>
					<DialogActions>
						{
							this.props.data.disabled === 1 ?
								<Button
									variant="contained"
									color="secondary"
									disabled={this.state.loading}
									onClick={this.aktif}
								>Aktifkan</Button> :
								<Button
									variant="contained"
									color="primary"
									disabled={this.state.loading}
									onClick={this.nonaktif}
								>Non Aktifkan</Button>
						}
					</DialogActions>
				</Dialog> */}
			</>
		);
	}
}

export default (StatusFilter);
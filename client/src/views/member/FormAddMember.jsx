import React from 'react';
import api from '../../config/api.js';

class FormAddMember extends React.Component {
	
	state = {
		name: '',
		email: '',
		hp: '',
		address: '',
		src_mac_address: '',
		member_profile_id: '',
		member_profile_name: '',

		profiles: [],

		loading: false,
	}

	componentDidMount() {
		this.getProfile();
	}

	getProfile = () => {
		this.setState({ loading: true }, () => {
			api.get(`/members/profiles`,
				{ headers: { token: localStorage.getItem('token') } }
			)
			.then(result => {
				this.setState({
					profiles: result.data,
					loading: false
				});
			})
			.catch(error => {
				console.log(error.response);
				this.setState({ loading: false });
			});
		});
	}
	
	handleClose = () => {
		this.props.closeFormAddMember();
	};

	handleChange = (e) => {
		this.setState({
			[e.target.name]: e.target.value,
		});
	};

	saveProfile = (e) => {
		e.preventDefault();
		this.setState({ loading: true }, () => {
			api.post(`/members`, this.state, { headers: { token: localStorage.getItem('token') } })
			.then(result => {
				this.props.success();
				this.handleClose();
			})
			.catch(error => {
				console.log(error.response)
				this.props.error(error.response.data);
				this.setState({ loading: false });
			});
		});
	}
		
	render() {
		const { classes } = this.props;
		return (
			<>
				{/* <Dialog
					open={true}
					aria-labelledby="form-dialog-title"
					onClose={this.handleClose}
				>
						<DialogContent>
							<form className={classes.form} onSubmit={this.saveProfile}>
								<FormControl margin="normal" required fullWidth>
									<TextField
										label="Nama"
										className={classes.textField}
										variant="outlined"
										onChange={this.handleChange}
										name="name"
										value={this.state.name}
									/>
								</FormControl>
								<FormControl margin="normal" required fullWidth>
									<TextField
										label="Email"
										className={classes.textField}
										variant="outlined"
										onChange={this.handleChange}
										name="email"
										value={this.state.email}
									/>
								</FormControl>
								<FormControl margin="normal" required fullWidth>
									<TextField
										label="No. Hp"
										className={classes.textField}
										variant="outlined"
										onChange={this.handleChange}
										name="hp"
										value={this.state.hp}
									/>
								</FormControl>
								<FormControl margin="normal" required fullWidth>
									<TextField
										label="Alamat"
										className={classes.textField}
										variant="outlined"
										onChange={this.handleChange}
										name="address"
										value={this.state.address}
									/>
								</FormControl>
								<FormControl margin="normal" required fullWidth>
									<TextField
										label="Mac Address"
										className={classes.textField}
										variant="outlined"
										onChange={this.handleChange}
										name="src_mac_address"
										value={this.state.src_mac_address}
									/>
								</FormControl>
								<FormControl margin="normal" required fullWidth variant="outlined">
									<InputLabel id="profile">Profile</InputLabel>
									<Select
										id="Profile"
										name="profile"
										value={this.state.member_profile_id}
										onChange={(e, child) => {
											this.setState({
												member_profile_id: child.props.value,
												member_profile_name: child.props.children,
											});
										}}
									>
										<MenuItem value="">
											<em>None</em>
										</MenuItem>
										{
											this.state.profiles.map((row, i) => (
												<MenuItem value={row.member_profile_id} key={i}>{row.member_profile_name}</MenuItem>
											))
										}
									</Select>
								</FormControl>
								<Button
									type="submit"
									variant="contained"
									color="primary"
									className={classes.submit}
									style={{marginRight: 5}}
									fullWidth
									disabled={this.state.loading}
								>Save</Button>
							</form>
						</DialogContent>
				</Dialog> */}
			</>
		);
	}
}

export default (FormAddMember);
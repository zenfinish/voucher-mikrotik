import React from 'react';
// import { Line, Bar } from 'react-chartjs-2';
// import { separator } from '../../config/helpers.js';
import api from '../../config/api.js';

class Main extends React.Component {
    
	state = {
		targetHarian: 2000000,
		harian: [],

		targetBulanan: 50000000,
		bulanan: [],

		targetTahunan: 1000000000,
		tahunan: [],

		total: 0,
		memberTotal: 0,
	}

	componentDidMount() {
		this.fetchTotal();
		this.fetchMemberTotal();
		this.fetchHarian();
		this.fetchBulanan();
		this.fetchTahunan();
	}

	fetchTotal = () => {
		api.get(`/transactions/total`)
		.then(result => {
			this.setState({
				total: result.data.total,
			});
		})
		.catch(error => {
			console.log(error.response);
		});
	}

	fetchMemberTotal = () => {
		api.get(`/members/transactions/total`)
		.then(result => {
			this.setState({
				memberTotal: result.data.total,
			});
		})
		.catch(error => {
			console.log(error.response);
		});
	}

	fetchHarian = () => {
		api.get(`/transactions/harian`)
		.then(result => {
			this.setState({
				harian: result.data,
			});
		})
		.catch(error => {
			console.log(error.response);
		});
	}

	fetchBulanan = () => {
		api.get(`/transactions/bulanan`)
		.then(result => {
			this.setState({
				bulanan: result.data,
			});
		})
		.catch(error => {
			console.log(error.response);
		});
	}

	fetchTahunan = () => {
		api.get(`/transactions/tahunan`)
		.then(result => {
			this.setState({
				tahunan: result.data,
			});
		})
		.catch(error => {
			console.log(error.response);
		});
	}
	
	render() {
		return (
			<>
				{/* <Grid container spacing={8} style={{ marginBottom: 10 }}>
					<Grid item xs={3}>
						<Card>
							<CardContent>Total Penjualan Voucher<br /><br />Rp. {separator(this.state.total, 0)}</CardContent>
						</Card>
					</Grid>
					<Grid item xs={3}>
						<Card>
							<CardContent>Total Pembayaran Member<br /><br />Rp. {separator(this.state.memberTotal, 0)}</CardContent>
						</Card>
					</Grid>
				</Grid>
				<Grid container spacing={8} style={{ marginBottom: 10 }}>
					<Grid item xs={12}>
						<h2>DAILY</h2>
						<Line
							data={{
								labels: this.state.harian.map((obj) => { return obj.tanggal; }),
								datasets: [
									{
										label: 'Target',
										fill: false,
										backgroundColor: '#FF4500',
										borderColor: '#FF4500',
										pointBorderColor: '#FF4500',
										pointBorderWidth: 5,
										pointHoverRadius: 8,
										pointHoverBackgroundColor: '#FF4500',
										pointRadius: 1,
										pointHitRadius: 50,
										data: this.state.harian.map(() => { return this.state.targetHarian; }),
									},
									{
										label: 'Penjualan',
										fill: false,
										backgroundColor: '#FF8C00',
										borderColor: '#FF8C00',
										pointBorderColor: '#FF8C00',
										pointBorderWidth: 5,
										pointHoverRadius: 8,
										pointHoverBackgroundColor: '#FF8C00',
										pointRadius: 1,
										pointHitRadius: 50,
										data: this.state.harian.map((obj) => { return obj.total; })
									},
								]
							}}
							options={{
								tooltips: {
									callbacks: {
										label: function(tooltipItem, data) {
											let nama = '';
											if (tooltipItem.datasetIndex === 0) {
												nama = 'Target';
											} else if (tooltipItem.datasetIndex === 1) {
												nama = 'Penjualan';
											};
											return nama + ' ' + separator(tooltipItem.value, 0);
										},
									}
								},
								scales: {
									yAxes: [{
										ticks: {
											callback: function(label, index, labels) {
												return separator(label, 0);
											},
										},
									}],
								},
							}}
						/>
					</Grid>
				</Grid>
				<Grid container spacing={8} style={{ marginBottom: 10 }}>
					<Grid item xs={6}>
						<h2>MONTHLY</h2>
						<Bar
							data={{
								labels: this.state.bulanan.map((obj) => { return obj.bulan; }),
								datasets: [
									{
										label: 'Target',
										fill: false,
										backgroundColor: '#FF4500',
										borderColor: '#FF4500',
										pointBorderColor: '#FF4500',
										pointBorderWidth: 5,
										pointHoverRadius: 8,
										pointHoverBackgroundColor: '#FF4500',
										pointRadius: 1,
										pointHitRadius: 50,
										data: this.state.bulanan.map(() => { return this.state.targetBulanan; }),
									},
									{
										label: 'Penjualan',
										fill: false,
										backgroundColor: '#FF8C00',
										borderColor: '#FF8C00',
										pointBorderColor: '#FF8C00',
										pointBorderWidth: 5,
										pointHoverRadius: 8,
										pointHoverBackgroundColor: '#FF8C00',
										pointRadius: 1,
										pointHitRadius: 50,
										data: this.state.bulanan.map((obj) => { return obj.total; })
									},
								]
							}}
							options={{
								tooltips: {
									callbacks: {
										label: function(tooltipItem, data) {
											let nama = '';
											if (tooltipItem.datasetIndex === 0) {
												nama = 'Target';
											} else if (tooltipItem.datasetIndex === 1) {
												nama = 'Penjualan';
											};
											return nama + ' ' + separator(tooltipItem.value, 0);
										},
									}
								},
								scales: {
									yAxes: [{
										ticks: {
											callback: function(label, index, labels) {
												return separator(label, 0);
											},
										},
									}],
								},
							}}
						/>
					</Grid>
					<Grid item xs={6}>
						<h2>YEARLY</h2>
						<Bar
							data={{
								labels: this.state.tahunan.map((obj) => { return obj.tahun; }),
								datasets: [
									{
										label: 'Target',
										fill: false,
										backgroundColor: '#FF4500',
										borderColor: '#FF4500',
										pointBorderColor: '#FF4500',
										pointBorderWidth: 5,
										pointHoverRadius: 8,
										pointHoverBackgroundColor: '#FF4500',
										pointRadius: 1,
										pointHitRadius: 50,
										data: this.state.tahunan.map(() => { return this.state.targetTahunan; }),
									},
									{
										label: 'Penjualan',
										fill: false,
										backgroundColor: '#FF8C00',
										borderColor: '#FF8C00',
										pointBorderColor: '#FF8C00',
										pointBorderWidth: 5,
										pointHoverRadius: 8,
										pointHoverBackgroundColor: '#FF8C00',
										pointRadius: 1,
										pointHitRadius: 50,
										data: this.state.tahunan.map((obj) => { return obj.total; })
									},
								]
							}}
							options={{
								tooltips: {
									callbacks: {
										label: function(tooltipItem, data) {
											let nama = '';
											if (tooltipItem.datasetIndex === 0) {
												nama = 'Target';
											} else if (tooltipItem.datasetIndex === 1) {
												nama = 'Penjualan';
											};
											return nama + ' ' + separator(tooltipItem.value, 0);
										},
									}
								},
								scales: {
									yAxes: [{
										ticks: {
											callback: function(label, index, labels) {
												return separator(label, 0);
											},
										},
									}],
								},
							}}
						/>
					</Grid>
				</Grid> */}
			</>
		);
	}

};

export default (Main);

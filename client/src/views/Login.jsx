import React, { useEffect } from 'react';
import api from '../config/api.js';
import { Navigate } from 'react-router-dom';
import { Modal, notification } from 'antd';

const classInput = "border border-black w-full p-2 rounded";

class Login extends React.Component {

	state = {
		username: '',
		password: '',

		redirect: false,
	}

	login = (e) => {
		e.preventDefault();
		api.post(`/users/login`, {
			username: this.state.username,
			password: this.state.password,
		})
		.then(result => {
			notification.success({
				message: 'Login berhasil'
			});
			localStorage.setItem('token', result.data);
			this.setState({ redirect: true });
		})
		.catch(error => {
			Modal.error({
				content: error.response.data,
			});
		});
	}

	handleChange = (e) => {
		this.setState({
			[e.target.name]: e.target.value,
		});
	};

	render() {
		return (
			<>
				<div className="grid h-screen place-items-center">
					<form className="grid grid-cols-1 gap-1 w-64" onSubmit={this.login}>
						<div>
							<label>Username</label>
							<input
								type="text"
								name="username"
								className={classInput}
								onChange={this.handleChange}
							/>
						</div>
						<div>
							<label>Password</label>
							<input
								type="password"
								name="password"
								className={classInput}
								onChange={this.handleChange}
							/>
						</div>
						<div>
							<button className={classInput} type="submit">LOGIN</button>
						</div>
					</form>
				</div>
					{/* <Paper>
						<Typography component="h1" variant="h5">VERD Voucher Hotspot</Typography>
						<form onSubmit={this.login}>
							<FormControl margin="normal" required fullWidth>
								<InputLabel htmlFor="username">Username</InputLabel>
								<Input id="username" name="username" autoComplete="username" autoFocus onChange={this.handleChange} />
							</FormControl>
							<FormControl margin="normal" required fullWidth>
								<InputLabel htmlFor="password">Password</InputLabel>
								<Input name="password" type="password" id="password" autoComplete="password" onChange={this.handleChange} />
							</FormControl>
							<Button
								type="submit"
								variant="contained"
								color="primary"
								style={{marginRight: 5}}
								fullWidth
							>LOGIN</Button>
						</form>
					</Paper>
					<Alert
						open={this.state.openAlert}
						closeAlert={() => {this.setState({ openAlert: false })}}
						variant={this.state.variant}
						message={this.state.message}
					/> */}
				{
					this.state.redirect ?
						<Navigate to={`/dashboard`} />
					: null
				}
			</>
		);
	}

}

export default (Login);
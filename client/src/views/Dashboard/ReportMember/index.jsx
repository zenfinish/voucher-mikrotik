import React from 'react';
import api from '../../../config/api.js';
import { separator } from '../../../config/helpers.js';

class index extends React.Component {
	
	state = {
		dari_bulan: "01",
		sampai_bulan: "01",
		tahun: "2021",
		loading: false,
		data: [],
	};

	handleChange = (e) => {
		this.setState({
			[e.target.name]: e.target.value,
		});
	};

	cetak = () => {
		this.setState({
			loading: true
		}, () => {
			api.get(`/transactions/member?dari_bulan=${this.state.dari_bulan}&sampai_bulan=${this.state.sampai_bulan}&tahun=${this.state.tahun}`)
			.then(result => {
				this.setState({
					data: result.data,
					loading: false,
				});
			})
			.catch(error => {
				this.error(error.response.data);
				this.setState({
					loading: false,
				});
			});
		});
	}

	footerData = () => {
		let total = 0;
		let data = this.state.data;
		for (let i = 0; i < data.length; i++) {
			total += data[i].jumlah;
		};
		return [
			{ id: "Total:", jumlah: separator(total, 0) }
		];
	}

	error = (data) => {
		this.messager.alert({
			title: "Alert",
			msg: data
		});
	}

	render() {
		let total = 0;
		this.state.data.map(row => {
			return total += row.jumlah;
		});
		return (
			<div>
				<h3>Report Member</h3>
				<div>
					Dari Bulan:
						<select name="dari_bulan" onChange={this.handleChange}>
							<option value="01">Januari</option>
							<option value="02">Februari</option>
							<option value="03">Maret</option>
							<option value="04">April</option>
							<option value="05">Mei</option>
							<option value="06">Juni</option>
							<option value="07">Juli</option>
							<option value="08">Agustus</option>
							<option value="09">September</option>
							<option value="10">Oktober</option>
							<option value="11">November</option>
							<option value="12">Desember</option>
						</select>
					Sampai Bulan:
						<select name="sampai_bulan" onChange={this.handleChange}>
							<option value="01">Januari</option>
							<option value="02">Februari</option>
							<option value="03">Maret</option>
							<option value="04">April</option>
							<option value="05">Mei</option>
							<option value="06">Juni</option>
							<option value="07">Juli</option>
							<option value="08">Agustus</option>
							<option value="09">September</option>
							<option value="10">Oktober</option>
							<option value="11">November</option>
							<option value="12">Desember</option>
						</select>
					Tahun:
						<select name="tahun" onChange={this.handleChange}>
							<option value="2021">2021</option>
							<option value="2022">2022</option>
							<option value="2023">2023</option>
						</select>
					<button onClick={this.cetak}>Tampilkan</button>
				</div>
				{/* <DataGrid
					data={this.state.data}
					style={{ height:400 }}
					columnResizing
					showFooter
					footerData={this.footerData()}
					loading={this.state.loading}
				>
					<GridColumnGroup>
						<GridHeaderRow>
							<GridColumn field="name" title="Nama" width={200} align="center"></GridColumn>
							<GridColumn field="id" title="ID" width={150} align="center"></GridColumn>
							<GridColumn field="jumlah" title="Jumlah" width={150} align="center" render={({ value }) => separator(value, 0)}></GridColumn>
							<GridColumn field="img" title="Bukti Transfer" width={150} align="center"
								render={({ value }) => (
									<a href={`${process.env.REACT_APP_PUBLIC_IMAGE}/${value}`} target="_blank" rel="noopener noreferrer">Lihat</a>
								)}
							/>
						</GridHeaderRow>
					</GridColumnGroup>
				</DataGrid> */}
			</div>
		);
	}

}

export default ((index));
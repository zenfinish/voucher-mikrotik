import React from 'react';
// import CardVoucher from './CardVoucher.jsx';
import api from '../../config/api.js';
import { separator } from '../../config/helpers.js';
// import FormBeliVoucher from './FormBeliVoucher';
// import Barcode from './Barcode';

class Main extends React.Component {

	state = {
		openFormBeliVoucher: false,
		openBarcode: false,
		voucherAktif: null,
		voucherGuguk: null,

		data: [],
		dataVoucher: [],

		loading: false,
	}

	componentDidMount() {
		this.fetchData();
		api.get(`/profile/reguler`,
			{ headers: { token: localStorage.getItem('token') } }
		)
		.then(result => {
			this.setState({
				dataVoucher: result.data,
			});
		})
		.catch(error => {
			console.log(error.response);
		});
	}

	footerData = () => {
		let total = 0;
		for (let i = 0; i < this.state.data.length; i++) {
			total += this.state.data[i].price;
		};
		return [
		  	{ voucher: "Total:", price_format: separator(total, 0) }
		];
	}
	
	print = (data) => {
		this.setState({
			openFormBeliVoucher: true,
			voucherAktif: {
				name: data.name,
				price: data.price,
			},
		});
	}

	cetakVoucher = (whatsapp) => {
		if (!whatsapp || whatsapp === "0" || whatsapp === "") {
			return;
		} else {
			this.setState({
				loading: true,
			}, () => {
				api.post(`/mikrotik/tool-user-manager-user-add`, {
					profile: this.state.voucherAktif.name,
					voucher: this.state.voucherAktif.name,
					price: this.state.voucherAktif.price,
					hp: whatsapp,
				}, { headers: { token: localStorage.getItem('token') } })
				.then(result => {
					this.closeFormBeliVoucher();
					this.fetchData();
					// var doc = document.getElementById('print').contentWindow.document;
					// doc.open();
					// doc.write(`
					// 	<div>VERD HOTSPOT VOUCHER</div>
					// 	<div>${result.data.tgl} Kasir: ${result.data.user_name}</div>
					// 	<table>
					// 		<tr>
					// 			<td>Username</td>
					// 			<td>:</td>
					// 			<td>${result.data.username}</td>
					// 		</tr>
					// 		<tr>
					// 			<td>Password</td>
					// 			<td>:</td>
					// 			<td>${result.data.password}</td>
					// 		</tr>
					// 	</table>
					// 	<div>${result.data.voucher}</div>
					// 	<div>CS : wa 0811 7230 699</div>
					// `);
					// doc.close();
					// document.getElementById("print").contentWindow.print();
					this.setState({ loading: false });
				})
				.catch(error => {
					this.error(error.response.data);
					this.setState({ loading: false });
				});
			});
		}
	}

	fetchData = () => {
		api.get(`/transactions/peruser`,
			{ headers: { token: localStorage.getItem('token') } }
		)
		.then(result => {
			this.setState({
				data: result.data,
			});
		})
		.catch(error => {
			console.log(error.response);
		});
	}

	cetakUlang = (data) => {
		api.get(`/transactions/perid/${data._id}`,
			{ headers: { token: localStorage.getItem('token') } }
		)
		.then(result => {
			var doc = document.getElementById('print').contentWindow.document;
			doc.open();
			doc.write(`
				<div>VERD HOTSPOT VOUCHER</div>
				<div>${result.data.createdAt} Kasir: ${result.data.username}</div>
				<table>
					<tr>
						<td>Username</td>
						<td>:</td>
						<td>${result.data.user}</td>
					</tr>
					<tr>
						<td>Password</td>
						<td>:</td>
						<td>${result.data.password}</td>
					</tr>
				</table>
				<div>${result.data.voucher}</div>
				<div>CS : wa 0811 7230 699</div>
			`);
			doc.close();
			document.getElementById("print").contentWindow.print();
		})
		.catch(error => {
			console.log(error.response);
		});
	}
	
	closeFormBeliVoucher = () => {
		this.setState({
			voucherAktif: null,
			openFormBeliVoucher: false,
		});
	};

	error = (data) => {
		this.messager.alert({
			title: "Alert",
			msg: data
		});
	}

	render() {
		return (
			<>
				{/* <Card>
					<CardContent>
						{
							this.props.loading ? <img src="https://svc.opushealth.com/balcoltrasavings/img/Processing.gif" alt="" /> :
							this.state.dataVoucher.map((result, index) => (
								<CardVoucher
									dataVoucher={result}
									key={index}
									print={this.print.bind(this, result)}
								/>
							))
						}
						<div><Button variant="outlined" onClick={() => this.setState({ openBarcode: true })}>Barcode</Button></div>
					</CardContent>
				</Card>

				<Card style={{ marginTop: 5 }}>
					<CardContent>
						<DataGrid
							data={this.state.data}
							style={{ height:250 }}
							columnResizing
							showFooter
							footerData={this.footerData()}
						>
							<GridColumnGroup>
								<GridHeaderRow>
									<GridColumn colspan={2} title={`Data Pesanan`}>
										
									</GridColumn>
									<GridColumn title={(<Button>Refresh</Button>)}></GridColumn>
								</GridHeaderRow>
								<GridHeaderRow>
									<GridColumn field="" title="No. Whatsapp" width={100}></GridColumn>
									<GridColumn field="" title="Nama" width={150}></GridColumn>
									<GridColumn
										title="Action"
										width={150}
										field="action"
										render={
											(row) => (
												<div><button>Proses</button></div>
											)
										}
									/>
								</GridHeaderRow>
							</GridColumnGroup>
						</DataGrid>
					</CardContent>
				</Card>

				<Card style={{ marginTop: 5 }}>
					<CardContent>
						<DataGrid
							data={this.state.data}
							style={{height:250}}
							columnResizing
							showFooter
							footerData={this.footerData()}
						>
							<GridColumnGroup>
								<GridHeaderRow>
									<GridColumn colspan={3} title={`Penjualan Hari Ini : ` + new Date().toDateString()}></GridColumn>
								</GridHeaderRow>
								<GridHeaderRow>
									<GridColumn field="jam" title="Jam" width={80}></GridColumn>
									<GridColumn field="voucher" title="Voucher" width={150}></GridColumn>
									<GridColumn field="price_format" title="Price" width={70} align="right"></GridColumn>
								</GridHeaderRow>
							</GridColumnGroup>
						</DataGrid>
					</CardContent>
				</Card> */}

				{/* {
					this.state.openFormBeliVoucher ?
						<FormBeliVoucher 
							closeFormBeliVoucher={this.closeFormBeliVoucher}
							voucherAktif={this.state.voucherAktif}
							cetakVoucher={this.cetakVoucher}
							loading={this.state.loading}
						/>
					: null
				}
				{
					this.state.openBarcode ?
						<Barcode 
							closeBarcode={() => this.setState({ openBarcode: false })}
						/>
					: null
				}
				<div style={{ display: 'none' }}>
					<iframe title="print" id="print"></iframe>
				</div>
				<Messager ref={ref => this.messager = ref}></Messager> */}
			</>
		);
	}

};

export default Main;

import React from 'react';
import { Input } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import SaveIcon from '@material-ui/icons/Save';
import CircularProgress from '@material-ui/core/CircularProgress';

export default class FormBeliVoucher extends React.Component {
	
	state = {
		whatsapp: "0"
	}
	
	handleClose = () => {
		this.setState({ whatsapp: "0" });
		this.props.closeFormBeliVoucher();
	};
		
	render() {
		return (
			<Dialog
				open={true}
				aria-labelledby="form-dialog-title"
				onClose={this.handleClose}
			>
					<DialogTitle id="form-dialog-title">Cetak Voucher</DialogTitle>
					<DialogContent>
						<DialogContentText>
							Beli {this.props.voucherAktif ? this.props.voucherAktif.name : ''} ?
						</DialogContentText>
						<Input
							placeholder="Masukkan No. Whatsapp"
							type="number"
							fullWidth
							value={this.state.whatsapp}
							onChange={(e) => this.setState({ whatsapp: e.target.value })}
						/>
					</DialogContent>
					<DialogActions>
						<Button
							variant="contained"
							color="secondary"
							style={{marginRight: 5}}
							fullWidth
							onClick={() => this.props.cetakVoucher(this.state.whatsapp)}
							disabled={this.props.loading}
						><SaveIcon style={{ marginRight: 10 }} /> Kirim</Button>
						{this.props.loading && <CircularProgress size={24} />}
					</DialogActions>
			</Dialog>
		);
	}

}
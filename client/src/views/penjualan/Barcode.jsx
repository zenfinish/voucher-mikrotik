import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import QRCode from "qrcode.react";
import api from '../../config/api.js';

export default class Barcode extends React.Component {
	
	state = {
		value: "",
		intervalId: null,
	}
	
	componentDidMount() {
		this.getBarcode();
		const intervalId = setInterval(async () => {
			this.getBarcode();
		}, 60000); // 1 menit = 60000
		this.setState({ intervalId: intervalId });
	}

	componentWillUnmount() {
		clearInterval(this.state.intervalId);
	}
	
	handleClose = () => {
		this.props.closeBarcode();
	};

	getBarcode = async () => {
		await api.get(`/users/barcode`)
		.then(result => {
			this.setState({ value: `${process.env.REACT_APP_LINK_WA}?text=${result.data}` });
		})
		.catch(error => {
			console.log(error.response);
		});
	}
		
	render() {
		return (
			<Dialog
				open={true}
				aria-labelledby="form-dialog-title"
				onClose={this.handleClose}
				fullWidth
			>
				<DialogContent>
					<QRCode value={`${this.state.value}`} style={{ width: "100%", height: "100%" }} />
				</DialogContent>
			</Dialog>
		);
	}

}
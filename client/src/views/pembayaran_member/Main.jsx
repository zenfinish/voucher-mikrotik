import React from 'react';
import api from '../../config/api.js';
import { tglIndo } from "../../config/helpers";
import List from './List.jsx';
import Bayar from './Bayar.jsx';

class Main extends React.Component {

	state = {
		dataProfile: [],

		dataTable: [],
		loading: false,
		total: 0,
    pageNumber: 1,
		pageSize: 20,
		
		openList: false,
		openBayar: false,
		dataAktif: {},
	}
	
	componentDidMount() {
		
		this.loadPage(this.state.pageNumber, this.state.pageSize);

	}

	handlePageChange(event) {
		this.loadPage(event.pageNumber, event.pageSize)
	}

	loadPage() {
		this.setState({ loading: true });
		api.get(`/members`,
			{ headers: { token: localStorage.getItem('token') } }
		)
		.then(result => {
			this.setState({
				dataTable: result.data,
				loading: false
			});
		})
		.catch(error => {
			console.log(error.response);
		});
	}

	error = (data) => {
		this.messager.alert({
			title: "Alert",
			msg: JSON.stringify(data)
		});
	}
	
	render() {
		return (
			<>
				{/* <DataGrid
					style={{ height: "80%" }}
					data={this.state.dataTable}
				>
					<GridColumnGroup frozen align="left" width={370}>
						<GridHeaderRow>
							<GridColumn
								field="created_at"
								title="Tanggal"
								render={({ value }) => (
									<>{tglIndo(value)}</>
								)}
								align="right"
								width={140}
							></GridColumn>
							<GridColumn field="name" title="Nama" width={230}></GridColumn>
						</GridHeaderRow>
					</GridColumnGroup>
					<GridColumn
						field="bulan_daftar"
						title="Bulan Daftar"
						width={150}
						render={({ value }) => tglIndo(value)}
						align="center"
					></GridColumn>
					<GridColumn
						field="terakhir_bayar"
						title="Terakhir Bayar"
						width={150}
						render={({ value }) => tglIndo(value)}
						align="center"
					></GridColumn>
					<GridColumn field="id" title="ID" width={50}></GridColumn>
					<GridColumn field="email" title="Email" width={110}></GridColumn>
					<GridColumn field="hp" title="No. Hp" width={110}></GridColumn>
					<GridColumn field="password" title="Password" width={100}></GridColumn>
					<GridColumn field="address" title="Alamat" width={100}></GridColumn>
					<GridColumn field="src_mac_address" title="Mac Address" width={150}></GridColumn>
					<GridColumnGroup frozen align="right" width={150}>
						<GridHeaderRow>
							<GridColumn
								field="email"
								title="Action"
								render={({ row }) => (
									<>
										<LinkButton
											onClick={() => {this.setState({ dataAktif: row, openBayar: true })}}
										>Bayar</LinkButton>
										<LinkButton
											iconCls="icon-add"
											onClick={() => {this.setState({ dataAktif: row, openList: true })}}
										>List</LinkButton>
									</>
								)}
								align="center"
							></GridColumn>
						</GridHeaderRow>
					</GridColumnGroup>
				</DataGrid>
				{
					this.state.openList ?
						<List 
							closeList={() => {this.setState({ openList: false });}}
							data={this.state.dataAktif}
							success={() => {this.loadPage()}}
							error={this.error}
						/>
					: null
				}
				{
					this.state.openBayar ?
						<Bayar 
							closeBayar={() => {this.setState({ openBayar: false });}}
							data={this.state.dataAktif}
							success={() => {this.loadPage()}}
							error={this.error}
						/>
					: null
				}
				<Messager ref={ref => this.messager = ref}></Messager> */}
			</>
		);
	}

};

export default Main;

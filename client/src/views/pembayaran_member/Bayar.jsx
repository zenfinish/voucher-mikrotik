import React from 'react';
import api from '../../config/api.js';

class Bayar extends React.Component {
	
	state = {
		disabled: '',

		bulan: '01',
		tahun: '2021',
		jumlah: '',
		img: "",
		profileImg: null,

		loading: false,
	}

	// componentDidMount() {
	// 	console.log('masuk=', this.props.data)
	// }
	
	handleClose = () => {
		this.props.closeBayar();
	};

	handleChange = (e) => {
		this.setState({
      [e.target.name]: e.target.value
    });
	};

	handleImg = (e) => {
		this.setState({
      img: URL.createObjectURL(e.target.files[0]),
			profileImg: e.target.files[0]
    });
	};

	simpan = () => {
		this.setState({ loading: true }, () => {
			const formData = new FormData()
			formData.append('member_id', this.props.data.id);
			formData.append('bulan', this.state.bulan);
			formData.append('tahun', this.state.tahun);
			formData.append('jumlah', this.state.jumlah);
			formData.append('file', this.state.profileImg);
			api.post(`/members/bayar`, formData, {
				headers: { 'content-type': 'multipart/form-data' }
			})
			.then(result => {
				this.props.success();
				this.handleClose();
			})
			.catch(error => {
				console.log(error.response)
				this.props.error(error.response.data);
				this.setState({ loading: false });
			});
		});
	}
		
	render() {
		return (
			<>
				{/* <Dialog
					open={true}
					aria-labelledby="form-dialog-title"
					onClose={this.handleClose}
					fullScreen
				>
					<DialogContent>
						<div><u>Bayar Member</u></div>
						<div>Nama: {this.props.data.name}</div>
						<div>Hp: {this.props.data.hp}</div>
						<div>
							Bulan:
								<select name="bulan" onChange={this.handleChange}>
									<option value="01">Januari</option>
									<option value="02">Februari</option>
									<option value="03">Maret</option>
									<option value="04">April</option>
									<option value="05">Mei</option>
									<option value="06">Juni</option>
									<option value="07">Juli</option>
									<option value="08">Agustus</option>
									<option value="09">September</option>
									<option value="10">Oktober</option>
									<option value="11">November</option>
									<option value="12">Desember</option>
								</select>
							Tahun:
								<select name="tahun" onChange={this.handleChange}>
									<option value="2021">2021</option>
									<option value="2022">2022</option>
									<option value="2023">2023</option>
								</select>
						</div>
						<div>Jumlah: <input type="text" name="jumlah" onChange={this.handleChange} /></div>
						<div>Bukti Transfer : <input type="file" onChange={this.handleImg} /></div>
						<div><img src={this.state.img} alt="" /></div>
					</DialogContent>
					<DialogActions>
						<Button
							variant="contained"
							color="secondary"
							disabled={this.state.loading}
							onClick={this.handleClose}
						>Tutup</Button>
						<Button
							variant="contained"
							color="primary"
							disabled={this.state.loading}
							onClick={this.simpan}
						>Simpan</Button>
					</DialogActions>
				</Dialog> */}
			</>
		);
	}
}

export default (Bayar);
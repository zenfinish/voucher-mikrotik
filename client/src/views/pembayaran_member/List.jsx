import React from 'react';
import api from '../../config/api.js';
import { separator } from "../../config/helpers";

class List extends React.Component {
	
	state = {
		data: [],
		loading: false,
	}

	componentDidMount() {
		this.refresh();
	}
	
	handleClose = () => {
		this.props.closeList();
	};

	refresh = () => {
		api.get(`/members/transactions/${this.props.data.id}`)
		.then(result => {
			this.setState({
				data: result.data,
			});
		})
		.catch(error => {
			console.log(error.response)
		});
	}
		
	render() {
		return (
			<>
				{/* <Dialog
					open={true}
					aria-labelledby="form-dialog-title"
					onClose={this.handleClose}
					fullWidth
					maxWidth="lg"
				>
					<DialogContent>
						<DataGrid
							data={this.state.data}
							style={{ height: "80%" }}
							columnResizing
						>
							<GridColumnGroup>
								<GridHeaderRow>
									<GridColumn colspan={5} title={`Member: ${this.props.data.name}`}></GridColumn>
								</GridHeaderRow>
								<GridHeaderRow>
									<GridColumn field="bulan_bayar" title="Bulan Bayar" align="right"></GridColumn>
									<GridColumn
										field="img"
										title="Bukti Bayar"
										render={({ value }) => (
											<a href={`${process.env.REACT_APP_PUBLIC_IMAGE}/${value}`} target="_blank" rel="noopener noreferrer">Lihat</a>
										)}
									></GridColumn>
									<GridColumn
										field="jumlah"
										title="Jumlah"
										align="right"
										render={({ value }) => separator(value, 0)}
									></GridColumn>
									<GridColumn field="tgl" title="Tanggal" align="right"></GridColumn>
									<GridColumn
										title="Action"
										field="action"
										render={
											({ value, row }) => (
												<button onClick={() => {
													api.delete(`/members/transactions/${row.id}`, {
														headers: { img: row.img, }
													})
													.then(result => {
														this.refresh();
													})
													.catch(error => {
														console.log(error.response)
													});
												}}>Hapus</button>
											)
										}
									/>
								</GridHeaderRow>
							</GridColumnGroup>
						</DataGrid>
					</DialogContent>
				</Dialog> */}
			</>
		);
	}
}

export default List;
import React from 'react';
import api from '../../config/api.js';
import { tglSql } from '../../config/helpers.js';
// import moment from "moment";

class index extends React.Component {
	
	state = {
		selectedDate: new Date(),
		dataReport: [],
		loading: false,
	};

	componentDidMount() {
		this.fetchData();
	}

	fetchData = () => {
		this.setState({
			loading: true
		}, () => {
			const tahun = tglSql(this.state.selectedDate).substr(0, 4);
			const bulan = tglSql(this.state.selectedDate).substr(5, 2);
			api.post(`/transactions`, {
				date1: `${tahun}-${bulan}-01`,
				date2: `${tahun}-${bulan}-31`,
				propinsi_id: "",
				user_id: "",
			}, {
				headers: { token: localStorage.getItem('token') }
			})
			.then(result => {
				this.setState({
					dataReport: result.data,
					loading: false,
				});
			})
			.catch(error => {
				this.error(error.response.data);
				this.setState({
					loading: false,
				});
			});
		});
	}

	error = (data) => {
		this.messager.alert({
			title: "Alert",
			msg: data
		});
	}

	render() {
		return (
			<>
				{/* <Messager ref={ref => this.messager = ref}></Messager>
				<MuiPickersUtilsProvider utils={DateFnsUtils}>
					<DatePicker
						margin="normal"
						label="Pilih Bulan"
						value={this.state.selectedDate}
						onChange={(selectedDate) => {
							this.setState({ selectedDate }, () => {
								this.fetchData();
							});
						}}
						format="MMM yyyy"
						variant="outlined"
						views={["year", "month"]}
					/>
				</MuiPickersUtilsProvider>
				<DataGrid
					data={this.state.dataReport}
					style={{ height:400 }}
					columnResizing
					loading={this.state.loading}
					rowCss={(row) => {
						if (row.salah === 1) {
							return { background: "#b8eecf", fontSize: "14px", fontStyle: "italic" };
						}
						return null;
					}}
					filterable
					pagination
				>
					<GridColumnGroup>
						<GridHeaderRow>
							<GridColumn field="tgl" title="Tanggal" width={200} align="right"></GridColumn>
							<GridColumn field="username" title="Username" width={100} align="center"></GridColumn>
							<GridColumn title="Download" width={100} align="center"
								render={({ row }) => Math.round(Number(row.download))}
							></GridColumn>
							<GridColumn title="Upload" width={100} align="center"
								render={({ row }) => Math.round(Number(row.upload))}
							></GridColumn>
							<GridColumn title="Total GB" width={100} align="center"
								render={({ row }) => Math.round((Number(row.upload) + Number(row.download)) / 1000)}
							></GridColumn>
							<GridColumn title="Expiration" width={200} align="right"
								render={({ row }) => {
									return `${row.expiration} - ${JSON.stringify(moment(row.expiration).isBefore(new Date()))}`
								}}
							></GridColumn>
							<GridColumn field="voucher" title="Voucher" width={150}></GridColumn>
							<GridColumn field="hp" title="No. Hp" width={150} align="center"></GridColumn>
						</GridHeaderRow>
					</GridColumnGroup>
				</DataGrid> */}
			</>
		);
	}

}

export default ((index));
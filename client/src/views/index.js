import Login from "./Login";
import Home from "./Home";
import Dashboard from "./Dashboard";
import Whatsapp from "./Whatsapp";
import NotFound from "./NotFound";

export {
    Login,
    Home,
    Dashboard,
    Whatsapp,
    NotFound,
};
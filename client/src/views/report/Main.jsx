import React from 'react';
import api from '../../config/api.js';
import { separator, tglSql } from '../../config/helpers.js';
import { Doughnut } from 'react-chartjs-2';
import _ from "lodash";

class Main extends React.Component {
	
	state = {
		selectedDate: new Date(),
		selectedDate2: new Date(),
		propinsi_id: '',
		user_id: '',
		dataReport: [],
		loading: false,
		userList: [],

		status: [
			{ value: null, text: "-Semua-" },
			{ value: "1", text: "Salah" },
		],

		dataChart1: [],
		dataChart2: [],
		dataChart3: [],
	};

	componentDidMount() {
		api.get(`/users`)
		.then(result => {
			this.setState({
				userList: result.data,
			});
		})
		.catch(error => {
			console.log(error.response)
		});

		api.get(`/transactions/voucher/all`)
		.then(result => {
			this.setState({
				dataChart3: result.data,
			});
		})
		.catch(error => {
			console.log(error.response)
		});
	}

	handleDateChange = date => {
		this.setState({ selectedDate: date });
	};

	handleDateChange2 = date => {
		this.setState({ selectedDate2: date });
	};

	handleChange = (e) => {
		this.setState({
			[e.target.name]: e.target.value,
		});
	};

	cetak = () => {
		this.setState({
			loading: true
		}, () => {
			api.post(`/transactions`, {
				date1: tglSql(this.state.selectedDate),
				date2: tglSql(this.state.selectedDate2),
				propinsi_id: this.state.propinsi_id,
				user_id: this.state.user_id,
			}, {
				headers: { token: localStorage.getItem('token') }
			})
			.then(result => {
				this.setState({
					dataReport: result.data,
					loading: false,
				});
				this.masukDataChart1(result.data);
				this.masukDataChart2(result.data);
			})
			.catch(error => {
				this.error(error.response.data);
				this.setState({
					loading: false,
				});
			});
		});
	}

	footerData = () => {
		let total = 0;
		let total_salah = 0;
		let data = this.state.dataReport;
		for (let i = 0; i < data.length; i++) {
			if (data[i].salah === 1) {
				total_salah += data[i].price;
			}
			total += data[i].price;
		};
		return [
		  { user_name: "Total Seluruh", price_format: separator(total, 0) },
			{ user_name: "Total Salah", price_format: separator(total_salah, 0) },
			{ user_name: "Total Bersih", price_format: separator(total - total_salah, 0) },
		];
	}

	error = (data) => {
		this.messager.alert({
			title: "Alert",
			msg: data
		});
	}

	masukDataChart1(guguk) {
		const datas = guguk.filter((row) => row.salah !== 1);
		const dataChart1 = _.orderBy(
			_(datas)
				.groupBy('hp')
				.map(function(items, hp) {
					return {
						hp: hp,
						totalBeli: items.length,
						totalUang: _.sumBy(items, function(o) { return o.price; }),
						data: items,
					};
				})
				.value()
		, ['totalBeli'], ['desc']).slice(0, 5);
		this.setState({ dataChart1 });
	}

	masukDataChart2(guguk) {
		const datas = guguk.filter((row) => row.salah !== 1);
		const dataChart2 = _(datas)
			.groupBy('voucher')
			.map(function(items, voucher) {
				return {
					voucher: voucher,
					totalBeli: items.length,
					totalUang: _.sumBy(items, function(o) { return o.price; }),
					data: items,
				};
			})
			.value();
		this.setState({ dataChart2 });
	}

	getRandomColor() {
		var letters = '0123456789ABCDEF'.split('');
		var color = '#';
		for (var i = 0; i < 6; i++ ) {
			color += letters[Math.floor(Math.random() * 16)];
		}
		return color;
			}

	render() {
		const { classes } = this.props;
		let total = 0;
		this.state.dataReport.map(row => {
			return total += row.price;
		});
		return (
			<>
				<h3>Report Penjualan</h3>
				{/* <MuiPickersUtilsProvider utils={DateFnsUtils}>
					<Grid container className={classes.grid}>
						<DatePicker
							margin="normal"
							label="Dari Tanggal"
							value={this.state.selectedDate}
							onChange={this.handleDateChange}
							format="d MMM yyyy"
							style={{ marginRight: 10 }}
							variant="outlined"
						/>
						<DatePicker
							margin="normal"
							label="Sampai Tanggal"
							value={this.state.selectedDate2}
							onChange={this.handleDateChange2}
							format="d MMM yyyy"
							style={{ marginRight: 10 }}
							variant="outlined"
						/>
						<TextField
							margin="normal"
							label="Propinsi"
							variant="outlined"
							onChange={this.handleChange}
							name="propinsi_id"
							select
							value={this.state.propinsi_id}
							style={{ marginRight: 10, width: 150 }}
						>
							<MenuItem value="">Semua</MenuItem>
							<MenuItem value="1">Lampung</MenuItem>
							<MenuItem value="2">Aceh</MenuItem>
						</TextField>
						<TextField
							margin="normal"
							label="Penjual"
							variant="outlined"
							onChange={this.handleChange}
							name="user_id"
							select
							value={this.state.user_id}
							style={{ marginRight: 10, width: 150 }}
						>
							<MenuItem value="">Semua</MenuItem>
							{this.state.userList.map((data, index) => (
								<MenuItem value={data.id} key={index}>{data.name}</MenuItem>
							))}
						</TextField>
					</Grid>
					<Button
						variant="contained"
						color="primary"
						onClick={this.cetak}
						style={{ marginRight: 5 }}
					>Tampilkan</Button>
				</MuiPickersUtilsProvider>
				<br /><br />
				<Paper className={classes.root}>
					<DataGrid
						data={this.state.dataReport}
						style={{ height:400 }}
						columnResizing
						showFooter
						footerData={this.footerData()}
						loading={this.state.loading}
						rowCss={(row) => {
							if (row.salah === 1) {
								return { background: "#b8eecf", fontSize: "14px", fontStyle: "italic" };
							}
							return null;
						}}
						filterable
					>
						<GridColumnGroup>
							<GridHeaderRow>
								<GridColumn field="tgl" title="Tanggal" width={200} align="right"></GridColumn>
								<GridColumn field="username" title="Username" width={100} align="center"></GridColumn>
								<GridColumn field="voucher" title="Voucher" width={150}></GridColumn>
								<GridColumn field="hp" title="No. Hp" width={150} align="center"></GridColumn>
								<GridColumn field="user_name" title="Penjual" width={150} align="center"></GridColumn>
								<GridColumn
									field="price_format"
									title="Price"
									width={100}
									align="right"
								></GridColumn>
								<GridColumn
									title="Status"
									field="salah"
									width={100}
									align="center"
									filter={() => (
										<ComboBox
											data={this.state.status}
											editable={false}
											inputStyle={{ textAlign: 'center' }}
										/>
									)}
								></GridColumn>
							</GridHeaderRow>
						</GridColumnGroup>
					</DataGrid>
				</Paper>
				<Messager ref={ref => this.messager = ref}></Messager>
				<Grid container spacing={8} style={{ marginTop: 10 }}>
					<Grid item xs={6}>
						<Card>
							<CardContent>
								5 Penjualan Terbanyak Berdasarkan Berapa Kali Beli<br /><br />
								Tgl. {tglSql(this.state.selectedDate)} s/d {tglSql(this.state.selectedDate2)}<br /><br />
								<Doughnut
									data={{
										labels: this.state.dataChart1.map((row) => { return `${row.hp} ${row.totalBeli}`; }),
										datasets: [{
											data: this.state.dataChart1.map((obj) => { return obj.totalBeli; }),
											backgroundColor: [
												'rgba(255, 99, 132, 0.2)',
												'rgba(54, 162, 235, 0.2)',
												'rgba(255, 206, 86, 0.2)',
												'rgba(75, 192, 192, 0.2)',
												'rgba(153, 102, 255, 0.2)',
												'rgba(255, 159, 64, 0.2)',
											],
											borderWidth: 1,
										}],
									}}
									options={{
										legend: {
											display: true,
											position: "right"
										}
									}}
								/>
							</CardContent>
						</Card>
					</Grid>
					<Grid item xs={6}>
						<Card>
							<CardContent>
								Total Penjualan Voucher Berdasarkan Nilai Voucher<br /><br />
								Tgl. {tglSql(this.state.selectedDate)} s/d {tglSql(this.state.selectedDate2)}<br /><br />
								<Doughnut
									data={{
										labels: this.state.dataChart2.map((row) => { return `${row.voucher} ${row.totalBeli}`; }),
										datasets: [{
											data: this.state.dataChart2.map((obj) => { return obj.totalBeli; }),
											backgroundColor: this.state.dataChart2.map(() => { return this.getRandomColor(); }),
											borderWidth: 1,
										}],
									}}
									options={{
										legend: {
											display: true,
											position: "right"
										}
									}}
								/>
							</CardContent>
						</Card>
					</Grid>
					<Grid item xs={12}>
						<Card>
							<CardContent>
								5 Penjualan Terbanyak Berdasarkan Jumlah Uang Seluruh Database<br /><br />
								<Doughnut
									data={{
										labels: this.state.dataChart3.map((row) => { return `${row.hp} ${separator(row.total, 0)}`; }),
										datasets: [{
											data: this.state.dataChart3.map((obj) => { return obj.total; }),
											backgroundColor: this.state.dataChart3.map(() => { return this.getRandomColor(); }),
											borderWidth: 1,
										}],
									}}
									options={{
										legend: {
											display: true,
											position: "right"
										}
									}}
								/>
							</CardContent>
						</Card>
					</Grid>
				</Grid> */}
			</>
		);
	}

}

export default ((Main));
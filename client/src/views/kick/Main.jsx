import React from 'react';
import api from '../../config/api.js';
import Alert from '../../components/Alert.jsx';

class Main extends React.Component {
	
	state = {
		user: "",

		message: "",
		variant: "error",
		openAlert: false,
	}
	
	laksanakan = () => {
		api.post(`/mikrotik/kick/`, {
			user: this.state.user,
		})
		.then(result => {
			this.setState({
				variant: 'success',
				message: 'Berhasil Kick',
				openAlert: true,
			});
		})
		.catch(error => {
			this.setState({
				variant: 'error',
				message: error.response.data,
				openAlert: true,
			});
		});
	}
	
	render() {
		return (
			<>
				<input type="text" placeholder="Masukkan User" onChange={(e) => this.setState({ user: e.target.value })} /><br />
				<button onClick={this.laksanakan}>Laksanakan</button>
				<Alert
					open={this.state.openAlert}
					closeAlert={() => {this.setState({ openAlert: false })}}
					variant={this.state.variant}
					message={this.state.message}
				/>
			</>
		);
	}

};

export default Main;

import React from 'react';
import api from '../../config/api.js';
import { Button } from 'antd';
import AddSession from './AddSession';
import { io } from "socket.io-client";

class index extends React.Component {
	
	state = {
		openAdd: false,
		sessions: [],
		socket: io(process.env.REACT_APP_BASE_URL),
	}
	
	componentDidMount() {
		this.fetchData();
		this.state.socket.on("SessionDeleted", result => {
            // let datas = this.state.notifikasi;
            // datas.push(result);
            // this.setState({ notifikasi: datas, soundNotif: true });
            // this.props.onNotif(result);
            console.log("masuk========", result)
        });
	}

	componentWillUnmount() {
		this.state.socket.disconnect();
	}
	
	fetchData = () => {
		api.get(`/whatsapp/sessions`)
		.then(result => {
			this.setState({ sessions: result.data });
		});
	}

	render() {
		const { sessions, openAdd } = this.state;
		return (
			<>
				<div className="grid grid-cols-3 gap-2">
					{
						sessions.length === 0 ?
							<div>
								<Button onClick={() => this.setState({ openAdd: true })}>Add Session</Button>
							</div>
						: null
					}
					{
						sessions.map((row, i) => (
							<React.Fragment key={i}>
								<div className="border border-black">
									<div>Nomor: {row[1].user.id.split(":")[0]}</div>
									<div>Id: {row[0]}</div>
									<div>Status: </div>
									<div>
										<Button>Hapus Session</Button>
									</div>
								</div>
							</React.Fragment>
						))
					}
				</div>
				{
					openAdd ?
						<AddSession
							open={openAdd}
							onClose={() => this.setState({ openAdd: false })}
						/>
					: null
				}
			</>
		);
	}

}

export default ((index));
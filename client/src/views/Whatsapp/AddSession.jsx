import React from 'react';
import api from '../../config/api.js';
import { Modal } from 'antd';
import { io } from "socket.io-client";

class AddSession extends React.Component {
	
	state = {
		qrqode: null,
        socket: io(process.env.REACT_APP_BASE_URL, {
            // withCredentials: true,
        }),
	}
	
	componentDidMount() {
		this.fetchData();
        this.state.socket.on("ScanBarcode", result => {
            // let datas = this.state.notifikasi;
            // datas.push(result);
            // this.setState({ notifikasi: datas, soundNotif: true });
            // this.props.onNotif(result);
            console.log("masuk========", result)
        });
	}

    componentWillUnmount() {
		this.state.socket.disconnect();
	}

	fetchData = () => {
		api.post(`/whatsapp/sessions/add`, {
            sessionId: `wa${Date.now()}`,
        })
		.then(result => {
			this.setState({
				qrqode: result.data.qr,
			});
		})
		.catch(error => {
			console.log(error.response)
		});
	}

	render() {
		const { qrqode } = this.state;
        const { open, onClose } = this.props;
		return (
			<>
				<Modal
					open={open}
					onCancel={onClose}
				>
					<img src={qrqode} className="w-full h-auto" />
				</Modal>
			</>
		);
	}

}

export default ((AddSession));
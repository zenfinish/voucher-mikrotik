import React from 'react';
import { Link } from 'react-router-dom';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ContactMailIcon from '@material-ui/icons/ContactMail';

class ListItem2 extends React.Component {

	render() {
		const { data } = this.props;
		return (
			<ListItem button component={Link} to={`/dashboard/${data.link}`}>
				<ListItemIcon>
					<ContactMailIcon />
				</ListItemIcon>
				<ListItemText primary={data.module_name} />
			</ListItem>
		);
	}

}

export default ListItem2;

import React from 'react';
import { withRouter } from 'react-router';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

class FormLogout extends React.Component {
    
    handleClose = () => {
        this.props.closeFormLogout();
    };
        
    render() {
        const { history } = this.props;
        return (
            <Dialog
                open={this.props.openFormLogout}
                aria-labelledby="form-dialog-title"
                onClose={this.handleClose}
            >
                <DialogTitle id="form-dialog-title">Logout</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Yakin ?
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button
                        variant="contained"
                        color="primary"
                        style={{marginRight: 5}}
                        fullWidth
                        onClick={() => {
                            localStorage.clear();
                            history.push("/");
                        }}
                    >Logout</Button>
                </DialogActions>
            </Dialog>
        );
    }

}

export default withRouter(FormLogout);
import React from 'react';
import { Routes } from './config';
import { RecoilRoot } from 'recoil';

class App extends React.Component {
	
	render() {
		return (
			<>
				<RecoilRoot>
					<Routes />
				</RecoilRoot>
			</>
		);
	};

};

export default App;

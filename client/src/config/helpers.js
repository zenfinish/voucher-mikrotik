export function separator(data, number) {
	if (data === undefined || data === null || data === '' || isNaN(data)) data = 0;
	return Number(data).toFixed(number).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
};

export function tglSql(data) {
	let yNow = data.getFullYear();
	let mNow = ("0" + (data.getMonth() + 1)).slice(-2);
	let dNow = ("0" + data.getDate()).slice(-2);
	let tanggal = [yNow, mNow, dNow].join('-');
	return tanggal;
};

export function tglIndo(data) {
	if (data) {
		// let hari = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
		let bulan = ['Januari', 'Februari', 'Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];
		let d = data.substr(8,2);
		let m = bulan[parseInt(data.substr(5,2))-1];
		let y = data.substr(0,4);
		return `${d} ${m} ${y}`;
	} else {
		return "";
	}
};

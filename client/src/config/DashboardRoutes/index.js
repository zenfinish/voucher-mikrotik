import React from "react";
import { Routes, Route } from "react-router-dom";
import {
  Home,
  NotFound,
  Whatsapp,
} from "../../views";
import Penjualan from '../../views/penjualan/Main.jsx';
import Member from '../../views/member/Main.jsx';
import PembayaranMember from '../../views/pembayaran_member/Main.jsx';
// import ProfileVoucher from '../../views/profile_voucher/Main.jsx';
// import UserManagement from '../../views/user_management/Main.jsx';
// import Report from '../../views/report/Main.jsx';
// import ReportMember from '../../views/Dashboard/ReportMember';
// import Dashboard2 from '../../views/dashboard/Main.jsx';
// import Kick from '../../views/kick/Main.jsx';
// import GantiPassword from '../../views/ganti_password/Main.jsx';
// import Mastri from '../../views/Mastri';
// import Whatsapp from '../../views/Whatsapp';

const DashboardRoutes = () => {
	return (
		<Routes>
			<Route exact path={`/`} element={<Home />} />

			{/* <Route path={`/dashboard2`} element={Dashboard2} />
			<Route path={`/voucher-profile`} element={ProfileVoucher} />
			<Route path={`/management-user`} element={UserManagement} /> */}
			<Route path={`/penjualan-voucher`} element={Penjualan} />
			{/* <Route path={`/report-voucher`} element={Report} />
			<Route path={`/report-member`} element={ReportMember} /> */}
			<Route path={`/pembayaran-member`} element={PembayaranMember} />
			<Route path={`/management-member`} element={Member} />
			{/* <Route path={`/kick`} element={Kick} /> */}
			{/* <Route path={`/mastri`} element={Mastri} /> */}
			<Route path={`/whatsapp`} element={<Whatsapp />} />

			{/* <Route path={`/ganti-password`} element={GantiPassword} /> */}
			<Route element={<NotFound />} />
		</Routes>
	);
};

export default (DashboardRoutes);
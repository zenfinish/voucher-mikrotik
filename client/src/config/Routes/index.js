import React from "react";
import { BrowserRouter, Routes as Switch, Route } from "react-router-dom";
import {
	Login,
	Dashboard,
	NotFound,
} from "../../views";

const Routes = () => {
	return (
		<BrowserRouter
			// forceRefresh={true}
		>
			<Switch>
				<Route exact path="/" element={<Login />} />
				<Route path="/dashboard/*" element={<Dashboard />} />
				<Route element={<NotFound />} />
			</Switch>
		</BrowserRouter>
	);
};

export default Routes;

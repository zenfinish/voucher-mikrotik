import { atom } from 'recoil';

export const userState = atom({
	key: 'userState',
	default: {
		id: null,
		username: null,
		name: null,
		modules: [],
	},
});
